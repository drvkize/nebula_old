#pragma once

#include "Memory/MemoryDefine.hpp"
#include "Memory/NebulaAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        class FixedSizeListAllocator : public NebulaAllocator
        {
            struct Node
            {
                union
                {
                    void* data;
                    Node* next;
                };

                Node();
                ~Node();
            };

            NebulaAllocator* parent_allocator;
            Node head;

            void insert( void* p );
            void* remove();

        public:
            FixedSizeListAllocator();
            virtual ~FixedSizeListAllocator();

            nbool init( NebulaAllocator* parent_allocator );
            void release();

            void* allocate( nsize size, nsize align ) override;
            nbool deallocate( void* p ) override;
            void clear();
        };
    }
}