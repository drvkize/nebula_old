#pragma once

// language specification
#include "Platform/Language/cpp.hpp"
#include "Platform/Language/common.hpp"
using namespace Nebula::Platform;

// os specific headers
#include <unistd.h>
#include <sys/types.h>
#include <time.h>
#include <pthread.h>
#include <dlfcn.h>

// os specific defines

// os specific functions

namespace Nebula
{
    namespace Platform
    {
        namespace Linux
        {
            std::tm* localtime( const std::time_t* t );
        }
    }

    namespace Memory
    {
        namespace Linux
        {
            void* allocate( nsize size );
            void deallocate( void* p );
        }
    }

    namespace Concurrent
    {
        namespace Linux
        {
            nsize getThreadID();
        }
    }

    namespace System
    {
        namespace Linux
        {
            using OSPluginHandle = void*;

            nbool isValid( OSPluginHandle handle );
            OSPluginHandle loadPlugin( const nchar* path );
            void unloadPlugin( OSPluginHandle handle );
            void* getEntryPoint( OSPluginHandle handle, const nchar* name );
        }
    }
}