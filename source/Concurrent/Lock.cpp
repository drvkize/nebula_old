#include "Concurrent/Lock.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        Lock::Lock()
        {
        }

        Lock::~Lock()
        {
        }

        void Lock::acquire()
        {
            lock.lock();
        }

        nbool Lock::tryAcquire()
        {
            return lock.try_lock();
        }

        void Lock::release()
        {
            lock.unlock();
        }
    }
}