#pragma once

#include "System/SystemDefine.hpp"
#include "IO/Serialization/NTypeReader.hpp"
using Nebula::IO::Serialization::NTypeReader;
#include "Reflection/Reflection.hpp"

namespace Nebula
{
    namespace System
    {
        class NebulaModule
        {
        protected:
            virtual nbool onInit( const NTypeReader& init_params );
            virtual void onRelease();
            virtual void onPush();
            virtual void onPop();

        public:
            NebulaModule();
            virtual ~NebulaModule();

            nbool init( const NTypeReader& init_params );
            void release();
            void push();
            void pop();
        };
    }
}