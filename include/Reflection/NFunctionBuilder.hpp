#pragma once

#include "ReflectionDefine.hpp"
#include "Reflection/NParameter.hpp"
#include "Reflection/NFunction.hpp"

namespace Nebula
{
    namespace Reflection
    {
        template<typename F>
        class NFunctionBuilder
        {
            using Type = F;
            using ReturnType = typename ntraits::function_traits<Type>::return_type;
            using ParameterTypes = typename ntraits::function_traits<Type>::parameter_types;
            static constexpr nsize ParameterCount = ParameterTypes::count;

            F* const func;
            nstring_view name;
            narray<nstring_view, ParameterCount> parameter_names;
            narray<any, ParameterCount> parameter_default_values;

        public:
            NFunctionBuilder( F* const func, const nstring_view name )
                : func( func )
                , name( name )
            {
            }

            template<typename... Tn>
            void setParameterNames( const Tn... names )
            {
                NParameterHelper::setParameterNames( parameter_names, names... );
            }

            template<typename... Tn>
            void setParameterDefaultValues( const Tn&... default_values )
            {
                NParameterHelper::setParameterDefaultValues( parameter_default_values, default_values... );
            }

            NFunctionPtr build()
            {
                narray<NParameterPtr, ParameterCount> parameters;
                NParameterHelper::packParameters<typename ParameterTypes>( parameters, array_view<nstring_view>( parameter_names ), array_view<any>( parameter_default_values ) );

                return NFunctionPtr( new NFunctionConcrete<F>( func, std::move( name ), std::move( parameters ) ) );
            }
        };
    }
}