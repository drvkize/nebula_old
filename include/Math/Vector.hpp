#pragma once

#include "Math/MathDefine.hpp"

namespace Nebula
{
    namespace Math
    {
        #define NEBULA_VECTOR_LOOP( op ) NEBULA_UNROLLED_LOOP( i, Dimension, op )
        
        #define NEBULA_VECTOR_OP( op ) {            \
            Vector<T, Dimension> result;            \
            NEBULA_VECTOR_OP( result[i] = op );     \
            return result;                          \
        }

        #define NEBULA_VECTOR_SELF_OP( op ) {       \
            NEBULA_VECTOR_LOOP( op )                \
            return *this;                           \
        }

        template<typename T, int D>
        class Vector
        {
        public:
            typedef T ScalarType;
            static constexpr int Dimension = D;

            T data[Dimension];

            inline Vector()
            {
            }

            inline Vector( const Vector& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = v.data[i] );
            }

            template<typename U>
            explicit inline Vector( const Vector<U,D>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = static_cast<T>( v[i] ) );
            }

            explicit inline Vector( const T& s )
            {
                NEBULA_VECTOR_LOOP( data[i] = s );
            }

            explicit inline Vector( const T* arr )
            {
                NEBULA_VECTOR_LOOP( data[i] = arr[i] );
            }

            inline Vector( const T& s0, const T& s1 )
            {
                static_assert( Dimension == 2, "[Vector::Vector()]: dimension is not 2" );
                data[0] = s0;
                data[1] = s1;
            }

            inline Vector( const T& s0, const T& s1, const T& s2 )
            {
                static_assert( Dimension == 3, "[Vector::Vector()]: dimension is not 3" );
                data[0] = s0;
                data[1] = s1;
                data[2] = s2;
            }

            inline Vector( const Vector<T, 2>& s01, const T& s2 )
            {
                static_assert( Dimension == 3, "[Vector::Vector()]: dimension is not 3" );
                data[0] = s01[0];
                data[1] = s01[1];
                data[2] = s2;
            }

            inline Vector( const T& s0, const Vector<T, 2>& s12 )
            {
                static_assert( Dimension == 3, "[Vector::Vector()]: dimension is not 3" );
                data[0] = s0;
                data[1] = s12[0];
                data[2] = s12[1];
            }

            inline Vector( const T& s0, const T& s1, const T& s2, const T& s3 )
            {
                static_assert( Dimension == 4, "[Vector::Vector()]: dimension is not 4" );
                data[0] = s0;
                data[1] = s1;
                data[2] = s2;
                data[3] = s3;
            }

            inline Vector( const Vector<T, 3>& s012, const T& s3 )
            {
                static_assert( Dimension == 4, "[Vector::Vector()]: dimension is not 4" );
                data[0] = s012[0];
                data[1] = s012[1];
                data[2] = s012[2];
                data[3] = s3;
            }

            inline Vector( const T& s0, const Vector<T, 3>& s123 )
            {
                static_assert( Dimension == 4, "[Vector::Vector()]: dimension is not 4" );
                data[0] = s0;
                data[1] = s123[0];
                data[2] = s123[1];
                data[3] = s123[2];
            }

            inline Vector( const Vector<T, 2>& s01, const T& s2, const T& s3 )
            {
                static_assert( D == 4, "[Vector::Vector()]: dimension is not 4" );
                data[0] = s01[0];
                data[1] = s01[1];
                data[2] = s2;
                data[3] = s3;
            }

            inline Vector( const T& s0, const Vector<T, 2>& s12, const T& s3 )
            {
                static_assert( Dimension == 4, "[Vector::Vector()]: dimension is not 4" );
                data[0] = s0;
                data[1] = s12[0];
                data[2] = s12[1];
                data[3] = s3;
            }

            inline Vector( const T& s0, const T& s1, const Vector<T, 2>& s23 )
            {
                static_assert( Dimension == 4, "[Vector::Vector()]: dimension is not 4" );
                data[0] = s0;
                data[1] = s1;
                data[2] = s23[0];
                data[3] = s23[1];
            }

            inline Vector( const Vector<T, 2>& s01, const Vector<T, 2>& s23 )
            {
                static_assert( Dimension == 4, "[Vector::Vector()]: dimension is not 4" );
                data[0] = s01[0];
                data[1] = s01[1];
                data[2] = s23[0];
                data[3] = s23[1];
            }

            inline Vector( const VectorPacked<T, D>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = v.data[i] );
            }

            inline T& operator()( const int i ) { return data[i]; }
            inline const T& operator()( const int i ) const { return data[i]; }

            inline Vector<T, 3> xyz()
            {
                static_assert( Dimension > 3, "[Vector::xyz()]: dimension must be larger 3" );
                return Vector<T, 3>( data[0], data[1], data[2] );
            }

            inline const Vector<T, 3> xyz() const
            {
                static_assert( Dimension > 3, "[Vector::xyz()]: dimension must be larger 3" );
                return Vector<T, 3>( data[0], data[1], data[2] );
            }

            inline Vector<T, 2> xy()
            {
                static_assert( Dimension > 2, "[Vector::xy()]: dimension must be larger 2" );
                return Vector<T, 2>( data[0], data[1] );
            }

            inline const Vector<T, 2>& xy() const
            {
                static_assert( Dimension > 2, "[Vector::xy()]: dimension must be larger 2" );
                return Vector<T, 2>( data[0], data[1] );
            }

            inline Vector<T, 2> zw()
            {
                static_assert( Dimension > 4, "[Vector::zw()]: dimension is not 4" );
                return Vector<T, 2>( data[2], data[3] );
            }

            inline const Vector<T, 2>& zw() const
            {
                static_assert( Dimension > 4, "[Vector::zw()]: dimension is not 4" );
                return Vector<T, 2>( data[2], data[3] );
            }

            inline void pack( VectorPacked<T, D>* const v ) const
            {
                NEBULA_VECTOR_LOOP( v->data[i] = data[i] );
            }
        };
    }

    namespace Platform
    {
        using int2 = Math::Vector<int, 2>;
        using int3 = Math::Vector<int, 3>;
        using int4 = Math::Vector<int, 4>;
        using float2 = Math::Vector<nfloat, 2>;
        using float3 = Math::Vector<nfloat, 3>;
        using float4 = Math::Vector<nfloat, 4>;
        using double2 = Math::Vector<ndouble, 2>;
        using double3 = Math::Vector<ndouble, 3>;
        using double4 = Math::Vector<ndouble, 4>;
    }
}

// spectialization
#include "Vector2.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"