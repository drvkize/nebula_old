#pragma once

#include "Platform/Language/cpp.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
        }
    }
}

namespace ntraits = Nebula::Platform::Traits;

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
            // yes, no
			typedef uint8 yes_type;
			struct no_type { uint8 padding[8]; };

            // null
            struct null_type {};

            // invalid
            struct invalid_type {};

            // default_constructible
            struct default_constructible {};

            // conjunction
			template<typename...>
			struct conjunction : public std::true_type {};

			template<typename B>
			struct conjunction<B> : B {};

			template<typename B, typename... Bn>
			struct conjunction<B, Bn...> : public std::conditional<nbool(B::value), conjunction<Bn...>, B>::type {};

			// disjunction
			template<typename...>
			struct disjunction : public std::false_type {};

			template<typename B>
			struct disjunction<B> : B {};

			template<typename B, typename... Bn>
			struct disjunction<B, Bn...> : public std::conditional<nbool(B::value), B, disjunction<Bn...>>::type {};

            // static_min
            namespace detail
            {
                template<nsize... In>
                struct static_min_impl {};

                template<nsize I0, nsize I1, nsize... In>
                struct static_min_impl<I0, I1, In...>
                {
                    static constexpr nsize value = I0 < I1 ? ntraits::detail::static_min_impl<I0, In...>::value : ntraits::detail::static_min_impl<I1, In...>::value;
                };

                template<nsize I>
                struct static_min_impl<I>
                {
                    static constexpr nsize value = I;
                };
            }

            template<nsize... In>
            struct static_min : ntraits::detail::static_min_impl<In...> {};

            // static_max
            namespace detail
            {
                template<nsize... In>
                struct static_max_impl {};

                template<nsize I0, nsize I1, nsize... In>
                struct static_max_impl<I0, I1, In...>
                {
                    static constexpr nsize value = I0 > I1 ? ntraits::detail::static_max_impl<I0, In...>::value : ntraits::detail::static_max_impl<I1, In...>::value;
                };

                template<nsize I>
                struct static_max_impl<I>
                {
                    static constexpr nsize value = I;    
                };
            }

            template<nsize... In>
            struct static_max : ntraits::detail::static_max_impl<In...> {};

            // cast signed integer type to unsigned
            template<typename Int>
            static constexpr auto to_unsigned( Int x )
                -> typename std::make_unsigned<Int>::type
            {
                assert( x >= 0 && "[to_unsigned]: require negative" );
                return static_cast<typename std::make_unsigned<Int>::type>( x );
            }

            // nothrow reinterpret_cast
            template<typename From, typename To>
            inline To byte_cast( const From& from )
            {
                static_assert( sizeof( To ) == sizeof( From ), "[byte_cast]: size not match" );

                To to;
                std::memcpy( &to, &from, sizeof( to ) );
                return to;
            }

            // get string length
            constexpr nsize length_of( const nchar* s )
            {
                return *s ? 1 + length_of( s + 1 ) : 0;
            }

            // get_first_argument_of_specialization
			template<typename T>
			struct get_first_argument_of_specialization
			{
				typedef void type;
			};

			template<template<typename...> class Ref, typename T, typename... Tn>
			struct get_first_argument_of_specialization<Ref<T, Tn...>>
			{
				typedef T type;
			};

            // type_holder
			template<typename T>
			struct type_holder
			{
				using type = T;
			};
        }
    }
}