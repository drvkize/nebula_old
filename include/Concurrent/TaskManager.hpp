#pragma once

#include "Concurrent/ConcurrentDefine.hpp"
#include "Concurrent/Event.hpp"
#include "Concurrent/MPMCQueue.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        class NEBULA_INTERFACE TaskManager
        {
            nvector<TaskRunner*> runners;
            MPMCQueue<TaskPtr>* pending_tasks;
            
            Event pending_task_event;
            nbool exit_flag;
            nbool destroy_flag;
            nsize assigned_task_count;
            nsize finished_task_count;
            std::atomic<nsize> running_task_count;

        protected:
            friend class TaskRunner;
            nbool runPendingQueue( TaskRunner* runner );
            void attachTask( TaskRunner* runner );
            void detachTask( TaskRunner* runner );
            inline nbool getExitFlag() const { return exit_flag; }
            inline nbool getDestroyFlag() const { return destroy_flag; }

        public:
            TaskManager();
            ~TaskManager();

            TaskManager( const TaskManager& other ) = delete;
            TaskManager& operator=( const TaskManager& rhs ) = delete;

            nbool init( nsize runner_count, nsize task_capacity );
            void release();

            nbool assign( TaskPtr task );

            inline nbool isIdle() const { return assigned_task_count == finished_task_count; }
            inline nsize getAssignedTaskCount() const { return assigned_task_count; }
            inline nsize getFinishedTaskCount() const { return finished_task_count; }
        };
    }
}