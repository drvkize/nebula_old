#include "System/PluginManager.hpp"
#include "System/NebulaPlugin.hpp"
#include "Log/Log.hpp"

namespace Nebula
{
    namespace System
    {
        PluginManager::PluginManager()
        {
        }

        PluginManager::~PluginManager()
        {
        }

        NebulaPlugin* PluginManager::load( const nchar* path )
        {
            OS::OSPluginHandle handle = OS::loadPlugin( path );
            if( !OS::isValid( handle ) )
            {
                Context::error( "[PluginManager::load()]: load plugin at \"{}\" return invalid handle\n", path );
                return nullptr;
            }

            NebulaPluginCreateFunc create_func = reinterpret_cast<NebulaPluginCreateFunc>( OS::getEntryPoint( handle, NebulaPluginCreateFuncName ) );
            if( create_func == nullptr )
            {
                Context::error( "[PluginManager::load()]: can not find create function \"{}\" of plugin at \"{}\"\n", NebulaPluginCreateFuncName, path );
                OS::unloadPlugin( handle );
                return nullptr;
            }

            NebulaPluginDestroyFunc destroy_func = reinterpret_cast<NebulaPluginDestroyFunc>( OS::getEntryPoint( handle, NebulaPluginDestroyFuncName ) );
            if( destroy_func == nullptr )
            {
                Context::error( "[PluginManager::load()]: can not find destroy function \"{}\" of plugin at \"{}\"\n", NebulaPluginDestroyFuncName, path );
                OS::unloadPlugin( handle );
                return nullptr;
            }

            NebulaPlugin* instance = create_func( nullptr );
            if( instance == nullptr )
            {
                Context::error( "[PluginManager::load()]: create instance failed for pluing at \"{}\"\n", path );
                OS::unloadPlugin( handle );
                return nullptr;
            }

            if( find( instance->getName().c_str() ) != nullptr )
            {
                Context::error( "[PluginManager::load()]: plugin {} already exist, when loading plugin at \"{}\"\n", instance->getName(), path );
                destroy_func( instance );
                OS::unloadPlugin( handle );
                return nullptr;
            }

            PluginInfo info;
            info.name = instance->getName();
            info.handle = handle;
            info.create_func = create_func;
            info.destroy_func = destroy_func;
            info.instance = instance;

            std::pair<Plugins_t::iterator, nbool> ret = plugins.insert( Plugins_t::value_type( info.name, info ) );

            Context::info( "[PluginManager::load()]: loaded plugin {} at \"{}\"\n", info.name, path );
            return info.instance;
        }

        void PluginManager::unload( const nchar* name )
        {
            plugins.erase( name );
        }

        NebulaPlugin* PluginManager::find( const nchar* name )
        {
            Plugins_t::iterator it = plugins.find( name );
            return it != plugins.end() ? it->second.instance : nullptr;
        }
    }
}