#pragma once

// language specification
#include <corecrt.h>
#include "Platform/Language/cpp.hpp"
#include "Platform/Language/common.hpp"
using namespace Nebula::Platform;

// os specific headers

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>

// os specific defines

// os specific functions

namespace Nebula
{
    namespace Platform
    {
        namespace Windows
        {
            std::tm* localtime( const std::time_t* t );
        }
    }

    namespace Memory
    {
        namespace Windows
        {
            // memory
            void* allocate( nsize size );
            void deallocate( void* p );
        }
    }

    namespace Concurrent
    {
        namespace Windows
        {
            nsize getThreadID();
        }
    }

    namespace System
    {
        namespace Windows
        {
            using OSPluginHandle = HMODULE;

            nbool isValid( OSPluginHandle handle );
            OSPluginHandle loadPlugin( const nchar* path );
            void unloadPlugin( OSPluginHandle handle );
            void* getEntryPoint( OSPluginHandle handle, const nchar* name );
        }
    }
}