#pragma once

#include "IO/IODefine.hpp"

namespace Nebula
{
    namespace IO
    {
        class NEBULA_INTERFACE StdFile
        {
        protected:
            FILE* file;

        public:
            static StdFilePtr create( const nchar* path, const nchar* mode );
            
            StdFile();
            ~StdFile();

            nbool init( const nchar* path, const nchar* mode );
            void release();

            nsize read( int8* buffer, nsize buffer_size );
            nsize write( const int8* data, nsize data_size );

            nbool seek( SeekOption option, int64 offset );
            int64 tell() const;
            nbool isEnd() const;
            nsize getFileSize();
            inline FILE* getFileHandle() { return file; }

            void flush();
        };
    }
}