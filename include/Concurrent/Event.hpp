#pragma once

#include "Concurrent/ConcurrentDefine.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        class NEBULA_INTERFACE Event
        {
            std::mutex mtx;
            std::condition_variable event;

        public:
            Event();
            ~Event();

            Event( const Event& other ) = delete;
            Event& operator=( const Event& rhs ) = delete;

            void setOne();
            void setAll();
            void waitOne( nsize wait_time = 0 );
        };
    }
}