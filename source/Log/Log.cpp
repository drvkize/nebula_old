#include "Log/Log.hpp"
#include "Log/Logger.hpp"
#include "Log/StdConsoleSink.hpp"
#include "Log/StdFileSink.hpp"

namespace Nebula
{
    namespace Context
    {
        Log::Logger& getLogger()
        {
            struct LogStub
            {
                Log::LoggerPtr logger;

                LogStub()
                {
                    nvector<Log::LoggerSinkPtr> sinks;
                    sinks.push_back( Log::ConsoleSinkMT::create() );
                    sinks.push_back( Log::StdFileSinkMT::create( "Log.txt" ) );
                    logger = Log::Logger::create( "default_logger", ( NEBULA_BUILD_DEBUG == 1 ? Log::LogLevel::LL_Trace : Log::LogLevel::LL_Info ), sinks );
                }

                ~LogStub()
                {
                    logger = nullptr;
                }
            };

            static LogStub _stub_log;
            return *_stub_log.logger;
        }
    }
}