#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Utility/string_view.hpp"
#include "Reflection/NProperty.hpp"
#include "Reflection/NConstructor.hpp"
#include "Reflection/NConstructorBuilder.hpp"
#include "Reflection/NMethod.hpp"
#include "Reflection/NMethodBuilder.hpp"

namespace Nebula
{
    namespace Reflection
    {
        class NClass
        {
            const nstring_view name;
            nunordered_map<nstring_view, NPropertyPtr> properties;
            nunordered_map<nstring_view, NConstructorPtr> constructors;
            nunordered_map<nstring_view, NMethodPtr> methods;

        public:
            void addProperty();
            NConstructorBuilderPtr addConstructor();
            NMethodBuilderPtr addMethod();

            template<typename... Tn>
            any construct( Tn... args )
            {
                
            }

            NEnumPtr getEnum( const nstring_view name );
            NMethodPtr getMethod( const nstring_view name );
            NPropertyPtr getProperty( const nstring_view name );
        };

        template<typename C>
        class NClassConcrete
        {
        public:
        };
    }
}