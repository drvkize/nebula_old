#pragma once

#include "System/SystemDefine.hpp"

namespace Nebula
{
    namespace System
    {
        class NEBULA_INTERFACE PluginManager
        {
            using Plugins_t = nunordered_map<nstring, PluginInfo>;

            Plugins_t plugins;

        public:
            PluginManager();
            ~PluginManager();

            PluginManager( const PluginManager& other ) = delete;
            PluginManager& operator=( const PluginManager& rhs ) = delete;

            NebulaPlugin* load( const nchar* path );
            void unload( const nchar* name );
            NebulaPlugin* find( const nchar* name );
        };
    }
}