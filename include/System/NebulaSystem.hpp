#pragma once

#include "System/SystemDefine.hpp"

namespace Nebula
{
    namespace System
    {
        class NEBULA_INTERFACE NebulaSystem
        {
        public:
            NebulaSystem();
            ~NebulaSystem();

            NebulaSystem( const NebulaSystem& other ) = delete;
            NebulaSystem& operator=( const NebulaSystem& rhs ) = delete;
        };
    }
}