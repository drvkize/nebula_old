#pragma once

#include "Concurrent/ConcurrentDefine.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        class NEBULA_INTERFACE Lock
        {
        public:
            Lock( const Lock& other ) = delete;
            Lock& operator=( const Lock& rhs ) = delete;

        private:
            std::mutex lock;
        
        public:
            Lock();
            ~Lock();

            void acquire();
            nbool tryAcquire();
            void release();
        };
    }
}