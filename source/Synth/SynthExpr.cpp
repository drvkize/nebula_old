#include "Synth/SynthExpr.hpp"

namespace Nebula
{
    namespace Synth
    {
        SynthExprList SynthExpr::empty_list;

        SynthExpr::~SynthExpr()
        {
        }

        void SynthExpr::addExpr( SynthExprPtr&& param )
        {
        }

        SynthExprList& SynthExpr::getExprList()
        {
            return empty_list;
        }

        void SynthExpr::setExpr0( SynthExprPtr&& expr )
        {
        }

        SynthExprPtr SynthExpr::getExpr0()
        {
            return nullptr;
        }

        void SynthExpr::setExpr1( SynthExprPtr&& expr )
        {
        }

        SynthExprPtr SynthExpr::getExpr1()
        {
            return nullptr;
        }

        void SynthExpr::setExpr2( SynthExprPtr&& expr )
        {
        }

        SynthExprPtr SynthExpr::getExpr2()
        {
            return nullptr;
        }
    }
}