#pragma once

#include "Platform/Platform.hpp"
#include "Reflection/NType.hpp"
using Nebula::Reflection::NType;

namespace Nebula
{
    namespace Platform
    {
        // might apply small buffer optimization later
        class any
        {
            struct place_holder
            {
                virtual ~place_holder() {};
                virtual std::unique_ptr<place_holder> clone() const = 0;
                virtual NType getType() const = 0;
                virtual const nstring_view getSignature() const = 0;
            };

            template<typename T>
            struct place_holder_concrete : public place_holder
            {
                using type = T;

                T value;

                place_holder_concrete( const T& value )
                    : value( value )
                {
                }

                place_holder_concrete( T&& value )
                    : value( std::move( value ) )
                {
                }

                virtual std::unique_ptr<place_holder> clone() const override
                {
                    return std::unique_ptr<place_holder>( new place_holder_concrete<T>( value ) );
                }

                virtual NType getType() const override { return NType::get<T>(); }
                virtual const nstring_view getSignature() const override { return NType::getSignature<T>(); }
            };

            std::unique_ptr<place_holder> ptr;

        public:
            any()
                : ptr( nullptr )
            {
            }

            any( any&& other )
                : ptr( std::move( other.ptr ) )
            {
            }

            any( const any& other )
                : ptr( other.ptr->clone() )
            {
            }

            template<typename T>
            any( const T& value )
                : ptr( new place_holder_concrete<std::decay_t<T>>( value ) )
            {
            }

            template<typename T>
            any( T&& value )
                : ptr( new place_holder_concrete<std::decay_t<T>>( std::move( value ) ) )
            {
            }

            any& operator=( any&& rhs )
            {
                ptr = std::move( rhs.ptr );
                return *this;
            }

            any& operator=( const any& rhs )
            {
                ptr = std::move( rhs.ptr->clone() );
                return *this;
            }

            template<typename T>
            any& operator=( T&& rhs )
            {
                using decayed_t = std::decay_t<T>;
                ptr.reset( new place_holder_concrete<decayed_t>( rhs ) );
                return *this;
            }

            template<typename T>
            any& operator=( const T& rhs )
            {
                using decayed_t = std::decay_t<T>;
                ptr.reset( new place_holder_concrete<decayed_t>( rhs ) );
                return *this;
            }

            void clear()
            {
                ptr.reset( nullptr );
            }

            nbool isNull() const { return ptr == nullptr; }
            inline NType getType() const { return ptr != nullptr ? ptr->getType() : NType(); }
            inline const nstring_view getSignature() const { return ptr->getSignature(); }

            template<typename T>
            friend T any_cast( any& value )
            {
                nassert( value.getType() == NType::get<T>(), "[any_cast()]: type mismatch" );
                return static_cast<any::place_holder_concrete<T>*>( value.ptr.get() )->value;
            }

            template<typename T>
            friend T any_cast( const any& value )
            {
                nassert( value.getType() == NType::get<T>(), "[any_cast()]: type mismatch" );
                return static_cast<any::place_holder_concrete<T>*>( value.ptr.get() )->value;
            }
        };
    }
}