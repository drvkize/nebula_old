#pragma once

#include "Platform/Platform.hpp"
#include "Memory/Memory.hpp"

namespace Nebula
{
    namespace IO
    {
        enum ConsoleColor
        {
            CC_Black,
            CC_Red,
            CC_Green,
            CC_Blue,
            CC_Yellow,
            CC_Magenta,
            CC_Cyan,
            CC_White
        };

        enum ConsoleFormat
        {
            CF_Bold,
            CF_Underline
        };

        enum SeekOption
        {
            SO_Set = std::ios_base::beg,
            SO_Current = std::ios_base::cur,
            SO_End = std::ios_base::end
        };

        class StdFile;
        using StdFilePtr = std::shared_ptr<StdFile>;

        template<typename T>
        class StdConsoleSPI;

        class HeteroStream;
        using HeteroStreamPtr = std::shared_ptr<HeteroStream>;

        class MemoryStream;
        using MemoryStreamPtr = std::shared_ptr<MemoryStream>;

        class StdFileStream;
        using StdFileStreamPtr = std::shared_ptr<StdFileStream>;
    }
}