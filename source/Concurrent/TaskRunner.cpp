#include "Concurrent/TaskRunner.hpp"
#include "Concurrent/TaskManager.hpp"
#include "Concurrent/Task.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        TaskRunner::TaskRunner( TaskManager* owner )
            : owner( owner )
            , task( nullptr )
            , running( false )
        {
        }

        TaskRunner::~TaskRunner()
        {
            release();
        }

        nbool TaskRunner::init()
        {
            return thread.init( &TaskRunner::process, this );
        }

        void TaskRunner::release()
        {
            nassert( task == nullptr, "[TaskRunner::release()]: task still running" );

            thread.release();
            owner = nullptr;
        }

        void TaskRunner::process( void* args )
        {
            TaskRunner* runner = reinterpret_cast<TaskRunner*>( args );
            TaskManager* owner = runner->owner;

            nassert( runner, "[TaskRunner::process()]: invalid runner instance" );
            nassert( owner, "[TaskRunner::process()]: invalid owner" );

            runner->running = true;

            while( !owner->getExitFlag() )
            {
                owner->attachTask( runner );

                TaskPtr task = runner->getTask();
                if( task == nullptr )
                {
                    continue;
                }

                if( !task->getAbortFlag() )
                {
                    switch( task->getState() )
                    {
                        case Task::TaskState::TS_Pending:
                        {
                            task->setState( Task::TaskState::TS_Running );

                            task->onStart();

                            if( task->getUpdateFlag() )
                            {
                                task->setState( Task::TaskState::TS_Running );
                            }
                            else
                            {
                                task->setState( Task::TS_Ending );
                            }
                            //task->setState( task->getUpdateFlag() ? Task::TaskState::TS_Running : Task:TaskState::TS_Ending );
                        }
                        break;
                        case Task::TaskState::TS_Running:
                        {
                            task->onUpdate();

                            if( task->getUpdateFlag() )
                            {
                                task->setState( Task::TaskState::TS_Running );
                            }
                            else
                            {
                                task->setState( Task::TaskState::TS_Ending );
                            }
                        }
                        break;
                        case Task::TaskState::TS_Ending:
                        {
                            task->onStop();
                            task->setState( Task::TaskState::TS_Ended );
                        }
                        break;
                        default:
                        {
                            nassert( 0, "[TaskRunner::process()]: invalid task state" );
                        }
                    }
                }
                else
                {
                    task->onAbort();
                    task->setState( Task::TaskState::TS_Ended );
                }

                owner->detachTask( runner );
            }

            runner->running = false;
        }

        nsize TaskRunner::getThreadID() const
        {
            return thread.getThreadID();
        }
    }
}