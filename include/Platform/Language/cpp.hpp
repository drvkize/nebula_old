#pragma once

// compiler information
#ifdef __has_feature
#define NEBULA_HAS_FEATURE( x ) __has_feature( x )
#else
#define NEBULA_HAS_FEATURE( x ) 0
#endif

#ifdef __has_include
#define NEBULA_HAS_INLCUDE( x ) __has_include( x )
#else
#define NEBULA_HAS_INCLUDE( x ) 0
#endif

#ifdef __has_cpp_attribute
#define NEBULA_HAS_CPP_ATTRIBUTE( x ) __has_cpp_attribute( x )
#else
#define NEBULA_HAS_CPP_ATTRIBUTE( x ) 0
#endif

#if defined( __GNUC__ ) && !defined( __clang__ )
#define NEBULA_COMPILER_GNUC
#define NEBULA_COMPILER_VERSION_GCC ( __GNUC__ * 10000 + __GUNC_MINOR__ * 100 + __GNUC_PATCHLEVEL__ )
#else
#define NEBULA_GCC_VERSION 0
#endif

#if __cplusplus >= 201103L || defined( __GXX_EXPERIMENTAL_CXX0X__ )
#define NEBULA_HAS_GXX_CXX11 NEBULA_GCC_VERSION
#else
#define NEBULA_HAS_GXX_CXX11 0
#endif

#ifdef _MSC_VER
#define NEBULA_COMPILER_MSC
#define NEBULA_COMPILER_VERSION_MSC _MSC_VER
#else
#define NEBULA_MSC_VER 0
#endif

#ifdef NEBULA_COMPILER_MSC
#define NEBULA_SHARED_LIBRARY_EXPORT __declspec( dllexport )
#define NEBULA_SHARED_LIBRARY_IMPORT __declspec( dllimport )
#else
#define NEBULA_SHARED_LIBRARY_EXPORT
#define NEBULA_SHARED_LIBRARY_IMPORT
#endif

#if NEBULA_BUILD_LIBRARY
#define NEBULA_INTERFACE NEBULA_SHARED_LIBRARY_EXPORT
#else
#define NEBULA_INTERFACE NEBULA_SHARED_LIBRARY_IMPORT
#endif

#if defined( __amd64__ ) || ( __amd64 ) || ( __x86_64__ ) || ( __x86_64 ) || ( _M_AMD64 )
#define NEBULA_BUILD_X64
#elif defined( i386 ) || ( __i386 ) || ( __i386__ ) || ( _M_IX86 )
#define NEBULA_BUILD_X86
#endif

#if defined( __SSE__ ) || ( _M_IX86_FP > 0 ) || ( _M_X64 > 0 )
#define NEBULA_SIMD_SSE
#elif defined( __ARM_NEON__ )
#define NEBULA_SIMD_NEON
#elif defined( __GNUC__ ) && !defined( __arm__ )
#define NEBULA_SIMD_GNU
#else
#define NEBULA_SIMD_SCALAR
#endif

// non-standard keywords
#if defined( __GNUC__ )
    #if defined( __cplusplus )
        #define nrestrict __restrict
    #endif
    #define nalign16 __attribute__( ( aligned( 16 ) ) )
#elif defined(_WIN32)
    #define nrestrict
    #define nalign16 __desclspec( align( 16 ) )
#else
    #define nrestrict restrict
    #define nalign16
#endif

#if defined( __GNUC__ )
    #define npure __attribute__( ( pure ) )
#else
    #define npure
#endif

#if defined( _WIN32 )
    #if defined( min ) || defined( max )
    #undef min
    #undef max
    #endif
#endif

// simd
#if defined( __cplusplus )
    #define simd_param( t, p ) const t& p
#else
    #define simd_param( t, p ) t p
#endif

// std headers
#include <cstring>
#include <vector>
#include <list>
#include <unordered_map>
#include <map>
#include <string>
#include <limits>
#include <algorithm>
#include <type_traits>
#include <iterator>
#include <utility>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <chrono>
#include <codecvt>

#include <cassert>
#include <chrono>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <thread>

// disable warnings
#pragma warning( disable: 4146 )
#pragma warning( disable: 4251 )
#pragma warning( disable: 4595 )
#pragma warning( disable: 4996 )

// type definitions
namespace Nebula
{
    namespace Platform
    {
        using uintptr = std::uintptr_t;
        using nbool = bool;
        using nchar = char;
        using nwchar = wchar_t;
        using int8 = signed char;
        using int16 = signed short;
        using int32 = signed int32_t;
        using int64 = signed int64_t;
        using uint8 = unsigned char;
        using uint16 = unsigned short;
        using uint32 = unsigned int32_t;
        using uint64 = unsigned int64_t;
        using nfloat = float;
        using ndouble = double;
        using nsize = std::size_t;

        template<typename CharType>
        using nbasic_string = std::basic_string<CharType, std::char_traits<CharType>, std::allocator<CharType>>;
        using nstring = nbasic_string<nchar>;
        using nwstring = nbasic_string<nwchar>;
        template<typename ValueType, typename... Tn>
        using nvector = std::vector<ValueType, Tn...>;
        template<typename KeyType, typename ValueType, typename... Tn>
        using nunordered_map = std::unordered_map<KeyType, ValueType, Tn...>;
        template<typename KeyType, typename ValueType, typename... Tn>
        using nmap = std::map<KeyType, ValueType, Tn...>;
        template<typename T, typename... Tn>
        using nlist = std::list<T, Tn...>;        
    }
}

namespace std
{
    template<>
    struct hash<Nebula::Platform::uint32>
    {
        constexpr Nebula::Platform::uint32 operator()( const Nebula::Platform::uint32& v ) const
        {
            return v;
        }
    };

    static inline std::string to_string( const std::string& v )
    {
        return "\"" + v + "\"";
    }

    static inline std::string to_string( const bool& v )
    {
        return v ? "true" : "false";
    }
}

// macros
#define nassert( Condition, Message ) assert( ( Condition ) && ( Message ) )
#define NEBULA_STRINGFY( name ) #name
#define NEBULA_STR( value ) NEBULA_STRINGFY( value )

#if defined( NEBULA_COMPILER_MSC )
#define NLONG_FMT "%ll"
#define NULONG_FMT "%llu"
#elif defined( NEBULA_COMPILER_GNUC )
#define NLONG_FMT "%l"
#define NULONG_FMT "%lu"
#endif