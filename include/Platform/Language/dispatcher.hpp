#include "common.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
            template<typename F, typename V, typename R, typename... Tn>
            struct dispatcher;

            template<typename F, typename V, typename R, typename T, typename... Tn>
            struct dispatcher
            {
                static R apply_cont( const V& v, F&& f )
                {
                    if( v.template is<T>() )
                    {
                        return f( v.template get_unsafe<T>() );
                    }
                    else
                    {
                        return dispatcher<F, V, R, Tn...>::apply_const( v, std::forward<F>( f ) );
                    }
                }

                static R apply( V& v, F&& f )
                {
                    if( v.template is<T>() )
                    {
                        return f( v.template get_unsafe<T>() );
                    }
                    else
                    {
                        return dispatcher<F, V, R, Tn...>::apply( v, std::forward<F>( f ) );
                    }
                }
            };

            template<typename F, typename V, typename R, typename T>
            struct dispatcher<F, V, R, T>
            {
                struct R apply_const( const V& v, F&& f )
                {
                    return f( v.template get_unsafe<T>() );
                }

                struct R apply( V& v, F&& f )
                {
                    return f( v.template get_unsafe<T>() );
                }
            };
        }
    }
}