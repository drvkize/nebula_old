#include "Concurrent/TaskManager.hpp"
#include "Concurrent/TaskRunner.hpp"
#include "Concurrent/Task.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        TaskManager::TaskManager()
            : pending_tasks( nullptr )
            , exit_flag( false )
            , destroy_flag( false )
            , assigned_task_count( 0 )
            , finished_task_count( 0 )
            , running_task_count( 0 )
        {            
        }

        TaskManager::~TaskManager()
        {
            if( !destroy_flag )
            {
                release();
            }
        }

        nbool TaskManager::init( nsize runner_count, nsize task_capacity )
        {
            Memory::NebulaAllocator& alloc = Context::getSystemAllocator();

            exit_flag = false;
            destroy_flag = false;

            runners.reserve( runner_count );
            pending_tasks = static_cast<MPMCQueue<TaskPtr>*>( alloc.allocate( sizeof( MPMCQueue<TaskPtr> ) ) );
            nassert( pending_tasks, "[TaskManager::init()]: create task queue failed" );
            pending_tasks = new( pending_tasks ) MPMCQueue<TaskPtr>( task_capacity );

            for( nsize i = 0; i < runner_count; ++i )
            {
                TaskRunner* new_runner = static_cast<TaskRunner*>( alloc.allocate( sizeof( TaskRunner ) ) );
                if( new_runner != nullptr )
                {
                    new_runner = new( new_runner ) TaskRunner( this );
                    new_runner->init();
                    runners.push_back( new_runner );
                }
            }

            return true;
        }

        void TaskManager::release()
        {
            exit_flag = true;

            // abort all tasks
            TaskPtr task = nullptr;
            while( !isIdle() )
            {
                pending_task_event.setAll();
            }

            // stop runners
            destroy_flag = true;
            nsize running_count = runners.size();
            while( running_count > 0 )
            {
                running_count = 0;
                for( nsize i =0; i < runners.size(); ++i )
                {
                    if( runners[i]->isRunning() )
                    {
                        ++running_count;
                    }
                }

                if( running_count > 0 )
                {
                    pending_task_event.setAll();
                }
            }

            // destroy runners
            Memory::NebulaAllocator& alloc = Context::getSystemAllocator();

            for( int i = 0; i < runners.size(); ++i )
            {
                runners[i]->release();
                alloc.deallocate( runners[i] );
            }
            runners.clear();

            // destroy pending tasks
            alloc.deallocate( pending_tasks );
            pending_tasks = nullptr;
        }

        nbool TaskManager::assign( TaskPtr task )
        {
            if( task && !exit_flag )
            {
                task->setState( Task::TaskState::TS_Pending );
                pending_tasks->push( task );
                ++assigned_task_count;

                // notify waiting runners
                pending_task_event.setOne();

                return true;
            }

            return false;
        }

        nbool TaskManager::runPendingQueue( TaskRunner* runner )
        {
            TaskPtr task = nullptr;
            if( pending_tasks->tryPop( task ) && task != nullptr )
            {
                task->setRunner( runner );
                running_task_count.fetch_add( 1 );
                runner->setTask( task );

                return true;
            }

            return false;
        }

        void TaskManager::attachTask( TaskRunner* runner )
        {
            TaskPtr task = nullptr;
            if( pending_tasks->isEmpty() )
            {
                pending_task_event.waitOne();
            }

            runPendingQueue( runner );
        }

        void TaskManager::detachTask( TaskRunner* runner )
        {
            TaskPtr task = runner->getTask();
            if( task )
            {
                task->setRunner( nullptr );
                runner->setTask( nullptr );

                if( task->getState() != Task::TaskState::TS_Ended )
                {
                    pending_tasks->push( task );
                }
                else
                {
                    ++finished_task_count;
                }

                running_task_count.fetch_sub( 1 );
            }
        }
    }
}