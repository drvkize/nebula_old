#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Reflection/NParameter.hpp"
#include "Reflection/NMethod.hpp"

namespace Nebula
{
    namespace Reflection
    {
        template<typename F>
        class NMethodBuilder
        {
            using Type = F;
            using ClassType = typename ntraits::function_traits<Type>::class_type;
            using ReturnType = typename ntraits::function_traits<Type>::return_type;
            using ParameterTypes = typename ntraits::function_traits<Type>::parameter_types;
            static constexpr nsize ParameterCount = ParameterTypes::count;

            F const func;
            nstring_view name;
            narray<nstring_view, ParameterCount> parameter_names;
            narray<any, ParameterCount> parameter_default_values;

        public:
            NMethodBuilder( F const func, const nstring_view name )
                : func( func )
                , name( name )
            {
            }

            template<typename... Tn>
            void setParameterNames( const Tn... names )
            {
                NParameterHelper::setParameterNames( parameter_names, names... );
            }

            template<typename... Tn>
            void setParameterDefaultValues( const Tn&... default_values )
            {
                NParameterHelper::setParameterDefaultValues( parameter_default_values, default_values... );
            }

            NMethodPtr build()
            {
                narray<NParameterPtr, ParameterCount> parameters;
                NParameterHelper::packParameters<typename ParameterTypes>( parameters, array_view<nstring_view>( parameter_names ), array_view<any>( parameter_default_values ) );

                return NMethodPtr( new NMethodConcrete<F>( func, std::move( name ), std::move( parameters ) ) );
            }
        };
    }
}