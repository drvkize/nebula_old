#pragma once

#include "IO/Serialization/NTypeDefine.hpp"
#include "IO/Serialization/NTypeMapper.hpp"
#include "IO/HeteroStream.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Serialization
        {
            #pragma pack( push, 1 )

            // scalar
            template<typename T>
            class NEBULA_INTERFACE NScalarReader
            {
            public:
                static constexpr NTypeTag ValidTag = ScalarTypeToTag<T>::Tag;

            private:
                NTypeTag tag;
                T data;

            public:
                inline NTypeTag getTag() const { return tag; }
                inline nsize getSize() const { return sizeof( NScalarReader ); }
                inline T getData() const { return toLocalEndian( data ); }
            };

            // string
            class NEBULA_INTERFACE NStringReader
            {
            public:
                static constexpr NTypeTag ValidTag = NTypeTag::NTT_String;
            
            private:
                NTypeTag tag;
                uint64 size;

            public:
                inline NTypeTag getTag() const { return tag; }
                inline nsize getSize() const { return toLocalEndian( size ); }
                inline nstring getData() const { return nstring( reinterpret_cast<const nchar*>( this ) + sizeof( NTypeTag ) + sizeof( uint64 ) ); }
            };

            // array
            template<typename T>
            class NArrayReader
            {
            public:
                static constexpr NTypeTag ValidTag = NTypeTag::NTT_Array;
                static constexpr NTypeTag ValidElementTag = ScalarTypeToTag<T>::Tag;
                static constexpr nbool IsScalarType = ValidElementTag != NTypeTag::NTT_Unknown;

            private:
                NTypeTag tag;
                uint64 size;
                uint64 count;
                NTypeTag element_tag;

                template<typename F, nbool IsScalar>
                struct MarshalHelper {};

                template<typename F>
                struct MarshalHelper<F, true>
                {
                    static const F& marshal( const int8* data, uint64 index )
                    {
                        return toLocalEndian( *( reinterpret_cast<const F*>( data ) + index ) );
                    }
                };

                template<typename F>
                struct MarshalHelper<F, false>
                {
                    static const F& marshal( const int8* data, uint64 index )
                    {
                        for( uint64 i = 0; i < index; ++i )
                        {
                            data += reinterpret_cast<const T*>( data )->getSize();
                        }

                        return *reinterpret_cast<const F*>( data );
                    }
                };

            public:
                inline NTypeTag getTag() const { return tag; }
                inline NTypeTag getElementTag() const { return element_tag; }
                inline nbool isScalarType() const { return element_tag != NTypeTag::NTT_Unknown; }
                inline nsize getSize() const { return toLocalEndian( size ); }
                inline nsize getCount() const { return toLocalEndian( count ); }

                const T& getElement( uint64 index ) const
                {
                    nassert( isScalarType() == IsScalarType, "[NArrayReader::getElement()], element type mismatch" );

                    const int8* data = reinterpret_cast<const int8*>( this ) + sizeof( NArrayReader<T> );
                    return MarshalHelper<T, IsScalarType>::marshal( data, index );
                }

                inline const T& operator[]( uint64 index ) const { return getElement( index ); }
            };

            // object
            class NEBULA_INTERFACE NObjectReader
            {
            public:
                static constexpr NTypeTag ValidTag = NTypeTag::NTT_Object;

            private:
                NTypeTag tag;
                uint64 size;
                uint64 count;

            public:
                inline NTypeTag getTag() const { return tag; }
                inline nsize getSize() const { return toLocalEndian( size ); }
                inline nsize getCount() const { return toLocalEndian( count ); }

                const NTypeReader& getMember( const nchar* name ) const;
                inline const NTypeReader& operator[]( const nchar* name ) const { return getMember( name ); }
            };

            // binary
            class NEBULA_INTERFACE NBinaryReader
            {
            public:
                static constexpr NTypeTag ValidTag = NTypeTag::NTT_Binary;
            
            private:
                NTypeTag tag;
                uint64 size;

            public:
                inline NTypeTag getTag() const { return tag; }
                inline nsize getSize() const { return toLocalEndian( size ); }
                inline const int8* const getData() const { return reinterpret_cast<const int8* const>( this ) + sizeof( NTypeTag ) + sizeof( uint64 ); }
            };

            class NEBULA_INTERFACE NTypeReader
            {
                NTypeTag tag;
                
                static inline nbool isDynamicType( NTypeTag tag ) { return tag > NTypeTag::NTT_Unknown && tag < NTypeTag::NTT_String; }
                static nsize staticSizer( const NTypeReader* reader );
                static nsize dynamicSizer( const NTypeReader* reader );

            public:
                static NTypeReader Null;
                static const NTypeReader& create( const int8* buffer );

                inline NTypeTag getTag() const { return tag; }
                inline nbool isNull() const { return tag == NTypeTag::NTT_Null; }
                inline nbool isBool() const { return tag == NTypeTag::NTT_Bool; }
                inline nbool isInt8() const { return tag == NTypeTag::NTT_Int8; }
                inline nbool isInt16() const { return tag == NTypeTag::NTT_Int16; }
                inline nbool isInt32() const { return tag == NTypeTag::NTT_Int32; }
                inline nbool isInt64() const { return tag == NTypeTag::NTT_Int64; }
                inline nbool isUInt8() const { return tag == NTypeTag::NTT_UInt8; }
                inline nbool isUInt16() const { return tag == NTypeTag::NTT_UInt16; }
                inline nbool isUInt32() const { return tag == NTypeTag::NTT_UInt32; }
                inline nbool isUInt64() const { return tag == NTypeTag::NTT_UInt64; }
                inline nbool isFloat() const { return tag == NTypeTag::NTT_Float; }
                inline nbool isDouble() const { return tag == NTypeTag::NTT_Double; }
                inline nbool isInt2() const { return tag == NTypeTag::NTT_Int2; }
                inline nbool isInt3() const { return tag == NTypeTag::NTT_Int3; }
                inline nbool isInt4() const { return tag == NTypeTag::NTT_Int4; }
                inline nbool isFloat2() const { return tag == NTypeTag::NTT_Float2; }
                inline nbool isFloat3() const { return tag == NTypeTag::NTT_Float3; }
                inline nbool isFloat4() const { return tag == NTypeTag::NTT_Float4; }
                inline nbool isFloat2x2() const { return tag == NTypeTag::NTT_Float2x2; }
                inline nbool isFloat3x3() const { return tag == NTypeTag::NTT_Float3x3; }
                inline nbool isFloat4x4() const { return tag == NTypeTag::NTT_Float4x4; }
                inline nbool isFloat4x3() const { return tag == NTypeTag::NTT_Float4x3; }
                inline nbool isDouble2() const { return tag == NTypeTag::NTT_Double2; }
                inline nbool isDouble3() const { return tag == NTypeTag::NTT_Double3; }
                inline nbool isDouble4() const { return tag == NTypeTag::NTT_Double4; }
                inline nbool isDouble2x2() const { return tag == NTypeTag::NTT_Double2x2; }
                inline nbool isDouble3x3() const { return tag == NTypeTag::NTT_Double3x3; }
                inline nbool isDouble4x4() const { return tag == NTypeTag::NTT_Double4x4; }
                inline nbool isDouble4x3() const { return tag == NTypeTag::NTT_Double4x3; }
                inline nbool isString() const { return tag == NTypeTag::NTT_String; }
                inline nbool isArray() const { return tag == NTypeTag::NTT_Array; }
                inline nbool isObject() const { return tag == NTypeTag::NTT_Object; }
                inline nbool isBinary() const { return tag == NTypeTag::NTT_Binary; }

                inline NTypeTag getArrayElementTag() const
                {
                    nassert( isArray(), "[NTypeReader::getArrayElementTag()]: tag mismatch" );
                    return reinterpret_cast<const NArrayReader<NTypeReader*>*>( this )->getElementTag();
                }

                template<typename T>
                inline T asScalar() const { return reinterpret_cast<const NScalarReader<T>*>( this )->getData(); }
                inline nbool asBool() const { nassert( isBool(), "[NTypeReader::asBool()], tag mismatch" ); return asScalar<nbool>(); }
                inline int8 asInt8() const { nassert( isInt8(), "[NTypeReader::asInt8()], tag mismatch" ); return asScalar<int8>(); }
                inline int16 asInt16() const { nassert( isInt16(), "[NTypeReader::asInt16()], tag mismatch" ); return asScalar<int16>(); }
                inline int32 asInt32() const { nassert( isInt32(), "[NTypeReader::asInt32()], tag mismatch" ); return asScalar<int32>(); }
                inline int64 asInt64() const { nassert( isInt64(), "[NTypeReader::asInt64()], tag mismatch" ); return asScalar<int64>(); }
                inline uint8 asUInt8() const { nassert( isUInt8(), "[NTypeReader::asUInt8()], tag mismatch" ); return asScalar<uint8>(); }
                inline uint16 asUInt16() const { nassert( isUInt16(), "[NTypeReader::asUInt16()], tag mismatch" ); return asScalar<uint16>(); }
                inline uint32 asUInt32() const { nassert( isUInt32(), "[NTypeReader::asUInt32()], tag mismatch" ); return asScalar<uint32>(); }
                inline uint64 asUInt64() const { nassert( isUInt64(), "[NTypeReader::asUInt64()], tag mismatch" ); return asScalar<uint64>(); }
                inline nfloat asFloat() const { nassert( isFloat(), "[NTypeReader::asFloat()], tag mismatch" ); return asScalar<nfloat>(); }
                inline ndouble asDouble() const { nassert( isDouble(), "[NTypeReader::asDouble()], tag mismatch" ); return asScalar<ndouble>(); }
                inline int2 asInt2() const { nassert( isInt2(), "[NTypeReader::asInt2()], tag mismatch" ); return asScalar<int2>(); }
                inline int3 asInt3() const { nassert( isInt3(), "[NTypeReader::asInt3()], tag mismatch" ); return asScalar<int3>(); }
                inline int4 asInt4() const { nassert( isInt4(), "[NTypeReader::asInt3()], tag mismatch" ); return asScalar<int4>(); }
                inline float2 asFloat2() const { nassert( isFloat2(), "[NTypeReader::asFloat2()], tag mismatch"); return asScalar<float2>(); }
                inline float3 asFloat3() const { nassert( isFloat3(), "[NTypeReader::asFloat3()], tag mismatch"); return asScalar<float3>(); }
                inline float4 asFloat4() const { nassert( isFloat4(), "[NTypeReader::asFloat4()], tag mismatch"); return asScalar<float4>(); }
                inline float2x2 asFloat2x2() const { nassert( isFloat2x2(), "[NTypeReader::asFloat2x2()], tag mismatch"); return asScalar<float2x2>(); }
                inline float3x3 asFloat3x3() const { nassert( isFloat3x3(), "[NTypeReader::asFloat3x3()], tag mismatch"); return asScalar<float3x3>(); }
                inline float4x4 asFloat4x4() const { nassert( isFloat4x4(), "[NTypeReader::asFloat4x4()], tag mismatch"); return asScalar<float4x4>(); }
                inline float4x3 asFloat4x3() const { nassert( isFloat4x3(), "[NTypeReader::asFloat4x3()], tag mismatch"); return asScalar<float4x3>(); }
                inline double2 asDouble2() const { nassert( isDouble2(), "[NTypeReader::asDouble2()], tag mismatch"); return asScalar<double2>(); }
                inline double3 asDouble3() const { nassert( isDouble3(), "[NTypeReader::asDouble3()], tag mismatch"); return asScalar<double3>(); }
                inline double4 asDouble4() const { nassert( isDouble4(), "[NTypeReader::asDouble4()], tag mismatch"); return asScalar<double4>(); }
                inline double2x2 asDouble2x2() const { nassert( isDouble2x2(), "[NTypeReader::asDouble2x2()], tag mismatch"); return asScalar<double2x2>(); }
                inline double3x3 asDouble3x3() const { nassert( isDouble3x3(), "[NTypeReader::asDouble3x3()], tag mismatch"); return asScalar<double3x3>(); }
                inline double4x4 asDouble4x4() const { nassert( isDouble4x4(), "[NTypeReader::asDouble4x4()], tag mismatch"); return asScalar<double4x4>(); }
                inline double4x3 asDouble4x3() const { nassert( isDouble4x3(), "[NTypeReader::asDouble4x3()], tag mismatch"); return asScalar<double4x3>(); }
                inline nstring asString() const { nassert( isString(), "[NTypeReader::asString()], tag mismatch" ); return reinterpret_cast<const NStringReader*>( this )->getData(); }
                template<typename T = NTypeReader>
                inline const NArrayReader<T>& asArray() const { nassert( isArray(), "[NTypeReader::asArray()], tag mismatch" ); return *reinterpret_cast<const NArrayReader<T>*>( this ); }
                inline const NObjectReader& asObject() const { nassert( isObject(), "[NTypeReader::asObject()], tag mismatch" ); return *reinterpret_cast<const NObjectReader*>( this ); }
                inline const NBinaryReader& asBinary() const { nassert( isBinary(), "[NTypeReader::asBinary()], tag mismatch" ); return *reinterpret_cast<const NBinaryReader*>( this ); }

                nsize getSize() const;
            };

            #pragma pack( pop )
        }
    }
}