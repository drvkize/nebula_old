#pragma once

#include "Memory/MemoryDefine.hpp"

namespace Nebula
{
    namespace Memory
    {
        // align by increase address
        void* alignUp( const void* p, nsize align );
        // align by decrease address
        void* alignDown( const void* p, nsize align );
        // is pointer aligned
        nbool isPointerAligned( const volatile void* p, const nsize align );

        class NEBULA_INTERFACE NebulaAllocator
        {
        public:
            NebulaAllocator( const NebulaAllocator& other ) = delete;
            NebulaAllocator& operator=( const NebulaAllocator& rhs ) = delete;

        public:
            NebulaAllocator();
            virtual ~NebulaAllocator();

            virtual void* allocate( nsize size, nsize align = 1 ) = 0;
            virtual nbool deallocate( void* p ) = 0;
            virtual void release();
        };
    }
}