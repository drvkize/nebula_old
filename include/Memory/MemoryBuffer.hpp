#pragma once

#include "Memory/MemoryDefine.hpp"
using namespace Nebula::Platform;
#include "Memory/nallocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        template<typename T, typename AllocatorType = nallocator<T>>
        class MemoryBuffer
        {
            using allocator_type = AllocatorType;
            static_assert( std::is_pod<T>::value, "[buffer]: T must be a POD type" );

        public:
            allocator_type allocator;
            T* data;
            nsize capacity;
            nsize count;

            void setCapacity( nsize new_capacity )
            {
                if( new_capacity == capacity )
                {
                    return;
                }

                if( new_capacity < count )
                {
                    count = new_capacity;
                }

                T* new_data = nullptr;
                if( new_capacity > 0 )
                {
                    new_data = static_cast<T*>( allocator.allocate( new_capacity * sizeof( T ) ) );
                    memcpy( new_data, data, count * sizeof( T ) );
                }

                allocator.deallocate( data );
                data = new_data;
                capacity = new_capacity;
            }

        public:
            static std::shared_ptr<MemoryBuffer<T>> create( nsize reserve = 0 )
            {
                return std::allocate_shared<MemoryBuffer<T>>( nallocator<MemoryBuffer<T>>(), reserve );
            }

            MemoryBuffer()
                : capacity( 0 )
                , count( 0 )
                , data( nullptr )
                , allocator( std::move( allocator_type() ) )
            {
            }

            MemoryBuffer( nsize capacity )
                : capacity( 0 )
                , count( 0 )
                , data( nullptr )
                , allocator( allocator_type() )
            {
                reserve( capacity );
            }

            MemoryBuffer( const T* data, nsize data_count )
                : allocator( allocator_type() )
            {
                copy( data, data_count );
            }

            MemoryBuffer( const MemoryBuffer& other )
                : allocator( other.allocator )
            {
                *this = other;
            }

            MemoryBuffer( MemoryBuffer&& other ) noexcept
            {
                swap( *this, other );
            }

            ~MemoryBuffer()
            {
                clean();
            }

            inline friend void swap( MemoryBuffer& lhs, MemoryBuffer& rhs ) noexcept
            {
                std::swap( lhs.data, rhs.data );
                std::swap( lhs.capacity, rhs.capacity );
                std::swap( lhs.count, rhs.count );
                std::swap( lhs.allocator, rhs.allocate );
            }

            MemoryBuffer& operator=( const MemoryBuffer& rhs )
            {
                copy( rhs.data, rhs.count );
                return *this;
            }

            MemoryBuffer& operator=( MemoryBuffer&& rhs ) noexcept
            {
                swap( *this, rhs );
                return *this;
            }

            nsize copy( const T* data, nsize count )
            {
                reserve( count );
                memcpy( this->data, data, count );
                this->count = count;
                return count;
            }

            nsize insert( const T* data, nsize count )
            {
                insert( data, 0, count );
            }

            nsize insert( const T* data, int64 offset, nsize count )
            {
                if( offset < 0 )
                {
                    return 0;
                }

                reserve( offset + count );
                memcpy( this->data + offset, data, count * sizeof( T ) );
                
                if( this->count < offset + count )
                {
                    this->count = offset + count;
                }
                return count;
            }

            void append( const T* data, nsize count )
            {
                nsize start = this->count;
                reserve( this->count + count );
                memcpy( this->data + start, data, count * sizeof( T ) );
                this->count += count;
            }

            void reserve( nsize new_capacity )
            {
                if( new_capacity > capacity )
                {
                    grow( new_capacity );
                }
            }

            void grow( nsize new_capacity = 0 )
            {
                nsize estimate = capacity * 2 + 8;
                setCapacity( new_capacity > estimate ? new_capacity : estimate );
            }

            void resize( nsize new_count )
            {
                if( new_count > capacity )
                {
                    setCapacity( new_count );
                }

                count = new_count;
            }

            inline void clear() { count = 0; }
            inline void clean() { clear(); shrink(); }
            inline void shrink() { setCapacity( 0 ); }
            inline nsize getCapacity() const { return capacity; }
            inline nsize getCount() const { return count; }
            inline nsize getSize() const { return count * sizeof( T ); }
            inline nbool isEmpty() const { return count == 0; }
            inline T* getData() { return data; }
            inline const T* getData() const { return data; }
        };
    }

    namespace Platform
    {
        using Buffer = Memory::MemoryBuffer<int8>;
        using BufferPtr = std::shared_ptr<Buffer>;
    }
}