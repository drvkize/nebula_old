#pragma once

#include "System/SystemDefine.hpp"
#include "IO/Serialization/NTypeReader.hpp"
using Nebula::IO::Serialization::NTypeReader;

namespace Nebula
{
    namespace System
    {
        class NebulaMachine
        {
        public:
            nbool init( const NTypeReader& initParameters );
            void release();
            void push();
            void pop();
        };
    }
}