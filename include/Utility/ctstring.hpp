#pragma once

#include "Platform/Platform.hpp"
#include "Platform/Language/index_sequence.hpp"

namespace Nebula
{
    namespace Platform
    {
        template<nsize N>
		class ctstring
		{
			const nchar data[N + 1];
			static constexpr auto index = typename ntraits::make_index_sequence<N> {};

		public:
			template<typename... Characters>
			constexpr ctstring( Characters... characters )
				: data { characters..., '\0' }
			{
			}

			template<nsize... Indexes>
			constexpr ctstring( const ctstring<N>& other, ntraits::index_sequence<Indexes...> dummy = ctstring::index )
				: data { other[Indexes]..., '\0' }
			{
			}

			template<nsize X, nsize... Indexes>
			constexpr ctstring( const ctstring<X>& other, ntraits::index_sequence<Indexes...> dummy )
				: data { other[Indexes]..., '\0' }
			{
			}

			template<nsize... Indexes>
			constexpr ctstring( const nchar( &value )[N + 1], ntraits::index_sequence<Indexes...> dummy )
				: ctstring( value[Indexes]... )
			{
			}

			constexpr ctstring( const nchar( &value )[N + 1] )
				: ctstring( value, ntraits::make_index_sequence<N>{} )
			{
			}

			constexpr nchar operator[]( const nsize index ) const
			{
				return index < N ? data[index] : throw std::out_of_range( "ctstring index out of range" );;
			}

			constexpr nsize length() const { return N; }
			constexpr const nchar* const c_str() const { return data; }
		};

		// length_of
		template<typename T>
		struct length_of
		{
			static_assert( std::is_void<T>::value, "Unsupported type of length_of" );
			static constexpr nsize value = 1;
		};

		template<nsize N>
		struct length_of<const nchar(&)[N]>
		{
			static constexpr nsize value = N - 1;
		};

		template<nsize N>
		struct length_of<nchar[N]>
		{
			static constexpr nsize value = N - 1;
		};

		template<nsize N>
		struct length_of<const nchar[N]>
		{
			static constexpr nsize value = N - 1;
		};

		template<nsize N>
		struct length_of<ctstring<N>>
		{
			static constexpr nsize value = N;
		};

		template<nsize N>
		struct length_of<const ctstring<N>>
		{
			static constexpr nsize value = N;
		};

		template<nsize N>
		struct length_of<const ctstring<N>&>
		{
			static constexpr nsize value = N;
		};

		template<typename T>
		struct is_ctstring
		{
			static constexpr nbool value = false;
		};

		// is_ctstring
		template<nsize N>
		struct is_ctstring<ctstring<N>>
		{
			static constexpr nbool value = true;
		};

		template<nsize N>
		struct is_ctstring<ctstring<N>&>
		{
			static constexpr nbool value = true;
		};

		template<nsize N>
		struct is_ctstring<const ctstring<N>>
		{
			static constexpr nbool value = true;
		};

		template<nsize N>
		struct is_ctstring<const ctstring<N>&>
		{
			static constexpr nbool value = true;
		};

		// concat_string
		template<typename Left, typename Right, nsize... IndexesLeft, nsize... IndexesRight>
		constexpr ctstring<sizeof...( IndexesLeft ) + sizeof...( IndexesRight )> concat_string( const Left& lhs, const Right& rhs, ntraits::index_sequence<IndexesLeft...> dummyLeft, ntraits::index_sequence<IndexesRight...> dummyRight )
		{
			return ctstring<sizeof...( IndexesLeft ) + sizeof...( IndexesRight )>( lhs[IndexesLeft]..., rhs[IndexesRight]... );
		}

		template<typename Left, typename Right>
		constexpr ctstring<length_of<Left>::value + length_of<Right>::value> concat_string( const Left& lhs, const Right& rhs )
		{
			return concat_string( lhs, rhs, typename ntraits::make_index_sequence<length_of<decltype(lhs)>::value>{}, typename ntraits::make_index_sequence<length_of<decltype(rhs)>::value>{} );
		}

		// sub_string
		template<nsize From, nsize Count, typename T>
		constexpr ctstring<Count> sub_string( const T& str )
		{
			return ctstring<Count>( str, ntraits::make_index_sequence_from_to<From, Count>{} );
		}

		// operator +
		template<nsize N, typename Right>
		constexpr ctstring<N + length_of<Right>::value> operator+( const ctstring<N>& lhs, const Right& rhs )
		{
			return concat_string( lhs, rhs );
		}

		template<typename Left, nsize N>
		constexpr ctstring<length_of<Left>::value + N> operator+( const Left& lhs, const ctstring<N>& rhs )
		{
			return concat_string( lhs, rhs );
		}

		template<nsize X, nsize Y>
		constexpr ctstring<X + Y> operator+( const ctstring<X>& lhs, const ctstring<Y>& rhs )
		{
			return concat_string( lhs, rhs );
		}

		// compare_characters
		template<nsize Length, nsize Index, typename Left, typename Right>
		constexpr auto compare_characters( const Left& lhs, const Right& rhs )
			->typename std::enable_if<Index != Length, nbool>::type
		{
			return lhs[Index] == rhs[Index] ? compare_characters<Length, Index + 1>( lhs, rhs ) : false;
		}

		template<nsize Length, nsize Index, typename Left, typename Right, typename std::enable_if<Index == Length, nbool>::type = 0>
		constexpr nbool compare_characters( const Left& lhs, const Right& rhs )
		{
			return true;
		}

		// operator ==
		template<nsize N, typename Right>
		constexpr auto operator==( const ctstring<N>& lhs, const Right& rhs )
			-> typename std::enable_if<N == length_of<Right>::value, nbool>::type
		{
			return compare_characters<N, 0>( lhs, rhs );
		}

		template<typename Left, nsize N>
		constexpr auto operator==( const Left& lhs, const ctstring<N>& rhs )
			-> typename std::enable_if<N == length_of<Left>::value, nbool>::type
		{
			return compare_characters<N, 0>( lhs, rhs );
		}

		template<nsize N, typename Right, typename std::enable_if<N != length_of<Right>::value, nbool>::type = 0>
		constexpr nbool operator==( const ctstring<N>& lhs, const Right& rhs )
		{
			return false;
		}

		template<typename Left, nsize N, typename std::enable_if<length_of<Left>::value != N, nbool>::type = 0>
		constexpr nbool operator==( const Left& lhs, const ctstring<N>& rhs )
		{
			return false;
		}

		template<nsize X, nsize Y, typename std::enable_if<X != Y, nbool>::type = 0>
		constexpr nbool operator==( const ctstring<X>& lhs, const ctstring<Y>& rhs )
		{
			return false;
		}

		template<nsize N, nsize... Indexes>
		constexpr auto string_factory( const nchar( &value )[N], ntraits::index_sequence<Indexes...> dummy )
            -> ctstring<N - 1>
		{
			return ctstring<N - 1>( value[Indexes]... );
		}

		template<nsize N>
		constexpr auto string_factory( const nchar( &value )[N] )
            -> ctstring<N - 1>
		{
			return string_factory( value, typename ntraits::make_index_sequence<N - 1>{} );
		}
    }
}