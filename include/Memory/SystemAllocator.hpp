#pragma once

#include "Memory/MemoryDefine.hpp"
#include "Memory/NebulaAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        class FixedSizeChunkAllocator;
        class CoalesceAllocator;
        
        class SystemAllocator : public NebulaAllocator
        {
            NebulaAllocator* parent_allocator;
            FixedSizeChunkAllocator* fixed_allocators;
            CoalesceAllocator* coalesce_allocators;
            nsize fixed_allocator_count;
            nsize coalesce_scale_step;

            // block size
            static constexpr nsize FIXED_MIN = 8;
            static constexpr nsize FIXED_MAX = 512;

            // block size * 256
            static constexpr nsize COALESCE_MIN = 1024 * 256;
            static constexpr nsize COALESCE_MAX = 1024 * 2 * 2 * 2 * 256;
            static constexpr nsize COALESCE_COUNT = 3;

        public:
            SystemAllocator();
            virtual ~SystemAllocator();

            nbool init( NebulaAllocator* parent_allocator );
            virtual void release() override;

            virtual void* allocate( nsize size, nsize align = 1 ) override;
            virtual nbool deallocate( void* p ) override;
        };
    }
}