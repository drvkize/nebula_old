#pragma once

#include "Math/MathDefine.hpp"
#include "Math/Vector.hpp"

namespace Nebula
{
    namespace Math
    {
        template<typename T>
        class Vector<T, 4>
        {
        public:
            using ScalarType = T;
            static constexpr int Dimension = 4;

            union
            {
                T data[Dimension];
                struct
                {
                    T x;
                    T y;
                    T z;
                    T w;
                };
            };

            inline Vector()
            {
            }

            inline Vector( const Vector<T, 4>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = v.data[i] );
            }

            template<typename U>
            explicit inline Vector( const Vector<T, 4>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = static_cast<T>( v.data[i] ) );
            }

            explicit inline Vector( const T s )
            {
                NEBULA_VECTOR_LOOP( data[i] = s );
            }
            
            explicit inline Vector( const T* arr )
            {
                NEBULA_VECTOR_LOOP( data[i] = arr[i] );
            }

            inline Vector( const T s0, const T s1, const T s2, const T s3 )
            {
                x = s0;
                y = s1;
                z = s2;
                w = s3;
            }

            inline Vector( const Vector<T, 3>& v012, const T s3 )
            {
                x = v012[0];
                y = v012[1];
                z = v012[2];
                w = s3;
            }

            inline Vector( const T s0, const Vector<T, 3>& v123 )
            {
                x = s0;
                y = v123[0];
                z = v123[1];
                w = v123[2];
            }

            inline Vector( const Vector<T, 2>& v01, const Vector<T, 2>& v23 )
            {
                x = v01[0];
                y = v01[1];
                z = v23[0];
                w = v23[1];
            }

            inline Vector( const Vector<T, 2>& v01, const T s2, const T s3 )
            {
                x = v01[0];
                y = v01[1];
                z = s2;
                w = s3;
            }

            inline Vector( const T s0, const T s1, const Vector<T, 2>& v23 )
            {
                x = s0;
                y = s1;
                z = v23[0];
                w = v23[1];
            }

            inline Vector( const T s0, const Vector<T, 2>& v12, const T s3 )
            {
                x = s0;
                y = v12[0];
                z = v12[1];
                w = s3;
            }

            inline T& operator()( const int i ) { return data[i]; }
            inline const T& operator()( const int i ) const { return data[i]; }
            inline T& operator[]( const int i ) { return data[i]; }
            inline const T& operator[]( const int i ) const { return data[i]; }

            inline Vector<T, 2> xy() { return Vector<T, 2>( xy ); }
            inline const Vector<T, 2> xy() const { return Vector<T, 2>( xy ); }
            inline Vector<T, 2> yz() { return Vector<T, 2>( yz ); }
            inline const Vector<T, 2> yz() const { return Vector<T, 2>( yz ); }
            inline Vector<T, 2> zw() { return Vector<T, 2>( zw ); }
            inline const Vector<T, 2> zw() const { return Vector<T, 2>( zw ); }
            inline Vector<T, 3> xyz() { return Vector<T, 3>( x, y, z ); }
            inline const Vector<T, 3> xyz() const { return Vector<T, 3>( x, y, z ); }
            inline Vector<T, 3> yzw() { return Vector<T, 3>( y, z, w ); }
            inline const Vector<T, 3> yzw() const { return Vector<T, 3>( y, z, w ); }

            inline void pack( VectorPacked<T, 4>* const v ) const
            {
                NEBULA_VECTOR_LOOP( v->data[i] = data[i] );
            }
        };
    }
}