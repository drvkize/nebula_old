#pragma once

#include "System/SystemDefine.hpp"

namespace Nebula
{
    namespace System
    {
        class NEBULA_INTERFACE NebulaApplication
        {
        public:
            NebulaApplication();
            ~NebulaApplication();

            NebulaApplication( const NebulaApplication& other ) = delete;
            NebulaApplication& operator=( const NebulaApplication& rhs ) = delete;
        };
    }
}