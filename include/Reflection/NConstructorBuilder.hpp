#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Reflection/NConstructor.hpp"

namespace Nebula
{
    namespace Reflection
    {
        template<typename C, typename... Tn>
        class NConstructorBuilder
        {
            using Type = C ( Tn... );
            using ReturnType = typename ntraits::function_traits<Type>::return_type;
            using ParameterTypes = typename ntraits::function_traits<Type>::parameter_types;
            static constexpr nsize ParameterCount = ParameterTypes::count;

            narray<nstring_view, ParameterCount> parameter_names;
            narray<any, ParameterCount> parameter_default_values;

        public:
            template<typename... Pn>
            void setParameterNames( const Pn... names )
            {
                NParameterHelper::setParameterNames( parameter_names, names... );
            }

            template<typename... Pn>
            void setParameterDefaultValues( const Pn&... default_values )
            {
                NParameterHelper::setParameterDefaultValues( parameter_default_values, default_values... );
            }

            NConstructorPtr build()
            { 
                narray<NParameterPtr, ParameterCount> parameters;
                NParameterHelper::packParameters<typename ParameterTypes>( parameters, array_view<nstring_view>( parameter_names ), array_view<any>( parameter_default_values ) );
                return NConstructorPtr( new NConstructorConcrete<C, Tn...>( std::move( parameters ) ) );
            }
        };
    }
}