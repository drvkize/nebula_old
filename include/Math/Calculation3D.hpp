#pragma once

#include "Math/MathDefine.hpp"
#include "Math/CalculationCommon.hpp"
#include "Math/Vector.hpp"

namespace Nebula
{
    namespace Math
    {
        // vector
        template<typename T, int D>
        inline T dot( const Vector<T, D>& v0, const Vector<T, D>& v1 )
        {
            T ret;
            NEBULA_UNROLLED_LOOP( i, D, ret += v0[i] * v1[i] );
            return ret;
        }

        template<typename T>
        inline T dot( const Vector<T, 2>& v0, const Vector<T, 2>& v1 )
        {
            return v0[0] * v1[0] + v0[1] * v1[1];
        }

        template<typename T>
        inline T dot( const Vector<T, 3>& v0, const Vector<T, 3>& v1 )
        {
            return v0[0] * v1[0] + v0[1] * v1[1] + v0[2] * v1[2];
        }

        template<typename T>
        inline T dot( const Vector<T, 4>& v0, const Vector<T, 4>& v1 )
        {
            return v0[0] * v1[0] + v0[1] * v1[1] + v0[2] * v1[2] + v0[3] * v1[3];
        }

        template<typename T>
        inline T cross( const Vector<T, 3>& v0, const Vector<T, 3>& v1 )
        {
            return Vector<T, 3>(
                v0.y * v1.z - v0.z * v1.y,
                v0.z * v1.x - v0.x * v1.z,
                v0.x * v1.y - v0.y * v1.x
            );
        }

        template<typename T, int D>
        inline T lengthSquared( const Vector<T, D>& v )
        {
            return dot( v, v );
        }

        template<typename T, int D>
        inline T length( const Vector<T, D>& v )
        {
            return sqrt( lengthSquared( v ) );
        }

        template<typename T, int D>
        inline T normalize( Vector<T, D>& v )
        {
            const T length = length( v );
            v *= ( T(1) / length );
            return length;
        }

        template<typename T, int D>
        inline T normalized( const Vector<T, D>& v )
        {
            return v * ( T( 1 ) / length( v ) );
        }

        template<typename T, int D>
        inline Vector<T, D> lerp( const Vector<T, D>& v0, const Vector<T, D>& v1, const T ratio )
        {
            const T one_minus_ratio = static_cast<T>( 1.0 ) - ratio;
            NEBULA_UNROLLED_LOOP( i, D, one_minus_ratio * v0[i] + ratio * v1[i] );
        }

        template<typename T, int D>
        inline Vector<T, D> min( const Vector<T, D>& v0, const Vector<T, D>& v1 )
        {
            Vector<T, D> ret;
            NEBULA_UNROLLED_LOOP( i, D, ret[i] = min( v0[i], v1[i] ) );
            return ret;
        }

        template<typename T, int D>
        inline Vector<T, D> max( const Vector<T, D>& v0, const Vector<T, D>& v1 )
        {
            Vector<T, D> ret;
            NEBULA_UNROLLED_LOOP( i, D, ret[i] = max( v0[i], v1[i] ) );
            return ret;
        }

        template<typename T, int D>
        inline T angle( const Vector<T, D>& v0, const Vector<T, D>& v1 )
        {
            const T divisor = length( v0 ) * length( v1 );
            if( divisor == T( 0 ) )
            {
                return T( 0 );
            }

            T cos = dot( v0, v1 ) / divisor;

            // floating error result acos return NAN
            return cos <= T( 1 ) ? acos( cos ) : ntraits::numeric<T>::invalid;
        }

        template<typename T, int D>
        inline nbool inRange2D( const Vector<T, D>& v, const Vector<T, D>& lhs, const Vector<T, D>& rhs )
        {
            return inRange( v[0], lhs[0], rhs[0] ) && inRange( v[1], lhs[1], rhs[1] );
        }

        template<typename T, int D>
        inline T distance( const Vector<T, D>& v0, const Vector<T, D>& v1 )
        {
            return ( v1 - v0 ).length();
        }

        template<typename T, int D>
        inline T distanceSquared( const Vector<T, D>& v0, const Vector<T, D>& v1 )
        {
            return ( v1 - v0 ).lengthSquared();
        }

        template<typename T, typename U, int D>
        inline Vector<T, D> convertFrom( const U& other )
        {
            union Conversion
            {
                Conversion() {}
                U other;
                VectorPacked<T, D> packed;
            }u;

            static_assert( sizeof( u.other ) == D * sizeof( T ), "[convertFrom()]: size mismatch" );

            u.other = other;
            return Vector<T, D>( u.packed );
        }

        template<typename T, typename U, int D>
        inline U convertTo( const Vector<T, D>& v )
        {
            union Conversion
            {
                Conversion() {}
                U other;
                VectorPacked<T, D> packed;
            }u;

            static_assert( sizeof( u.other ) == D * sizeof( T ), "[convertTo()]: size mismatch" );
            v.pack( &u.packed );
            return u.other;
        }
    }
}

#include "Vector2.hpp"
#include "Vector3.hpp"
#include "Vector4.hpp"