# Project
project( NebulaD3D11 )

# Preprocess
set( PLUGIN_BUILD_ARCHITECTURE "x86_64" )

if( ${CMAKE_BUILD_TYPE} STREQUAL "Debug" )
    set( PLUGIN_BUILD_TYPE "debug" )
    set( PLUGIN_BUILD_DEBUG 1 )
elseif( ${CMAKE_BUILD_TYPE} STREQUAL "Release" )
    set( PLUGIN_BUILD_TYPE "release" )
    set( PLUGIN_BUILD_DEBUG 0 )
endif()

set( PLUGIN_TARGET_NAME "nebula_d3d11" )
set( PLUGIN_VERSION_MAJOR 0 )
set( PLUGIN_VERSION_MINOR 1 )
set( PLUGIN_VERSION_PATCH 0 )
set( PLUGIN_INCLUDE_DIR "${NEBULA_PLUGIN_DIR}/NebulaD3D11/include" )
set( PLUGIN_SRC_DIR "${NEBULA_PLUGIN_DIR}/NebulaD3D11/source" )

configure_file(
    "${PLUGIN_INCLUDE_DIR}/inline/Config.hpp.in"
    "${PLUGIN_INCLUDE_DIR}/inline/Config.hpp"
)

add_compile_definitions( NEBULA_D3D11_BUILD_LIBRARY )

# Target
link_directories(
    ${NEBULA_LIB_DIR}
    ${NEBULA_EXTERN_LIB_DIR}
)

add_library(
    "${PLUGIN_TARGET_NAME}" SHARED
    "${PLUGIN_INCLUDE_DIR}/NebulaD3D11Define.hpp"
    "${PLUGIN_INCLUDE_DIR}/NebulaD3D11.hpp"
    "${PLUGIN_SRC_DIR}/NebulaD3D11.cpp"
)

target_include_directories(
    "${PLUGIN_TARGET_NAME}" PRIVATE
    "${PLUGIN_INCLUDE_DIR}"
    "${NEBULA_INCLUDE_DIR}"
    "${NEBULA_EXTERN_DIR}/include"
)

target_link_libraries(
    ${PLUGIN_TARGET_NAME} PRIVATE
    ${NEBULA_TARGET}
    "dxgi.lib"
    "d3d11.lib"
)

# Properties
set_target_properties(
    ${PLUGIN_TARGET_NAME} PROPERTIES
    CXX_STANDARD 11
    CXX_STANDARD_REQUIRED YES
    CXX_EXTENSIONS NO
)

if( MSVC )
    set_target_properties(
        ${PLUGIN_TARGET_NAME} PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY_DEBUG ${NEBULA_BINARY_DIR}
        RUNTIME_OUTPUT_DIRECTORY_RELEASE ${NEBULA_BINARY_DIR}
        LIBRARY_OUTPUT_DIRECTORY_DEBUG ${NEBULA_LIB_DIR}
        LIBRARY_OUTPUT_DIRECTORY_RELEASE ${NEBULA_LIB_DIR}
        ARCHIVE_OUTPUT_DIRECTORY_DEBUG ${NEBULA_LIB_DIR}
        ARCHIVE_OUTPUT_DIRECTORY_RELEASE ${NEBULA_LIB_DIR}
    )
endif()