#pragma once

#include "IO/IODefine.hpp"
#include "IO/StdConsoleSPI.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Windows
        {
            class NEBULA_INTERFACE StdConsoleImpl
            {
                class Context
                {
                public:
                    WORD current_attribute;
                    HANDLE output_handle;

                    Context();
                    ~Context();
                };

                static constexpr WORD ColorTable[] = {
                    0x0,        // black
                    0x000C,     // red
                    0x000A,     // green
                    0x0009,     // blue
                    0x000E,     // yellow
                    0x000D,     // magenta
                    0x000B,     // cyan
                    0x000F      // white
                };

                static constexpr WORD ForegroundColorMask = 0x000F;
                static constexpr WORD BackgroundColorMask = 0x00F0;

                static Context context;

            public:
                StdConsoleImpl();
                virtual ~StdConsoleImpl();

                StdConsoleImpl( const StdConsoleImpl& other ) = delete;
                StdConsoleImpl& operator=( const StdConsoleImpl& rhs ) = delete;

                static void setTitle( const nchar* title );
                static void setForegroundColor( ConsoleColor color );
                static void setBackgroundColor( ConsoleColor color );

                // bold and underline are not supported on windows
                static void setBold( nbool value ) {};
                static void setUnderline( nbool value ) {};

                static void write( const int8* data, nsize data_size );
                static void flush();
            };
        }

        using StdConsole = StdConsoleSPI<Windows::StdConsoleImpl>;
    }
}