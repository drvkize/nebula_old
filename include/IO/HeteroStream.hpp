#pragma once

#include "IO/IODefine.hpp"
#include "ByteSwap.hpp"
#include "Math/Vector.hpp"

namespace Nebula
{
    namespace IO
    {
        class NEBULA_INTERFACE HeteroStream
        {
        public:
            HeteroStream() {};
            virtual ~HeteroStream() {};

            HeteroStream( const HeteroStream& other ) = delete;
            HeteroStream& operator=( const HeteroStream& rhs ) = delete;

            template<typename T, typename std::enable_if<
                !std::is_trivially_copyable<T>::value>::type* = nullptr
            >
            nsize write( const T& data )
            {
                return T::serialize( *this, data );
            }

            // write scalar type T with size > 1
            template<typename T, typename std::enable_if<
                ( std::is_trivially_copyable<T>::value && sizeof( T ) > 1 )
                >::type* = nullptr
            >
            nsize write( const T& data )
            {
                const T swapped = toNetworkEndian( data );
                return write( reinterpret_cast<const int8*>( &swapped ), sizeof( T ) );
            }

            // write scalar type U with size == 1
            template<typename T, typename std::enable_if<
                ( std::is_trivially_copyable<T>::value && sizeof( T ) == 1 )
                >::type* = nullptr
            >
            nsize write( const T& data )
            {
                return write( reinterpret_cast<const int8*>( &data ), sizeof( T ) );
            }

            nsize write( HeteroStream& stream )
            {
                int64 offset = stream.tell();

                nsize written_size = 0;
                int8 write_batch[128];
                while( !stream.isEnd() )
                {
                    nsize read_size = stream.read( write_batch, 128 );
                    write( write_batch, read_size );
                    written_size += read_size;
                }

                stream.seek( SeekOption::SO_Set, offset );
                return written_size;
            }

            template<typename T, int D>
            nsize write( const Math::Vector<T, D>& value )
            {                
                nsize written_size = 0;
                NEBULA_UNROLLED_LOOP( i, D, written_size += write( value[i] ) );
                return written_size;
            }

            template<typename T, int R, int C>
            nsize write( const Math::Matrix<T, R, C>& value )
            {
                nsize written_size = 0;
                NEBULA_UNROLLED_LOOP( i, C, written_size += write( value[i] ) );
                return written_size;
            }

            template<typename T, typename std::enable_if<
                !std::is_trivially_copyable<T>::value>::type* = nullptr
            >
            nsize read( T& value )
            {
                return T::deserialize( *this, value );
            }

            // write scalar type T with size > 1
            template<typename T, typename std::enable_if<
                ( std::is_trivially_copyable<T>::value && sizeof( T ) > 1 )
                >::type* = nullptr
            >
            nsize read( T& value )
            {
                T to_swap;
                nsize read_size = read( reinterpret_cast<int8*>( &to_swap ), sizeof( T ) );
                value = toLocalEndian( to_swap );
                return read_size;
            }

            // write scalar type U with size == 1
            template<typename T, typename std::enable_if<
                ( std::is_trivially_copyable<T>::value && sizeof( T ) == 1 )
                >::type* = nullptr
            >
            nsize read( T& value )
            {
                nsize read_size = read( reinterpret_cast<int8*>( &value ), sizeof( T ) );
                return read_size;
            }

            nsize read( HeteroStream& stream )
            {
                int64 offset = stream.tell();

                nsize read_size = 0;
                int8 read_batch[128];
                while( !isEnd() )
                {
                    nsize read_batch_size = read( read_batch, 128 );
                    nsize write_size = stream.write( read_batch, read_batch_size );
                    read_size += write_size;
                }

                stream.seek( SeekOption::SO_Set, offset );
                return read_size;
            }

            template<typename T, int D>
            nsize read( Math::Vector<T, D>& value )
            {
                nsize written_size = 0;
                NEBULA_UNROLLED_LOOP( i, D, written_size += read( value[i] ) );
                return written_size;
            }

            template<typename T, int R, int C>
            nsize read( Math::Matrix<T, R, C>& value )
            {
                nsize written_size = 0;
                NEBULA_UNROLLED_LOOP( i, C, written_size += read( value[i] ) );
                return written_size;
            }

            virtual nsize write( const int8* data, nsize data_size ) = 0;
            virtual nsize read( int8* buffer, nsize buffer_size ) = 0;
            virtual nbool seek( SeekOption option, int64 offset ) = 0;
            virtual int64 tell() = 0;
            virtual nbool isEnd() const = 0;
        };
    }
}