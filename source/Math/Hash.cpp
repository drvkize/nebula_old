#include "Math/Hash.hpp"

namespace Nebula
{
	namespace Math
	{
        namespace Hash
        {
            uint32 DJB( const uint8* data, nsize size )
            {
                uint32 hash = 5381;
                while( size-- )
                {
                    hash = 33 * hash ^ *data++;
                }

                return hash;
            }

            uint32 DJB( const nchar* s )
            {
                uint32 hash = 5381;
                while( *s )
                {
                    hash = 33 * hash ^ *s++;
                }

                return hash;
            }

            uint32 FNV1A( const uint8* data, nsize size )
            {
                uint32 hash = 0x811c9dc5;
                while( size-- )
                {
                    hash ^= *data++;
                    hash *= 0x01000193;
                }

                return hash;
            }

            uint32 FNV1A( const nchar* s )
            {
                uint32 hash = 0x811c9dc5;
                while( *s )
                {
                    hash ^= *s++;
                    hash *= 0x01000193;
                }

                return hash;
            }

            uint32 Murmur2( const uint8* data, nsize size )
            {
                static const uint32 m = 0x5bd1e995ull;
                static const int r = 24;

                uint32 h = 0xc58f1a89 ^ static_cast<uint32>( size );

                while( size >= 4 )
                {
                    uint32 k = *(const uint32*)(const void*)data;

                    k *= m;
                    k ^= k >> r;
                    k *= m;

                    h *= m;
                    h ^= k;

                    data += 4;
                    size -= 4;
                }

                switch( size )
                {
                    case 3: h ^= data[2] << 16;
                    case 2: h ^= data[1] << 8;
                    case 1: h ^= data[0];
                        h *= m;
                };

                h ^= h >> 13;
                h *= m;
                h ^= h >> 15;

                return h;
            }

            uint32 Murmur2( const nchar* line )
            {
                return Murmur2( reinterpret_cast<const uint8*>( line ), std::strlen( line ) );
            }

            void combine( uint32& hash, uint32 append )
            {
                static const uint32 m = 0x5bd1e995;

                hash *= m;
                hash ^= append;

                hash ^= hash >> 13;
                hash *= m;
                hash ^= hash >> 15;
            }
        }
	}
}