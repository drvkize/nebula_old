#include "IO/Serialization/NTypeReader.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Serialization
        {
            NTypeReader NTypeReader::Null;

            nsize NTypeReader::staticSizer( const NTypeReader* reader )
            {
                static constexpr nsize _stub_size_table[] = {
                    0,
                    sizeof( NTypeTag ),
                    sizeof( NTypeTag ) + sizeof( nbool ),
                    sizeof( NTypeTag ) + sizeof( int8 ),
                    sizeof( NTypeTag ) + sizeof( int16 ),
                    sizeof( NTypeTag ) + sizeof( int32 ),
                    sizeof( NTypeTag ) + sizeof( int64 ),
                    sizeof( NTypeTag ) + sizeof( uint8 ),
                    sizeof( NTypeTag ) + sizeof( uint16 ),
                    sizeof( NTypeTag ) + sizeof( uint32 ),
                    sizeof( NTypeTag ) + sizeof( uint64 ),
                    sizeof( NTypeTag ) + sizeof( nfloat ),
                    sizeof( NTypeTag ) + sizeof( ndouble ),
                    0,
                    0,
                    0
                };

                return _stub_size_table[reader->getTag()];
            }

            nsize NTypeReader::dynamicSizer( const NTypeReader* reader )
            {
                return toLocalEndian(
                    *reinterpret_cast<const uint64*>(
                        reinterpret_cast<const int8*>( reader ) + sizeof( NTypeTag )
                        )
                    );
            }

            const NTypeReader& NTypeReader::create( const int8* buffer )
            {
                return *reinterpret_cast<const NTypeReader*>( buffer );
            }

            nsize NTypeReader::getSize() const
            {
                return isDynamicType( getTag() ) ? staticSizer( this ) : dynamicSizer( this );
            }

            const NTypeReader& NObjectReader::getMember( const nchar* name ) const
            {
                const int8* data = reinterpret_cast<const int8*>( this + 1 );
                for( nsize i = 0; i < count; ++i )
                {
                    const uint64 name_size = toLocalEndian( *reinterpret_cast<const uint64*>( data ) );
                    data += sizeof( uint64 );

                    const nchar* member_name = reinterpret_cast<const nchar*>( data );
                    data += name_size;
                    
                    if( strcmp( name, member_name ) == 0 )
                    {
                        return NTypeReader::create( data );
                    }
                    
                    nsize member_size = NTypeReader::create( data ).getSize();
                    data += member_size;
                }

                return NTypeReader::Null;
            }
        }
    }
}