#pragma once

#include "Nebula.hpp"
#include "Reflection/Reflection.hpp"
using Nebula::Reflection::NFunctionBuilder;
using Nebula::Reflection::NParameter;
using Nebula::Reflection::NParameterPtr;
using Nebula::Reflection::NFunction;
using Nebula::Reflection::NFunctionPtr;

float foo( float a, const nstring_view line )
{
    printf( "line = %s\n", line.c_str() );
    return a * 2;
}

void foo2()
{
    printf( "foo2\n" );
}

int test( int argc, char **argv )
{
    NFunctionBuilder<decltype( foo )> builder( foo, "foo" );
    builder.setParameterNames( "a", "line" );
    builder.setParameterDefaultValues( 1, "hello" );
    NFunctionPtr refl_foo = builder.build();

    array_view<NParameterPtr> parameters = refl_foo->getParameters();
    printf( "function: name = %s, return_type = %u, parameter_count = %llu, signature = %s\n",
        refl_foo->getName().c_str(),
        refl_foo->getReturnType().getID(),
        parameters.getCount(),
        refl_foo->getSignature().c_str()
    );

    for( nsize i = 0; i < parameters.getCount(); ++i )
    {
        printf( "parameter[%llu]: type = %u, index = %llu, name = %s, signature = %s\n",
            i,
            parameters[i]->getType().getID(),
            parameters[i]->getIndex(),
            parameters[i]->getName().c_str(),
            parameters[i]->getSignature().c_str()
        );
    }

    printf( "%f\n", any_cast<float>( refl_foo->invoke( 23.0f, nstring_view( "check" ) ) ) );

    NFunctionBuilder<decltype( foo2 )> builder2( foo2, "foo2" );
    NFunctionPtr refl_foo2 = builder2.build();
    refl_foo2->invoke();

    getchar();
    return 0;
}