#include "Memory/FixedSizeChunkAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        nsize FixedSizeChunkAllocator::findFreeChunk()
        {
            for( nsize i = 0; i < chunk_count; ++i )
            {
                if( !chunks[i].isFull() )
                {
                    return i;
                }
            }

            return chunk_count;
        }

        int FixedSizeChunkAllocator::findChunkByAddress( const void* p )
        {
            const int8* ptr = reinterpret_cast<const int8*>( p );
            int left = 0;
            int right = static_cast<int>( chunk_count ) - 1;

            while( left <= right )
            {
                int middle = ( left + right ) >> 1;

                if( ptr < chunks[middle].getDataAddress() )
                {
                    right = middle - 1;
                }
                else if( ptr > chunks[middle].getDataAddress() )
                {
                    left = middle + 1;
                }
                else
                {
                    return middle;
                }
            }

            return left - 1;
        }

        FixedSizeChunk* FixedSizeChunkAllocator::createChunk()
        {
			if( chunk_count == chunk_capacity )
			{
				chunk_capacity = chunk_capacity > 0 ? chunk_capacity + chunk_capacity / 2 : 2;
            
				// reallocate chunk array
				FixedSizeChunk* p = static_cast<FixedSizeChunk*>( parent_allocator->allocate( chunk_capacity * sizeof( FixedSizeChunk ), 16 ) );
				assert( p && "[FixedSizeChunkAllocator::createChunk()]: allocate chunk array failed" );
				if( p != nullptr )
				{
					if( chunks != nullptr )
					{
						memcpy( p, chunks, chunk_count * sizeof( FixedSizeChunk ) );
						parent_allocator->deallocate( chunks );
					}

					chunks = p;

					last_allocated_chunk = nullptr;
					last_deallocated_chunk = nullptr;
				}
			}

            // insert new chunk
            FixedSizeChunk new_chunk;
            if( new_chunk.init( parent_allocator, block_size, 256, 16 ) )
            {
                int insert_position = findChunkByAddress( new_chunk.getDataAddress() ) + 1;
                for( int i = static_cast<int>( chunk_count ) - 1; i >= insert_position; --i )
                {
                    chunks[i] = std::move( chunks[i-1] );
                }
                
                // chunks[insert_position] = std::move( *( new( &chunks[insert_position] ) FixedSizeChunk() ) );
                chunks[insert_position] = std::move( new_chunk );
                ++chunk_count;

                return &chunks[insert_position];
            }

            return nullptr;
        }

        void FixedSizeChunkAllocator::destroyChunk( nsize index )
        {
			if( index >= 0 && index < chunk_count )
			{
				chunks[index].release();
				for( nsize i = index + 1; i < chunk_count; ++i )
				{
					chunks[i - 1] = std::move( chunks[i] );
				}

				--chunk_count;
			}

            last_allocated_chunk = nullptr;
            last_deallocated_chunk = nullptr;
        }

        FixedSizeChunkAllocator::FixedSizeChunkAllocator()
            : parent_allocator( nullptr )
            , block_size( 0 )
            , chunk_count( 0 )
            , chunk_capacity( 0 )
            , chunks( nullptr )
            , last_allocated_chunk( nullptr )
            , last_deallocated_chunk( nullptr )
        {
        }

        FixedSizeChunkAllocator::~FixedSizeChunkAllocator()
        {
        }

        nbool FixedSizeChunkAllocator::init( NebulaAllocator* parent_allocator, nsize block_size )
        {
            this->parent_allocator = parent_allocator;
            this->block_size = block_size;

            createChunk();

            return true;
        }

        void FixedSizeChunkAllocator::release()
        {
            // clean up
            if( parent_allocator != nullptr && chunk_count > 0 )
            {
                for( nsize i = 0; i < chunk_count; ++i )
                {
                    chunks[i].release();
                }

                parent_allocator->deallocate( chunks );

				parent_allocator = nullptr;
				block_size = 0;
				chunk_count = 0;
				chunk_capacity = 0;
				chunks = nullptr;
				last_allocated_chunk = nullptr;
				last_deallocated_chunk = nullptr;
            }
        }

        void* FixedSizeChunkAllocator::allocate( nsize size, nsize align )
        {
            // try last allocated chunk
            if( last_allocated_chunk != nullptr )
            {
                void* ret = last_allocated_chunk->allocate();
                if( ret != nullptr )
                {
                    return ret;
                }
            }

            // try to find an available chunk
            nsize chunk_index = findFreeChunk();
            if( chunk_index < chunk_count )
            {
                FixedSizeChunk* chunk = &chunks[chunk_index];
                if( chunk != nullptr )
                {
                    last_allocated_chunk = chunk;
                    return last_allocated_chunk->allocate();
                }
            }

            // try to create a new chunk
            FixedSizeChunk* new_chunk = createChunk();
            if( new_chunk != nullptr )
            {
                last_allocated_chunk = new_chunk;
                return last_allocated_chunk->allocate();
            }

            return nullptr;
        }

        nbool FixedSizeChunkAllocator::deallocate( void* p )
        {
            if( p != nullptr )
            {
                // try last deallocated chunk
                if( last_deallocated_chunk != nullptr && last_deallocated_chunk->isAllocated( p ) )
                {
                    last_deallocated_chunk->deallocate( p );
                }
                // try last allocated chunk
                else if( last_allocated_chunk != nullptr && last_allocated_chunk->isAllocated( p ) )
                {
                    last_deallocated_chunk = last_allocated_chunk;
                    last_deallocated_chunk->deallocate( p );
                }
                // try to find by address
                else
                {
                    int chunk_index = findChunkByAddress( p );
                    if( chunk_index >= 0 && chunks[chunk_index].isAllocated( p ) )
                    {
                        last_deallocated_chunk = &chunks[chunk_index];
                        last_deallocated_chunk->deallocate( p );
                    }
                    else
                    {
                        return false;
                    }
                }

                if( last_deallocated_chunk->isEmpty() && chunk_count > 1 )
                {
                    destroyChunk( last_deallocated_chunk - chunks );
                }

                return true;
            }

            return false;
        }
    }
}