#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;

namespace Nebula
{
    namespace Math
    {
        static constexpr nfloat PI = 3.14159265358979323846264338327950288419716939937510f;
        static constexpr nfloat PI2 = PI * 2.0f;
        static constexpr nfloat PI05 = PI * 0.5f;

        union IntFloat
        {
            IntFloat() {}
            IntFloat( int32 i ) : i( i ) {}
            IntFloat( nfloat f ) : f( f ) {}
            IntFloat( uint32 ui ) : ui( ui ) {}

            int32 i;
            nfloat f;
            uint32 ui;
        };

        union IntDouble
        {
            IntDouble() {}
            IntDouble( ndouble d ) {}
            
            int32 i[2];
            ndouble d;
            uint32 ui[2];
        };

        template<typename T, int D>
        class Vector;

        template<typename T, int D>
        class VectorPacked;

        template<typename T, int R, int C>
        class Matrix;

        #define NEBULA_UNROLLED_LOOP( iter, count, op ) {                           \
            const int iter = 0;                                                     \
            { op; }                                                                 \
            if( ( count ) > 1 )                                                     \
            {                                                                       \
                const int iter = 1;                                                 \
                { op; }                                                             \
                if( ( count ) > 2 )                                                 \
                {                                                                   \
                    const int iter = 2;                                             \
                    { op; }                                                         \
                    if( ( count ) > 3 )                                             \
                    {                                                               \
                        const int iter = 3;                                         \
                        { op; }                                                     \
                        if( ( count ) > 4 )                                         \
                        {                                                           \
                            for( int iter = 4; iter < count; ++iter )               \
                            {                                                       \
                                op;                                                 \
                            }                                                       \
                        }                                                           \
                    }                                                               \
                }                                                                   \
            }                                                                       \
        }
    }
}