#include "Log/LogMessage.hpp"

namespace Nebula
{
    namespace Log
    {
        LogMessage::LogMessage()
            : level( LogLevel::LL_Off )
        {
        }

        LogMessage::LogMessage( const nstring* logger_name, LogLevel level, const nstring& message )
            : logger_name( logger_name )
            , level( level )
        {
            std::time_t t = std::chrono::system_clock::to_time_t( std::chrono::system_clock::now() );
            time = *Platform::OS::localtime( &t );
            thread_id = Concurrent::OS::getThreadID();
            data.copy( reinterpret_cast<const int8*>( message.c_str() ), message.size() );
        }
    }
}