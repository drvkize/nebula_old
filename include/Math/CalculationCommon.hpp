#pragma once

#include "Math/MathDefine.hpp"
#include "Platform/Language/numeric.hpp"

namespace Nebula
{
    namespace Math
    {
        #ifdef min
        #undef min
        #endif

        template<typename T>
        static inline T min( const T& lhs, const T& rhs )
        {
            return lhs < rhs ? lhs : rhs;
        }

        #ifdef max
        #undef max
        #endif

        template<typename T>
        static inline T max( const T& lhs, const T& rhs )
        {
            return lhs > rhs ? lhs : rhs;
        }

        template<typename T>
        static inline T clamp( const T& x, const T& min, const T& max )
        {
            return ( x < min ) ? min : ( ( x < max ) ? x : max );
        }

        template<typename T>
        static inline T abs( const T& x )
        {
            return x < 0 ? -x : x;
        }

        static inline nfloat ceil( nfloat x )
        {
            return ::ceilf( x );
        }

        static inline nfloat floor( nfloat x )
        {
            return ::floorf( x );
        }

        static inline ndouble ceil( ndouble x )
        {
            return ::ceil( x );
        }

        static inline ndouble floor( ndouble x )
        {
            return ::floor( x );
        }

        static inline nfloat sqrt( nfloat x )
        {
            return ::sqrtf( x );
        }

        static inline ndouble sqrt( ndouble x )
        {
            return ::sqrt( x );
        }

        static inline nfloat sqrtFast( nfloat x )
        {
// #if defined( NEBULA_WINDOWS_DESKTOP ) && defined( NEBULA_BUILD_ARCH_IX86 )
// 			__m128 s = _mm_sqrt_ss( _mm_set_ss( x ) );
// 			nfloat r;
// 			_mm_store_ss( &r, s );
// 			return r;
// #else
            IntFloat i = x;
            i.i = 0x5f3759df - ( i.i >> 1 );
			x = i.f * x;
			return x * ( 1.5f - ( i.f * x * 0.5f ) );
// #endif
        }

        static inline nfloat rcp( nfloat x )
        {
            return 1.0f / x;
        }

        static inline ndouble rcp( ndouble x )
        {
            return 1.0 / x;
        }

        static inline nfloat rcpFast( nfloat x )
        {
// #if defined( NEBULA_WINDOWS_DESKTOP ) && defined( NEBULA_BUILD_ARCH_IX86 )
// 			__m128 s = _mm_rcp_ss( _mm_set_ss( x ) );
// 			nfloat r;
// 			_mm_store_ss( &r, s );
// 			return r;
// #else
            IntFloat i = x;
			i.i = 0x7f000000 - i.i;
			return i.f * ( 2.0f - x * i.f );
// #endif
        }

        static inline nfloat rcpSqrtSafe( nfloat x )
        {
            return 1.0f / sqrtf( x + ntraits::numeric<nfloat>::min );
        }

        static inline ndouble rcpSqrtSafe( ndouble x )
        {
            return 1.0 / sqrt( x + ntraits::numeric<ndouble>::min );
        }

        static inline uint32 log2Fast( uint32 v )
        {
            static const uint32 b[] = { 0xAAAAAAAA, 0xCCCCCCCC, 0xF0F0F0F0, 0xFF00FF00, 0xFFFF0000 };
			register uint32 r = ( v & b[0] ) != 0;
			for( uint32 i = 4; i > 0; i-- )
			{
				r |= ( ( v & b[i] ) != 0 ) << i;
			}

			return r;
        }

        static inline uint64 log2Fast( uint64 v )
		{
			static const uint64 b[] = { 0xAAAAAAAA, 0xCCCCCCCC, 0xF0F0F0F0, 0xFF00FF00, 0xFFFF0000, 0xFFFFFFFF00000000 };
			register uint64 r = ( v & b[0] ) != 0;
			for( uint64 i = 5; i > 0; i-- )
			{
				r |= ( ( v & b[i] ) != 0 ) << i;
			}

			return r;
		}

		static inline uint32 log2( uint32 v )
		{
			const uint32 b[] = { 0x2, 0xc, 0xf0, 0xff00, 0xffff0000 };
			const uint32 s[] = { 1, 2, 4, 8, 16 };
			register uint32 r = 0;
			for( int i = 4; i >= 0; i-- )
			{
				if( v & b[i] )
				{
					v >>= s[i];
					r |= s[i];
				}
			}

			return r;
		}

		static inline uint64 log2( uint64 v )
		{
			const uint64 b[] = { 0x2, 0xc, 0xf0, 0xff00, 0xffff0000, 0xffffffff00000000 };
			const uint64 s[] = { 1, 2, 4, 8, 16, 32 };
			register uint64 r = 0;
			for( int i = 5; i >= 0; i-- )
			{
				if( v & b[i] )
				{
					v >>= s[i];
					r |= s[i];
				}
			}

			return r;
		}

        static inline nfloat mod( nfloat x, nfloat y )
		{
			return ::fmodf( x, y );
		}

		static inline ndouble mod( ndouble x, ndouble y )
		{
			return ::fmod( x, y );
		}

		static inline uint64 pow( uint64 base, uint64 exp )
		{
			uint64 ret = 1;
			while( exp )
			{
				if( exp & 1 )
				{
					ret *= base;
				}

				exp >>= 1;
				base *= base;
			}

			return ret;
		}

		static inline nfloat pow( nfloat x, nfloat y )
		{
			return ::powf( x, y );
		}

		static inline ndouble pow( ndouble x, ndouble y )
		{
			return ::pow( x, y );
		}

		static inline nfloat exp( nfloat x )
		{
			return ::expf( x );
		}

		static inline ndouble exp( ndouble x )
		{
			return ::exp( x );
		}

		static inline nfloat log( nfloat x )
		{
			return ::logf( x );
		}

		static inline ndouble log( ndouble x )
		{
			return ::log( x );
		}

		static inline nfloat rad2Deg( nfloat x )
		{
			return x * PI / 180.0f;
		}

		static inline ndouble rad2Deg( ndouble x )
		{
			return x * PI / 180.0f;
		}

		static inline nfloat deg2Rad( nfloat x )
		{
			return x * 180.0f / PI;
		}

		static inline ndouble deg2Rad( ndouble x )
		{
			return x * 180.0 / PI;
		}

		static inline nfloat sin( nfloat x )
		{
			return ::sinf( x );
		}

		static inline ndouble sin( ndouble x )
		{
			return ::sin( x );
		}

		static inline nfloat cos( nfloat x )
		{
			return ::cosf( x );
		}

		static inline ndouble cos( ndouble x )
		{
			return ::cos( x );
		}

		static inline nfloat tan( nfloat x )
		{
			return ::tanf( x );
		}

		static inline ndouble tan( ndouble x )
		{
			return ::tan( x );
		}

		static inline nfloat asin( nfloat x )
		{
			return ::asinf( x );
		}

		static inline ndouble asin( ndouble x )
		{
			return ::asin( x );
		}

		static inline nfloat acos( nfloat x )
		{
			return ::acosf( x );
		}

		static inline ndouble acos( ndouble x )
		{
			return ::acos( x );
		}

		static inline nfloat atan( nfloat x )
		{
			return ::atanf( x );
		}

		static inline ndouble atan( ndouble x )
		{
			return ::atan( x );
		}

		static inline nfloat atan2( nfloat y, nfloat x )
		{
			return ::atan2f( y, x );
		}

		static inline ndouble atan2( ndouble y, ndouble x )
		{
			return ::atan2( y, x );
		}

		static inline void sincos( nfloat x, nfloat &s, nfloat&c )
		{
// #if defined( NEBULA_WINDOWS_DESKTOP ) && defined( NEBULA_BUILD_ARCH_IX86 )
// 			_asm
// 			{
// 				fld x
// 				fsincos
// 				mov edx, s
// 				mov ecx, c
// 				fstp dword ptr [ecx]
// 				fstp dword ptr [edx]
// 			}
// #else
			s = ::sinf( x );
			c = ::cosf( x );
// #endif
		}

		static inline void sincos( ndouble x, ndouble& s, ndouble& c )
		{
// #if defined( NEBULA_WINDOWS_DESKTOP ) && defined( NEBULA_BUILD_ARCH_IX86 )
// 			__asm
// 			{
// 				fld x
// 				fsincos
// 				mov edx, s
// 				mov ecx, c
// 				fstp qword ptr[ecx]
// 				fstp qword ptr[edx]
// 			}
// #else
			s = ::sin( x );
			c = ::cos( x );
// #endif
		}

		static inline int round( nfloat x )
		{
// #if defined( NEBULA_WINDOWS_DESKTOP ) && defined( NEBULA_BUILD_ARCH_IX86 )
// 			int i;
// 			__asm
// 			{
// 				fld x
// 				fistp i
// 			}
// 			return i;
// #else
			return static_cast<int>( x + 0.5f );
// #endif
		}

		static inline int round( ndouble x )
		{
			return static_cast<int>( x + 0.5 );
		}

		static inline nfloat lerp( nfloat p, nfloat q, nfloat k )
		{
			return p + ( q - p ) * k;
		}

		static inline ndouble lerp( ndouble p, ndouble q, ndouble k )
		{
			return p + ( q - p ) * k;
		}

		static inline nfloat saturate( nfloat x )
		{
			return x < 0.0f ? 0.0f : x < 1.0f ? x : 1.0f;
		}

		static inline ndouble saturate( ndouble x )
		{
			return x < 0.0 ? 0.0 : x < 1.0 ? x : 1.0;
		}

		template<typename T>
		static inline nbool inRange( const T& v, const T& lhs, const T& rhs )
		{
			return v >= lhs && v < rhs;
		}
    }
}