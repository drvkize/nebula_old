#pragma once

#include "IO/IODefine.hpp"
#include "IO/StdConsoleSPI.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Linux
        {
            class NEBULA_INTERFACE StdConsoleImpl
            {
            public:
                StdConsoleImpl();
                virtual ~StdConsoleImpl();

                StdConsoleImpl( const StdConsoleImpl& other ) = delete;
                StdConsoleImpl& operator=( const StdConsoleImpl& rhs ) = delete;

                static void setTitle( const nchar* title );
                static void setForegroundColor( ConsoleColor color );
                static void setBackgroundColor( ConsoleColor color );

                static void setBold( nbool value );
                static void setUnderline( nbool value );

                static void write( const int8* data, nsize data_size );
                static void flush();
            };
        }

        using StdConsole = StdConsoleSPI<Linux::StdConsoleImpl>;
    }
}