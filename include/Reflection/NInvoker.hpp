#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Platform/Language/function_traits.hpp"
#include "Platform/Language/index_sequence.hpp"
#include "Utility/any.hpp"

namespace Nebula
{
    namespace Reflection
    {
        struct InvokeFunction {};
        struct InvokeConstructor {};
        struct InvokeMethod {};

        template<typename F, typename Tag, typename Index>
        struct NInvoker {};

        template<typename F, nsize... I>
        struct NInvoker<F, InvokeFunction, ntraits::index_sequence<I...>>
        {
            using ReturnType = typename ntraits::function_traits<F>::return_type; 
            using ParameterTypes = typename ntraits::function_traits<F>::parameter_types;
            static constexpr nsize ParameterCount = ParameterTypes::count;

            template<typename... Tn, typename std::enable_if<( std::is_void<ReturnType>::value == false && sizeof...( Tn ) == ParameterCount )>::type* = nullptr>
            static inline any invoke( F* func, Tn&&... args )
            {
                return any( func( any_cast<ParameterTypes::type<I>>( args )... ) );
            }

            // specialization for func return void
            template<typename... Tn, typename std::enable_if<( std::is_void<ReturnType>::value == true && sizeof...( Tn ) == ParameterCount )>::type* = nullptr>
            static inline any invoke( F* func, Tn&&... args )
            {
                func( any_cast<ParameterTypes::type<I>>( args )... );
                return any();
            }

            // specialization for invalid parameter count, fallback
            template<typename... Tn, typename std::enable_if<(sizeof...( Tn ) != ParameterCount )>::type* = nullptr>
            static inline any invoke( F* func, Tn&&... args )
            {
                nassert( 0, "[NInovker::invoke()]: parameter count mismatch" );
                return any();
            }
        };

        template<typename F, nsize... I>
        struct NInvoker<F, InvokeConstructor, ntraits::index_sequence<I...>>
        {
            using ReturnType = typename ntraits::function_traits<F>::return_type; 
            using ParameterTypes = typename ntraits::function_traits<F>::parameter_types;
            static constexpr nsize ParameterCount = ParameterTypes::count;

            template<typename... Tn, typename std::enable_if<( std::is_void<ReturnType>::value == false && sizeof...( Tn ) == ParameterCount )>::type* = nullptr>
            static inline any invoke( Tn&&... args )
            {
                return any( ReturnType( any_cast<ParameterTypes::type<I>>( args )... ) );
            }

            // specialization for invalid parameter count, fallback
            template<typename... Tn, typename std::enable_if<( std::is_void<ReturnType>::value == true || sizeof...( Tn ) != ParameterCount )>::type* = nullptr>
            static inline any invoke( Tn&&... args )
            {
                nassert( 0, "[NInovker::invoke()]: parameter count mismatch" );
                return any();
            }
        };

        template<typename F, nsize... I>
        struct NInvoker<F, InvokeMethod, ntraits::index_sequence<I...>>
        {
            using ReturnType = typename ntraits::function_traits<F>::return_type; 
            using ClassType = typename ntraits::function_traits<F>::class_type;
            using ParameterTypes = typename ntraits::function_traits<F>::parameter_types;
            static constexpr nsize ParameterCount = ParameterTypes::count;

            template<typename... Tn, typename std::enable_if<( std::is_void<ReturnType>::value == false && sizeof...( Tn ) == ParameterCount )>::type* = nullptr>
            static inline any invoke( ClassType* obj, F func, Tn&&... args )
            {
                return any( ( obj->*func )( any_cast<ParameterTypes::type<I>>( args )... ) );
            }

            // specialization for func return void
            template<typename... Tn, typename std::enable_if<( std::is_void<ReturnType>::value == true && sizeof...( Tn ) == ParameterCount )>::type* = nullptr>
            static inline any invoke( ClassType* obj, F func, Tn&&... args )
            {
                ( obj->*func )( any_cast<ParameterTypes::type<I>>( args )... );
                return any();
            }

            // specialization for invalid parameter count, fallback
            template<typename... Tn, typename std::enable_if<(sizeof...( Tn ) != ParameterCount )>::type* = nullptr>
            static inline any invoke( ClassType* obj, F func, Tn&&... args )
            {
                nassert( 0, "[NInovker::invoke()]: parameter count mismatch" );
                return any();
            }
        };
    }
}