#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;

namespace Nebula
{
    namespace Platform
    {
        template<typename T, nsize N>
        struct narray
        {
            T _data[N];

            template<typename... Tn, typename std::enable_if<ntraits::conjunction<std::is_same<T, Tn>...>::value>::type* = nullptr>
            constexpr narray( Tn... data )
                : _data{ data... }
            {
            }

            narray( const narray& other )
            {
                for( nsize i = 0; i < N; ++i )
                {
                    _data[i] = other._data[i];
                }
            }

            narray( narray&& other ) noexcept
            {
                for( nsize i = 0; i < N; ++i )
                {
                    _data[i] = std::move( other._data[i] );
                }
            }

            T& operator[]( nsize index ) { return _data[index]; }
            constexpr const T& operator[]( nsize index ) const { return _data[index]; }

            T* data() { return _data; }
            constexpr const T* data() const { return _data; }

            nsize size() const { return N; }
        };

        // dummy narray with 0 element, suppress C2229
        template<typename T>
        struct narray<T, 0>
        {
            template<typename... Tn, typename std::enable_if<ntraits::conjunction<std::is_same<T, Tn>...>::value>::type* = nullptr>
            constexpr narray( Tn... data )
            {
            }

            T* data() { return nullptr; }
            constexpr const T* data() const { return nullptr; }

            nsize size() const { return 0; }
        };
    }
}