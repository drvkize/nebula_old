#pragma once

#include "IODefine.hpp"

namespace Nebula
{
    namespace IO
    {
        #define NEBULA_IS_BIG_ENDIAN (*(uint16 *)"\0\xff" < 0x100)

        namespace detail
        {
            template<typename Left, typename Right>
            struct TypePair
            {
                union
                {
                    const Left left;
                    const Right right;
                };

            public:
                constexpr TypePair( const Left& left )
                    : left( left )
                {                        
                }

                constexpr const Left& toLeft() { return left; }
                constexpr const Right& toRight() { return right; }
            };

            template<typename T, nsize size>
            struct ByteSwapper
            {
            public:
                static constexpr const T& get( const T& data )
                {
                    return data;
                }
            };

            template<typename T>
            struct ByteSwapper<T, 2>
            {
                static constexpr uint16 swap( const uint16& data )
                {
                    return ( data >> 8 ) | ( data << 8 );
                }

            public:
                static constexpr const T& get( const T& data )
                {
                    return TypePair<uint16, T>( swap( TypePair<T, uint16>( data ).toRight() ) ).toRight();
                }
            };

            template<typename T>
            struct ByteSwapper<T, 4>
            {
                static constexpr uint32 swap( const uint32& data )
                {
                    return ( ( data >> 24 ) & 0x000000ff )
                        | ( ( data >> 8 ) & 0x0000ff00 )
                        | ( ( data << 8 ) & 0x00ff0000 )
                        | ( ( data << 24 ) & 0xff000000 );
                }
            
            public:
                static constexpr const T& get( const T& data )
                {
                    return TypePair<uint32, T>( swap( TypePair<T, uint32>( data ).toRight() ) ).toRight();
                }
            };

            template<typename T>
            struct ByteSwapper<T, 8>
            {
                static constexpr uint64 swap( const uint64& data )
                {
                    return ( ( data >> 56 ) & 0x00000000000000ff )
                        | ( ( data >> 40 ) & 0x000000000000ff00 )
                        | ( ( data >> 24 ) & 0x0000000000ff0000 )
                        | ( ( data >> 8 ) & 0x00000000ff000000 )
                        | ( ( data << 8 ) & 0x000000ff00000000 )
                        | ( ( data << 24 ) & 0x0000ff0000000000 )
                        | ( ( data << 40 ) & 0x00ff000000000000 )
                        | ( ( data << 56 ) & 0xff00000000000000 );
                }

            public:
                static constexpr const T& get( const T& data )
                {
                    return TypePair<uint64, T>( swap( TypePair<T, uint64>( data ).toRight() ) ).toRight();
                }
            };

            template<typename T, nsize Size = sizeof( T ), nbool NeedSwap = ( Size > 1 ) && !std::is_class<T>::value>
            struct ByteSwapAdapter
            {
                static constexpr const T& get( const T& data ) { return ByteSwapper<T, Size>::get( data ); }
            };

            template<typename T>
            struct ByteSwapAdapter<T, 1, false>
            {
                static constexpr const T& get( const T& data ) { return data; }
            };
        }

        template<typename T>
        static constexpr const T& byteSwap( const T& data )
        {
            return detail::ByteSwapAdapter<T>::get( data );
        }

        template<typename T>
        static constexpr const T& toBigEndian( const T& data )
        {
            return NEBULA_IS_BIG_ENDIAN ? data : byteSwap( data );
        }

        template<typename T>
        static constexpr const T& toSmallEndian( const T& data )
        {
            return NEBULA_IS_BIG_ENDIAN ? byteSwap( data ) : data;
        }

        template<typename T>
        static constexpr const T& toNetworkEndian( const T& data )
        {
            return toBigEndian( data );
        }

        template<typename T>
        static constexpr const T& toLocalEndian( const T& data )
        {
            return NEBULA_IS_BIG_ENDIAN ? data : byteSwap( data );
        }
    }
}