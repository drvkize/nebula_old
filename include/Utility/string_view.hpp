#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;

namespace Nebula
{
    namespace Platform
    {
        template<typename Char>
        class nbasic_string_view
        {
        public:
            using char_type = Char;
            using iterator = char_type*;
        
        private:
            const char_type* data;
            nsize count;

        public:
            constexpr nbasic_string_view() noexcept
                : data( nullptr )
                , count( 0 )
            {
            }

            template<nsize N>
            constexpr nbasic_string_view( const Char ( &s )[N] ) noexcept
                : data( s )
                , count( N )
            {
            }

            constexpr nbasic_string_view( const Char* s, nsize count ) noexcept
                : data( s )
                , count( count )
            {
            }

            constexpr nbasic_string_view( const Char* s )
                : data( s )
                , count( ntraits::length_of( s ) )
            {
            }

            constexpr nbasic_string_view( const nbasic_string<Char>& s ) noexcept
                : data( s.c_str() )
                , count( s.count() )
            {
            }

            constexpr nbasic_string_view( const nbasic_string_view& other )
                : data( other.data )
                , count ( other.count )
            {
            }

            constexpr nbasic_string_view( nbasic_string_view&& other )
                : data( other.data )
                , count( other.count )
            {
            }

            nbasic_string_view& operator=( const nbasic_string_view& rhs )
            {    
                data = rhs.data;
                count = rhs.count;
                return *this;
            }

            nbasic_string_view& operator=( nbasic_string_view&& rhs ) noexcept
            {
                data = rhs.data;
                count = rhs.count;
                return *this;
            }

            constexpr nbool contains( const Char ch )
            {
                nsize cnt = count;
                const char_type* p = data;
                while( cnt-- )
                {
                    if( *p == ch )
                    {
                        return true;
                    }
                }

                return false;
            }

            constexpr const Char* getData() const { return data; }
            constexpr const Char* c_str() const { return data; }
            constexpr nsize getCount() const { return count; }
            constexpr iterator begin() const { return data; }
            constexpr iterator end() const { return data + count; }

            // for fmt library
            constexpr void remove_prefix( nsize n )
            {
                data += n;
                count -= n;
            }

            int compare( nbasic_string_view other ) const
            {
                nsize strcount = count < other.count ? count : other.count;
                int result = std::char_traits<Char>::compare( data, other.data, strcount );
                if( result == 0 )
                {
                    result = ( count == other.count ) ? 0 : ( count < other.count ? -1 : 1 );
                }

                return result;
            }

            constexpr nbool isNullOrEmpty() const { return data == nullptr || count == 0 || data[0] == '\0'; }

            friend nbool operator==( nbasic_string_view lhs, nbasic_string_view rhs )
            {
                return lhs.compare( rhs ) == 0;
            }

            friend nbool operator!=( nbasic_string_view lhs, nbasic_string_view rhs )
            {
                return lhs.compare( rhs ) != 0;
            }

            friend nbool operator<( nbasic_string_view lhs, nbasic_string_view rhs )
            {
                return lhs.compare( rhs ) < 0;
            }

            friend nbool operator>( nbasic_string_view lhs, nbasic_string_view rhs )
            {
                return lhs.compare( rhs ) > 0;
            }

            friend nbool operator<=( nbasic_string_view lhs, nbasic_string_view rhs )
            {
                return lhs.compare( rhs ) <= 0;
            }

            friend nbool operator>=( nbasic_string_view lhs, nbasic_string_view rhs )
            {
                return lhs.compare( rhs ) >= 0;
            }
        };

        using nstring_view = nbasic_string_view<nchar>;
        using nwstring_view = nbasic_string_view<nwchar>;
    }
}