#include "Memory/FixedSizeChunk.hpp"

namespace Nebula
{
    namespace Memory
    {
        FixedSizeChunk::FixedSizeChunk()
            : parent_allocator( nullptr )
            , address( nullptr )
            , data_address( nullptr )
            , block_size( 0 )
            , block_count( 0 )
            , next_free_block( 0 )
            , free_block_count( 0 )
        {
        }

        FixedSizeChunk::FixedSizeChunk( FixedSizeChunk&& other ) noexcept
        {
			move( std::move( other ) );
            // swap( *this, other );
        }

        FixedSizeChunk::~FixedSizeChunk()
        {
        }

		void FixedSizeChunk::move( FixedSizeChunk&& rhs ) noexcept
		{
			parent_allocator = rhs.parent_allocator;
			address = rhs.address;
			data_address = rhs.data_address;
			block_size = rhs.block_size;
			block_count = rhs.block_count;
			next_free_block = rhs.next_free_block;
			free_block_count = rhs.free_block_count;
		}

        void swap( FixedSizeChunk& lhs, FixedSizeChunk& rhs ) noexcept
        {
            std::swap( lhs.parent_allocator, rhs.parent_allocator );
            std::swap( lhs.address, rhs.address );
            std::swap( lhs.data_address, rhs.data_address );
            std::swap( lhs.block_size, rhs.block_size );
            std::swap( lhs.block_count, rhs.block_count );
            std::swap( lhs.next_free_block, rhs.next_free_block );
            std::swap( lhs.free_block_count, rhs.free_block_count );
        }

        FixedSizeChunk& FixedSizeChunk::operator=( FixedSizeChunk&& rhs ) noexcept
        {
			move( std::move( rhs ) );
            //swap( *this, rhs );
            return *this;
        }

        nbool FixedSizeChunk::init( NebulaAllocator* parent_allocator, nsize block_size, nsize block_count, nsize align )
        {
            assert( parent_allocator != nullptr && "[FixedSizeChunk::init]: parent allocator can not be null" );
            assert( block_count > 0 && "[FixedSizeChunk::init]: block count is 0 " );
            assert( block_size > 0 && "[FixedSizeChunk::init]: block size is 0 " );

            void* p = parent_allocator->allocate( block_count * block_size, align );
			assert( p && "[FixedSizeChunk::init()]: allocate chunk failed" );
            if( p != nullptr )
            {
                int8* start = reinterpret_cast<int8*>( alignUp( p, align ) );

                // initialize chunk
                int8* write = start;
                for( nsize i = 1; i < block_count; ++i )
                {
                    writeBlock( write, i );
                    write += block_size;
                }

                // commit
                this->parent_allocator = parent_allocator;
                this->address = p;
                this->data_address = start;
                this->block_size = block_size;
                this->block_count = block_count;
                this->next_free_block = 0;
                this->free_block_count = block_count;

                return true;
            }

            return false;
        }

        void FixedSizeChunk::release()
        {
            assert( isEmpty() && "[FixedSizeChunk::release]: memory leak, chunk is not clean" );

			if( parent_allocator )
			{
				parent_allocator->deallocate( address );
				parent_allocator = nullptr;
				address = nullptr;
				data_address = nullptr;
				block_size = 0;
				block_count = 0;
				next_free_block = 0;
				free_block_count = 0;
			}
        }

        void* FixedSizeChunk::allocate()
        {
            if( free_block_count > 0 )
            {
                int8* ret = data_address + next_free_block * block_size;
                next_free_block = readBlock( ret );
                --free_block_count;
                return ret;
            }

            return nullptr;
        }

        void FixedSizeChunk::deallocate( void* p )
        {
            if( p != nullptr )
            {
                int8* ptr = static_cast<int8*>( p );
                writeBlock( ptr, next_free_block );
                next_free_block = ( ptr - data_address ) / block_size;
                ++free_block_count;
            }
        }
    }
}