#pragma once

#include "LogDefine.hpp"
#include "LoggerSink.hpp"
#include "LogMessage.hpp"

namespace Nebula
{
    namespace Log
    {
        class NEBULA_INTERFACE Logger
        {
            nstring name;
            nvector<LoggerSinkPtr> sinks;
            std::atomic<LogLevel> level;

        public:
            static LoggerPtr create( const nchar* name, LogLevel level, const nvector<LoggerSinkPtr>& sinks );
            Logger();
            ~Logger();

            nbool init( const nchar* name, LogLevel level, const nvector<LoggerSinkPtr>& sinks );
            void release();

            template<typename... Tn>
            void log( LogLevel level, const nchar* message )
            {
                if( shouldLog( level ) )
                {
                    LogMessage msg( &name, level, message );

                    for( nsize i = 0; i < sinks.size(); ++i )
                    {
                        sinks[i]->log( msg );
                    }
                }
            }

            template<typename... Tn>
            void log( LogLevel level, const nchar* fmt, const Tn&... tn )
            {
                if( shouldLog( level ) )
                {
                    nstring line = nformat( fmt, tn... );

                    LogMessage msg( &name, level, line );

                    for( nsize i = 0; i < sinks.size(); ++i )
                    {
                        sinks[i]->log( msg );
                    }
                }
            }

            template<typename... Tn>
            void trace( const nchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Trace, fmt, tn... );
            }

            template<typename... Tn>
            void debug( const nchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Debug, fmt, tn... );
            }

            template<typename... Tn>
            void info( const nchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Info, fmt, tn... );
            }

            template<typename... Tn>
            void warning( const nchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Warning, fmt, tn... );
            }

            template<typename... Tn>
            void error( const nchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Error, fmt, tn... );
            }

            template<typename... Tn>
            void critical( const nchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Critical, fmt, tn... );
            }

            // nwchar support
            void log( LogLevel level, const nwchar* message )
            {
                if( shouldLog( level ) )
                {
                    std::wstring_convert<std::codecvt_utf8<nwchar>> conv;

                    LogMessage msg( &name, level, conv.to_bytes( message ).c_str() );

                    for( nsize i = 0; i < sinks.size(); ++i )
                    {
                        sinks[i]->log( msg );
                    }
                }
            }

            template<typename... Tn>
            void log( LogLevel level, const nwchar* fmt, const Tn&... tn )
            {
                if( shouldLog( level ) )
                {
                    nstring line = fmt::format( fmt, tn... );
                    LogMessage msg( name, level, line );

                    for( nsize i = 0; i < sinks.size(); ++i )
                    {
                        sinks[i]->log( msg );
                    }
                }
            }

            template<typename T0, typename... Tn>
            void trace( const nwchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Trace, fmt, tn... );
            }

            template<typename T0, typename... Tn>
            void debug( const nwchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Debug, fmt, tn... );
            }

            template<typename T0, typename... Tn>
            void info( const nwchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Info, fmt, tn... );
            }

            template<typename T0, typename... Tn>
            void warning( const nwchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Warning, fmt, tn... );
            }

            template<typename T0, typename... Tn>
            void error( const nwchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Error, fmt, tn... );
            }

            template<typename T0, typename... Tn>
            void critical( const nwchar* fmt, const Tn&... tn )
            {
                log( LogLevel::LL_Critical, fmt, tn... );
            }

            nbool shouldLog( LogLevel level ) const;

            void setLogLevel( LogLevel level );
            LogLevel getLogLevel() const;

            virtual void flush();
        };
    }
}