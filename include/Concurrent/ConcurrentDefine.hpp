#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;
#include "Memory/Memory.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        class Log;
        class ReadWriteLock;
        class Event;
        class Thread;
        class Task;
        class TaskRunner;
        class TaskManager;

        using TaskPtr = std::shared_ptr<Task>;
    }
}