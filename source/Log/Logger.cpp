#include "Log/Logger.hpp"

namespace Nebula
{
    namespace Log
    {
        LoggerPtr Logger::create( const nchar* name, LogLevel level, const nvector<LoggerSinkPtr>& sinks )
        {
            Logger* p = new Logger();
            if( p )
            {
                if( p->init( name, level, sinks ) )
                {
                    return LoggerPtr( p );
                }
                else
                {
                    p->release();
                }
            }

            return nullptr;
        }

        Logger::Logger()
        {
        }

        Logger::~Logger()
        {
        }

        nbool Logger::init( const nchar* name, LogLevel level, const nvector<LoggerSinkPtr>& sinks )
        {
            this->name = name;
            this->level.store( level, std::memory_order_relaxed );
            
            for( nsize i = 0; i < sinks.size(); ++i )
            {
                if( sinks[i] != nullptr )
                {
                    this->sinks.push_back( sinks[i] );
                }
            }

            return this->sinks.size() > 0;
        }

        void Logger::release()
        {
            flush();
        }

        nbool Logger::shouldLog( LogLevel level ) const
        {
            return level >= getLogLevel();
        }

        LogLevel Logger::getLogLevel() const
        {
            return level.load( std::memory_order::memory_order_relaxed );
        }

        void Logger::flush()
        {
            for( nsize i = 0; i < sinks.size(); ++i )
            {
                sinks[i]->flush();
            }
        }
    }
}