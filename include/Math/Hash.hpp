#pragma once

#include "Math/MathDefine.hpp"
namespace Nebula
{
    namespace Math
    {
        namespace Hash
        {
            // DJB hash
            NEBULA_INTERFACE uint32 djb( uint8* data, nsize size );
            NEBULA_INTERFACE uint32 djb( const nchar* str );
            
            template<nsize N>
            static constexpr uint32 djb_ct( const nchar( &s )[N], nsize I = N )
            {
                return I == 1 ? 33 * 5381 ^ s[0] : ( 33 * DJB( s, I - 1 ) ^ s[I - 1] );
            }

            // FNV1A hash
			static constexpr uint32 fnv_prime = 0x01000193u;
			static constexpr uint32 fnv_seed = 0x811C9DC5u;
            static uint32 fnv( const uint8* data, nsize size )
			{
				uint32 hash = fnv_seed;
				while( size-- )
				{
					hash ^= *data ^ hash;
					hash *= fnv_prime;
					++data;
				}

				return hash;
			}

			static uint32 fnv( const nchar* line )
			{
				uint32 hash = fnv_seed;
				while( *line != '\0' )
				{
					hash ^= *line;
					hash *= fnv_prime;
					++line;
				}

				return hash;
			}

			template<nsize N>
			static constexpr uint32 fnv_ct( const nchar( &s )[N], uint32 I = N - 1 )
			{
				return I > 0 ? ( s[I] ^ fnv_ct( s, I - 1 ) ) * fnv_prime : ( s[I] ^ fnv_seed ) * fnv_prime;
			}

			static constexpr uint32 strlen_ct( const nchar* s )
			{
				return *s != '\0' ? 1u + strlen_ct( s + 1 ) : 0u;
			}

			static constexpr uint32 fnv_ct( const nchar* s, uint32 I )
			{
				return I > 0 ? ( s[I] ^ fnv_ct( s, I - 1 ) ) * fnv_prime : ( s[0] ^ fnv_seed ) * fnv_prime;
			}

			static constexpr uint32 fnv_ct( const nchar* s )
			{
				return fnv_ct( s, strlen_ct( s ) );
			}

			// Murmur2
			NEBULA_INTERFACE uint32 murmur2( const uint8* data, nsize size );
			NEBULA_INTERFACE uint32 murmur2( const nchar* line );

			template<uint32 N>
			static constexpr uint32 murmur2_ct( const nchar( &str )[N], uint32 h = 0xc58f1a89 ^ ( N - 1 ), uint32 I = 0, uint32 C = N - 1 )
			{
			#define _mm_m 0x5bd1e995ull
			#define _mm_r 24
			#define _mm_shift_0 13
			#define _mm_shift_1 15
			#define _mm_str_0 (uint32)( str[I] )
			#define _mm_str_1 (uint32)( str[I + 1] << 8 )
			#define _mm_str_2 (uint32)( str[I + 2] << 16 )
			#define _mm_str_3 (uint32)( str[I + 3] << 24 )
			#define _mm_k( str, I ) ( _mm_str_0 + _mm_str_1 + _mm_str_2 + _mm_str_3 )
			#define _mm_mix0( h, k ) ( (uint32)( h * _mm_m ) ^ (uint32)( ( (uint32)( k * _mm_m ) ^ ( (uint32)( k * _mm_m ) >> _mm_r ) ) * _mm_m ) )
			#define _mm_mix1( h ) ( (uint32)( ( h ^ ( h >> _mm_shift_0 ) ) * _mm_m ) ^ ( (uint32)( ( h ^ ( h >> _mm_shift_0 ) ) * _mm_m ) >> _mm_shift_1 ) )

				return ( C >= 4 ) ? murmur2_ct( str, _mm_mix0( h, _mm_k( str, I ) ), I + 4, C - 4 ) : (
					( C == 3 ) ? _mm_mix1( (uint32)( ( h ^ _mm_str_2 ^ _mm_str_1 ^ _mm_str_0 ) * _mm_m ) ) : (
					( C == 2 ) ? _mm_mix1( (uint32)( ( h ^ _mm_str_1 ^ _mm_str_0 ) * _mm_m ) ) : (
						( C == 1 ) ? _mm_mix1( (uint32)( ( h ^ _mm_str_0 ) * _mm_m ) ) :
						_mm_mix1( h )
						)
						)
					);

			#undef _mm_m
			#undef _mm_r
			#undef _mm_shift_0
			#undef _mm_shift_1
			#undef _mm_str_0
			#undef _mm_str_1
			#undef _mm_str_2
			#undef _mm_str_3
			#undef _mm_k
			#undef _mm_mix0
			#undef _mm_mix1
			}
            
			static constexpr uint32 murmur2_ct( const nchar* str, uint32 h, uint32 I, uint32 C )
			{
			#define _mm_m 0x5bd1e995ull
			#define _mm_r 24
			#define _mm_shift_0 13
			#define _mm_shift_1 15
			#define _mm_str_0 (uint32)( str[I] )
			#define _mm_str_1 (uint32)( str[I + 1] << 8 )
			#define _mm_str_2 (uint32)( str[I + 2] << 16 )
			#define _mm_str_3 (uint32)( str[I + 3] << 24 )
			#define _mm_k( str, I ) ( _mm_str_0 + _mm_str_1 + _mm_str_2 + _mm_str_3 )
			#define _mm_mix0( h, k ) ( (uint32)( h * _mm_m ) ^ (uint32)( ( (uint32)( k * _mm_m ) ^ ( (uint32)( k * _mm_m ) >> _mm_r ) ) * _mm_m ) )
			#define _mm_mix1( h ) ( (uint32)( ( h ^ ( h >> _mm_shift_0 ) ) * _mm_m ) ^ ( (uint32)( ( h ^ ( h >> _mm_shift_0 ) ) * _mm_m ) >> _mm_shift_1 ) )

				return ( C >= 4 ) ? murmur2_ct( str, _mm_mix0( h, _mm_k( str, I ) ), I + 4, C - 4 ) : (
					( C == 3 ) ? _mm_mix1( (uint32)( ( h ^ _mm_str_2 ^ _mm_str_1 ^ _mm_str_0 ) * _mm_m ) ) : (
					( C == 2 ) ? _mm_mix1( (uint32)( ( h ^ _mm_str_1 ^ _mm_str_0 ) * _mm_m ) ) : (
					( C == 1 ) ? _mm_mix1( (uint32)( ( h ^ _mm_str_0 ) * _mm_m ) ) :
					_mm_mix1( h )
					)
					)
					);

			#undef _mm_m
			#undef _mm_r
			#undef _mm_shift_0
			#undef _mm_shift_1
			#undef _mm_str_0
			#undef _mm_str_1
			#undef _mm_str_2
			#undef _mm_str_3
			#undef _mm_k
			#undef _mm_mix0
			#undef _mm_mix1
			}

            static constexpr uint32 murmur2_ct( const nchar* str, nsize length )
			{
				return murmur2_ct( str, 0xc58f1a89 ^ ( (uint32)length ), 0, (uint32)length );
			}

			static constexpr uint32 murmur2_ct( const nchar* str )
			{
				return murmur2_ct( str, strlen_ct( str ) );
			}

            // combine
			NEBULA_INTERFACE void combine( uint32& seed, uint32 append );

            static constexpr uint32 combine_ct( uint32& seed, uint32 append )
            {
                return seed ^ seed + 0x9e3779b9 + ( seed << 6 ) + ( seed >> 2 );
            }
		}
    }
}

#define NEBULA_STRING_HASH( str ) Math::Hash::fnv_ct( NEBULA_STRINGFY( str ) )
#define NEBULA_ENUM_NAME_VALUE( name, value ) name = value
#define NEBULA_ENUM_NAME_STR( name, str ) NEBULA_ENUM_NAME_VALUE( name, NEBULA_STRING_HASH( str ) )
#define NEBULA_ENUM_NAME( name ) NEBULA_ENUM_NAME_STR( name, name )
#define NEBULA_DEFINE_VALUE_STRING_PAIR( T )                                \
template<typename T, T value>                                               \
struct _stub_value_string_pair {};
#define NEBULA_MAKE_VALUE_STRING_PAIR( T, V, str )                          \
template<>                                                                  \
struct _stub_value_string_pair<T, V>                                        \
{																			\
    using type = T;                                                         \
    static constexpr uint32 value = V;										\
	static constexpr const nchar* const line = str;		                    \
};
#define NEBULA_GET_STRING_BY_VALUE( T, V ) _stub_value_string_pair<T, V>::line