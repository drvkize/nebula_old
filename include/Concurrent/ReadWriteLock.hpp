#pragma once

#include "Concurrent/ConcurrentDefine.hpp"
#include "Concurrent/Lock.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        class NEBULA_INTERFACE ReadWriteLock
        {
            volatile uint32 count;
            Lock count_lock;
            Lock write_lock;

        public:
            ReadWriteLock( const ReadWriteLock& other ) = delete;
            ReadWriteLock& operator=( const ReadWriteLock& rhs ) = delete;

        public:
            ReadWriteLock();
            ~ReadWriteLock();

            void lockRead();
            void unlockRead();
            void lockWrite();
            void unlockWrite();

            uint32 getReadCount() const { return count; }
        };
    }
}