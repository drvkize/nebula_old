#pragma once

#include "Platform/Language/common.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
            namespace detail
            {
                // member detector result
                enum MemberDetectorResult
                {
                    MDR_NotMember,
                    MDR_Type,
                    MDR_Variable,
                    MDR_Function
                };    
            }
        }
    }
}

#define NEBULA_MEMBER_FUNCTION_POINTER( class, name ) &class::name

#define NEBULA_GENERATE_MEMBER_DETECTOR( name )																										                                    \
namespace Nebula                                                                                                                                                                        \
{                                                                                                                                                                                       \
    namespace Platform                                                                                                                                                                  \
    {                                                                                                                                                                                   \
        namespace Traits                                                                                                                                                                \
        {                                                                                                                                                                               \
			namespace detail																																		                    \
			{																																						                    \
				template<typename T>																																                    \
				struct member_detector_impl_##name																													                    \
				{																																					                    \
					struct member_t { uint8 padding; };																												                    \
					struct type_t { uint8 padding[2]; };																											                    \
					struct variable_t { uint8 padding[3]; };																										                    \
					struct function_t { uint8 padding[4]; };																										                    \
					struct other_t { uint8 padding[5]; };																											                    \
																																									                    \
					struct BaseType { int name; };																													                    \
					template<typename U>																															                    \
					struct DerivedType : U, BaseType {};																											                    \
																																									                    \
					template<typename U>																															                    \
					static other_t test_member( decltype( U::name )* );																								                    \
					template<typename U>																															                    \
					static member_t test_member( ... );																												                    \
																																									                    \
					template<typename U>																															                    \
					static type_t test_type( typename U::name* );																									                    \
					template<typename U>																															                    \
					static other_t test_type( ... );																												                    \
																																									                    \
					template<typename U>																															                    \
					static variable_t test_variable( decltype( U::name )* );																						                    \
					template<typename U>																															                    \
					static other_t test_variable( ... );																											                    \
																																									                    \
					template<typename U>																															                    \
					static function_t test_function( decltype( &U::name ) );																						                    \
					template<typename U>																															                    \
					static other_t test_function( ... );																											                    \
																																									                    \
					static constexpr nbool is_member = sizeof( test_member<DerivedType<T>>( 0 ) ) == sizeof( member_t );											                    \
					static constexpr nbool is_type = sizeof( test_type<T>( 0 ) ) == sizeof( type_t );																                    \
					static constexpr nbool is_variable = sizeof( test_variable<T>( 0 ) ) == sizeof( variable_t );													                    \
					static constexpr nbool is_function = sizeof( test_function<T>( 0 ) ) == sizeof( function_t );													                    \
				};																																					                    \
                template<nbool is_member, nbool is_type, nbool is_variable, nbool is_member_function>																                    \
                struct result_t { static constexpr ntraits::detail::MemberDetectorResult value = ntraits::detail::MemberDetectorResult::MDR_NotMember; };                               \
                template<>																																			                    \
                struct result_t<true, true, false, false> { static constexpr ntraits::detail::MemberDetectorResult value = ntraits::detail::MemberDetectorResult::MDR_Type; };          \
                template<>																																			                    \
                struct result_t<true, false, true, true> { static constexpr ntraits::detail::MemberDetectorResult value = ntraits::detail::MemberDetectorResult::MDR_Variable; };       \
                template<>																																			                    \
                struct result_t<true, false, false, true> { static constexpr ntraits::detail::MemberDetectorResult value = ntraits::detail::MemberDetectorResult::MDR_Function; };      \
                template<typename T>																																	                \
                struct member_detector_##name																															                \
                {																																						                \
                public:																																					                \
                    static constexpr ntraits::detail::MemberDetectorResult value = result_t<                                                                                            \
                        member_detector_impl_##name<T>::is_member,                                                                                                                      \
                        member_detector_impl_##name<T>::is_type,                                                                                                                        \
                        member_detector_impl_##name<T>::is_variable,                                                                                                                    \
                        member_detector_impl_##name<T>::is_function>::value;		                                                                                                    \
                };																																			                            \
			}																															                                                \
            template<typename T>                                                                                                                                                        \
            struct has_member_variable_##name                                                                                                                                           \
            {                                                                                                                                                                           \
                static constexpr nbool value = ( ntraits::detail::member_detector_##name<T>::value == ntraits::detail::MemberDetectorResult::MDR_Variable );                            \
            };                                                                                                                                                                          \
            template<typename T>                                                                                                                                                        \
            struct has_member_function_with_name_##name                                                                                                                                 \
            {                                                                                                                                                                           \
                static constexpr nbool value = ( ntraits::detail::member_detector_##name<T>::value == ntraits::detail::MemberDetectorResult::MDR_Function );                            \
            };                                                                                                                                                                          \
            template<typename T, typename R, typename... Tn>                                                                                                                            \
            struct has_member_function_##name                                                                                                                                           \
            {                                                                                                                                                                           \
            private:                                                                                                                                                                    \
                using FuncType = R (T::*)( Tn... );                                                                                                                                     \
                template<typename U, U>                                                                                                                                                 \
                struct Check {};                                                                                                                                                        \
                static yes_type test( Check<FuncType, NEBULA_MEMBER_FUNCTION_POINTER( T, name )>* );                                                                                    \
                static no_type test( ... );                                                                                                                                             \
            public:                                                                                                                                                                     \
                static constexpr nbool value = has_member_function_with_name_##name<T>::value && sizeof( test( 0 ) == sizeof( yes_type ) );                                             \
            };                                                                                                                                                                          \
        }                                                                                                                                                                               \
    }                                                                                                                                                                                   \
}
