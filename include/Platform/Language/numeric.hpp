#pragma once

#include "Platform/Language/common.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
            template<typename T>
            struct is_integral : std::false_type {};

            template<>
            struct is_integral<int8> : std::true_type {};

            template<>
            struct is_integral<int16> : std::true_type {};

            template<>
            struct is_integral<int32> : std::true_type {};

            template<>
            struct is_integral<int64> : std::true_type {};

            template<>
            struct is_integral<uint8> : std::true_type {};

            template<>
            struct is_integral<uint16> : std::true_type {};

            template<>
            struct is_integral<uint32> : std::true_type {};

            template<>
            struct is_integral<uint64> : std::true_type {};

            template<>
            struct is_integral<nsize> : std::true_type {};

            template<typename T>
            struct is_floating_point : std::false_type {};

            template<>
            struct is_floating_point<nfloat> : std::true_type {};

            template<>
            struct is_floating_point<ndouble> : std::true_type {};

            template<typename T>
            struct numeric_integer
            {
                static constexpr T min = std::numeric_limits<T>::min();
                static constexpr T max = std::numeric_limits<T>::max();
                static constexpr T invalid = ~( (T)0 );
            };

            template<typename T>
            struct numeric_float_point
            {
                static constexpr T min = std::numeric_limits<T>::min();
                static constexpr T max = std::numeric_limits<T>::max();
                static constexpr T epsilon = std::numeric_limits<T>::epsilon();
                static constexpr T infinity = std::numeric_limits<T>::infinity();
                static constexpr T nan = std::numeric_limits<T>::quiet_NaN();
                static constexpr T invalid = infinity;
            };
            
            template<typename T, typename E = void>
            struct numeric {};

            template<typename T>
            struct numeric<T, typename std::enable_if<ntraits::is_integral<T>::value>::type>
            {
                static constexpr T min = numeric_integer<T>::min;
                static constexpr T max = numeric_integer<T>::max;
                static constexpr T invalid = numeric_integer<T>::invalid;
            };
            
            template<typename T>
            struct numeric<T, typename std::enable_if<ntraits::is_floating_point<T>::value>::type>
            {
                static constexpr T min = numeric_float_point<T>::min;
                static constexpr T max = numeric_float_point<T>::max;
                static constexpr T epsilon = numeric_float_point<T>::epsilon;
                static constexpr T infinity = numeric_float_point<T>::infinity;
                static constexpr T nan = numeric_float_point<T>::nan;
                static constexpr T invalid = numeric_float_point<T>::infinity;

                static inline nbool isNAN( T x ) { return isnan( x ); }
                static inline nbool isINF( T x ) { return isinf( x ); }
                static inline nbool isValid( T x ) { return !isnan( x ) && !isinf( x ); }
                static inline nbool isAlmostEqual( T lhs, T rhs, int ulp = 2 )
                {
                    T x = std::abs( lhs - rhs );
                    T y = std::abs( lhs + rhs );
                    return x <= epsilon * y * ulp || x < min;
                }
            };
        }
    }
}