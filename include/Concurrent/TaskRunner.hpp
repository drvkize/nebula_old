#pragma once

#include "ConcurrentDefine.hpp"
#include "Thread.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        class TaskRunner
        {            
            TaskManager* owner;
            Thread thread;
            TaskPtr task;
            nbool running;

            static void process( void* args );

        public:
            TaskRunner( TaskManager* owner );
            ~TaskRunner();

            TaskRunner( const TaskRunner& other ) = delete;
            TaskRunner& operator=( const TaskRunner& rhs ) = delete;

            nbool init();
            void release();

            inline void setTask( TaskPtr task ) { this->task = task; }
            inline TaskPtr getTask() { return task; }
            nsize getThreadID() const;
            inline nbool isRunning() const { return running; }
        };
    }
}