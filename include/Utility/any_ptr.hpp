#pragma once

#include "Platform/Platform.hpp"
#include "Reflection/NType.hpp"
using Nebula::Reflection::NType;

namespace Nebula
{
    namespace Platform
    {
        class any_ptr
        {
            void* const obj;
            NType type;

        public:
            any_ptr()
                : obj( nullptr )
            {
            }

            template<typename T>
            any_ptr( T* const obj )
                : obj( obj )
                , type( NType::get<T>() )
            {
            }

            template<typename T>
            explicit operator T* const() const
            {
                return type == NType::get<T>() ? static_cast<T* const>( obj ) : nullptr;
            }

            template<typename T>
            explicit operator const T* const () const
            {
                return type == NType::get<T>() ? static_cast<const T* const>( const_cast<const void* const>( obj ) ) : nullptr;
            }

            inline nbool isNull() const { return obj == nullptr; }

            inline NType getType() const { return type; }
        };
    }
}