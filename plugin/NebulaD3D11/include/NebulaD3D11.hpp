#pragma once

#include "NebulaD3D11Define.hpp"
#include "System/NebulaPlugin.hpp"

namespace Nebula
{
    namespace System
    {
        class NEBULA_D3D11_INTERFACE NebulaD3D11 : public NebulaPlugin
        { 
        public:
            NebulaD3D11();
            virtual ~NebulaD3D11();

            NebulaD3D11( const NebulaD3D11& other ) = delete;
            NebulaD3D11& operator=( const NebulaD3D11& rhs ) = delete;

            virtual inline nstring getName() const { return "NebulaD3D11"; }
        };
    }
}

extern "C"
{
    NEBULA_D3D11_INTERFACE Nebula::System::NebulaPlugin* createPlugin( void* init_parameters );
    NEBULA_D3D11_INTERFACE void destroyPlugin( Nebula::System::NebulaPlugin* plugin );
}