#pragma once

#include "IO/IODefine.hpp"
#include "Math/Vector.hpp"
#include "Math/Matrix.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Serialization
        {
            // tag
            enum NTypeTag : int8
            {
                NTT_Unknown,
                NTT_Null,
                NTT_Bool,
                NTT_Int8,
                NTT_Int16,
                NTT_Int32,
                NTT_Int64,
                NTT_UInt8,
                NTT_UInt16,
                NTT_UInt32,
                NTT_UInt64,
                NTT_Float,
                NTT_Double,
                NTT_Int2,
                NTT_Int3,
                NTT_Int4,
                NTT_Float2,
                NTT_Float3,
                NTT_Float4,
                NTT_Float2x2,
                NTT_Float3x3,
                NTT_Float4x4,
                NTT_Float4x3,
                NTT_Double2,
                NTT_Double3,
                NTT_Double4,
                NTT_Double2x2,
                NTT_Double3x3,
                NTT_Double4x4,
                NTT_Double4x3,
                NTT_String,
                NTT_Array,
                NTT_Object,
                NTT_Binary
            };

            // readers
            class NTypeReader;

            template<typename T>
            class NScalarReader;
            using NBoolReader = NScalarReader<nbool>;
            using NInt8Reader = NScalarReader<int8>;
            using NInt16Reader = NScalarReader<int16>;
            using NInt32Reader = NScalarReader<int32>;
            using NInt64Reader = NScalarReader<int64>;
            using NUInt8Reader = NScalarReader<uint8>;
            using NUInt16Reader = NScalarReader<uint16>;
            using NUInt32Reader = NScalarReader<uint32>;
            using NUInt64Reader = NScalarReader<uint64>;
            using NFloatReader = NScalarReader<nfloat>;
            using NDoubleReader = NScalarReader<ndouble>;
            using NInt2Reader = NScalarReader<int2>;
            using NInt3Reader = NScalarReader<int3>;
            using NInt4Reader = NScalarReader<int4>;
            using NFloat2Reader = NScalarReader<float2>;
            using NFloat3Reader = NScalarReader<float3>;
            using NFloat4Reader = NScalarReader<float4>;
            using NFloat2x2Reader = NScalarReader<float2x2>;
            using NFloat3x3Reader = NScalarReader<float3x3>;
            using NFloat4x4Reader = NScalarReader<float4x4>;
            using NFloat4x3Reader = NScalarReader<float4x3>;
            using NDouble2Reader = NScalarReader<double2>;
            using NDouble3Reader = NScalarReader<double3>;
            using NDouble4Reader = NScalarReader<double4>;
            using NDouble2x2Reader = NScalarReader<double2x2>;
            using NDouble3x3Reader = NScalarReader<double3x3>;
            using NDouble4x4Reader = NScalarReader<double4x4>;
            using NDouble4x3Reader = NScalarReader<double4x3>;

            class NStringReader;
            template<typename T>
            class NArrayReader;
            class NObjectReader;
            class NBinaryReader;

            // reader pointer
            using NTypeReaderPtr = std::shared_ptr<const NTypeReader>;

            template<typename T>
            using NScalarReaderPtr = std::shared_ptr<NScalarReader<T>>;
            using NStringReaderPtr = std::shared_ptr<NStringReader>;
            template<typename T>
            using NArrayReaderPtr = std::shared_ptr<NArrayReader<T>>;
            using NObjectReaderPtr = std::shared_ptr<NObjectReader>;
            using NBinaryReaderPtr = std::shared_ptr<NBinaryReader>;

            // writers
            class NTypeWriter;

            template<typename T>
            class NScalarWriter;
            using NBoolWriter = NScalarWriter<nbool>;
            using NInt8Writer = NScalarWriter<int8>;
            using NInt16Writer = NScalarWriter<int16>;
            using NInt32Writer = NScalarWriter<int32>;
            using NInt64Writer = NScalarWriter<int64>;
            using NUInt8Writer = NScalarWriter<uint8>;
            using NUInt16Writer = NScalarWriter<uint16>;
            using NUInt32Writer = NScalarWriter<uint32>;
            using NUInt64Writer = NScalarWriter<uint64>;
            using NFloatWriter = NScalarWriter<nfloat>;
            using NDoubleWriter = NScalarWriter<ndouble>;
            using NInt2Writer = NScalarWriter<int2>;
            using NInt3Writer = NScalarWriter<int3>;
            using NInt4Writer = NScalarWriter<int4>;
            using NFloat2Writer = NScalarWriter<float2>;
            using NFloat3Writer = NScalarWriter<float3>;
            using NFloat4Writer = NScalarWriter<float4>;
            using NFloat2x2Writer = NScalarWriter<float2x2>;
            using NFloat3x3Writer = NScalarWriter<float3x3>;
            using NFloat4x4Writer = NScalarWriter<float4x4>;
            using NFloat4x3Writer = NScalarWriter<float4x3>;
            using NDouble2Writer = NScalarWriter<double2>;
            using NDouble3Writer = NScalarWriter<double3>;
            using NDouble4Writer = NScalarWriter<double4>;
            using NDouble2x2Writer = NScalarWriter<double2x2>;
            using NDouble3x3Writer = NScalarWriter<double3x3>;
            using NDouble4x4Writer = NScalarWriter<double4x4>;
            using NDouble4x3Writer = NScalarWriter<double4x3>;

            class NStringWriter;
            template<typename T>
            class NArrayWriter;
            class NObjectWriter;
            class NBinaryWriter;

            // writer pointer
            using NTypeWriterPtr = std::shared_ptr<NTypeWriter>;
            template<typename T>
            using NScalarWriterPtr = std::shared_ptr<NScalarWriter<T>>;
            using NStringWriterPtr = std::shared_ptr<NStringWriter>;
            template<typename T>
            using NArrayWriterPtr = std::shared_ptr<NArrayWriter<T>>;
            using NObjectWriterPtr = std::shared_ptr<NObjectWriter>;
            using NBinaryWriterPtr = std::shared_ptr<NBinaryWriter>;
        }
    }
}
