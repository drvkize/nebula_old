#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Reflection/NType.hpp"
#include "Reflection/NParameter.hpp"
#include "Reflection/NProperty.hpp"
#include "Reflection/NFunction.hpp"
#include "Reflection/NFunctionBuilder.hpp"
#include "Reflection/NConstructor.hpp"
#include "Reflection/NConstructorBuilder.hpp"
#include "Reflection/NMethod.hpp"
#include "Reflection/NMethodBuilder.hpp"