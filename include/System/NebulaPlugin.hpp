#pragma once

#include "System/SystemDefine.hpp"

namespace Nebula
{
    namespace System
    {
        class NEBULA_INTERFACE NebulaPlugin
        {
        public:
            NebulaPlugin();
            virtual ~NebulaPlugin();

            NebulaPlugin( const NebulaPlugin& other ) = delete;
            NebulaPlugin& operator=( const NebulaPlugin& rhs ) = delete;

            virtual nstring getName() const = 0;
            NebulaPlugin* findInstance( const nchar* name );
        };
    }
}