#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Reflection/NType.hpp"

namespace Nebula
{
    namespace Reflection
    {
        class NArgument
        {
            NType type;
            nstring name;
        };

        class NProperty : public NArgument
        {
            nsize offset;
            nvector<NMethod> getters;
            nvector<NMethod> setters;

        public:

        };

        class NClass : public NArgument
        {
            
        };
    }
}