#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Utility/variant.hpp"

namespace Nebula
{
    namespace Reflection
    {
        struct NMetadata
        {
            nstring_view key;
            nvar value;

            NMetadata( nstring_view key, nvar value )
                : key( key )
                , value( value )
            {
            }

            template<typename ValueType>
            NMetadata( nstring_view key, const ValueType& value ) noexcept
                : key( key )
                , value( value )
            {
            }
        };
    }
}