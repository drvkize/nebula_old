#include "Memory/MemoryDefine.hpp"
#include "Memory/OSAllocator.hpp"
#include "Memory/SystemAllocator.hpp"

namespace Nebula
{
    namespace Context
    {
        struct MemoryStub
        {
            Memory::OSAllocator os_alloc;
            Memory::SystemAllocator sys_alloc;

            MemoryStub()
            {
                sys_alloc.init( &os_alloc );
            }

            ~MemoryStub()
            {
                sys_alloc.release();
            }
        };

        MemoryStub& getMemoryStub()
        {
            static MemoryStub _stub_memory;
            return _stub_memory;
        }

        Memory::NebulaAllocator& getOSAllocator()
        {
            return getMemoryStub().os_alloc;
        }

        Memory::NebulaAllocator& getSystemAllocator()
        {
            return getMemoryStub().sys_alloc;
        }
    }
}