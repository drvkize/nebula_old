#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Utility/any_ptr.hpp"
#include "Utility/any.hpp"
#include "Utility/string_view.hpp"
#include "Utility/array_view.hpp"

namespace Nebula
{
    namespace Reflection
    {
        class NMethod
        {
            const nstring_view name;

        public:
            NMethod( const nstring_view name )
                : name( name )
            {
            };

            virtual ~NMethod()
            {
            }

            virtual any invoke( any_ptr& obj ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5, any&& a6 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5, any&& a6, any&& a7 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5, any&& a6, any&& a7, any&& a8 ) const = 0;
            virtual any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5, any&& a6, any&& a7, any&& a8, any&& a9 ) const = 0;

            inline const nstring_view getName() const { return name; }
            virtual NType getType() const = 0;
            virtual NType getClassType() const = 0;
            virtual NType getReturnType() const = 0;
            virtual const nstring_view getSignature() const = 0;
            virtual const array_view<NParameterPtr> getParameters() const = 0;
        };

        template<typename F>
        class NMethodConcrete : public NMethod
        {
        public:
            using Type = F;
            using ClassType = typename ntraits::function_traits<Type>::class_type;
            using ReturnType = typename ntraits::function_traits<Type>::return_type;
            using ParameterTypes = typename ntraits::function_traits<Type>::parameter_types;
            static constexpr nsize ParameterCount = ParameterTypes::count;
            using InvokerType = NInvoker<Type, InvokeMethod, ntraits::make_index_sequence<ParameterCount>>;
            
        private:
            F const func;
            narray<NParameterPtr, ParameterCount> parameters;

        public:
            NMethodConcrete( F const func, const nstring_view name )
                : func( func )
                , NMethod( name )
            {
            }

            NMethodConcrete( F const func, nstring_view&& name, narray<NParameterPtr, ParameterCount>&& parameters )
                : func( func )
                , NMethod( name )
                , parameters( std::move( parameters ) )
            {
            }

            virtual ~NMethodConcrete()
            {
            }

            virtual inline any invoke( any_ptr& obj ) const override
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0 ) const override
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ), std::move( a2 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ), std::move( a2 ), std::move( a3 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ), std::move( a2 ), std::move( a3 ), std::move( a4 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ), std::move( a2 ), std::move( a3 ), std::move( a4 ), std::move( a5 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5, any&& a6 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ), std::move( a2 ), std::move( a3 ), std::move( a4 ), std::move( a5 ), std::move( a6 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5, any&& a6, any&& a7 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ), std::move( a2 ), std::move( a3 ), std::move( a4 ), std::move( a5 ), std::move( a6 ), std::move( a7 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5, any&& a6, any&& a7, any&& a8 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ), std::move( a2 ), std::move( a3 ), std::move( a4 ), std::move( a5 ), std::move( a6 ), std::move( a7 ), std::move( a8 ) );
            }

            virtual inline any invoke( any_ptr& obj, any&& a0, any&& a1, any&& a2, any&& a3, any&& a4, any&& a5, any&& a6, any&& a7, any&& a8, any&& a9 ) const
            {
                return InvokerType::invoke( static_cast<ClassType*>( obj ), func, std::move( a0 ), std::move( a1 ), std::move( a2 ), std::move( a3 ), std::move( a4 ), std::move( a5 ), std::move( a6 ), std::move( a7 ), std::move( a8 ), std::move( a9 ) );
            }

            virtual NType getType() const override { return NType::get<Type>(); }
            virtual NType getClassType() const override { return NType::get<ClassType>(); }
            virtual NType getReturnType() const override { return NType::get<ReturnType>(); }
            virtual const nstring_view getSignature() const override { return NType::getSignature<Type>(); };
            virtual const array_view<NParameterPtr> getParameters() const { return parameters; }
        }; 
    }
}