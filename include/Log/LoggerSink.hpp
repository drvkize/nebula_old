#pragma once

#include "Log/LogDefine.hpp"

namespace Nebula
{
    namespace Log
    {
        class LoggerSink
        {
            std::atomic<LogLevel> level;

        protected:
            inline nbool shouldLog( LogLevel level ) const
            {
                return level >= getLogLevel();
            }

        public:
            LoggerSink() = default;

            virtual void log( const LogMessage& message ) = 0;
            virtual void flush() = 0;

            inline void setLogLevel( LogLevel level )
            {
                this->level.store( level );
            }

            inline LogLevel getLogLevel() const
            {
                return level.load( std::memory_order::memory_order_relaxed );
            }
        };

        struct DummyLock
        {
            inline void acquire() {}
            inline nbool tryAcquire() { return true; }
            inline void release() {}
        };
    }
}