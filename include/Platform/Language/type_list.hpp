#pragma once

#include "Platform/Language/common.hpp"
#include "Platform/Language/type_test.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
            // argument_element
            template<nsize I, typename T>
            struct argument_element
            {
            };

            template<nsize I, template<typename...> class T, typename Head, typename... Tail>
            struct argument_element<I, T<Head, Tail...>> : ntraits::argument_element<I - 1, T<Tail...>> {};

            template<template<typename...> class T, typename Header, typename... Tail>
            struct argument_element<0, T<Header, Tail...>>
            {
                using type = Header;
            };

            // type_list
            template<typename... Tn>
            struct type_list
            {   
                static const nsize count = sizeof...( Tn );

                template<nsize I>
                using type = typename argument_element<I, type_list<Tn...>>::type;
            };

            // is_type_list
            template<typename T>
            struct is_type_list
            {
                static constexpr nbool value = ntraits::is_specialization_of<T, type_list>::value;
            };

            template<typename T>
            static constexpr nbool is_type_list_v = ntraits::is_type_list<T>::value;

            template<nsize I, typename T>
            using type_list_element_t = typename argument_element<I, T>::type;

            // push_back
            template<typename T, typename F, nbool E = ntraits::is_type_list_v<F>>
            struct push_back {};

            template<typename T, typename... Fn>
            struct push_back_n {};

            template<typename T, typename F>
            using push_back_t = typename ntraits::push_back<T, F>::type;
            template<typename T, typename... Fn>
            using push_back_n_t = typename ntraits::push_back_n<T, Fn...>::type;

            template<typename F, typename... Tn>
            struct push_back<ntraits::type_list<Tn...>, F, false>
            {
                using type = ntraits::type_list<Tn..., F>;
            };

            template<typename F>
            struct push_back<void, F, false>
            {
                using type = ntraits::type_list<F>;
            };

            template<typename T, typename F, typename... Fn>
            struct push_back_n<T, F, Fn...>
            {
                using type = typename ntraits::push_back_n<typename ntraits::push_back<T, F>::type, Fn...>::type;
            };

            template<typename T, typename F>
            struct push_back_n<T, F>
            {
                using type = typename ntraits::push_back<T, F>::type;
            };

            template<typename T, typename... Fn>
            struct push_back<T, ntraits::type_list<Fn...>, true>
            {
                using type = typename ntraits::push_back_n<T, Fn...>::type;
            };

            // make_type_list
            // will expand any type_list in Tn...
            template<typename... Tn>
            struct make_type_list
            {
                using type = typename ntraits::push_back_n<ntraits::type_list<>, Tn...>::type;
                constexpr static nsize count = sizeof...( Tn );
            };

            template<>
            struct make_type_list<>
            {
                using type = ntraits::type_list<>;
            };

            template<typename... Tn>
            using make_type_list_t = typename ntraits::make_type_list<Tn...>::type;

            // as_type_list
            // will NOT expand type_list in Tn...
            template<typename... Tn>
            struct as_type_list
            {
                using type = typename ntraits::type_list<Tn...>;
            };

            template<template<typename...> class Template, typename... Tn>
            struct as_type_list<Template<Tn...>>
            {
                using type = typename ntraits::type_list<Tn...>;
            };

            template<typename... Tn>
            using as_type_list_t = typename as_type_list<Tn...>::type;

            // push_front
            template<typename T, typename... Fn>
            struct push_front_n
            {
                using type = typename ntraits::push_back_n<typename ntraits::make_type_list<Fn...>::type, T>::type;
            };

            template<typename T, typename F>
            struct push_front
            {
                using type = typename ntraits::push_back<typename ntraits::make_type_list<F>::type, T>::type;
            };

            template<typename T, typename...Fn>
            using push_front_n_t = typename ntraits::push_front_n<T, Fn...>::type;

            template<typename T, typename F>
            using push_front_t = typename ntraits::push_front<T, F>::type;

            // pop_back
            template<typename T>
            struct pop_back {};

            template<typename T>
            using pop_back_t = typename ntraits::pop_back<T>::type;

            template<typename T, typename... Tn>
            struct pop_back<ntraits::type_list<T, Tn...>>
            {
                using type = typename ntraits::push_back_n<ntraits::type_list<T>, typename pop_back<ntraits::type_list<Tn...>>::type>::type;
            };

            template<typename T, typename F>
            struct pop_back<ntraits::type_list<T, F>>
            { 
                using type = ntraits::type_list<T>;
            };

            template<typename T>
            struct pop_back<ntraits::type_list<T>>
            {
                using type = ntraits::type_list<>;
            };

            // pop_front
            template<typename T>
            struct pop_front {};

            template<typename T>
            using pop_front_t = typename ntraits::pop_front<T>::type;

            template<typename T, typename... Tn>
            struct pop_front<ntraits::type_list<T, Tn...>>
            {
                using type = ntraits::type_list<Tn...>;
            };

            template<typename T>
            struct pop_front<ntraits::type_list<T>>
            {
                using type = ntraits::type_list<>;
            };
        }
    }
}