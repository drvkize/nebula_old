#pragma once

#include "Nebula.hpp"
#include "Utility/any.hpp"
using Nebula::Platform::any;

int test( int argc, char **argv )
{
    any x = 257;
    printf( "%d\n", any_cast<int>( x ) );
    const int& int_x = any_cast<int>( x );
    any y = x;
    printf( "%d\n", int_x );
    printf( "%d\n", any_cast<int>( y ) );
    x = nstring( "string" );
    printf( "%s\n", any_cast<nstring>( x ).c_str() );

    getchar();
    return 0;
}