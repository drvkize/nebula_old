#pragma once

#include "NebulaDefine.hpp"

namespace Nebula
{
    namespace System
    {
        class NebulaSystem;
        class NebulaDevice;
        class NebulaApplication;
        class NebulaPlugin;
        class PluginManager;

        static const nchar* NebulaPluginCreateFuncName = "createPlugin";
        static const nchar* NebulaPluginDestroyFuncName = "destroyPlugin";

        using NebulaPluginCreateFunc = NebulaPlugin* (*)( void* init_parameter );
        using NebulaPluginDestroyFunc = void (*)( NebulaPlugin* plugin );

        struct PluginInfo
        {
            nstring name;
            OS::OSPluginHandle handle;
            NebulaPluginCreateFunc create_func;
            NebulaPluginDestroyFunc destroy_func;
            NebulaPlugin* instance;
        };
    }
}