#include "Platform/Windows/StdConsoleImpl.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Windows
        {
            // OSStdConsole::Context
            StdConsoleImpl::Context::Context()
            {
                output_handle = GetStdHandle( STD_OUTPUT_HANDLE );

                CONSOLE_SCREEN_BUFFER_INFO info;
                GetConsoleScreenBufferInfo( output_handle, &info );
                current_attribute = info.wAttributes;
            }

            StdConsoleImpl::Context::~Context()
            {
            }

            // OSStdConsole
            StdConsoleImpl::Context StdConsoleImpl::context;

            StdConsoleImpl::StdConsoleImpl()
            {
            }

            StdConsoleImpl::~StdConsoleImpl()
            {
            }

            void StdConsoleImpl::setTitle( const nchar* title )
            {
                SetConsoleTitleA( title );
            }

            void StdConsoleImpl::setForegroundColor( ConsoleColor color )
            {
                context.current_attribute &= ~ForegroundColorMask;
                context.current_attribute |= ForegroundColorMask & ColorTable[color];
                SetConsoleTextAttribute( context.output_handle, context.current_attribute );
            }

            void StdConsoleImpl::setBackgroundColor( ConsoleColor color )
            {
                context.current_attribute &= ~BackgroundColorMask;
                context.current_attribute |= BackgroundColorMask & ( ColorTable[color] << 4 );
                SetConsoleTextAttribute( context.output_handle, context.current_attribute );
            }

            void StdConsoleImpl::write( const int8* data, nsize data_size )
            {
                WriteFile( context.output_handle, data, (DWORD)data_size, nullptr, nullptr );
            }
            
            void StdConsoleImpl::flush()
            {
                FlushFileBuffers( context.output_handle );
            }
        }
    }
}