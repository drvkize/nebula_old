#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;

namespace Nebula
{
    namespace Platform
    {
        template<typename T>
        class scoped_object
        {
        public:
            using data_t = T;

        private:
            data_t data;

        public:
            scoped_object( const scoped_object& other ) = delete;
            scoped_object& operator=( const scoped_object& rhs ) = delete;

            inline T& get() { return data; }
            inline const T& get() const { return data; }
        };
    }
}