#pragma once

#include "Memory/MemoryDefine.hpp"
#include "Memory/NebulaAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        class OSAllocator : public NebulaAllocator
        {
        public:
            OSAllocator();
            virtual ~OSAllocator();

            virtual void* allocate( nsize size, nsize align = 1 ) override;
            virtual nbool deallocate( void* p ) override;
        };
    }
}