#include "Memory/NebulaAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        // align by increase address
        void* alignUp( const void* p, nsize align )
        {
            if( align < 2 ) return const_cast<void*>( p );
            
            assert( p != nullptr && "[alignUp]: align pointer is null" );
            assert( align > 1 && ( align & ( align - 1 ) ) == 0 && "[alignUp]: alignment is not power of 2" );
            uintptr addr = (uintptr)p;
            addr = ( addr + ( align - 1 ) ) & -align;
            return (void*)addr;
        }

        // align by decrease address
        void* alignDown( const void* p, nsize align )
        {
            if( align < 2 ) return const_cast<void*>( p );

            assert( p != nullptr && "[alignDown]: align pointer is null" );
            assert( align > 1 && ( align & ( align - 1 ) ) == 0 && "[alignUp]: alignment is not power of 2" );
            uintptr addr = (uintptr)p;
            addr &= -align;
            return (void*)addr;
        }

        // is pointer align
        nbool isPointerAligned( const volatile void* p, const nsize align )
        {
            return ( (uintptr)p & ( align -1 ) ) == 0;
        }

        NebulaAllocator::NebulaAllocator()
        {
        }

        NebulaAllocator::~NebulaAllocator()
        {
        }

        void NebulaAllocator::release()
        {
        }
    }
}