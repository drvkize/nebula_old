#include "Memory/CoalesceChunk.hpp"

namespace Nebula
{
    namespace Memory
    {
        CoalesceChunk::CoalesceChunk()
            : parent_allocator( nullptr )
            , index( nullptr )
            , address( nullptr )
            , data_address( nullptr )
            , data_size( 0 )
        {
        }

        CoalesceChunk::CoalesceChunk( CoalesceChunk&& other ) noexcept
        {
            move( std::move( other ) );
            // swap( *this, other );
        }

        CoalesceChunk::~CoalesceChunk()
        {
        }

        void CoalesceChunk::move( CoalesceChunk&& rhs ) noexcept
        {
            parent_allocator = rhs.parent_allocator;
            index = rhs.index;
            address = rhs.address;
            data_address = rhs.data_address;
            data_size = rhs.data_size;
        }

        void swap( CoalesceChunk& lhs, CoalesceChunk& rhs ) noexcept
        {
            std::swap( lhs.parent_allocator, rhs.parent_allocator );
            std::swap( lhs.index, rhs.index );
            std::swap( lhs.address, rhs.address );
            std::swap( lhs.data_address, rhs.data_address );
            std::swap( lhs.data_size, rhs.data_size );
        }

        CoalesceChunk& CoalesceChunk::operator=( CoalesceChunk&& rhs ) noexcept
        {
            move( std::move( rhs ) );
            // swap( *this, rhs );
            return *this;
        }

        nbool CoalesceChunk::init( NebulaAllocator* parent_allocator, nsize size, nsize align, CoalesceBlockTree* index )
        {
            assert( parent_allocator != nullptr && "[CoalesceChunk::init]: parent allocator is null" );
            assert( size > 0 && "[CoalesceChunk::init]: init size can not be zero" );

			void* p = parent_allocator->allocate( size, align );
			assert( p && "[CoalesceChunk::init()]: allocate chunk failed" );

            if( p != nullptr )
            {
                int8* start = reinterpret_cast<int8*>( alignUp( p, align ) );

                // initialize chunk
                // [first block]->[block 0][block 0 data]->[block 1][block 1 data]->...->[block n][block n data]->[last block]
                Block* first_block = reinterpret_cast<Block*>( start );
                first_block->prev = nullptr;
				first_block->next = nullptr;

                Block* data_block = reinterpret_cast<Block*>( start + sizeof( Block ) );
                Block::insertAfter( first_block, data_block );

                Block* last_block = reinterpret_cast<Block*>( start + size - sizeof( Block ) );
                Block::insertAfter( data_block, last_block );

                first_block->setUsed();
                data_block->setFree();
                last_block->setUsed();

                // commit
                this->parent_allocator = parent_allocator;
                this->index = index;
                this->address = reinterpret_cast<int8*>( p );
                this->data_address = start;
                this->data_size = size;

                // update index
                index->add( getFirstBlock() );

                return true;
            }

            return false;
        }

        void CoalesceChunk::release()
        {
            assert( isEmpty() && "[CoalesceChunk::release]: memory leak, chunk is not clean" );

            index->remove( getFirstBlock() );

            if( parent_allocator != nullptr && address != nullptr )
            {
                parent_allocator->deallocate( address );
                
                index = nullptr;
                address = nullptr;
                data_address = nullptr;
                data_size = 0;

                parent_allocator = nullptr;
            }
        }

        CoalesceChunk::Block* CoalesceChunk::split( Block* block, nsize size, nsize align )
        {
            assert( !block->isUsed() && "[CoalesceChunk::split]: block to split is used" );

            // test if have enough space
            int8* block_end = reinterpret_cast<int8*>( block->next );
            int8* data_start = reinterpret_cast<int8*>( alignDown( block_end - size, align ) );
            int8* block_start = data_start - sizeof( Block );

            if( block_start < block->getData() )
            {
                // not enough space
                return nullptr;
            }

            // lock

			// remove block from index before split
			index->remove(block);

            // initialize new block
            Block* new_block = reinterpret_cast<Block*>( block_start );
            new_block->setUsed();
            Block::insertAfter( block, new_block );

            // add block back with changed size
            index->add( block );
            
            // unlock

            return new_block;
        }

        void CoalesceChunk::merge( Block* block )
        {
            // lock

            // try merge next block
            if( block->next->isFree() )
            {
                index->remove( block->next );
                Block::remove( block->next );
            }

            if( block->prev->isFree() )
            {
                index->remove( block->prev );
                Block::remove( block );
                index->add( block->prev );
            }
            else
            {
                block->setFree();
                index->add( block );
            }

            // unlock
        }

		void CoalesceChunk::printBlocks() const
		{
			printf("chunk(%2p, " NULONG_FMT "): [align(" NULONG_FMT ")][head(" NULONG_FMT ")]", address, data_size, data_address - address, sizeof( Block ) );
			for( const Block* block = getFirstBlock(); block != getLastBlock(); block = block->next )
			{
				printf( "[%s(" NULONG_FMT ")]", block->isFree() ? "free" : "used", block->getDataSize() );
			}
			printf("[tail(" NULONG_FMT ")]\n", sizeof( Block ) );
		}
    }
}