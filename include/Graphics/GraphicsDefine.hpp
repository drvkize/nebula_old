#pragma once

#include "NebulaDefine.hpp"

namespace Nebula
{
    namespace Graphics
    {
        class Buffer;
        class Texture;
        class Shader;
        class BufferStream;
        class TextureStream;

        using BufferPtr = std::shared_ptr<Buffer>;
        using TexturePtr = std::shared_ptr<Texture>;
        using ShaderPtr = std::shared_ptr<Shader>;
        using BufferStreamPtr = std::shared_ptr<BufferStream>;
        using TextureStreamPtr = std::shared_ptr<TextureStream>;
    }
}