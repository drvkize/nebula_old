#pragma once

#include "Memory/MemoryDefine.hpp"
#include "Memory/FixedSizeChunkAllocator.hpp"
#include "Memory/CoalesceBlock.hpp"

namespace Nebula
{
    namespace Memory
    {
        class CoalesceBlockTree
        {
            using Block = CoalesceBlock;

            struct Node
            {
                static constexpr uint32 RED = 0;
                static constexpr uint32 BLACK = 1;

				const Block* block;
                Node* left;
                Node* right;
                uint32 color;

                inline void flipColor() { color ^= 1; }
                inline nbool isRed() const { return color == RED; }
                inline nbool isBlack() const { return color == BLACK; }
                inline nbool isLeftRed() const { return left != nullptr && left->isRed(); }
                inline nbool isLeftBlack() const { return left != nullptr && left->isBlack(); }
                inline nbool isRightRed() const { return right != nullptr && right->isRed(); }
                inline nbool isRightBlack() const { return right != nullptr && right->isBlack(); }
                inline nsize getAvailableSize( nsize align ) const { return block != nullptr ? Block::getAvailableSize( block, align ) : 0; }
                inline nsize getDataSize() const { return block != nullptr ? block->getDataSize() : 0; }

                static int compare( Node* node, nsize size );
            };

            FixedSizeChunkAllocator node_allocator;
            Node* root;
            nsize count;

            // helpers
            static Node* getMin( Node* node );
            static Node* getMax( Node* node );
            static void flipColor( Node* node );
            static Node* rotateLeft( Node* node );
            static Node* rotateRight( Node* node );
            static Node* fixup( Node* node );
            static Node* moveRedRight( Node* node );
            static Node* moveRedLeft( Node* node );
            
            Node* createNode( const Block* block );
            void destroyNode( Node* node, nbool destroy_child = false );

            Node* deleteMinRecursive( Node* node );
            Node* insertRecursive( Node* node, const Block* block );
            Node* deleteRecursive( Node* node, const Block* block );
        
        public:
            CoalesceBlockTree();
            ~CoalesceBlockTree();

            nbool init( NebulaAllocator* parent_allocator );
            void release();

            void add( const Block* block );
            void remove( const Block* block );
            const Block* find( nsize size, nsize align );

			// debug
			void printNode( Node* node, int depth = 0 ) const;
			void printAllNodes() const { printNode(root); }
        };
    }
}