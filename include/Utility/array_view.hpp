#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;
#include "Utility/array.hpp"

namespace Nebula
{
    namespace Platform
    {
        template<typename T>
        class array_view
        {
        public:
            using type = T;
            using iterator = const T*;

        private:
            const T* data;
            const nsize count;
        
        public:
            constexpr array_view()
                : data( nullptr )
                , count( 0 )
            {
            }

            template<nsize N>
            constexpr array_view( const T(&arr)[N] )
                : data( arr )
                , count( N )
            {
            }

            template<nsize N>
            constexpr array_view( narray<T, N>&& arr )
                : data( arr.data() )
                , count( N )
            {
            }

            template<nsize N>
            constexpr array_view( const narray<T, N>& arr )
                : data( arr.data() )
                , count( N )
            {
            }

            template<nsize N>
            constexpr array_view( const T* data, nsize count )
                : data( data )
                , count( count )
            {
            }

            constexpr array_view( const array_view& other )
                : data( other.data )
                , count( other.count )
            {
            }

            constexpr const T* getData() const { return data; }
            constexpr nsize getCount() const { return count; }
            constexpr iterator begin() const { return data; }
            constexpr iterator end() const { return data + count; }

            const T& operator[]( nsize index ) const
            {
                nassert( index < count, "[array_view::operator[]]: index out of range" );
                return *( data + index );
            }

            nsize size() const { return count; }
        };
    }
}