#include "Platform/Windows/PlatformWindows.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Windows
        {
            std::tm* localtime( const std::time_t* t )
            {
                return ::localtime( t );
            }
        }
    }

    namespace Memory
    {
        namespace Windows
        {
            void* allocate( nsize size )
            {
                return ::malloc( size );
            }

            void deallocate( void* p )
            {
                ::free( p );
            }
        }
    }

    namespace Concurrent
    {
        namespace Windows
        {
            nsize getThreadID()
            {
                return ::GetCurrentThreadId();
            }
        }
    }

    namespace System
    {
        namespace Windows
        {
            nbool isValid( OSPluginHandle handle )
            {
                return handle != NULL;
            }

            OSPluginHandle loadPlugin( const nchar* path )
            {
                return LoadLibrary( path );
            }

            void unloadPlugin( OSPluginHandle handle )
            {
                FreeLibrary( handle );
            }

            void* getEntryPoint( OSPluginHandle handle, const nchar* name )
            {
                return GetProcAddress( handle, name );
            }
        }
    }
}