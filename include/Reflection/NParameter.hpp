#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Reflection/NType.hpp"
#include "Reflection/NMetadata.hpp"
#include "Utility/string_view.hpp"
#include "Utility/array.hpp"
#include "Utility/array_view.hpp"
#include "Utility/any.hpp"
#include "Platform/Language/type_list.hpp"
#include "Platform/Language/function_traits.hpp"

namespace Nebula
{
    namespace Reflection
    {
        class NParameter
        {
        protected:
            virtual const any& getDefaultValueImpl() const { static any _stub_null_default_value; return _stub_null_default_value; }

        public:
            virtual nsize getIndex() const = 0;
            virtual NType getType() const = 0;
            virtual const nstring_view getSignature() const = 0;
            virtual inline nbool hasName() const { return false; }
            virtual nstring_view getName() const { return ""; }
            
            virtual inline nbool hasDefaultValue() const { return false; }
            template<typename T>
            inline const T& getDefaultValue() const
            {
                return getDefaultValueImpl().get<T>();
            }
        };

        template<typename T, nsize Index, nbool HasName, nbool HasDefaultValue>
        class NParameterConcrete : public NParameter {};

        template<typename T, nsize Index>
        class NParameterConcrete<T, Index, false, false> : public NParameter
        {
        public:
            static NParameterPtr create()
            {
                return NParameterPtr ( new NParameterConcrete() );
            }

            inline nsize getIndex() const override { return Index; }
            virtual inline NType getType() const override { return NType::get<T>(); }
            virtual inline const nstring_view getSignature() const override { return NType::getSignature<T>(); }
        };

        template<typename T, nsize Index>
        class NParameterConcrete<T, Index, true, false> : public NParameter
        {
            const nstring_view name;

        public:
            static NParameterPtr create( const nstring_view name )
            {
                return NParameterPtr( new NParameterConcrete( name ) );
            }

            constexpr NParameterConcrete( const nstring_view name )
                : name( name )
            {
            }

            virtual ~NParameterConcrete() {}

            inline nsize getIndex() const override { return Index; }
            virtual inline NType getType() const override { return NType::get<T>(); };
            virtual inline const nstring_view getSignature() const override { return NType::getSignature<T>(); }
            virtual inline nbool hasName() const override { return true; }
            virtual inline nstring_view getName() const override { return name; }
        };

        template<typename T, nsize Index>
        class NParameterConcrete<T, Index, false, true> : public NParameter
        {
            const any& default_value;

        protected:
            virtual const any& getDefaultValueImpl() const override { return default_value; }

        public:
            static NParameterPtr create( const any&& default_value )
            {
                return NParameterPtr( new NParameterConcrete( std::move( default_value ) ) );
            }

            NParameterConcrete( const any&& default_value )
                : default_value( default_value )
            {
            }

            virtual ~NParameterConcrete() {}

            inline nsize getIndex() const override { return Index; }
            virtual inline NType getType() const override { return NType::get<T>(); };
            virtual inline const nstring_view getSignature() const override { return NType::getSignature<T>(); }
            virtual inline nbool hasDefaultValue() const override { return true; }
        };

        template<typename T, nsize Index>
        class NParameterConcrete<T, Index, true, true> : public NParameter
        {
            const nstring_view name;
            const any& default_value;

        protected:
            virtual const any& getDefaultValueImpl() const override { return default_value; }

        public:
            static NParameterPtr create( const nstring_view name, const any&& default_value )
            {
                return NParameterPtr( new NParameterConcrete( name, std::move( default_value ) ) );
            }

            NParameterConcrete( const nstring_view name, const any&& default_value )
                : name( name )
                , default_value( default_value )
            {
            }

            virtual ~NParameterConcrete() {}

            inline nsize getIndex() const override { return Index; }
            virtual inline NType getType() const override { return NType::get<T>(); };
            virtual inline const nstring_view getSignature() const override { return NType::getSignature<T>(); }
            virtual inline nbool hasName() const override { return true; }
            virtual inline nstring_view getName() const  override { return name; }
            virtual inline nbool hasDefaultValue() const { return true; }
        };

        class NParameterHelper
        {
            template<typename TypeList>
            static void packParametersImpl( narray<NParameterPtr, TypeList::count>& parameters, const array_view<nstring_view> names, const array_view<any> default_values )
            {
                packParametersImpl<TypeList>( parameters, names, default_values, std::make_index_sequence<TypeList::count>{} );
            }

            template<typename TypeList, nsize Index, nsize... Indexes>
            static void packParametersImpl( narray<NParameterPtr, TypeList::count>& parameters, const array_view<nstring_view> names, const array_view<any> default_values, std::index_sequence<Index, Indexes...> dummy )
            {
                using ParameterType_tt = NParameterConcrete<typename TypeList::type<Index>, Index, true, true>;
                using ParameterType_tf = NParameterConcrete<typename TypeList::type<Index>, Index, true, false>;
                using ParameterType_ft = NParameterConcrete<typename TypeList::type<Index>, Index, false, true>;
                using ParameterType_ff = NParameterConcrete<typename TypeList::type<Index>, Index, false, false>;

                nbool has_name = Index < names.getCount() && !names[Index].isNullOrEmpty();
                nbool has_default_value = Index < default_values.getCount() && !default_values[Index].isNull();

                if( has_name )
                {
                    if( has_default_value )
                    {
                        parameters[Index] = ParameterType_tt::create( names[Index], std::move( default_values[Index] ) );
                    }
                    else
                    {
                        parameters[Index] = ParameterType_tf::create( names[Index] );
                    }
                }
                else
                {
                    if( has_default_value )
                    {
                        parameters[Index] = ParameterType_ft::create( std::move( default_values[Index] ) );
                    }
                    else
                    {
                        parameters[Index] = ParameterType_ff::create();
                    }
                }

                packParametersImpl<TypeList>( parameters, names, default_values, std::index_sequence<Indexes...>{} );
            }

            template<typename TypeList>
            static void packParametersImpl( narray<NParameterPtr, TypeList::count>& parameters, const array_view<nstring_view> names, const array_view<any> default_values, std::index_sequence<> dummy )
            {
                return;
            }

            template<nsize N, typename T, typename... Tn>
            static void setParameterNamesImpl( narray<nstring_view, N>& parameter_names, const T name, const Tn... names )
            {
                parameter_names[N - sizeof...( Tn ) - 1] = name;
                setParameterNamesImpl( parameter_names, names... );
            }

            template<nsize N, typename T>
            static void setParameterNamesImpl( narray<nstring_view, N>& parameter_names, const T name )
            {
                parameter_names[N - 1] = name;
            }

            template<nsize N, typename T, typename... Tn>
            static void setParameterDefaultValuesImpl( narray<any, N>& parameter_default_values, const T& default_value, const Tn&... default_values )
            {
                parameter_default_values[N - sizeof...( Tn ) - 1] = any( default_value );
                setParameterDefaultValuesImpl( parameter_default_values, default_values... );
            }

            template<nsize N, typename T>
            static void setParameterDefaultValuesImpl( narray<any, N>& parameter_default_values, const T& default_value )
            {
                parameter_default_values[N - 1] = any( default_value );
            }

        public:
            template<typename TypeList>
            static inline void packParameters(
                narray<NParameterPtr, TypeList::count>& parameters,
                const array_view<nstring_view> names,
                const array_view<any> default_values
                )
            {
                packParametersImpl<TypeList>( parameters, names, default_values );
            }

            template<typename TypeList>
            static inline void packParameters( narray<NParameterPtr, TypeList::count>& parameters )
            {
                packParametersImpl<TypeList>( parameters, array_view<nstring_view>(), array_view<any>() );
            }

            template<nsize N, typename... Tn>
            static inline void setParameterNames( narray<nstring_view, N>& parameter_names, Tn... names )
            {
                setParameterNamesImpl( parameter_names, names... );
            }

            template<nsize N, typename... Tn>
            static inline void setParameterDefaultValues( narray<any, N>& parameter_default_values, const Tn&... default_values )
            {
                setParameterDefaultValuesImpl( parameter_default_values, default_values... );
            }
        };
    }
}