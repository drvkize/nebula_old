#include "Platform/Platform.hpp"
#define FMT_HEADER_ONLY
#include "fmt/format.h"
#include "fmt/time.h"

namespace Nebula
{
    namespace Platform
    {
        template<typename Char, typename... Tn>
        static inline nstring nformat( const Char* fmt, Tn... tn )
        {
            return fmt::format( fmt, tn... );
        }

        template<typename... Tn>
        static inline nstring nformat( const nstring& fmt, Tn... tn )
        {
            return fmt::format( fmt, tn... );
        }

        template<typename... Tn>
        static inline nwstring nformat( const nwstring& fmt, Tn... tn )
        {
            return fmt::format( fmt, tn... );
        }
    }
}