#pragma once

#include "IO/IODefine.hpp"
#include "Platform/Language/member_detection.hpp"

NEBULA_GENERATE_MEMBER_DETECTOR( setTitle );

namespace Nebula
{
    namespace IO
    {
        template<typename T>
        class StdConsoleSPI : public T
        {
        public:
            StdConsoleSPI()
            {
            }

            virtual ~StdConsoleSPI()
            {
            }

            StdConsoleSPI( const StdConsoleSPI& other ) = delete;
            StdConsoleSPI& operator=( const StdConsoleSPI& rhs ) = delete;

            static inline void setTitle( const nchar* title )
            {
                static_assert( ntraits::has_member_function_setTitle<T, void, const nchar*>::value, "[StdConsoleSPI::setTitle()]: missing static polymorph interface" );

                T::setTitle( title );
            }

            static inline void setForegroundColor( ConsoleColor color )
            {
                T::setForegroundColor( color );
            }

            static inline void setBackgroundColor( ConsoleColor color )
            {
                T::setBackgroundColor( color );
            }

            static inline void setBold( nbool value )
            {
                T::setBold( value );
            }

            static inline void setUnderline( nbool value )
            {
                T::setUnderline( value );
            }

            static inline void write( const nstring& line )
            {
                T::write( reinterpret_cast<const int8*>( line.c_str() ), line.length() );
            }

            static inline void write( const int8* data, nsize data_size )
            {
                T::write( data, data_size );
            }

            static inline void flush()
            {
                T::flush();
            }
        };
    }
}