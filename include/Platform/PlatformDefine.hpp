#pragma once

#include "Config/Config.hpp"

#include "Platform/Language/cpp.hpp"
#include "Platform/Language/common.hpp"
#include "Platform/Language/numeric.hpp"
#include "Platform/Language/type_test.hpp"

using namespace Nebula::Platform;

// platform specific
#ifdef _WIN32
    // platform tag
    #define NEBULA_PLATFORM_WINDOWS 1
    // os namespace
    #include "Platform/Windows/PlatformWindows.hpp"
    #define OS Windows
#elif __linux__
    // platform tag
    #define NEBULA_PLATFORM_LINUX 1
    // os namespace
    #include "Platform/Linux/PlatformLinux.hpp"
    #define OS Linux
#elif __APPLE__
    #include "TargetConditionals.hpp"
    #if TARGET_OS_IPHONE
        // platform tag
        #define NEBULA_PLATFORM_IOS 1
        // os namespace
        #define OS iOS
    #elif TARGET_OS_MAC
        // platform tag
        #define NEUBLA_PLATFORM_MAC 1
        // os namespace
        #define OS Mac
    #endif
#endif