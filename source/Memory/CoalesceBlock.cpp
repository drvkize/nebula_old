#include "Memory/CoalesceBlock.hpp"
#include "Memory/NebulaAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        void CoalesceBlock::insertAfter( Block* prev, Block* block )
        {
            assert( prev != nullptr && "[CoalesceBlock::insertAfter]: prev block is null" );
            assert( block != nullptr && "[CoalesceBlock::insertAfter]: block to insert is null" );

            block->prev = prev;
            block->next = prev->next;

            if( prev->next != nullptr )
            {
                prev->next->prev = block;
            }

            prev->next = block;
        }

        void CoalesceBlock::remove( Block* block )
        {
            assert( block != nullptr && "[CoalesceBlock::remove]: block to remove is null" );

            Block* prev = block->prev;
            Block* next = block->next;

            if( prev != nullptr )
            {
                prev->next = next;
            }

            if( next != nullptr )
            {
                next->prev = prev;
            }
        }

        nsize CoalesceBlock::getAvailableSize( const Block* block, nsize align )
        {
            int8* data_start = reinterpret_cast<int8*>( alignUp( reinterpret_cast<const void*>( block->getData() + sizeof( Block ) ), align ) );
            if( block->getDataEnd() > data_start )
            {
                return block->getDataEnd() - data_start;
            }

            return 0;
        }

        int CoalesceBlock::compare( const Block* left, const Block* right )
        {
            assert( left != nullptr && right != nullptr && "[CoalesceBlock::compare] block to compare is null" );

            nsize left_size = left->getDataSize();
            nsize right_size = right->getDataSize();

            // compare size first, same size return lower address block
            if( left_size < right_size ) return -1;
            else if( left_size > right_size ) return 1;
            else if( left < right ) return -1;
            else if( left > right ) return 1;
            return 0;
        }
    }
}