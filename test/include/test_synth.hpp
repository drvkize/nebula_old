#pragma once

#include "Nebula.hpp"
#include "IO/StdFile.hpp"
using Nebula::IO::StdFile;
using Nebula::IO::StdFilePtr;
#include "Memory/MemoryBuffer.hpp"
#include "Synth/SynthParser.hpp"
#include "Synth/SynthContext.hpp"
using namespace Nebula::Synth;
#include "Log/Log.hpp"

int test( int argc, char** argv )
{
    printf( "Nebula Synthetic language test\n" );

    if( argc < 2 )
    {
        printf( "Usage: test <file_name>\n" );
        return 0;
    }

    nstring file_path = argv[1];
    StdFilePtr file = StdFile::create( file_path.c_str(), "r" );
    if( file == nullptr )
    {
        Nebula::Context::error( "open file {0} failed\n", file_path );
        return 1;
    }

    nsize file_size = file->getFileSize();
    BufferPtr buffer = Buffer::create( file_size + 1 );
    buffer->getData()[file_size] = '\0';

    nsize read_size = file->read( buffer->getData(), file_size );
    Nebula::Context::info( "read file {0}( {1} )\n", file_path, file_size );
    file = nullptr;

    SynthContext ctx;
    ctx.loc.begin.filename = &file_path;
    ctx.cursor = reinterpret_cast<const nchar*>( buffer->getData() );
    SynthParser parser( ctx );
    parser.parse();

    if( ctx.root_expr != nullptr )
    {
        nstring ast = ctx.root_expr->to_string( 0 );
        printf( "%s\n", ast.c_str() );
    }
    else
    {
        printf( "root_expr == nullptr\n" );
    }

    return 0;
}