#include "Log/LogDefine.hpp"
#include "Log/Logger.hpp"

namespace Nebula
{
    namespace Context
    {
        NEBULA_INTERFACE Log::Logger& getLogger();

        template<typename CharType, typename... Tn>
        void trace( const CharType* fmt, Tn... args )
        {
            getLogger().trace( fmt, args... );
        }

        template<typename CharType, typename... Tn>
        void debug( const CharType* fmt, Tn... args )
        {
            getLogger().debug( fmt, args... );
        }

        template<typename CharType, typename... Tn>
        void info( const CharType* fmt, Tn... args )
        {
            getLogger().info( fmt, args... );
        }

        template<typename CharType, typename... Tn>
        void warning( const CharType* fmt, Tn... args )
        {
            getLogger().warning( fmt, args... );
        }

        template<typename CharType, typename... Tn>
        void error( const CharType* fmt, Tn... args )
        {
            getLogger().error( fmt, args... );
        }

        template<typename CharType, typename... Tn>
        void critical( const CharType* fmt, Tn... args )
        {
            getLogger().critical( fmt, args... );
        }
    }
}