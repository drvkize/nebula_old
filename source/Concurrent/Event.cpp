#include "Concurrent/Event.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        Event::Event()
        {
        }

        Event::~Event()
        {
        }

        void Event::setOne()
        {
            std::unique_lock<std::mutex> lock( mtx );
            event.notify_one();
        }

        void Event::setAll()
        {
            std::unique_lock<std::mutex> lock( mtx );
            event.notify_all();
        }

        void Event::waitOne( nsize wait_time )
        {
            std::unique_lock<std::mutex> lock( mtx );
            if( wait_time == 0 )
            {
                event.wait( lock );
            }
            else
            {
                event.wait_for( lock, std::chrono::milliseconds( wait_time ) );
            }
        }
    }
}