#include "NebulaD3D11.hpp"

namespace Nebula
{
    namespace System
    {
        NebulaD3D11::NebulaD3D11()
        {
        }

        NebulaD3D11::~NebulaD3D11()
        {
        }
    }
}

Nebula::System::NebulaPlugin* createPlugin( void* init_parameters )
{
    return new Nebula::System::NebulaD3D11();
}
void destroyPlugin( Nebula::System::NebulaPlugin* plugin )
{
    delete static_cast<Nebula::System::NebulaD3D11*>( plugin );
}