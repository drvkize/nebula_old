#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;
#include "Reflection/NType.hpp"
using Nebula::Reflection::NType;
#include "Reflection/NFunction.hpp"
using Nebula::Reflection::NFunction;

#pragma warning( push )
#pragma warning( disable: 4307 )
#include "Math/Hash.hpp"

namespace Nebula
{
    namespace Synth
    {
        enum SynthExprType : uint32
        {
            NEBULA_ENUM_NAME( SET_UNKNOWN ),

            // literal expr
            NEBULA_ENUM_NAME( SET_NULL ),
            NEBULA_ENUM_NAME( SET_BOOL ),
            NEBULA_ENUM_NAME( SET_INT ),
            NEBULA_ENUM_NAME( SET_FLOAT ),
            NEBULA_ENUM_NAME( SET_STRING ),

            // composite expr
            NEBULA_ENUM_NAME( SET_PARENS ),
            NEBULA_ENUM_NAME( SET_BRACES ),
            NEBULA_ENUM_NAME( SET_BRACKETS ),

            // operator expr
            NEBULA_ENUM_NAME( SET_DOT ),
            NEBULA_ENUM_NAME( SET_NEGATE ),
            NEBULA_ENUM_NAME( SET_PLUS ),
            NEBULA_ENUM_NAME( SET_MINUS ),
            NEBULA_ENUM_NAME( SET_MUL ),
            NEBULA_ENUM_NAME( SET_DIV ),
            NEBULA_ENUM_NAME( SET_OR ),
            NEBULA_ENUM_NAME( SET_AND ),
            NEBULA_ENUM_NAME( SET_LT ),
            NEBULA_ENUM_NAME( SET_GT ),
            NEBULA_ENUM_NAME( SET_LE ),
            NEBULA_ENUM_NAME( SET_GE ),
            NEBULA_ENUM_NAME( SET_EQ ),
            NEBULA_ENUM_NAME( SET_NE ),

            // stmt
            NEBULA_ENUM_NAME( SET_IF ),
            NEBULA_ENUM_NAME( SET_ELIF ),
            NEBULA_ENUM_NAME( SET_ELSE ),
            NEBULA_ENUM_NAME( SET_WHILE ),
            NEBULA_ENUM_NAME( SET_BREAK ),
            NEBULA_ENUM_NAME( SET_RETURN ),            
            NEBULA_ENUM_NAME( SET_ASSIGN ),
            NEBULA_ENUM_NAME( SET_PLUS_ASSIGN ),
            NEBULA_ENUM_NAME( SET_MINUS_ASSIGN ),
            NEBULA_ENUM_NAME( SET_MUL_ASSIGN ),
            NEBULA_ENUM_NAME( SET_DIV_ASSIGN ),
            NEBULA_ENUM_NAME( SET_STMTS ),
            NEBULA_ENUM_NAME( SET_PARAMDECLS ),
            NEBULA_ENUM_NAME( SET_NODE ),

            // callable expr
            NEBULA_ENUM_NAME( SET_CALLABLE ),

            // reference expr
            NEBULA_ENUM_NAME( SET_IDENT ),
            NEBULA_ENUM_NAME( SET_INVOKE )
        };
        using SET = SynthExprType;

        class SynthExpr;
        using SynthExprPtr = std::shared_ptr<SynthExpr>;
        using SynthExprList = nlist<SynthExprPtr>;

        template<uint32 ExprType>
        class SynthExprConcrete;

        struct SynthIdentifier;
        using SynthIdentifierPtr = std::shared_ptr<SynthIdentifier>;
        struct SynthScope;
        using SynthScopePtr = std::shared_ptr<SynthScope>;
        struct SynthContext;

        template<uint32 ExprType, typename... Tn>
        SynthExprPtr makeExpr( Tn&&... args )
        {
            return SynthExprConcrete<ExprType>::create( std::move( args )... );
        };

        // SynthExprType to string
        NEBULA_DEFINE_VALUE_STRING_PAIR( SET );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_UNKNOWN, "<unknown>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_NULL, "<null>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_BOOL, "<bool_literal>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_INT, "<integer_literal>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_FLOAT, "<float_literal>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_STRING, "<string_literal>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_PARENS, "()" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_BRACES, "{}" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_BRACKETS, "[]" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_DOT, "." );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_NEGATE, "<negate>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_PLUS, "+" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_MINUS, "-" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_MUL, "*" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_DIV, "/" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_OR, "||" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_AND, "&&" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_LT, "<" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_GT, ">" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_LE, "<=" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_GE, ">=" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_EQ, "==" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_NE, "!=" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_IF, "<if>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_ELIF, "<elif>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_ELSE, "<else>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_WHILE, "<while>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_BREAK, "<break>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_RETURN, "<return>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_ASSIGN, "=" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_PLUS_ASSIGN, "+=" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_MINUS_ASSIGN, "-=" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_MUL_ASSIGN, "*=" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_DIV_ASSIGN, "/=" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_STMTS, "<stmts>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_PARAMDECLS, "<param_decls>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_NODE, "<node>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_IDENT, "<identifier>" );
        NEBULA_MAKE_VALUE_STRING_PAIR( SET, SET::SET_INVOKE, "<invoke>" );
    }
}

#pragma warning( pop )