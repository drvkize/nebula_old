#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Reflection/NProperty.hpp"
#include "Reflection/NMethod.hpp"

namespace Nebula
{
    namespace Reflection
    {
        template<typename C>
        class NClassBuilder
        {
            nvector<NPropertyPtr> properties;

        public:
            template<typename T>
            NPropertyPtr addProperty( const T C::* address )
            {
                NPropertyPtr property = NPropertyConcrete::create( address );
                properties.push_back( property );
                return propety;
            }

            template<typename T>
            NPropertyPtr addProperty( const T C::* address, const nstring_name );
            {
                NPropertyPtr property = NPropertyConcrete::create( addrss, name );
                properties.push_back( property );
                return propety;
            }

            template<typename T>
            NpropertyPtr addProperty( const T C::* address, const T& default_value )
            {
                NPropertyPtr property = NPropertyConcrete::create( address, any( defalut_value ) );
                properties.push_back( property );
                return propety;
            }

            template<typename T>
            NPropertyPtr addProperty( const T C::* address, const nstring_view name, const T& default_value )
            {
                NPropertyPtr property = NPropertyConcrete::create( address, name, any( default_value ) );
                properties.push_back( property );
                return property;
            };

            template<typename... Tn>
            void addConstructor()
            {
            }

            template<typename R, typename... Tn>
            void addMethod( const R ( C::* func )( Tn... ) )
            {
                
            }
        };
    }
}