#pragma once

#include "System/SystemDefine.hpp"

namespace Nebula
{
    namespace System
    {
        class NEBULA_INTERFACE NebulaDevice
        {        
        public:
            NebulaDevice();
            virtual ~NebulaDevice();

            NebulaDevice( const NebulaDevice& other ) = delete;
            NebulaDevice& operator=( const NebulaDevice& rhs ) = delete;
        };
    }
}