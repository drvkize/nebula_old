#include "Memory/CoalesceBlockTree.hpp"
#include "Memory/OSAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        int CoalesceBlockTree::Node::compare( Node* node, nsize size )
        {
            return node->getDataSize() < size ? -1 : ( node->getDataSize() > size ? 1 : 0 );
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::getMin( Node* node )
        {
            while( node != nullptr && node->left != nullptr )
            {
                node = node->left;
            }
            return node;
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::getMax( Node* node )
        {
            while( node != nullptr && node->right != nullptr )
            {
                node = node->right;
            }
            return node;
        }

        void CoalesceBlockTree::flipColor( Node* node )
        {
            node->flipColor();
            node->left->flipColor();
            node->right->flipColor();
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::rotateLeft( Node* node )
        {
            Node* x = node->right;
            node->right = x->left;
            x->left = node;
            x->color = x->left->color;
            x->left->color = Node::RED;
            return x;
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::rotateRight( Node* node )
        {
            Node* x = node->left;
            node->left = x->right;
			x->right = node;
            x->color = x->right->color;
            x->right->color = Node::RED;
            return x;
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::fixup( Node* node )
        {
            if( node != nullptr )
            {
                if( node->isRightRed() )
                {
                    node = rotateLeft( node );
                }

                if( node->isLeftRed() && node->left->isLeftRed() )
                {
                    node = rotateRight( node );
                }

                if( node->isLeftRed() && node->isRightRed() )
                {
                    flipColor( node );
                }
            }

            return node;
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::moveRedRight( Node* node )
        {
            flipColor( node );
            if( node->left != nullptr && node->left->isLeftRed() )
            {
                node = rotateRight( node );
                flipColor( node );
            }

            return node;
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::moveRedLeft( Node* node )
        {
            flipColor( node );
            if( node->right != nullptr && node->right->isLeftRed() )
            {
                node->right = rotateRight( node->right );
                node = rotateLeft( node );
                flipColor( node );
            }
            return node;
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::createNode( const Block* block )
        {
            assert( block != nullptr && "[CoalesceBlockTree::createNode]: can not create node for null block" );

            Node* node = static_cast<Node*>( node_allocator.allocate( sizeof( Node ), 4 ) );
            if( node != nullptr )
            {
                node->left = nullptr;
                node->right = nullptr;
                node->block = block;
                node->color = Node::RED;
                ++count;
            }

            return node;
        }

        void CoalesceBlockTree::destroyNode( Node* node, nbool destroy_childs )
        {
            if( node != nullptr )
            {
                node_allocator.deallocate( node );
                --count;

				if( destroy_childs )
				{
					if( node->left != nullptr )
					{
						destroyNode( node->left );
					}

					if( node->right != nullptr )
					{
						destroyNode( node->right );
					}
				}
            }
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::insertRecursive( Node* node, const Block* block )
        {
            if( node == nullptr )
            {
                return createNode( block );
            }

            // split 4 nodes
            if( node->isLeftRed() && node->isRightRed() )
            {
                flipColor( node );
            }

            int cmp = Block::compare( block, node->block );
            if( cmp < 0 )
            {
                node->left = insertRecursive( node->left, block );
            }
            else if( cmp > 0 )
            {
                node->right = insertRecursive( node->right, block );
            }
            else
            {
                assert( 0 && "[CoalesceBlockTree::insertRecursive]: block already inserted" );
            }

            // fix right lean red link
            if( node->isRightRed() )
            {
                node = rotateLeft( node );
            }

            // fix two left lean red links in a row
            if( node->isLeftRed() && node->left->isLeftRed() )
            {
                node = rotateRight( node );
            }

            return node;
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::deleteMinRecursive( Node* node )
        {
            if( node->left == nullptr )
            {
                destroyNode( node );
                return nullptr;
            }

            if( node->isLeftBlack() && node->left->isLeftBlack() )
            {
                node = moveRedLeft( node );
            }

            node->left = deleteMinRecursive( node->left );

            return fixup( node );
        }

        CoalesceBlockTree::Node* CoalesceBlockTree::deleteRecursive( Node* node, const Block* block )
        {
            if( node != nullptr )
            {
                int cmp = Block::compare( block, node->block );
                if( cmp < 0 )
                {
                    if( node->isLeftBlack() && node->left->isLeftBlack() )
                    {
                        node = moveRedLeft( node );
                    }

                    node->left = deleteRecursive( node->left, block );
                }
                else
                {
                    // lean 3-nodes right
                    if( node->isLeftRed() )
                    {
                        node = rotateRight( node );
                    }

                    if( cmp == 0 && node->right == nullptr )
                    {
                        destroyNode( node );
                        return nullptr;
                    }

                    if( node->isRightBlack() && node->right->isLeftBlack() )
                    {
                        node = moveRedRight( node );
                    }

					cmp = Block::compare( block, node->block );
                    if( cmp == 0 )
                    {
                        Node* right_min = getMin( node->right );
                        node->block = right_min->block;
                        node->right = deleteMinRecursive( node->right );
                    }
                    else
                    {
                        node->right = deleteRecursive( node->right, block );
                    }
                }

                return fixup( node );
            }

            return nullptr;
        }

        CoalesceBlockTree::CoalesceBlockTree()
            : root( nullptr )
            , count( 0 )
        {
        }

        CoalesceBlockTree::~CoalesceBlockTree()
        {
			count = 0;
			root = nullptr;
        }

        nbool CoalesceBlockTree::init( NebulaAllocator* parent_allocator )
        {
            return node_allocator.init( parent_allocator, sizeof( Node ) );
        }

        void CoalesceBlockTree::release()
        {
			destroyNode( root, true );
			root = nullptr;

			node_allocator.release();
        }

        void CoalesceBlockTree::add( const Block* block )
        {
            root = insertRecursive( root, block );
        }

        void CoalesceBlockTree::remove( const Block* block )
        {
            root = deleteRecursive( root, block );

            if( root != nullptr )
            {
                root->color = Node::BLACK;
            }
        }

        const CoalesceBlockTree::Block* CoalesceBlockTree::find( nsize size, nsize align )
        {
            nsize target_size = align > 1 ? size + align + sizeof( Block ) : size + sizeof( Block );

            Node* node = root;
            Node* last = nullptr;

            while( node != nullptr )
            {
                if( target_size <= node->getDataSize() )
                {
                    last = node;
                    node = node->left;
                }
                else
                {
                    node = node->right;
                }
                /*else if( node->getDataSize() == target_size )
                {
					last = node;
					node = node->left;
                }*/
            }

            return ( last != nullptr ) ? last->block : nullptr;
        }

		void CoalesceBlockTree::printNode( Node* node, int depth ) const
		{
			if (node != nullptr)
			{
				printNode(node->left, depth + 1 );
				printNode(node->right, depth + 1);
			}
		}
    }
}