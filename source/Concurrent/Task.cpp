#include "Concurrent/Task.hpp"
#include "Concurrent/TaskRunner.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        Task::Task()
            : state( TaskState::TS_Initialized )
            , runner( nullptr )
            , update_flag( false )
            , abort_flag( false )
        {
        }

        Task::~Task()
        {
            abort();
        }

        void Task::onStart()
        {
        }

        void Task::onUpdate()
        {
        }

        void Task::onStop()
        {
        }

        void Task::onAbort()
        {
        }

        void Task::abort()
        {
            while( isFinished() )
            {
                abort_flag = true;
            }
        }

        void Task::reset( nbool update_flag )
        {
            if( getState() == TaskState::TS_Ended )
            {
                this->update_flag = update_flag;
                abort_flag = false;
                setState( TaskState::TS_Initialized );
            }
        }

        nsize Task::getThreadID() const
        {
            if( runner )
            {
                return runner->getThreadID();
            }

            return 0;
        }

        void Task::setUpdateFlag( nbool value )
        {
            update_flag = value;
        }

        nbool Task::getUpdateFlag() const
        {
            return update_flag;
        }

        nbool Task::getAbortFlag() const
        {
            return abort_flag;
        }

        void Task::setState( TaskState s )
        {
            state = s;
        }

        Task::TaskState Task::getState() const
        {
            return state;
        }

        nbool Task::isAssigned() const
        {
            return state != TaskState::TS_Initialized;
        }

        nbool Task::isRunning() const
        {
            return state == TaskState::TS_Pending || state == TaskState::TS_Running || state == TaskState::TS_Ending;
        }

        nbool Task::isFinished() const
        {
            return state == TaskState::TS_Ended;
        }
    }
}