#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;

namespace Nebula
{
    namespace Platform
    {
        class timer
        {
            using time_point_t = std::chrono::time_point<std::chrono::high_resolution_clock>;
            time_point_t start;

        public:
            timer()
            {
                reset();
            }

            timer( const timer& other ) noexcept
            {
                *this = other;
            }

            timer( timer&& other ) noexcept
            {
                swap( *this, other );
            }

            inline void friend swap( timer& lhs, timer& rhs ) noexcept
            {
                std::swap( lhs.start, rhs.start );
            }

            ~timer()
            {
            }

            timer& operator=( const timer& rhs )
            {
                start = rhs.start;
                return *this;
            }

            timer& operator=( timer&& rhs ) noexcept
            {
                swap( *this, rhs );
                return *this;
            }

            static time_point_t now()
            {
                return std::chrono::high_resolution_clock::now();
            }

            int64 elapsed_milli() const
            {
                return std::chrono::duration_cast<std::chrono::milliseconds>( now() - start ).count();
            }

            int64 elapsed_micro() const
            {
                return std::chrono::duration_cast<std::chrono::microseconds>( now() - start ).count();
            }

            int64 elapsed_nano() const
            {
                return std::chrono::duration_cast<std::chrono::nanoseconds>( now() - start ).count();
            }

            inline void reset() { start = now(); }
        };
    }
}