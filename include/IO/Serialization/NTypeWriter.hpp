#pragma once

#include "IO/Serialization/NTypeDefine.hpp"
#include "IO/Serialization/NTypeMapper.hpp"
#include "IO/HeteroStream.hpp"
#include "Memory/MemoryBuffer.hpp"
#include "Math/Vector.hpp"
#include "Math/Matrix.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Serialization
        {
            class NEBULA_INTERFACE NTypeWriter
            {
            public:
                NTypeWriter();
                virtual ~NTypeWriter();

                virtual NTypeTag getTag() const = 0;
                virtual nsize serialize( HeteroStreamPtr stream ) = 0;
            };

            // scalar
            template<typename T>
            class NEBULA_INTERFACE NScalarWriter : public NTypeWriter
            {
                NTypeTag tag;
                T data;

            public:
                static NScalarWriterPtr<T> create( const T& value )
                {
                    return NScalarWriterPtr<T>( new NScalarWriter<T>( value ) );
                }

                NScalarWriter( const T& value )
                    : tag( ScalarTypeToTag<T>::Tag )
                    , data( value )
                {
                }

                virtual ~NScalarWriter()
                {
                }

                virtual inline NTypeTag getTag() const override { return tag; }

                virtual nsize serialize( HeteroStreamPtr stream ) override
                {
                    nsize written = 0;
                    written += stream->write( tag );
                    written += stream->write( data );
                    return written;
                };
            };

            // string
            class NEBULA_INTERFACE NStringWriter : public NTypeWriter
            {
                NTypeTag tag;
                nstring data;

            public:
                static NStringWriterPtr create( const nstring& value );

                NStringWriter( const nstring& value );
                virtual ~NStringWriter();

                virtual inline NTypeTag getTag() const override { return tag; }

                virtual nsize serialize( HeteroStreamPtr stream ) override;
            };

            // array
            template<typename T = NTypeWriterPtr>
            class NArrayWriter : public NTypeWriter
            {
            public:
                static constexpr NTypeTag ElementTag = ScalarTypeToTag<T>::Tag;
                static constexpr nbool IsScalarElement = ElementTag != NTypeTag::NTT_Unknown;

            private:
                NTypeTag tag;
                nvector<T> elements;

                template<typename F, nbool IsScalarType>
                struct SerializeHelper {};

                template<typename F>
                struct SerializeHelper<F, true>
                {
                    static nsize serialize( const F& value, HeteroStreamPtr stream )
                    {
                        return stream->write( value );
                    }
                };

                template<typename F>
                struct SerializeHelper<F, false>
                {
                    static nsize serialize( const F& value, HeteroStreamPtr stream )
                    {
                        static_assert( std::is_same<F, NTypeWriterPtr>::value, "[NArrayWriter::SerializeHelper::serialize()]: non scalar type must be NTypeWriterPtr" );
                        return value.serialzie( stream );
                    }
                };

            public:
                static NArrayWriterPtr<T> create();
                static NArrayWriterPtr<T> create( const nvector<T>& value );

                NArrayWriter();
                NArrayWriter( const nvector<T>& value );
                virtual ~NArrayWriter();

                virtual inline NTypeTag getTag() const override { return tag; }

                T add( T element );

                virtual nsize serialize( HeteroStreamPtr stream ) override;
            };

            template<typename T>
            NArrayWriterPtr<T> NArrayWriter<T>::create()
            {
                return NArrayWriterPtr<T>( new NArrayWriter<T>() );
            }

            template<typename T>
            NArrayWriterPtr<T> NArrayWriter<T>::create( const nvector<T>& value )
            {
                return NArrayWriterPtr<T>( new NArrayWriter<T>( value ) );
            }

            template<typename T>
            NArrayWriter<T>::NArrayWriter()
                : tag( NTypeTag::NTT_Array )
            {
            }

            template<typename T>
            NArrayWriter<T>::NArrayWriter( const nvector<T>& value )
                : tag( NTypeTag::NTT_Array )
                , elements( value )
            {
            }

            template<typename T>
            NArrayWriter<T>::~NArrayWriter()
            {
            }

            template<typename T>
            T NArrayWriter<T>::add( T element )
            {
                elements.push_back( element );
                return element;
            }

            template<typename T>
            nsize NArrayWriter<T>::serialize( HeteroStreamPtr stream )
            {
                nsize written = 0;

                // tag
                written += stream->write( tag );

                // size
                int64 size_offset = stream->tell();
                written += stream->write( 0ull );

                // count
                written += stream->write( static_cast<uint64>( elements.size() ) );

                // element_tag
                written += stream->write( ElementTag );

                // elements
                for( nsize i = 0; i < elements.size(); ++i )
                {
                    written += SerializeHelper<T, IsScalarElement>::serialize( elements[i], stream );
                }

                // fill size value
                int64 end_offset = stream->tell();
                stream->seek( SeekOption::SO_Set, size_offset );
                stream->write( static_cast<uint64>( written ) );
                stream->seek( SeekOption::SO_Set, end_offset );

                return written;
            }

            // object
            class NEBULA_INTERFACE NObjectWriter : public NTypeWriter
            {
                NTypeTag tag;
                nmap<nstring, NTypeWriterPtr> members;

            public:
                static NObjectWriterPtr create();

                NObjectWriter();
                virtual ~NObjectWriter();

                virtual inline NTypeTag getTag() const override { return tag; }

                void add( const nstring& name, nbool value );
                void add( const nstring& name, int8 value );
                void add( const nstring& name, int16 value );
                void add( const nstring& name, int32 value );
                void add( const nstring& name, int64 value );
                void add( const nstring& name, uint8 value );
                void add( const nstring& name, uint16 value );
                void add( const nstring& name, uint32 value );
                void add( const nstring& name, uint64 value );
                void add( const nstring& name, nfloat value );
                void add( const nstring& name, ndouble value );
                void add( const nstring& name, const int2& value );
                void add( const nstring& name, const int3& value );
                void add( const nstring& name, const int4& value );
                void add( const nstring& name, const float2& value );
                void add( const nstring& name, const float3& value );
                void add( const nstring& name, const float4& value );
                void add( const nstring& name, const float2x2& value );
                void add( const nstring& name, const float3x3& value );
                void add( const nstring& name, const float4x4& value );
                void add( const nstring& name, const float4x3& value );
                void add( const nstring& name, const double2& value );
                void add( const nstring& name, const double3& value );
                void add( const nstring& name, const double4& value );
                void add( const nstring& name, const double2x2& value );
                void add( const nstring& name, const double3x3& value );
                void add( const nstring& name, const double4x4& value );
                void add( const nstring& name, const double4x3& value );
                void add( const nstring& name, const nstring& value );
                inline void add( const nstring& name, const nchar* value )
                {
                    add( name, nstring( value ) );
                }

                template<typename T>
                NArrayWriter<T>& addArray( const nstring& name )
                {
                    NArrayWriterPtr<T> writer = NArrayWriter<T>::create();
                    members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, writer ) );
                    return *writer;
                }

                template<typename T>
                void add( const nstring& name, const nvector<T>& value )
                {
                    NArrayWriterPtr<T> writer = NArrayWriter<T>::create( value );
                    members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, writer ) );
                }

                NObjectWriter& addObject( const nstring& name );
                void addBinary( const nstring& name, const int8* data, nsize size );

                virtual nsize serialize( HeteroStreamPtr stream ) override;
            };

            class NEBULA_INTERFACE NBinaryWriter : public NTypeWriter
            {
                NTypeTag tag;
                Memory::MemoryBuffer<int8> data;

            public:
                static NBinaryWriterPtr create( const int8* value, nsize size );

                NBinaryWriter( const int8* value, nsize size );
                virtual ~NBinaryWriter();

                virtual inline NTypeTag getTag() const override { return tag; }

                virtual nsize serialize( HeteroStreamPtr stream ) override;
            };
        }
    }
}