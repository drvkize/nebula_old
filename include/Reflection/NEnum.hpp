#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Reflection/NType.hpp"

namespace Nebula
{
    namespace Reflection
    {
        class NEnum
        {
            uint32 name;
            NType type;

        public:
            NEnum();
            ~NEnum();
        };
    }
}