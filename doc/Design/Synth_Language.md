# Synthetic Language
## Design Goal
A data driven paradigm language using descriptive syntax. Grammar is a hierachy of function like( nodes ). All identifers include function names are considered state of the parent function.

The compiler will track the node when a state was modified or referenced, and generate a graph showing the data stream. The graph can also be generated from Synth Graph Editor directly.

With the graph, compiler will apply optimization and generate C++, Python or shader language code if there is a supported back end. Data exchagne between runtime or hardware devices will be handled by **Nebula** internally.
## Code Snippet
Node without side effect ( no state was modified )
```synth
add = ( a, b ){
    return a + b;
}
```
In the example above, the state "add_count" was not used, however in Synth all state can be accessed unless they have "private" keyword, which means "add_count" may changed by other node.
The Synth Graph for "add" is:
```graphviz
digraph node_without_side_effect {
    subgraph cluster_node_add {
        style = filled;
        color = dimgrey;
        fontsize = 12;
        fontcolor = white;
        
        label="Add"

        add [shape=record label="{a|b}|return" style=filled fillcolor=deepskyblue fontcolor=white]
    }
}
```
Node with side effect
```synth
file = {
    file_handle = null;

    open( path, mode ) {
        file_handle = system.bindings( "StdFile::Create", path, mode );
    }

    read_line() {
        return system.synth.bindings( "StdFile::readLine", file_handle );
    }
}
```
Notice, in above example, "open" and "read_line" are child node of file. The state "file_handle" was tracked by compiler when it is modified( in "open" ) or referenced( in "read_line" ). Also if the called function or "system.synth.bindings" has side effect, the callee function is also considered to have side effect. In the example above, node "read_line" has side effect because the binding function "StdFile::readLine" change read pointer everytime it was invoked.
The Synth Graph for "file" is:
```graphviz
digraph node_file {
    subgraph cluster_node_file {
        style = filled;
        color = dimgrey;
        fontsize = 12;
        fontcolor = white;
        
        label="file"

        add [shape=record label="file_handle" style=filled fillcolor=deepskyblue fontcolor=white]
    }
}
```
The Synth Graph for "file.open" is:
```graphviz
digraph node_file_open {
    subgraph cluster_node_file_open {
        style = filled;
        color = dimgrey;
        fontsize = 12;
        fontcolor = white;
        
        label="file.open"

        add [shape=record label="{path|mode}|file_handle" style=filled fillcolor=deepskyblue fontcolor=white]
    }
}
```
The Synth Graph for "file.read_line" is:
```graphviz
digraph node_file_read_line {
    subgraph cluster_node_file_read_line {
        style = filled;
        color = dimgrey;
        fontsize = 12;
        fontcolor = white;
        
        label="file.read_line"

        add [shape=record label="file_handle|return" style=filled fillcolor=deepskyblue fontcolor=white]
    }
}
```
Following is a complete example of linear regression in Synth
```synth
# node with side effect ( state preserved, because file_handle preseve state of file also because c++ bind invokation )
file = {
    file_handle = null;

    # node with side effect
    open = ( path, mode ){
        file_handle = system.synth.bindings( "StdFile::Create", path, mode );
    }

    read_line(){
        return system.synth.bindings( "StdFile::readLine", file_handle );
    }
}

# node without side effect ( no state preserved )
data_loader = ( path ){

    data_file = file();
    data_file.load( path );

    x = [];
    y = [];

    line = data_file.redata_loader
    while( line != "" )data_loader
    {
        segments = line.split( "," );
        x.append( (float)( segments[0] ) );
        y.append( (float)( segments[1] ) );
        line = data_file.read_line();
    }

    return x, y;
}

# pure data object
linear_relation_1d = ( v0, v1 ){    
    a = v0;
    b = v1;
}

# node without side effect ( no state preserved )
linear_regression = (){
    train = ( x, y ){
        x.sum()
        mx = sx / x.len();
        my = y.mean();
        dxy = dot( x, y );
        dxx = dot( x, x );

        denom = sx * mx - dxx;
        a = ( sx * my - dxy ) / denom;
        b = ( dxy * mx - my* dxx ) / demom;
        
        return linear_1d( a, b );
    }

    predict = ( l, x ){
        return x * l.a + l.b;
    }

    evaluate( l, x, yhat ){
        y = predict( l, x );
        d1 = y - yhat;
        d2 = y - y.mean();
        r2 = 1 - dot( d1, d1 ) / dot( d2, d2 )
        return r2;
    }
}

main = (){
    train_path = "train_1d.csv";
    evaluate_path = "evaluate_1d.csv";

    x, y = data_loader( train_path );
    # equivalent to linear_regression().train( x, y );
    lr = linear_regression.train( x, y );

    x, y = data_loader( evaludate_path );
    result = linear_regression.evaluate( lr, x, y );
    system.print( "r2 of linear regression is: {0}", result );
}
```
The Synth Graph for above code is:
```graphviz
digraph linear_regression_example {
    rankdir=LR
    node [shape=record label="train_path" style=filled fillcolor=deepskyblue fontcolor=white]

    subgraph cluster_main {
        style = filled;
        color = gray17;
        fontsize = 12;
        fontcolor = white;
        label="main"

        subgraph cluster_string_train_path {
            color = dimgrey;
            label = "string"
            train_path_value [label="\"train_1d.csv\""]
        }

        subgraph cluster_string_evaluate_path {
            color = dimgrey;
            label="string"
            evaluate_path_value [label="\"evaluate_1d.csv\"" ]
        }

        subgraph cluster_data_loader_train {
            color = dimgrey;
            label="data_loader"
            loader_train [shape=record label="{path|{return_x|return_y}}" ]
        } 

        subgraph cluster_linear_regression_train {
            color = dimgrey;
            label="linear_regression.train"
            lr_train [shape=record label="{{x|y}|{return_a|return_b}}" ]
        }

        subgraph cluster_linear_relation_1d {
            color = dimgrey;
            label="linear_relation_1d"
            ab [shape=record label="a|b" ]
        }

        subgraph cluster_data_loader_evaluate {
            color = dimgrey;
            label="data_loader"
            loader_evaluate [shape=record label="{path|{return_x|return_y}}" ]
        }

        subgraph cluster_linear_regression_evaluate {
            color = dimgrey;
            label="linear_regression.train"
            lr_evaluate [shape=record label="{{{lr|{a|b}}|x|yhat}|return_r2}" ]
        }

        subgraph cluster_system_print {
            color = dimgrey;
            label = "system.print"
            print [shape=record label="string" ]
        }

        edge [color=darkorange]
        train_path_value -> loader_train -> lr_train -> ab -> lr_evaluate
        evaluate_path_value -> loader_evaluate -> lr_evaluate
        lr_evaluate -> print
    }
}
```

