#pragma once

#include "Concurrent/ConcurrentDefine.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        template<typename T>
        class MPMCQueue
        {
        public:
            static constexpr nsize CacheLineSize = 128;

            static_assert(
                std::is_nothrow_copy_assignable<T>::value
                || std::is_nothrow_move_assignable<T>::value,
                "[MPMCQueue]: T must be nothrow copy or move assignable"
            );

            static_assert(
                std::is_nothrow_destructible<T>::value,
                "[MPMCQueue]: T must be nothrow destructible"
            );

            struct Slot
            {
                alignas( CacheLineSize ) std::atomic<nsize> turn = { 0 };
                using storage_t = typename std::aligned_storage<sizeof( T ), alignof( T )>::type;

                storage_t storage;

                ~Slot() noexcept
                {
                    if( turn & 1 )
                    {
                        destroy();
                    }
                }

                template<typename... Tn>
                void construct( Tn&&... args ) noexcept
                {
                    static_assert(
                        std::is_nothrow_constructible<T, Tn&&...>::value,
                        "[MPMCQueue]: T must be nothrow constructible with Tn&&..."
                    );

                    new( &storage ) T ( std::forward<Tn>( args )... );
                }

                void destroy() noexcept
                {
                    static_assert(
                        std::is_nothrow_destructible<T>::value,
                        "[MPMCQueue]: T must be nothrow destructible"
                    );

                    reinterpret_cast<T*>( &storage )->~T();
                }

                T&& move() noexcept
                {
                    return reinterpret_cast<T&&>( storage );
                }
            };

            const nsize capacity;
            Slot* slots;
            void* buf;

            alignas( CacheLineSize ) std::atomic<nsize> head;
            alignas( CacheLineSize ) std::atomic<nsize> tail;

            constexpr nsize idx( nsize i ) const noexcept { return i % capacity; }
            constexpr nsize turn( nsize i ) const noexcept { return i / capacity; }

        public:
            explicit MPMCQueue( const nsize capacity )
                : capacity( capacity )
                , head( 0 )
                , tail( 0 )
            {
                nassert( capacity > 1, "[MPMCQueue::MPMCQueue()]: capacity must be greater than 1" );

                nsize space = capacity * sizeof( Slot ) + CacheLineSize - 1;
                buf = malloc( space );
                nassert( buf, "[MPMCQueue::MPMCQueue()]: allocate buffer failed" );

                void* buff_aligned = buf;
                slots = reinterpret_cast<Slot*>( std::align( CacheLineSize, capacity * sizeof( Slot ), buff_aligned, space ) );
                if( slots == nullptr )
                {
                    free( buf );
                    buf = nullptr;
                    nassert( 0, "[MPMCQueue::MPMCQueue()]: align slots failed" );
                }

                for( nsize i = 0; i < capacity; ++i )
                {
                    new( &slots[i] ) Slot();
                }

                static_assert(
                    sizeof( MPMCQueue<T> ) % CacheLineSize == 0,
                    "[MPMCQueue::MPMCQueue()]: size must be a multiple of cache line size to prevent false sharing between adjacent queues"
                );

                static_assert(
                    sizeof( Slot ) % CacheLineSize == 0,
                    "[MPMCQueue::MPMCQueue()]: slot size must be a multiple of cache line size to prevent false sharing between adjacent slots"
                );

                nassert(
                    ( reinterpret_cast<int8*>( &tail ) - reinterpret_cast<int8*>( &head ) >= CacheLineSize ),
                    "[MPMCQueue::MPMCQueue()]: head and tail must be a chche line apart to prevent false sharing"
                );
            }

            ~MPMCQueue() noexcept
            {
                for( nsize i = 0; i < capacity; ++i )
                {
                    slots[i].~Slot();
                }

                free( buf );
                buf = nullptr;
            }

            MPMCQueue( const MPMCQueue& other ) = delete;
            MPMCQueue& operator=( const MPMCQueue& rhs ) = delete;

            template<typename... Tn>
            void emplace( Tn&&... args ) noexcept
            {
                static_assert( std::is_nothrow_constructible<T, Tn&&...>::value,
                "[MPMCQueue::emplace()]: T must be nothrow constructible with Tn&&..."
                );

                auto const h = head.fetch_add( 1 );
                auto& slot = slots[idx( h )];
                while( turn( h ) * 2 != slot.turn.load( std::memory_order_acquire ) );
                slot.construct( std::forward<Tn>( args )... );
                slot.turn.store( turn( h ) * 2 + 1, std::memory_order_release );
            }

            template<typename... Tn>
            nbool tryEmplace( Tn&&... args ) noexcept
            {
                static_assert( std::is_nothrow_constructible<T, Tn&&...>::value,
                "[MPMCQueue::tryEmplace()]: T must be nothrow constructible with Tn&&..."
                );

                auto h = head.load( std::memory_order_acquire );
                while( true )
                {
                    auto& slot = slots[idx( h )];
                    if( turn( h ) * 2 == slot.turn.load( std::memory_order_acquire ) )
                    {
                        if( head.compare_exchange_strong( h, h + 1 ) )
                        {
                            slot.construct( std::forward<Tn>( args )... );
                            slot.turn.store( turn( h ) * 2 + 1, std::memory_order_release );
                            return true;
                        }
                    }
                    else
                    {
                        auto const prevHead = h;
                        h = head.load( std::memory_order_acquire );
                        if( h == prevHead )
                        {
                            return false;
                        }                        
                    }
                }
            }

            void push( const T& v ) noexcept
            {
                static_assert(
                    std::is_nothrow_copy_constructible<T>::value,
                    "[MPMCQueue::push()]: T must be nothrow copy constructible"
                );

                emplace( v );
            }

            template<typename P, typename = typename std::enable_if<std::is_nothrow_constructible<T, P&&>::value>::type>
            void push( P&& v ) noexcept
            {
                emplace( std::forward<P>( v ) );
            }

            nbool tryPush( const T& v ) noexcept
            {
                static_assert(
                    std::is_nothrow_copy_constructible<T>::value,
                    "[MPMCQueue::tryPush()]: T must be nothrow copy constructible"
                );

                return tryEmplace( v );
            }

            template<typename P, typename = typename std::enable_if<std::is_nothrow_constructible<T, P&&>::value>::type>
            nbool tryPush( const P& v ) noexcept
            {
                return tryEmplace( std::forward<P>( v ) );
            }

            void pop( T& v ) noexcept
            {
                auto const t = tail.fetch_add( 1 );
                auto& slot = slots[idx( t ) ];
                while( turn( t ) * 2 + 1 != slot.turn.load( std::memory_order_acquire ) );
                v = slot.move();
                slot.destroy();
                slot.turn.store( turn( t ) * 2 + 2, std::memory_order_release );
            }

            nbool tryPop( T& v ) noexcept
            {
                auto t = tail.load( std::memory_order_acquire );
                while( true )
                {
                    auto& slot = slots[idx( t )];
                    if( turn( t ) * 2 + 1 == slot.turn.load( std::memory_order_acquire ) )
                    {
                        if( tail.compare_exchange_strong( t, t + 1 ) )
                        {
                            v = slot.move();
                            slot.destroy();
                            slot.turn.store( turn( t ) * 2 + 2, std::memory_order_release );
                            return true;
                        }
                    }
                    else
                    {
                        auto const prevTail = t;
                        t = tail.load( std::memory_order_acquire );
                        if( t == prevTail )
                        {
                            return false;
                        }
                    }
                }                
            }

            nbool isEmpty() const { return tail == head; }
        };
    }
}