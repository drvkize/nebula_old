#pragma once

#include "Memory/MemoryDefine.hpp"
#include "Memory/NebulaAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        template<typename T>
        struct nallocator
        {
            using value_type = T;
            using pointer_type = T*;
        
            using propagate_on_container_copy_assignment = std::true_type;
            using propagate_on_container_move_assignment = std::true_type;
            using propagate_on_container_swap = std::true_type;

            explicit nallocator() noexcept {}

            template<typename U>
            nallocator( const nallocator<U>& ) noexcept {}

            pointer_type allocate( nsize n )
            {
				return static_cast<value_type*>( Context::getSystemAllocator().allocate( n * sizeof( value_type ) ) );
            }

            void deallocate( value_type* p, nsize size = 0 ) noexcept
            {
                Context::getSystemAllocator().deallocate( p );
            }
        };

        template<class T, class U>
        nbool operator==( const nallocator<T>&, const nallocator<U>& ) noexcept
        {
            return true;
        }

        template<typename T, class U>
        nbool operator!=( const nallocator<T>& x, const nallocator<U>& y ) noexcept
        {
            return false;
        }
    }
}