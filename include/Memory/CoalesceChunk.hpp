#pragma once

#include "Memory/MemoryDefine.hpp"
#include "CoalesceBlock.hpp"
#include "CoalesceBlockTree.hpp"

namespace Nebula
{
    namespace Memory
    {
        class CoalesceChunk
        {
        public:
            CoalesceChunk( const CoalesceChunk& other ) = delete;
            CoalesceChunk& operator=( const CoalesceChunk& rhs ) = delete;

        private:
            using Block = CoalesceBlock;

            NebulaAllocator* parent_allocator;
            CoalesceBlockTree* index;
            int8* address;
            int8* data_address;
            nsize data_size;

        public:
            CoalesceChunk();
            CoalesceChunk( CoalesceChunk&& other ) noexcept;
            ~CoalesceChunk();

            void move( CoalesceChunk&& rhs ) noexcept;
            friend void swap( CoalesceChunk& lhs, CoalesceChunk& rhs ) noexcept;
            CoalesceChunk& operator=( CoalesceChunk&& rhs ) noexcept;

            nbool init( NebulaAllocator* parent_allocator, nsize size, nsize align, CoalesceBlockTree* index );
            void release();

            Block* split( Block* block, nsize size, nsize align );
            void merge( Block* block );

			inline Block* getFirstBlock() { return reinterpret_cast<Block*>( data_address )->next; }
            inline const Block* getFirstBlock() const { return reinterpret_cast<const Block*>( data_address )->next; }
			inline Block* getLastBlock() { return reinterpret_cast<Block*>( data_address + data_size - sizeof(Block) ); }
			inline const Block* getLastBlock() const { return reinterpret_cast<const Block*>( data_address + data_size - sizeof( Block ) ); }
            inline const int8* getStartAddress() { return getFirstBlock()->getData(); }
            inline nbool isEmpty() const { return getFirstBlock()->getBlockSize() + 2 * sizeof( Block ) == data_size; }
            inline nbool isAllocated( void* p ) { return p >= getStartAddress() && p < data_address + data_size - sizeof( Block ); }

			// debug 
			void printBlocks() const;
        };
    }
}