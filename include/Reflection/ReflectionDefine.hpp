#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;

namespace Nebula
{
    namespace Reflection
    {
        class NType;
        class NEnum;
        class NParameter;
        class NProperty;
        class NFunction;
        class NConstructor;
        class NMethod;
        class NClass;
        struct NMetadata;

        template<typename F>
        class NFunctionBuilder;
        template<typename C, typename... Tn>
        class NConstructorBuilder;
        template<typename F>
        class NMethodBuilder;

        using NEnumPtr = std::unique_ptr<NEnum>;
        using NParameterPtr = std::unique_ptr<NParameter>;
        using NPropertyPtr = std::unique_ptr<NProperty>;
        using NFunctionPtr = std::unique_ptr<NFunction>;
        using NConstructorPtr = std::unique_ptr<NConstructor>;
        using NMethodPtr = std::unique_ptr<NMethod>;
        using NMetadataPtr = std::unique_ptr<NMetadata>;

        template<typename F>
        using NFunctionBuilderPtr = std::unique_ptr<NFunctionBuilder<F>>;
        template<typename C, typename... Tn>
        using NConstructorBuilderPtr = std::unique_ptr<NConstructorBuilder<C, Tn...>>;
        template<typename F>
        using NMethodBuilderPtr = std::unique_ptr<NMethodBuilder<F>>;
    }
}