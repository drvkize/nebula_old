#pragma once

#include "Concurrent/ConcurrentDefine.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        class NEBULA_INTERFACE Task
        {
        public:
            enum TaskState
            {
                TS_Initialized,
                TS_Assigned,
                TS_Pending,
                TS_Running,
                TS_Ending,
                TS_Ended
            };

            Task( const Task& other ) = delete;
            Task& operator=( const Task& rhs ) = delete;

        protected:
            friend class TaskManager;
            friend class TaskRunner;

            std::atomic<TaskState> state;
            TaskRunner* runner;

            nbool update_flag;
            nbool abort_flag;

            inline void setRunner( TaskRunner* runner ) { this->runner = runner; }
            void setState( TaskState new_state );

        public:
            Task();
            virtual ~Task();

            virtual void onStart();
            virtual void onUpdate();
            virtual void onStop();
            virtual void onAbort();

            void abort();
            void reset( nbool update_flag );

            nsize getThreadID() const;
            void setUpdateFlag( nbool value );
            nbool getUpdateFlag() const;
            nbool getAbortFlag() const;
            TaskState getState() const;
            nbool isAssigned() const;
            nbool isRunning() const;
            nbool isFinished() const;
        };
    }
}