%require "3.0.4"

%{
#pragma warning( push )
#pragma warning( disable: 4065 )
%}

%defines
%skeleton "lalr1.cc"
%define parser_class_name {SynthParser}
%define api.namespace {Nebula::Synth}
%define api.token.constructor
%define api.value.type variant
%define parse.assert
%define parse.error verbose
%locations

%code requires
{
    #include "Synth/SynthDefine.hpp"
    #include "Synth/SynthExpr.hpp"
}

%param { Nebula::Synth::SynthContext& ctx }

%code
{
    #include "Synth/SynthContext.hpp"
    #define M( x ) std::move( x )

    namespace Nebula
    {
        namespace Synth
        {
            SynthParser::symbol_type yylex( SynthContext& ctx );

            void SynthParser::error( const SynthParser::location_type& l, const std::string& m )
            {
                printf( "%s: (%d,%d)-(%d,%d), %s\n", l.begin.filename != nullptr ? l.begin.filename->c_str() : "(unknown file)", l.begin.line, l.begin.column, l.end.line, l.end.column, m.c_str() );
            }
        }
    }
}

%token END 0 "end of file"
%token NNULL "null" TRUE "true" FALSE "false"
%token <nstring> IDENTIFIER STRING_LITERAL
%token <int32> INT_LITERAL
%token <nfloat> FLOAT_LITERAL
%token DOT "." OR "||" AND "&&" LT "<" GT ">" LE "<=" GE ">=" EQ "==" NE "!="
%token PLUS "+" MINUS "-" MUL "*" DIV "/"
%token IF "if" ELIF "elif" ELSE "else" WHILE "while" BREAK "break" RETURN "return"
%token ASSIGN "=" PLUS_ASSIGN "+=" MINUS_ASSIGN "-=" MUL_ASSIGN "*=" DIV_ASSIGN "/="

%precedence IF
%precedence ELIF
%precedence ELSE
%left ','
%right ASSIGN PLUS_ASSIGN MINUS_ASSIGN MUL_ASSIGN DIV_ASSIGN
%left OR
%left AND
%left LT GT LE GE EQ NE
%left PLUS MINUS
%left MUL DIV
%right UMINUS
%left DOT

%type <SynthExprPtr> null identifier bool_literal int_literal float_literal string_literal
%type <SynthExprPtr> parens_expr_begin parens_expr_begin1 parens_expr
%type <SynthExprPtr> brackets_expr_begin brackets_expr_begin1 brackets_expr
%type <SynthExprPtr> lbrace empty_braces braces_expr_begin braces_expr
%type <SynthExprPtr> invoke_expr
%type <SynthExprPtr> stmt_begin stmts stmt
%type <SynthExprPtr> node_expr
%type <SynthExprPtr> expr
%type <SynthExprPtr> root_node

%%
root_node:              identifier ASSIGN node_expr             { $$ = makeExpr<SET::SET_ASSIGN>( M( $1 ), M( $3 ) ); ctx.root_expr = $$; };

lbrace:                 '{'                                     { $$ = nullptr; };
empty_braces:           lbrace '}'                              { $$ = nullptr; };
stmt_begin:             lbrace stmt                             { if( $1 == nullptr ) $1 = makeExpr<SET::SET_STMTS>(); $1->addExpr( M( $2 ) ); $$ = $1; }
|                       stmt_begin stmt                         { $1->addExpr( M( $2 ) ); $$ = $1; };
stmts:                  stmt_begin '}'                          { $$ = $1; };
stmt:                   stmts                                   { $$ = $1; }
|                       identifier ASSIGN expr ';'              { $$ = makeExpr<SET::SET_ASSIGN>( M( $1 ), M( $3 ) ); }
|                       identifier ASSIGN node_expr             { $$ = makeExpr<SET::SET_ASSIGN>( M( $1 ), M( $3 ) ); }
|                       identifier PLUS_ASSIGN expr ';'         { $$ = makeExpr<SET::SET_PLUS_ASSIGN>( M( $1 ), M( $3 ) ); }
|                       identifier MINUS_ASSIGN expr ';'        { $$ = makeExpr<SET::SET_MINUS_ASSIGN>( M( $1 ), M( $3 ) ); }
|                       identifier MUL_ASSIGN expr ';'          { $$ = makeExpr<SET::SET_MUL_ASSIGN>( M( $1 ), M( $3 ) ); }
|                       identifier DIV_ASSIGN expr ';'          { $$ = makeExpr<SET::SET_DIV_ASSIGN>( M( $1 ), M( $3 ) ); }
|                       IF '(' expr ')' stmt                    { $$ = makeExpr<SET::SET_IF>( M( $3 ), M( $5 ) ); }
|                       ELIF '(' expr ')' stmt                  { $$ = makeExpr<SET::SET_ELIF>( M( $3 ), M( $5 ) ); }
|                       ELSE stmt                               { $$ = makeExpr<SET::SET_ELSE>( M( $2 ) ); }
|                       WHILE '(' expr ')' stmt                 { $$ = makeExpr<SET::SET_WHILE>( M( $3 ), M( $5 ) ); }
|                       BREAK ';'                               { $$ = makeExpr<SET::SET_BREAK>(); }
|                       RETURN expr ';'                         { $$ = makeExpr<SET::SET_RETURN>( M( $2 ) ); }
|                       RETURN ';'                              { $$ = makeExpr<SET::SET_RETURN>( nullptr ); }
|                       expr ';'                                { $$ = $1; }
|                       ';'                                     {};

braces_expr_begin:      lbrace expr                             { if( $1 == nullptr ) $1 = makeExpr<SET::SET_BRACES>(); $1->addExpr( M( $2 ) ); $$ = $1; }
|                       braces_expr_begin ',' expr              { $1->addExpr( M( $3 ) ); $$ = $1; };
braces_expr:            braces_expr_begin '}'                   { $$ = $1; }
|                       empty_braces                            { $$ = makeExpr<SET::SET_BRACES>(); };

parens_expr_begin:      '('                                     { $$ = makeExpr<SET::SET_PARENS>(); };
parens_expr_begin1:     parens_expr_begin expr                  { $1->addExpr( M( $2 ) ); $$ = $1; }
|                       parens_expr_begin1 ',' expr             { $1->addExpr( M( $3 ) ); $$ = $1; };
parens_expr:            parens_expr_begin ')'                   { $$ = $1; }
|                       parens_expr_begin1 ')'                  { $$ = $1; };

brackets_expr_begin:    '['                                     { $$ = makeExpr<SET::SET_BRACKETS>(); };
brackets_expr_begin1:   brackets_expr_begin expr                { $1->addExpr( M( $2 ) ); $$ = $1; }
|                       brackets_expr_begin1 ',' expr           { $1->addExpr( M( $3 ) ); $$ = $1; };
brackets_expr:          brackets_expr_begin ']'                 { $$ = $1; }
|                       brackets_expr_begin1 ']'                { $$ = $1; };

invoke_expr:            identifier parens_expr                  { $$ = makeExpr<SET::SET_INVOKE>( M( $1 ), M( $2 ) ); };

expr:                   null                                    { $$ = $1; }
|                       bool_literal                            { $$ = $1; }
|                       int_literal                             { $$ = $1; }
|                       float_literal                           { $$ = $1; }
|                       string_literal                          { $$ = $1; }
|                       identifier                              { $$ = $1; }
|                       parens_expr                             { $$ = $1; }
|                       brackets_expr                           { $$ = $1; }
|                       braces_expr                             { $$ = $1; }
|                       invoke_expr                             { $$ = $1; }
|                       expr DOT identifier                     { $$ = makeExpr<SET::SET_DOT>( M( $1 ), M( $3 ) ); }
|                       MINUS expr              %prec UMINUS    { $$ = makeExpr<SET::SET_NEGATE>( M( $2 ) ); }
|                       expr PLUS expr                          { $$ = makeExpr<SET::SET_PLUS>( M( $1 ), M( $3 ) ); }
|                       expr MINUS expr                         { $$ = makeExpr<SET::SET_MINUS>( M( $1 ), M( $3 ) ); }
|                       expr MUL expr                           { $$ = makeExpr<SET::SET_MUL>( M( $1 ), M( $3 ) ); }
|                       expr DIV expr                           { $$ = makeExpr<SET::SET_DIV>( M( $1 ), M( $3 ) ); }
|                       expr LT expr                            { $$ = makeExpr<SET::SET_LT>( M( $1 ), M( $3 ) ); }
|                       expr GT expr                            { $$ = makeExpr<SET::SET_GT>( M( $1 ), M( $3 ) ); }
|                       expr LE expr                            { $$ = makeExpr<SET::SET_LE>( M( $1 ), M( $3 ) ); }
|                       expr GE expr                            { $$ = makeExpr<SET::SET_GE>( M( $1 ), M( $3 ) ); }
|                       expr EQ expr                            { $$ = makeExpr<SET::SET_EQ>( M( $1 ), M( $3 ) ); }
|                       expr NE expr                            { $$ = makeExpr<SET::SET_NE>( M( $1 ), M( $3 ) ); }
|                       expr AND expr                           { $$ = makeExpr<SET::SET_AND>( M( $1 ), M( $3 ) ); }
|                       expr OR expr                            { $$ = makeExpr<SET::SET_OR>( M( $1 ), M( $3 ) ); };

null:                   NNULL                                   { $$ = makeExpr<SET::SET_NULL>(); };
bool_literal:           TRUE                                    { $$ = makeExpr<SET::SET_BOOL>( true ); }
|                       FALSE                                   { $$ = makeExpr<SET::SET_BOOL>( false ); };
int_literal:            INT_LITERAL                             { $$ = makeExpr<SET::SET_INT>( M( $1 ) ); };
float_literal:          FLOAT_LITERAL                           { $$ = makeExpr<SET::SET_FLOAT>( M( $1 ) ); };
string_literal:         STRING_LITERAL                          { $$ = makeExpr<SET::SET_STRING>( M( $1 ) ); };
identifier:             IDENTIFIER                              { $$ = makeExpr<SET::SET_IDENT>( M( $1 ) ); };

node_expr:              parens_expr stmts                       { $$ = makeExpr<SET::SET_NODE>( M( $1 ), M( $2 ) ); }
|                       parens_expr empty_braces                { $$ = makeExpr<SET::SET_NODE>( M( $1 ), nullptr ); };
%%

namespace Nebula
{
    namespace Synth
    {
        SynthParser::symbol_type yylex( SynthContext& ctx )
        {
            const nchar* anchor = ctx.cursor;
            auto makeSymbol = [&]( auto func, auto&&... params ) { ctx.loc.columns( ctx.cursor - anchor ); return func( params..., ctx.loc ); };

            %{ /*re2c*/
                re2c:yyfill:enable = 0;
                re2c:define:YYCTYPE = "nchar";
                re2c:define:YYCURSOR = "ctx.cursor";
                re2c:define:YYMARKER = "ctx.marker";

                // location
                "\r\n" | [\r\n]         { ctx.loc.lines(); return yylex( ctx ); }
                [\t\v\b\f ]             { ctx.loc.columns(); return yylex( ctx ); }
                // end of file
                "\000"                  { return makeSymbol( &SynthParser::make_END ); }
                // comment
                "#" [^\r\n]*            { return yylex( ctx ); }

                // null literal
                "null"                  { return makeSymbol( &SynthParser::make_NNULL ); }
                // bool literal
                "true"                  { return makeSymbol( &SynthParser::make_TRUE ); }
                "false"                 { return makeSymbol( &SynthParser::make_FALSE ); }
                // int literal
                [-]?[0-9]+              { return makeSymbol( &SynthParser::make_INT_LITERAL, std::stol( std::string( anchor, ctx.cursor ) ) ); }
                // float literal
                [+-]?[0-9]*'.'[0-9]+    { return makeSymbol( &SynthParser::make_FLOAT_LITERAL, std::stof( std::string( anchor, ctx.cursor ) ) ); }
                // string literal
                "\"" [^"]* "\""         { return makeSymbol( &SynthParser::make_STRING_LITERAL, nstring( anchor + 1, ctx.cursor - 1 ) ); }
                
                // operator
                "."                     { return makeSymbol( &SynthParser::make_DOT ); }
                "+"                     { return makeSymbol( &SynthParser::make_PLUS ); }
                "-"                     { return makeSymbol( &SynthParser::make_MINUS ); }
                "*"                     { return makeSymbol( &SynthParser::make_MUL ); }
                "/"                     { return makeSymbol( &SynthParser::make_DIV ); }
                "&&"                    { return makeSymbol( &SynthParser::make_AND ); }
                "||"                    { return makeSymbol( &SynthParser::make_OR ); }
                ">"                     { return makeSymbol( &SynthParser::make_GT ); }
                "<"                     { return makeSymbol( &SynthParser::make_LT ); }
                ">="                    { return makeSymbol( &SynthParser::make_GE ); }
                "<="                    { return makeSymbol( &SynthParser::make_LE ); }
                "=="                    { return makeSymbol( &SynthParser::make_EQ ); }
                "!="                    { return makeSymbol( &SynthParser::make_NE ); }

                // keywords
                "="                     { return makeSymbol( &SynthParser::make_ASSIGN ); }
                "+="                    { return makeSymbol( &SynthParser::make_PLUS_ASSIGN ); }
                "-="                    { return makeSymbol( &SynthParser::make_MINUS_ASSIGN ); }
                "*="                    { return makeSymbol( &SynthParser::make_MUL_ASSIGN ); }
                "/="                    { return makeSymbol( &SynthParser::make_DIV_ASSIGN ); }
                "if"                    { return makeSymbol( &SynthParser::make_IF ); }
                "elif"                  { return makeSymbol( &SynthParser::make_ELIF ); }
                "else"                  { return makeSymbol( &SynthParser::make_ELSE ); }
                "while"                 { return makeSymbol( &SynthParser::make_WHILE ); }
                "break"                 { return makeSymbol( &SynthParser::make_BREAK ); }
                "return"                { return makeSymbol( &SynthParser::make_RETURN ); }

                // identifers
                [a-zA-Z_][a-zA-Z_0-9]*  { return makeSymbol( &SynthParser::make_IDENTIFIER, nstring( anchor, ctx.cursor ) ); }

                // pass though other characters
                .                       { return makeSymbol( []( auto... params ) { return SynthParser::symbol_type( params... ); }, typename SynthParser::token_type( ctx.cursor[-1] & 0xff ) ); }
            %}
        }
    }
}

#pragma warning( pop )