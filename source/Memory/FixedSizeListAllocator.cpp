#include "Memory/FixedSizeListAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        FixedSizeListAllocator::Node::Node()
            : data( nullptr )
        {
        }

        FixedSizeListAllocator::Node::~Node()
        {
        }

        FixedSizeListAllocator::FixedSizeListAllocator()
            : parent_allocator( nullptr )
        {
        }

        FixedSizeListAllocator::~FixedSizeListAllocator()
        {
        }

        void FixedSizeListAllocator::insert( void* p )
        {
            Node* to_insert = reinterpret_cast<Node*>( p );
            to_insert->next = head.next;
            head.next = to_insert;
        }

        void* FixedSizeListAllocator::remove()
        {
            if( head.next != nullptr )
            {
                Node* to_remove = head.next;
                head.next = to_remove->next;
                return reinterpret_cast<void*>( to_remove );
            }

            return nullptr;
        }

        nbool FixedSizeListAllocator::init( NebulaAllocator* parent_allocator )
        {
            this->parent_allocator = parent_allocator;
            return true;
        }

        void FixedSizeListAllocator::release()
        {
            clear();

            parent_allocator = nullptr;
        }

        void* FixedSizeListAllocator::allocate( nsize size, nsize align )
        {
            assert( size >= sizeof( void* ) && "[FixedSizeListAllocator::allocate]: require size too small" );

            void* p = remove();
            return ( p != nullptr && isPointerAligned( p, align ) ) ? p : parent_allocator->allocate( size, align );
        }

        nbool FixedSizeListAllocator::deallocate( void* p )
        {
            if( p != nullptr )
            {
                insert( p );
            }

            return true;
        }

        void FixedSizeListAllocator::clear()
        {
            void* p = remove();
            while( p != nullptr )
            {
                parent_allocator->deallocate( p );
                p = remove();
            }
        }
    }
}