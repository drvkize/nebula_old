#pragma once

#include "Math/MathDefine.hpp"
#include "Math/Vector.hpp"

namespace Nebula
{
    namespace Math
    {
        template<typename T>
        class Vector<T, 3>
        {
        public:
            using ScalarType = T;
            static constexpr int Dimension = 3;

            union
            {
                T data[Dimension];
                struct
                {
                    T x;
                    T y;
                    T z;
                };
            };

            inline Vector()
            {
            }

            inline Vector( const Vector<T, 3>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = v.data[i] )
            }

            template<typename U>
            explicit inline Vector( const Vector<U, 3>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = static_cast<T>( v[i] ) );
            }

            explicit inline Vector( const T s )
            {
                NEBULA_VECTOR_LOOP( data[i] = s; );
            }

            explicit inline Vector( const T* arr )
            {
                NEBULA_VECTOR_LOOP( data[i] = arr[i] );
            }

            inline Vector( const T s0, const T s1, const T s2 )
            {
                x = s0;
                y = s1;
                z = s2;
            }

            inline Vector( const Vector<T, 2>& v01, const T s3 )
            {
                x = v01[0];
                y = v01[1];
                z = s3;
            }

            inline Vector( const T s0, const Vector<T, 2>& v12 )
            {
                x = s0;
                y = v12[0];
                z = v12[1];
            }

            explicit inline Vector( const Vector<T, 4>& v )
            {
                data[0] = v.data[0];
                data[1] = v.data[1];
                data[2] = v.data[2];
            }

            explicit inline Vector( const VectorPacked<T, 3>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = v.data[i] );
            }

            inline T& operator()( const int i ) { return data[i]; }
            inline const T& operator()( const int i ) const { return data[i] ; }
            inline T& operator[]( const int i ) { return data[i]; }
            inline const T& operator[]( const int i ) const { return data[i]; }
            inline Vector<T, 3> xyz() { return Vector<T, 3>( x, y, z ); }
            inline const Vector<T, 3> xyz() const { return Vector<T, 3>( x, y, z ); }
            inline Vector<T, 2> xy() { return Vector<T, 2>( x, y ); }
            inline const Vector<T, 2> xy() const { return Vector<T, 2>( x, y ); }
            inline Vector<T, 2> yz() { return Vector<T, 2>( y, z ); }
            inline const Vector<T, 2> yz() const { return Vector<T, 2>( y, z ); }
            inline void pack( VectorPacked<T, 3>* const v ) const
            {
                NEBULA_VECTOR_LOOP( v->data[i] = data[i] );
            }
        };
    }
}
