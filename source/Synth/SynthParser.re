// A Bison parser, made by GNU Bison 3.0.4.

// Skeleton implementation for Bison LALR(1) parsers in C++

// Copyright (C) 2002-2015 Free Software Foundation, Inc.

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// As a special exception, you may create a larger work that contains
// part or all of the Bison parser skeleton and distribute that work
// under terms of your choice, so long as that work isn't itself a
// parser generator using the skeleton or a modified version thereof
// as a parser skeleton.  Alternatively, if you modify or redistribute
// the parser skeleton itself, you may (at your option) remove this
// special exception, which will cause the skeleton and the resulting
// Bison output files to be licensed under the GNU General Public
// License without this special exception.

// This special exception was added by the Free Software Foundation in
// version 2.2 of Bison.


// First part of user declarations.
#line 3 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:404

#pragma warning( push )
#pragma warning( disable: 4065 )

#line 41 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:404

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

#include "SynthParser.hpp"

// User implementation prologue.

#line 55 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:412
// Unqualified %code blocks.
#line 27 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:413

    #include "Synth/SynthContext.hpp"
    #define M( x ) std::move( x )

    namespace Nebula
    {
        namespace Synth
        {
            SynthParser::symbol_type yylex( SynthContext& ctx );

            void SynthParser::error( const SynthParser::location_type& l, const std::string& m )
            {
                printf( "%s: (%d,%d)-(%d,%d), %s\n", l.begin.filename != nullptr ? l.begin.filename->c_str() : "(unknown file)", l.begin.line, l.begin.column, l.end.line, l.end.column, m.c_str() );
            }
        }
    }

#line 75 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:413


#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> // FIXME: INFRINGES ON USER NAME SPACE.
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#define YYRHSLOC(Rhs, K) ((Rhs)[K].location)
/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

# ifndef YYLLOC_DEFAULT
#  define YYLLOC_DEFAULT(Current, Rhs, N)                               \
    do                                                                  \
      if (N)                                                            \
        {                                                               \
          (Current).begin  = YYRHSLOC (Rhs, 1).begin;                   \
          (Current).end    = YYRHSLOC (Rhs, N).end;                     \
        }                                                               \
      else                                                              \
        {                                                               \
          (Current).begin = (Current).end = YYRHSLOC (Rhs, 0).end;      \
        }                                                               \
    while (/*CONSTCOND*/ false)
# endif


// Suppress unused-variable warnings by "using" E.
#define YYUSE(E) ((void) (E))

// Enable debugging if requested.
#if YYDEBUG

// A pseudo ostream that takes yydebug_ into account.
# define YYCDEBUG if (yydebug_) (*yycdebug_)

# define YY_SYMBOL_PRINT(Title, Symbol)         \
  do {                                          \
    if (yydebug_)                               \
    {                                           \
      *yycdebug_ << Title << ' ';               \
      yy_print_ (*yycdebug_, Symbol);           \
      *yycdebug_ << std::endl;                  \
    }                                           \
  } while (false)

# define YY_REDUCE_PRINT(Rule)          \
  do {                                  \
    if (yydebug_)                       \
      yy_reduce_print_ (Rule);          \
  } while (false)

# define YY_STACK_PRINT()               \
  do {                                  \
    if (yydebug_)                       \
      yystack_print_ ();                \
  } while (false)

#else // !YYDEBUG

# define YYCDEBUG if (false) std::cerr
# define YY_SYMBOL_PRINT(Title, Symbol)  YYUSE(Symbol)
# define YY_REDUCE_PRINT(Rule)           static_cast<void>(0)
# define YY_STACK_PRINT()                static_cast<void>(0)

#endif // !YYDEBUG

#define yyerrok         (yyerrstatus_ = 0)
#define yyclearin       (yyla.clear ())

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab
#define YYRECOVERING()  (!!yyerrstatus_)

#line 11 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:479
namespace Nebula { namespace Synth {
#line 161 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:479

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  SynthParser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              // Fall through.
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }


  /// Build a parser object.
  SynthParser::SynthParser (Nebula::Synth::SynthContext& ctx_yyarg)
    :
#if YYDEBUG
      yydebug_ (false),
      yycdebug_ (&std::cerr),
#endif
      ctx (ctx_yyarg)
  {}

  SynthParser::~SynthParser ()
  {}


  /*---------------.
  | Symbol types.  |
  `---------------*/



  // by_state.
  inline
  SynthParser::by_state::by_state ()
    : state (empty_state)
  {}

  inline
  SynthParser::by_state::by_state (const by_state& other)
    : state (other.state)
  {}

  inline
  void
  SynthParser::by_state::clear ()
  {
    state = empty_state;
  }

  inline
  void
  SynthParser::by_state::move (by_state& that)
  {
    state = that.state;
    that.clear ();
  }

  inline
  SynthParser::by_state::by_state (state_type s)
    : state (s)
  {}

  inline
  SynthParser::symbol_number_type
  SynthParser::by_state::type_get () const
  {
    if (state == empty_state)
      return empty_symbol;
    else
      return yystos_[state];
  }

  inline
  SynthParser::stack_symbol_type::stack_symbol_type ()
  {}


  inline
  SynthParser::stack_symbol_type::stack_symbol_type (state_type s, symbol_type& that)
    : super_type (s, that.location)
  {
      switch (that.type_get ())
    {
      case 44: // root_node
      case 45: // lbrace
      case 46: // empty_braces
      case 47: // stmt_begin
      case 48: // stmts
      case 49: // stmt
      case 50: // braces_expr_begin
      case 51: // braces_expr
      case 52: // parens_expr_begin
      case 53: // parens_expr_begin1
      case 54: // parens_expr
      case 55: // brackets_expr_begin
      case 56: // brackets_expr_begin1
      case 57: // brackets_expr
      case 58: // invoke_expr
      case 59: // expr
      case 60: // null
      case 61: // bool_literal
      case 62: // int_literal
      case 63: // float_literal
      case 64: // string_literal
      case 65: // identifier
      case 66: // node_expr
        value.move< SynthExprPtr > (that.value);
        break;

      case 8: // INT_LITERAL
        value.move< int32 > (that.value);
        break;

      case 9: // FLOAT_LITERAL
        value.move< nfloat > (that.value);
        break;

      case 6: // IDENTIFIER
      case 7: // STRING_LITERAL
        value.move< nstring > (that.value);
        break;

      default:
        break;
    }

    // that is emptied.
    that.type = empty_symbol;
  }

  inline
  SynthParser::stack_symbol_type&
  SynthParser::stack_symbol_type::operator= (const stack_symbol_type& that)
  {
    state = that.state;
      switch (that.type_get ())
    {
      case 44: // root_node
      case 45: // lbrace
      case 46: // empty_braces
      case 47: // stmt_begin
      case 48: // stmts
      case 49: // stmt
      case 50: // braces_expr_begin
      case 51: // braces_expr
      case 52: // parens_expr_begin
      case 53: // parens_expr_begin1
      case 54: // parens_expr
      case 55: // brackets_expr_begin
      case 56: // brackets_expr_begin1
      case 57: // brackets_expr
      case 58: // invoke_expr
      case 59: // expr
      case 60: // null
      case 61: // bool_literal
      case 62: // int_literal
      case 63: // float_literal
      case 64: // string_literal
      case 65: // identifier
      case 66: // node_expr
        value.copy< SynthExprPtr > (that.value);
        break;

      case 8: // INT_LITERAL
        value.copy< int32 > (that.value);
        break;

      case 9: // FLOAT_LITERAL
        value.copy< nfloat > (that.value);
        break;

      case 6: // IDENTIFIER
      case 7: // STRING_LITERAL
        value.copy< nstring > (that.value);
        break;

      default:
        break;
    }

    location = that.location;
    return *this;
  }


  template <typename Base>
  inline
  void
  SynthParser::yy_destroy_ (const char* yymsg, basic_symbol<Base>& yysym) const
  {
    if (yymsg)
      YY_SYMBOL_PRINT (yymsg, yysym);
  }

#if YYDEBUG
  template <typename Base>
  void
  SynthParser::yy_print_ (std::ostream& yyo,
                                     const basic_symbol<Base>& yysym) const
  {
    std::ostream& yyoutput = yyo;
    YYUSE (yyoutput);
    symbol_number_type yytype = yysym.type_get ();
    // Avoid a (spurious) G++ 4.8 warning about "array subscript is
    // below array bounds".
    if (yysym.empty ())
      std::abort ();
    yyo << (yytype < yyntokens_ ? "token" : "nterm")
        << ' ' << yytname_[yytype] << " ("
        << yysym.location << ": ";
    YYUSE (yytype);
    yyo << ')';
  }
#endif

  inline
  void
  SynthParser::yypush_ (const char* m, state_type s, symbol_type& sym)
  {
    stack_symbol_type t (s, sym);
    yypush_ (m, t);
  }

  inline
  void
  SynthParser::yypush_ (const char* m, stack_symbol_type& s)
  {
    if (m)
      YY_SYMBOL_PRINT (m, s);
    yystack_.push (s);
  }

  inline
  void
  SynthParser::yypop_ (unsigned int n)
  {
    yystack_.pop (n);
  }

#if YYDEBUG
  std::ostream&
  SynthParser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  SynthParser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  SynthParser::debug_level_type
  SynthParser::debug_level () const
  {
    return yydebug_;
  }

  void
  SynthParser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }
#endif // YYDEBUG

  inline SynthParser::state_type
  SynthParser::yy_lr_goto_state_ (state_type yystate, int yysym)
  {
    int yyr = yypgoto_[yysym - yyntokens_] + yystate;
    if (0 <= yyr && yyr <= yylast_ && yycheck_[yyr] == yystate)
      return yytable_[yyr];
    else
      return yydefgoto_[yysym - yyntokens_];
  }

  inline bool
  SynthParser::yy_pact_value_is_default_ (int yyvalue)
  {
    return yyvalue == yypact_ninf_;
  }

  inline bool
  SynthParser::yy_table_value_is_error_ (int yyvalue)
  {
    return yyvalue == yytable_ninf_;
  }

  int
  SynthParser::parse ()
  {
    // State.
    int yyn;
    /// Length of the RHS of the rule being reduced.
    int yylen = 0;

    // Error handling.
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// The lookahead symbol.
    symbol_type yyla;

    /// The locations where the error started and ended.
    stack_symbol_type yyerror_range[3];

    /// The return value of parse ().
    int yyresult;

    // FIXME: This shoud be completely indented.  It is not yet to
    // avoid gratuitous conflicts when merging into the master branch.
    try
      {
    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stack.  The initial state will be set in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystack_.clear ();
    yypush_ (YY_NULLPTR, 0, yyla);

    // A new symbol was pushed on the stack.
  yynewstate:
    YYCDEBUG << "Entering state " << yystack_[0].state << std::endl;

    // Accept?
    if (yystack_[0].state == yyfinal_)
      goto yyacceptlab;

    goto yybackup;

    // Backup.
  yybackup:

    // Try to take a decision without lookahead.
    yyn = yypact_[yystack_[0].state];
    if (yy_pact_value_is_default_ (yyn))
      goto yydefault;

    // Read a lookahead token.
    if (yyla.empty ())
      {
        YYCDEBUG << "Reading a token: ";
        try
          {
            symbol_type yylookahead (yylex (ctx));
            yyla.move (yylookahead);
          }
        catch (const syntax_error& yyexc)
          {
            error (yyexc);
            goto yyerrlab1;
          }
      }
    YY_SYMBOL_PRINT ("Next token is", yyla);

    /* If the proper action on seeing token YYLA.TYPE is to reduce or
       to detect an error, take that action.  */
    yyn += yyla.type_get ();
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yyla.type_get ())
      goto yydefault;

    // Reduce or error.
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
        if (yy_table_value_is_error_ (yyn))
          goto yyerrlab;
        yyn = -yyn;
        goto yyreduce;
      }

    // Count tokens shifted since error; after three, turn off error status.
    if (yyerrstatus_)
      --yyerrstatus_;

    // Shift the lookahead token.
    yypush_ ("Shifting", yyn, yyla);
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystack_[0].state];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    {
      stack_symbol_type yylhs;
      yylhs.state = yy_lr_goto_state_(yystack_[yylen].state, yyr1_[yyn]);
      /* Variants are always initialized to an empty instance of the
         correct type. The default '$$ = $1' action is NOT applied
         when using variants.  */
        switch (yyr1_[yyn])
    {
      case 44: // root_node
      case 45: // lbrace
      case 46: // empty_braces
      case 47: // stmt_begin
      case 48: // stmts
      case 49: // stmt
      case 50: // braces_expr_begin
      case 51: // braces_expr
      case 52: // parens_expr_begin
      case 53: // parens_expr_begin1
      case 54: // parens_expr
      case 55: // brackets_expr_begin
      case 56: // brackets_expr_begin1
      case 57: // brackets_expr
      case 58: // invoke_expr
      case 59: // expr
      case 60: // null
      case 61: // bool_literal
      case 62: // int_literal
      case 63: // float_literal
      case 64: // string_literal
      case 65: // identifier
      case 66: // node_expr
        yylhs.value.build< SynthExprPtr > ();
        break;

      case 8: // INT_LITERAL
        yylhs.value.build< int32 > ();
        break;

      case 9: // FLOAT_LITERAL
        yylhs.value.build< nfloat > ();
        break;

      case 6: // IDENTIFIER
      case 7: // STRING_LITERAL
        yylhs.value.build< nstring > ();
        break;

      default:
        break;
    }


      // Compute the default @$.
      {
        slice<stack_symbol_type, stack_type> slice (yystack_, yylen);
        YYLLOC_DEFAULT (yylhs.location, slice, yylen);
      }

      // Perform the reduction.
      YY_REDUCE_PRINT (yyn);
      try
        {
          switch (yyn)
            {
  case 2:
#line 79 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_ASSIGN>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); ctx.root_expr = yylhs.value.as< SynthExprPtr > (); }
#line 653 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 3:
#line 81 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = nullptr; }
#line 659 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 4:
#line 82 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = nullptr; }
#line 665 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 5:
#line 83 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { if( yystack_[1].value.as< SynthExprPtr > () == nullptr ) yystack_[1].value.as< SynthExprPtr > () = makeExpr<SET::SET_STMTS>(); yystack_[1].value.as< SynthExprPtr > ()->addExpr( M( yystack_[0].value.as< SynthExprPtr > () ) ); yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 671 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 6:
#line 84 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yystack_[1].value.as< SynthExprPtr > ()->addExpr( M( yystack_[0].value.as< SynthExprPtr > () ) ); yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 677 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 7:
#line 85 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 683 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 8:
#line 86 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 689 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 9:
#line 87 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_ASSIGN>( M( yystack_[3].value.as< SynthExprPtr > () ), M( yystack_[1].value.as< SynthExprPtr > () ) ); }
#line 695 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 10:
#line 88 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_ASSIGN>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 701 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 11:
#line 89 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_PLUS_ASSIGN>( M( yystack_[3].value.as< SynthExprPtr > () ), M( yystack_[1].value.as< SynthExprPtr > () ) ); }
#line 707 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 12:
#line 90 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_MINUS_ASSIGN>( M( yystack_[3].value.as< SynthExprPtr > () ), M( yystack_[1].value.as< SynthExprPtr > () ) ); }
#line 713 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 13:
#line 91 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_MUL_ASSIGN>( M( yystack_[3].value.as< SynthExprPtr > () ), M( yystack_[1].value.as< SynthExprPtr > () ) ); }
#line 719 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 14:
#line 92 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_DIV_ASSIGN>( M( yystack_[3].value.as< SynthExprPtr > () ), M( yystack_[1].value.as< SynthExprPtr > () ) ); }
#line 725 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 15:
#line 93 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_IF>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 731 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 16:
#line 94 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_ELIF>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 737 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 17:
#line 95 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_ELSE>( M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 743 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 18:
#line 96 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_WHILE>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 749 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 19:
#line 97 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_BREAK>(); }
#line 755 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 20:
#line 98 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_RETURN>( M( yystack_[1].value.as< SynthExprPtr > () ) ); }
#line 761 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 21:
#line 99 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_RETURN>( nullptr ); }
#line 767 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 22:
#line 100 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 773 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 23:
#line 101 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    {}
#line 779 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 24:
#line 103 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { if( yystack_[1].value.as< SynthExprPtr > () == nullptr ) yystack_[1].value.as< SynthExprPtr > () = makeExpr<SET::SET_BRACES>(); yystack_[1].value.as< SynthExprPtr > ()->addExpr( M( yystack_[0].value.as< SynthExprPtr > () ) ); yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 785 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 25:
#line 104 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yystack_[2].value.as< SynthExprPtr > ()->addExpr( M( yystack_[0].value.as< SynthExprPtr > () ) ); yylhs.value.as< SynthExprPtr > () = yystack_[2].value.as< SynthExprPtr > (); }
#line 791 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 26:
#line 105 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 797 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 27:
#line 106 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_BRACES>(); }
#line 803 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 28:
#line 108 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_PARENS>(); }
#line 809 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 29:
#line 109 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yystack_[1].value.as< SynthExprPtr > ()->addExpr( M( yystack_[0].value.as< SynthExprPtr > () ) ); yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 815 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 30:
#line 110 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yystack_[2].value.as< SynthExprPtr > ()->addExpr( M( yystack_[0].value.as< SynthExprPtr > () ) ); yylhs.value.as< SynthExprPtr > () = yystack_[2].value.as< SynthExprPtr > (); }
#line 821 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 31:
#line 111 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 827 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 32:
#line 112 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 833 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 33:
#line 114 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_BRACKETS>(); }
#line 839 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 34:
#line 115 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yystack_[1].value.as< SynthExprPtr > ()->addExpr( M( yystack_[0].value.as< SynthExprPtr > () ) ); yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 845 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 35:
#line 116 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yystack_[2].value.as< SynthExprPtr > ()->addExpr( M( yystack_[0].value.as< SynthExprPtr > () ) ); yylhs.value.as< SynthExprPtr > () = yystack_[2].value.as< SynthExprPtr > (); }
#line 851 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 36:
#line 117 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 857 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 37:
#line 118 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[1].value.as< SynthExprPtr > (); }
#line 863 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 38:
#line 120 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_INVOKE>( M( yystack_[1].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 869 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 39:
#line 122 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 875 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 40:
#line 123 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 881 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 41:
#line 124 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 887 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 42:
#line 125 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 893 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 43:
#line 126 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 899 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 44:
#line 127 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 905 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 45:
#line 128 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 911 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 46:
#line 129 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 917 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 47:
#line 130 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 923 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 48:
#line 131 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = yystack_[0].value.as< SynthExprPtr > (); }
#line 929 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 49:
#line 132 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_DOT>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 935 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 50:
#line 133 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_NEGATE>( M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 941 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 51:
#line 134 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_PLUS>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 947 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 52:
#line 135 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_MINUS>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 953 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 53:
#line 136 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_MUL>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 959 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 54:
#line 137 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_DIV>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 965 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 55:
#line 138 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_LT>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 971 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 56:
#line 139 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_GT>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 977 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 57:
#line 140 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_LE>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 983 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 58:
#line 141 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_GE>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 989 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 59:
#line 142 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_EQ>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 995 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 60:
#line 143 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_NE>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 1001 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 61:
#line 144 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_AND>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 1007 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 62:
#line 145 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_OR>( M( yystack_[2].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 1013 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 63:
#line 147 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_NULL>(); }
#line 1019 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 64:
#line 148 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_BOOL>( true ); }
#line 1025 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 65:
#line 149 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_BOOL>( false ); }
#line 1031 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 66:
#line 150 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_INT>( M( yystack_[0].value.as< int32 > () ) ); }
#line 1037 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 67:
#line 151 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_FLOAT>( M( yystack_[0].value.as< nfloat > () ) ); }
#line 1043 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 68:
#line 152 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_STRING>( M( yystack_[0].value.as< nstring > () ) ); }
#line 1049 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 69:
#line 153 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_IDENT>( M( yystack_[0].value.as< nstring > () ) ); }
#line 1055 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 70:
#line 155 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_NODE>( M( yystack_[1].value.as< SynthExprPtr > () ), M( yystack_[0].value.as< SynthExprPtr > () ) ); }
#line 1061 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;

  case 71:
#line 156 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:859
    { yylhs.value.as< SynthExprPtr > () = makeExpr<SET::SET_NODE>( M( yystack_[1].value.as< SynthExprPtr > () ), nullptr ); }
#line 1067 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
    break;


#line 1071 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:859
            default:
              break;
            }
        }
      catch (const syntax_error& yyexc)
        {
          error (yyexc);
          YYERROR;
        }
      YY_SYMBOL_PRINT ("-> $$ =", yylhs);
      yypop_ (yylen);
      yylen = 0;
      YY_STACK_PRINT ();

      // Shift the result of the reduction.
      yypush_ (YY_NULLPTR, yylhs);
    }
    goto yynewstate;

  /*--------------------------------------.
  | yyerrlab -- here on detecting error.  |
  `--------------------------------------*/
  yyerrlab:
    // If not already recovering from an error, report this error.
    if (!yyerrstatus_)
      {
        ++yynerrs_;
        error (yyla.location, yysyntax_error_ (yystack_[0].state, yyla));
      }


    yyerror_range[1].location = yyla.location;
    if (yyerrstatus_ == 3)
      {
        /* If just tried and failed to reuse lookahead token after an
           error, discard it.  */

        // Return failure if at end of input.
        if (yyla.type_get () == yyeof_)
          YYABORT;
        else if (!yyla.empty ())
          {
            yy_destroy_ ("Error: discarding", yyla);
            yyla.clear ();
          }
      }

    // Else will try to reuse lookahead token after shifting the error token.
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;
    yyerror_range[1].location = yystack_[yylen - 1].location;
    /* Do not reclaim the symbols of the rule whose action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;   // Each real token shifted decrements this.
    {
      stack_symbol_type error_token;
      for (;;)
        {
          yyn = yypact_[yystack_[0].state];
          if (!yy_pact_value_is_default_ (yyn))
            {
              yyn += yyterror_;
              if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                  yyn = yytable_[yyn];
                  if (0 < yyn)
                    break;
                }
            }

          // Pop the current state because it cannot handle the error token.
          if (yystack_.size () == 1)
            YYABORT;

          yyerror_range[1].location = yystack_[0].location;
          yy_destroy_ ("Error: popping", yystack_[0]);
          yypop_ ();
          YY_STACK_PRINT ();
        }

      yyerror_range[2].location = yyla.location;
      YYLLOC_DEFAULT (error_token.location, yyerror_range, 2);

      // Shift the error token.
      error_token.state = yyn;
      yypush_ ("Shifting", error_token);
    }
    goto yynewstate;

    // Accept.
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    // Abort.
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (!yyla.empty ())
      yy_destroy_ ("Cleanup: discarding lookahead", yyla);

    /* Do not reclaim the symbols of the rule whose action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (1 < yystack_.size ())
      {
        yy_destroy_ ("Cleanup: popping", yystack_[0]);
        yypop_ ();
      }

    return yyresult;
  }
    catch (...)
      {
        YYCDEBUG << "Exception caught: cleaning lookahead and stack"
                 << std::endl;
        // Do not try to display the values of the reclaimed symbols,
        // as their printer might throw an exception.
        if (!yyla.empty ())
          yy_destroy_ (YY_NULLPTR, yyla);

        while (1 < yystack_.size ())
          {
            yy_destroy_ (YY_NULLPTR, yystack_[0]);
            yypop_ ();
          }
        throw;
      }
  }

  void
  SynthParser::error (const syntax_error& yyexc)
  {
    error (yyexc.location, yyexc.what());
  }

  // Generate an error message.
  std::string
  SynthParser::yysyntax_error_ (state_type yystate, const symbol_type& yyla) const
  {
    // Number of reported tokens (one for the "unexpected", one per
    // "expected").
    size_t yycount = 0;
    // Its maximum.
    enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
    // Arguments of yyformat.
    char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];

    /* There are many possibilities here to consider:
       - If this state is a consistent state with a default action, then
         the only way this function was invoked is if the default action
         is an error action.  In that case, don't check for expected
         tokens because there are none.
       - The only way there can be no lookahead present (in yyla) is
         if this state is a consistent state with a default action.
         Thus, detecting the absence of a lookahead is sufficient to
         determine that there is no unexpected or expected token to
         report.  In that case, just report a simple "syntax error".
       - Don't assume there isn't a lookahead just because this state is
         a consistent state with a default action.  There might have
         been a previous inconsistent state, consistent state with a
         non-default action, or user semantic action that manipulated
         yyla.  (However, yyla is currently not documented for users.)
       - Of course, the expected token list depends on states to have
         correct lookahead information, and it depends on the parser not
         to perform extra reductions after fetching a lookahead from the
         scanner and before detecting a syntax error.  Thus, state
         merging (from LALR or IELR) and default reductions corrupt the
         expected token list.  However, the list is correct for
         canonical LR with one exception: it will still contain any
         token that will not be accepted due to an error action in a
         later state.
    */
    if (!yyla.empty ())
      {
        int yytoken = yyla.type_get ();
        yyarg[yycount++] = yytname_[yytoken];
        int yyn = yypact_[yystate];
        if (!yy_pact_value_is_default_ (yyn))
          {
            /* Start YYX at -YYN if negative to avoid negative indexes in
               YYCHECK.  In other words, skip the first -YYN actions for
               this state because they are default actions.  */
            int yyxbegin = yyn < 0 ? -yyn : 0;
            // Stay within bounds of both yycheck and yytname.
            int yychecklim = yylast_ - yyn + 1;
            int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
            for (int yyx = yyxbegin; yyx < yyxend; ++yyx)
              if (yycheck_[yyx + yyn] == yyx && yyx != yyterror_
                  && !yy_table_value_is_error_ (yytable_[yyx + yyn]))
                {
                  if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                    {
                      yycount = 1;
                      break;
                    }
                  else
                    yyarg[yycount++] = yytname_[yyx];
                }
          }
      }

    char const* yyformat = YY_NULLPTR;
    switch (yycount)
      {
#define YYCASE_(N, S)                         \
        case N:                               \
          yyformat = S;                       \
        break
        YYCASE_(0, YY_("syntax error"));
        YYCASE_(1, YY_("syntax error, unexpected %s"));
        YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
        YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
        YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
        YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
#undef YYCASE_
      }

    std::string yyres;
    // Argument number.
    size_t yyi = 0;
    for (char const* yyp = yyformat; *yyp; ++yyp)
      if (yyp[0] == '%' && yyp[1] == 's' && yyi < yycount)
        {
          yyres += yytnamerr_ (yyarg[yyi++]);
          ++yyp;
        }
      else
        yyres += *yyp;
    return yyres;
  }


  const signed char SynthParser::yypact_ninf_ = -51;

  const signed char SynthParser::yytable_ninf_ = -1;

  const short int
  SynthParser::yypact_[] =
  {
      -2,   -51,     6,    -1,   -51,    -9,   -51,   215,    -5,    -4,
     -51,   -51,   -51,   -51,   -51,   -51,   -51,   279,   -51,   -51,
     -51,   233,   -51,   -29,   -51,   -51,    18,     2,   -51,   -51,
     504,   -51,   -51,   -51,   -51,   -51,    -9,   279,   -51,   137,
     -51,   163,   -51,    32,   -51,   504,   279,   -51,   -51,   504,
     279,   -51,    -2,   279,   279,   279,   279,   279,   279,   279,
     279,   279,   279,   279,   279,   -51,   504,     9,    10,   189,
      11,    15,   240,   -51,   137,   -51,   -51,   365,   -19,   -51,
     -51,   504,   504,   -51,   350,   391,    -3,    -3,    -3,    -3,
      -3,    -3,    24,    24,    32,    32,   279,   279,   -51,   279,
     -51,   -51,   378,   365,   -51,   279,   279,   279,   279,   279,
     290,   321,   334,   -51,    -4,   407,   -51,   420,   449,   462,
     491,   189,   189,   189,   -51,   -51,   -51,   -51,   -51,   -51,
     -51,   -51
  };

  const unsigned char
  SynthParser::yydefact_[] =
  {
       0,    69,     0,     0,     1,     0,    28,     0,     0,     0,
       2,    63,    64,    65,    68,    66,    67,     0,     3,    31,
      33,     0,    27,     0,    47,    45,     0,     0,    46,    48,
      29,    39,    40,    41,    42,    43,    44,     0,    32,     0,
      71,     0,    70,    50,     4,    24,     0,    26,    36,    34,
       0,    37,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    38,    30,     0,     0,     0,
       0,     0,     0,    23,     0,     8,     5,     0,    44,     7,
       6,    25,    35,    49,    62,    61,    55,    56,    57,    58,
      59,    60,    51,    52,    53,    54,     0,     0,    17,     0,
      19,    21,     0,    24,    22,     0,     0,     0,     0,     0,
       0,     0,     0,    20,    45,     0,    10,     0,     0,     0,
       0,     0,     0,     0,     9,    11,    12,    13,    14,    15,
      16,    18
  };

  const signed char
  SynthParser::yypgoto_[] =
  {
     -51,   -51,    -8,    -7,   -51,    -6,   -26,   -51,   -51,   -51,
     -51,     4,   -51,   -51,   -51,   -51,    30,   -51,   -51,   -51,
     -51,   -51,     0,   -50
  };

  const signed char
  SynthParser::yydefgoto_[] =
  {
      -1,     2,    21,    22,    41,    75,    76,    23,    24,     7,
       8,    25,    26,    27,    28,    29,    77,    31,    32,    33,
      34,    35,    36,    10
  };

  const unsigned char
  SynthParser::yytable_[] =
  {
       3,    39,    40,    42,     1,    46,     4,    52,    47,     9,
     105,   106,   107,   108,   109,    80,    61,    62,    63,    64,
       6,    11,    12,    13,     1,    14,    15,    16,     5,    37,
       6,    74,    18,    74,    52,    38,    50,    30,    17,    78,
      65,    78,    52,    98,    51,    63,    64,    43,    96,    97,
      99,    45,    83,   100,    18,   116,    49,     6,     0,    20,
      48,    74,     0,     0,     0,     0,    74,    66,     0,    78,
       0,     0,     0,     0,    78,     0,    81,     0,     0,     0,
      82,     0,    65,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,   129,   130,   131,     0,     0,
       0,     0,   102,     0,   103,     0,    39,    40,    42,   114,
       0,     0,     0,    74,    74,    74,     0,     0,     0,     0,
       0,    78,    78,    78,     0,     0,   110,   111,     0,   112,
       0,     0,     0,     0,     0,   115,   117,   118,   119,   120,
      11,    12,    13,     1,    14,    15,    16,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    17,     0,     0,
      67,    68,    69,    70,    71,    72,    11,    12,    13,     1,
      14,    15,    16,    18,    44,    73,     6,     0,    20,     0,
       0,     0,     0,    17,     0,     0,    67,    68,    69,    70,
      71,    72,    11,    12,    13,     1,    14,    15,    16,    18,
      79,    73,     6,     0,    20,     0,     0,     0,     0,    17,
       0,     0,    67,    68,    69,    70,    71,    72,    11,    12,
      13,     1,    14,    15,    16,    18,     0,    73,     6,     0,
      20,     0,     0,     0,     0,    17,    11,    12,    13,     1,
      14,    15,    16,    11,    12,    13,     1,    14,    15,    16,
       0,    18,     0,    17,     6,    19,    20,     0,     0,     0,
      17,     0,     0,     0,     0,     0,     0,     0,     0,    18,
      44,     0,     6,     0,    20,     0,    18,     0,   101,     6,
       0,    20,    11,    12,    13,     1,    14,    15,    16,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    17,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,     0,     0,    18,     0,     0,     6,     0,
      20,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     121,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,     0,     0,     0,
      52,   122,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,     0,   123,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    52,     0,   104,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,     0,     0,   113,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,     0,     0,   124,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   125,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,     0,     0,   126,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     127,    52,    53,    54,    55,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,     0,     0,   128
  };

  const signed char
  SynthParser::yycheck_[] =
  {
       0,     9,     9,     9,     6,    34,     0,    10,    37,     5,
      29,    30,    31,    32,    33,    41,    19,    20,    21,    22,
      39,     3,     4,     5,     6,     7,     8,     9,    29,    34,
      39,    39,    36,    41,    10,    40,    34,     7,    20,    39,
      36,    41,    10,    69,    42,    21,    22,    17,    39,    39,
      39,    21,    52,    38,    36,   105,    26,    39,    -1,    41,
      42,    69,    -1,    -1,    -1,    -1,    74,    37,    -1,    69,
      -1,    -1,    -1,    -1,    74,    -1,    46,    -1,    -1,    -1,
      50,    -1,    78,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,   121,   122,   123,    -1,    -1,
      -1,    -1,    72,    -1,    74,    -1,   114,   114,   114,   105,
      -1,    -1,    -1,   121,   122,   123,    -1,    -1,    -1,    -1,
      -1,   121,   122,   123,    -1,    -1,    96,    97,    -1,    99,
      -1,    -1,    -1,    -1,    -1,   105,   106,   107,   108,   109,
       3,     4,     5,     6,     7,     8,     9,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,    -1,    -1,
      23,    24,    25,    26,    27,    28,     3,     4,     5,     6,
       7,     8,     9,    36,    37,    38,    39,    -1,    41,    -1,
      -1,    -1,    -1,    20,    -1,    -1,    23,    24,    25,    26,
      27,    28,     3,     4,     5,     6,     7,     8,     9,    36,
      37,    38,    39,    -1,    41,    -1,    -1,    -1,    -1,    20,
      -1,    -1,    23,    24,    25,    26,    27,    28,     3,     4,
       5,     6,     7,     8,     9,    36,    -1,    38,    39,    -1,
      41,    -1,    -1,    -1,    -1,    20,     3,     4,     5,     6,
       7,     8,     9,     3,     4,     5,     6,     7,     8,     9,
      -1,    36,    -1,    20,    39,    40,    41,    -1,    -1,    -1,
      20,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    36,
      37,    -1,    39,    -1,    41,    -1,    36,    -1,    38,    39,
      -1,    41,     3,     4,     5,     6,     7,     8,     9,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    20,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    -1,    -1,    36,    -1,    -1,    39,    -1,
      41,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      40,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    -1,    -1,    -1,
      10,    40,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    -1,    40,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    10,    -1,    38,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    -1,    -1,    38,    10,    11,    12,
      13,    14,    15,    16,    17,    18,    19,    20,    21,    22,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    -1,    -1,    38,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    38,    10,
      11,    12,    13,    14,    15,    16,    17,    18,    19,    20,
      21,    22,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    -1,    -1,    38,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      38,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    -1,    -1,    38
  };

  const unsigned char
  SynthParser::yystos_[] =
  {
       0,     6,    44,    65,     0,    29,    39,    52,    53,    54,
      66,     3,     4,     5,     7,     8,     9,    20,    36,    40,
      41,    45,    46,    50,    51,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    34,    40,    45,
      46,    47,    48,    59,    37,    59,    34,    37,    42,    59,
      34,    42,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    54,    59,    23,    24,    25,
      26,    27,    28,    38,    45,    48,    49,    59,    65,    37,
      49,    59,    59,    65,    59,    59,    59,    59,    59,    59,
      59,    59,    59,    59,    59,    59,    39,    39,    49,    39,
      38,    38,    59,    59,    38,    29,    30,    31,    32,    33,
      59,    59,    59,    38,    54,    59,    66,    59,    59,    59,
      59,    40,    40,    40,    38,    38,    38,    38,    38,    49,
      49,    49
  };

  const unsigned char
  SynthParser::yyr1_[] =
  {
       0,    43,    44,    45,    46,    47,    47,    48,    49,    49,
      49,    49,    49,    49,    49,    49,    49,    49,    49,    49,
      49,    49,    49,    49,    50,    50,    51,    51,    52,    53,
      53,    54,    54,    55,    56,    56,    57,    57,    58,    59,
      59,    59,    59,    59,    59,    59,    59,    59,    59,    59,
      59,    59,    59,    59,    59,    59,    59,    59,    59,    59,
      59,    59,    59,    60,    61,    61,    62,    63,    64,    65,
      66,    66
  };

  const unsigned char
  SynthParser::yyr2_[] =
  {
       0,     2,     3,     1,     2,     2,     2,     2,     1,     4,
       3,     4,     4,     4,     4,     5,     5,     2,     5,     2,
       3,     2,     2,     1,     2,     3,     2,     1,     1,     2,
       3,     2,     2,     1,     2,     3,     2,     2,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     3,
       2,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     1,     1,     1,     1,     1,     1,     1,
       2,     2
  };



  // YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
  // First, the terminals, then, starting at \a yyntokens_, nonterminals.
  const char*
  const SynthParser::yytname_[] =
  {
  "\"end of file\"", "error", "$undefined", "\"null\"", "\"true\"",
  "\"false\"", "IDENTIFIER", "STRING_LITERAL", "INT_LITERAL",
  "FLOAT_LITERAL", "\".\"", "\"||\"", "\"&&\"", "\"<\"", "\">\"", "\"<=\"",
  "\">=\"", "\"==\"", "\"!=\"", "\"+\"", "\"-\"", "\"*\"", "\"/\"",
  "\"if\"", "\"elif\"", "\"else\"", "\"while\"", "\"break\"", "\"return\"",
  "\"=\"", "\"+=\"", "\"-=\"", "\"*=\"", "\"/=\"", "','", "UMINUS", "'{'",
  "'}'", "';'", "'('", "')'", "'['", "']'", "$accept", "root_node",
  "lbrace", "empty_braces", "stmt_begin", "stmts", "stmt",
  "braces_expr_begin", "braces_expr", "parens_expr_begin",
  "parens_expr_begin1", "parens_expr", "brackets_expr_begin",
  "brackets_expr_begin1", "brackets_expr", "invoke_expr", "expr", "null",
  "bool_literal", "int_literal", "float_literal", "string_literal",
  "identifier", "node_expr", YY_NULLPTR
  };

#if YYDEBUG
  const unsigned char
  SynthParser::yyrline_[] =
  {
       0,    79,    79,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   103,   104,   105,   106,   108,   109,
     110,   111,   112,   114,   115,   116,   117,   118,   120,   122,
     123,   124,   125,   126,   127,   128,   129,   130,   131,   132,
     133,   134,   135,   136,   137,   138,   139,   140,   141,   142,
     143,   144,   145,   147,   148,   149,   150,   151,   152,   153,
     155,   156
  };

  // Print the state stack on the debug stream.
  void
  SynthParser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (stack_type::const_iterator
           i = yystack_.begin (),
           i_end = yystack_.end ();
         i != i_end; ++i)
      *yycdebug_ << ' ' << i->state;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  SynthParser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    // Print the symbols being reduced, and their result.
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
               << " (line " << yylno << "):" << std::endl;
    // The symbols being reduced.
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
                       yystack_[(yynrhs) - (yyi + 1)]);
  }
#endif // YYDEBUG


#line 11 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:1167
} } // Nebula::Synth
#line 1613 "/home/drvkize/codex/nebula/source/Synth/SynthParser.re" // lalr1.cc:1167
#line 157 "/home/drvkize/codex/nebula/source/Synth/Synth.y" // lalr1.cc:1168


namespace Nebula
{
    namespace Synth
    {
        SynthParser::symbol_type yylex( SynthContext& ctx )
        {
            const nchar* anchor = ctx.cursor;
            auto makeSymbol = [&]( auto func, auto&&... params ) { ctx.loc.columns( ctx.cursor - anchor ); return func( params..., ctx.loc ); };

            %{ /*re2c*/
                re2c:yyfill:enable = 0;
                re2c:define:YYCTYPE = "nchar";
                re2c:define:YYCURSOR = "ctx.cursor";
                re2c:define:YYMARKER = "ctx.marker";

                // location
                "\r\n" | [\r\n]         { ctx.loc.lines(); return yylex( ctx ); }
                [\t\v\b\f ]             { ctx.loc.columns(); return yylex( ctx ); }
                // end of file
                "\000"                  { return makeSymbol( &SynthParser::make_END ); }
                // comment
                "#" [^\r\n]*            { return yylex( ctx ); }

                // null literal
                "null"                  { return makeSymbol( &SynthParser::make_NNULL ); }
                // bool literal
                "true"                  { return makeSymbol( &SynthParser::make_TRUE ); }
                "false"                 { return makeSymbol( &SynthParser::make_FALSE ); }
                // int literal
                [-]?[0-9]+              { return makeSymbol( &SynthParser::make_INT_LITERAL, std::stol( std::string( anchor, ctx.cursor ) ) ); }
                // float literal
                [+-]?[0-9]*'.'[0-9]+    { return makeSymbol( &SynthParser::make_FLOAT_LITERAL, std::stof( std::string( anchor, ctx.cursor ) ) ); }
                // string literal
                "\"" [^"]* "\""         { return makeSymbol( &SynthParser::make_STRING_LITERAL, nstring( anchor + 1, ctx.cursor - 1 ) ); }
                
                // operator
                "."                     { return makeSymbol( &SynthParser::make_DOT ); }
                "+"                     { return makeSymbol( &SynthParser::make_PLUS ); }
                "-"                     { return makeSymbol( &SynthParser::make_MINUS ); }
                "*"                     { return makeSymbol( &SynthParser::make_MUL ); }
                "/"                     { return makeSymbol( &SynthParser::make_DIV ); }
                "&&"                    { return makeSymbol( &SynthParser::make_AND ); }
                "||"                    { return makeSymbol( &SynthParser::make_OR ); }
                ">"                     { return makeSymbol( &SynthParser::make_GT ); }
                "<"                     { return makeSymbol( &SynthParser::make_LT ); }
                ">="                    { return makeSymbol( &SynthParser::make_GE ); }
                "<="                    { return makeSymbol( &SynthParser::make_LE ); }
                "=="                    { return makeSymbol( &SynthParser::make_EQ ); }
                "!="                    { return makeSymbol( &SynthParser::make_NE ); }

                // keywords
                "="                     { return makeSymbol( &SynthParser::make_ASSIGN ); }
                "+="                    { return makeSymbol( &SynthParser::make_PLUS_ASSIGN ); }
                "-="                    { return makeSymbol( &SynthParser::make_MINUS_ASSIGN ); }
                "*="                    { return makeSymbol( &SynthParser::make_MUL_ASSIGN ); }
                "/="                    { return makeSymbol( &SynthParser::make_DIV_ASSIGN ); }
                "if"                    { return makeSymbol( &SynthParser::make_IF ); }
                "elif"                  { return makeSymbol( &SynthParser::make_ELIF ); }
                "else"                  { return makeSymbol( &SynthParser::make_ELSE ); }
                "while"                 { return makeSymbol( &SynthParser::make_WHILE ); }
                "break"                 { return makeSymbol( &SynthParser::make_BREAK ); }
                "return"                { return makeSymbol( &SynthParser::make_RETURN ); }

                // identifers
                [a-zA-Z_][a-zA-Z_0-9]*  { return makeSymbol( &SynthParser::make_IDENTIFIER, nstring( anchor, ctx.cursor ) ); }

                // pass though other characters
                .                       { return makeSymbol( []( auto... params ) { return SynthParser::symbol_type( params... ); }, typename SynthParser::token_type( ctx.cursor[-1] & 0xff ) ); }
            %}
        }
    }
}

#pragma warning( pop )
