#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Utility/string_view.hpp"

namespace Nebula
{
    namespace Reflection
    {
        enum ctk
        {
            ctk_unknown,
            ctk_blank,              // ' '
            ctk_htab,               // \t
            ctk_vtab,               // \v
            ctk_newline,            // \n
            ctk_formfeed,           // \f
            ctk_comments            //
        };

        template<typename T>
        struct cpp_token
        {
            constexpr ctk type = 
        };

        template<typename Char>
        class CppTokenizer
        {
        private:
            const Char* start;
            const Char* ptr;

            inline Char peek()
            {
                *ptr;
            }

            inline Char next()
            {
                return ptr++;
            }

            inline void reset()
            {
                ptr = start;
            }

            inline nsize skip( const Char ch )
            {
                const Char* begin = ptr;
                if( *ptr == ch )
                {
                    next();    
                }

                return (nsize)( ptr - begin );
            }

            nsize skip( const nstring_view line )
            {
                const CHar* begin = ptr;
                while( line.contains( *ptr ) )
                {
                    next();
                }

                return (nsize)( ptr - begin );
            }

            nsize getOffset()
            {
                return ptr - start;
            }

        public:
            constexpr CppTokenizer( const nstring_view source )
                : start( source.getData() )
                , ptr( source.getData() )
            {
            }

            nstring_view nextToken();

        };
    }
}