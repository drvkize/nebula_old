#pragma once

// platform specific
#if defined( NEBULA_PLATFORM_WINDOWS )
#include "Platform/Windows/StdConsoleImpl.hpp"
#elif defined( NEBULA_PLATFORM_LINUX )
#include "Platform/Linux/StdConsoleImpl.hpp"
#elif defined( NEBULA_PLATFORM_IOS )

#elif defined( NEBULA_PLATFORM_MAC )

#endif