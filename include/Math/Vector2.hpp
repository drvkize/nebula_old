#pragma once

#include "Math/MathDefine.hpp"
#include "Math/Vector.hpp"

namespace Nebula
{
    namespace Math
    {
        template<typename T>
        class Vector<T, 2>
        {
        public:
            using ScalarType = T;
            static constexpr int Dimension = 2;

            union
            {
                T data[Dimension];
                struct
                {
                    T x;
                    T y;
                };
            };

            inline Vector()
            {
            }

            inline Vector( const Vector<T, 2>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = v.data[i] );
            }

            template<typename U>
            explicit inline Vector( const Vector<U, 2>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = static_cast<T>( v[i] ) );
            }

            explicit inline Vector( const T s )
            {
                NEBULA_VECTOR_LOOP( data[i] = s );
            }

            explicit inline Vector( const T* arr )
            {
                NEBULA_VECTOR_LOOP( data[i] = arr[i] );
            }

            inline Vector( const T s0, const T s1 )
            {
                x = s0;
                y = s1;
            }

            explicit inline Vector( const Vector<T, 3>& v )
            {
                data[0] = v.data[0];
                data[1] = v.data[1];
            }

            explicit inline Vector( const Vector<T, 4>& v )
            {
                data[0] = v.data[0];
                data[1] = v.data[1];
            }

            explicit inline Vector( const VectorPacked<T, 2>& v )
            {
                NEBULA_VECTOR_LOOP( data[i] = v.data[i] );
            }

            inline T& operator()( const int i ) { return data[i]; }
            inline const T& operator()( const int i ) const { return data[i]; }
            inline T& operator[]( const int i ) { return data[i]; }
            inline const T& operator[]( const int i ) const { return data[i]; }

            inline Vector<T, 2> xy() { return Vector<T, 2>( x, y ); }
            inline const Vector<T, 2>& xy() const { return Vector<T, 2>( x, y ); }

            inline void pack( VectorPacked<T, 2>* const v ) const
            {
                NEBULA_VECTOR_LOOP( v[i] = data[i] );
            }
        };

        template<typename T>
        class VectorPacked<T, 2>
        {
        public:
            union
            {
                T data[2];
                struct
                {
                    T x;
                    T y;
                };
            };

            VectorPacked() {};
            explicit VectorPacked( const Vector<T, 2>& v )
            {
                v.pack( this );
            }

            VectorPacked& operator=( const Vector<T, 2>& v )
            {
                v.pack( this );
                return *this;
            }
        };
    }
}