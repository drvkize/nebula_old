#include "IO/StdFileStream.hpp"
#include "IO/StdFile.hpp"

namespace Nebula
{
    namespace IO
    {
        StdFileStreamPtr StdFileStream::create( StdFilePtr file )
        {
            StdFileStreamPtr ptr = std::allocate_shared<StdFileStream>( Memory::nallocator<StdFileStream>() );
            if( ptr != nullptr && ptr->init( file ) )
            {
                return ptr;
            }

            return nullptr;
        }

        StdFileStream::StdFileStream()
        {
        }

        StdFileStream::~StdFileStream()
        {
        }

        nbool StdFileStream::init( StdFilePtr file )
        {
            if( file == nullptr )
            {
                return false;
            }

            this->file = file;

            return true;
        }

        void StdFileStream::release()
        {
            file = nullptr;
        }

        nsize StdFileStream::write( const int8* data, nsize data_size )
        {
            return file->write( data, data_size );
        }

        nsize StdFileStream::read( int8* buffer, nsize buffer_size )
        {
            return file->read( buffer, buffer_size );
        }

        nbool StdFileStream::seek( SeekOption option, int64 offset )
        {
            return file->seek( option, offset );
        }

        int64 StdFileStream::tell()
        {
            return file->tell();
        }

        nbool StdFileStream::isEnd() const
        {
            return file->isEnd();
        }
    }
}