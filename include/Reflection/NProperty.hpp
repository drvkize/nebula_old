#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Reflection/NType.hpp"
#include "Utility/any.hpp"

namespace Nebula
{
    namespace Reflection
    {
        class NProperty
        {
        protected:
            virtual any getDefaultValueImpl() const { return any(); };

        public:
            virtual ~NProperty() {}

            virtual NType getType() const = 0;
            virtual const nstring_view getSignature() const = 0;
            virtual nbool hasName() const { return false; }
            virtual const nstring_view getName() const { return ""; }
            virtual nbool hasDefaultValue() const { return false; }
            template<typename T>
            const T getDefaultValue() const
            {
                return any_cast<T>( getDefaultValueImpl() );
            }
        };

        template<typename T, nbool HasName, nbool HasDefaultValue>
        class NPropertyConcrete : public NProperty {};

        template<typename T, typename C>
        class NPropertyConcrete<T C::*, false, false> : public NProperty
        {
            const T C::* address;

        public:
            static NPropertyPtr create( const T C::* address )
            {
                return NPropertyPtr( new NPropertyConcrete( address ) );
            }

            NPropertyConcrete( const T C::* address )
                : address( address )
            {
            }

            virtual ~NPropertyConcrete() {}

            virtual NType getType() const override { return NType::get<T>(); }
            virtual const nstring_view getSignature() const override { return NType::getSignature<T>(); }
        };

        template<typename T, typename C>
        class NPropertyConcrete<T C::*, true, false> : public NProperty
        {
            const T C::* address;
            const nstring_view name;

        public:
            static NPropertyPtr create( const T C::* address, const nstring_view name )
            {
                return NPropertyPtr( new NPropertyConcrete( address, name ) );
            }

            NPropertyConcrete( const T C::* address, const nstring_view name )                
                : address( address )
                , name( name )
            {
            }

            virtual ~NPropertyConcrete() {}

            virtual NType getType() const override { return NType::get<T>(); }
            virtual const nstring_view getSignature() const override { return NType::getSignature<T>(); }
            virtual inline nbool hasName() const override { return true; }
            virtual inline const nstring_view getName() const override { return name; }
        };

        template<typename T, typename C>
        class NPropertyConcrete<T C::*, false, true> : public NProperty
        {
            const T C::* address;
            any default_value;

        protected:
            virtual any getDefaultValueImpl() const override { return default_value; }

        public:
            static NPropertyPtr create( const T C::* address, any&& default_value )
            {
                return NPropertyPtr( NPropertyConcrete( address, std::move( default_value ) ) );
            }

            NPropertyConcrete( const T C::* address, any&& default_value )
                : address( address )
                , defaule_value( default_value )
            {
            }

            virtual ~NPropertyConcrete();

            virtual NType getType() const override { return NType::get<T>(); }
            virtual const nstring_view getSignature() const override { return NType::getSignature<T>(); }
            virtual inline nbool hasDefaultValue() const override { return true; }
        };

        template<typename T, typename C>
        class NPropertyConcrete<T C::*, true, true> : public NProperty
        {
            const T C::* address;
            const nstring_view name;
            any default_value;

        protected:
            virtual any getDefaultValueImpl() const override { return default_value; }

        public:
            static NPropertyPtr create( const T C::* address, const nstring_view name, any&& default_vaule )
            {
                return NPropertyPtr( new NPropertyConcrete( address, name, std::move( default_value ) ) );
            }

            NPropertyConcrete( const T C::* address, const nstring_view name, any&& default_value )
                : address( address )
                , name( name )
                , default_value( default_value )
            {
            }

            virtual ~NPropertyConcrete() {}
            
            virtual NType getType() const override { return NType::get<T>(); }
            virtual const nstring_view getSignature() const override { return NType::getSignature<T>(); }
            virtual inline nbool hasName() const override { return true; }
            virtual inline const nstring_view getName() const override { return name; }
            virtual inline nbool hasDefaultValue() const override { return true; }
        };
    }
}