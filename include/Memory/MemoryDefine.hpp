#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;

namespace Nebula
{
    namespace Memory
    {
        class NebulaAllocator;
    }

    namespace Context
    {
        NEBULA_INTERFACE Memory::NebulaAllocator& getOSAllocator();
        NEBULA_INTERFACE Memory::NebulaAllocator& getSystemAllocator();
    }
}