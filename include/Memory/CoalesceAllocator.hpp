#pragma once

#include "Memory/MemoryDefine.hpp"
#include "Memory/NebulaAllocator.hpp"
#include "Memory/CoalesceChunk.hpp"
#include "Memory/CoalesceBlockTree.hpp"

namespace Nebula
{
    namespace Memory
    {
        class NEBULA_INTERFACE CoalesceAllocator : public NebulaAllocator
        {
            using Block = CoalesceBlock;

            NebulaAllocator* parent_allocator;
            nsize chunk_size;
            nsize chunk_capacity;
            nsize chunk_count;
            CoalesceChunk* chunks;
            CoalesceBlockTree* block_tree;

            int findChunkByAddress( const void* p );
            CoalesceChunk* createChunk();
            void destroyChunk( int index );

        public:
            CoalesceAllocator();
            virtual ~CoalesceAllocator();

            nbool init( NebulaAllocator* parent_allocator, nsize chunk_size );
            virtual void release() override;

            virtual void* allocate( nsize size, nsize align = 1 ) override;
            virtual nbool deallocate( void* p ) override;

			void printTree() { block_tree->printAllNodes(); }
			void printChunks();
        };
    }
}