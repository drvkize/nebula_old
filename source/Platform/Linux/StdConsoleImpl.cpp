#include "Platform/Linux/StdConsoleImpl.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Linux
        {
            StdConsoleImpl::StdConsoleImpl()
            {
            }

            StdConsoleImpl::~StdConsoleImpl()
            {
            }

            void StdConsoleImpl::setTitle( const nchar* title )
            {
                printf( "\033]%s\007", title );
            }

            void StdConsoleImpl::setForegroundColor( ConsoleColor color )
            {
                static constexpr int8 color_table[] =
                {
                    30,     // CC_Black
                    31,     // CC_Red
                    32,     // CC_Green
                    34,     // CC_Blue
                    33,     // CC_Yellow
                    35,     // CC_Magenta
                    36,     // CC_Cyan
                    37      // CC_White
                };

                printf( "\033[%cm", color_table[color] );                
            }

            void StdConsoleImpl::setBackgroundColor( ConsoleColor color )
            {
                static constexpr int8 color_table[] =
                {
                    40,     // CC_Black
                    41,     // CC_Red
                    42,     // CC_Green
                    44,     // CC_Blue
                    43,     // CC_Yellow
                    45,     // CC_Magenta
                    46,     // CC_Cyan
                    47      // CC_White
                };

                printf( "\033[%cm", color_table[color] );
            }

            void StdConsoleImpl::setBold( nbool value )
            {
                printf( "\033[%sm", value ? "1" : "21" );
            }

            void StdConsoleImpl::setUnderline( nbool value )
            {
                printf( "\033[%sm", value ? "4": "24" );
            }

            void StdConsoleImpl::write( const int8* data, nsize data_size )
            {
                printf( "%s", data );
            }

            void StdConsoleImpl::flush()
            {
                fflush( stdout );
            }
        }
    }
}