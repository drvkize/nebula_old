#pragma once

#include "NebulaDefine.hpp"

namespace Nebula
{
    namespace Log
    {
        enum LogLevel : uint32
        {
            LL_Off = 0,
            LL_Trace,
            LL_Debug,
            LL_Info,
            LL_Warning,
            LL_Error,
            LL_Critical,

            LL_Count
        };

        class LoggerSink;
        using LoggerSinkPtr = std::shared_ptr<LoggerSink>;

        class Logger;
        using LoggerPtr = std::shared_ptr<Logger>;

        struct LogMessage;

        class Formatter;
        using FormatterPtr = std::shared_ptr<Formatter>;

        template<typename Lock_t>
        class StdFileSink;

        template<typename Lock_t>
        using StdFileSinkPtr = std::shared_ptr<StdFileSink<Lock_t>>;

        template<typename Lock_t>
        class StdConsoleSink;

        template<typename Lock_t>
        using StdConsoleSinkPtr = std::shared_ptr<StdConsoleSink<Lock_t>>;

        template<typename Lock_t>
        class HtmlFileSink;

        template<typename Lock_t>
        using HtmlFileSinkPtr = std::shared_ptr<HtmlFileSink<Lock_t>>;
    }
}