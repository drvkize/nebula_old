#pragma once

#include "Concurrent/ConcurrentDefine.hpp"
#include "Concurrent/Event.hpp"
#include "Concurrent/Lock.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        using ThreadFunc = void(*)( void* );

        class NEBULA_INTERFACE Thread
        {
        public:
            enum ThreadState
            {
                TS_Unknown,
                TS_Initialized,
                TS_Suspended,
                TS_Running,
                TS_Finished
            };

            Thread( const Thread& other ) = delete;
            Thread& operator=( const Thread& rhs ) = delete;

        private:
            std::thread thread;
            Event signal_run;
            Event signal_stop;

            ThreadState state;
            Lock state_lock;

            ThreadFunc func;
            void* args;

            static void process( void* args );
            void setState( ThreadState new_state );
        
        public:
            Thread();
            ~Thread();

            nbool init( ThreadFunc func, void* args = nullptr );
            void release();

            void resume();
            void suspend( nsize time = 0 );

            inline nsize getThreadID() const { return OS::getThreadID(); }
            inline nbool isInitialized() { return state != ThreadState::TS_Unknown; }
            inline nbool isSuspended() { return state == ThreadState::TS_Suspended; }
            inline nbool isRunning() { return state == ThreadState::TS_Running; }
            inline nbool isFinished() { return state == ThreadState::TS_Finished; }
            inline nbool isThisThread() { return std::this_thread::get_id() == thread.get_id(); }
        };
    }
}