#pragma once

#include "SynthDefine.hpp"

#pragma warning( push )
#pragma warning( disable: 4307 )

namespace Nebula
{
    namespace Synth
    {
        class SynthExpr
        {
            static SynthExprList empty_list;

            static constexpr const nchar* getExprName( SET type )
            {
                switch( type )
                {
                    case SET::SET_NULL: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_NULL );
                    case SET::SET_BOOL: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_BOOL );
                    case SET::SET_INT: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_INT );
                    case SET::SET_FLOAT: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_FLOAT );
                    case SET::SET_STRING: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_STRING );
                    case SET::SET_PARENS: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_PARENS );
                    case SET::SET_BRACES: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_BRACES );
                    case SET::SET_BRACKETS: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_BRACKETS );
                    case SET::SET_DOT: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_DOT );
                    case SET::SET_NEGATE: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_NEGATE );
                    case SET::SET_PLUS: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_PLUS );
                    case SET::SET_MINUS: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_MINUS );
                    case SET::SET_MUL: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_MUL );
                    case SET::SET_DIV: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_DIV );
                    case SET::SET_OR: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_OR );
                    case SET::SET_AND: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_AND );
                    case SET::SET_LT: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_LT );
                    case SET::SET_GT: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_GT );
                    case SET::SET_LE: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_LE );
                    case SET::SET_GE: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_GE );
                    case SET::SET_EQ: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_EQ );
                    case SET::SET_NE: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_NE );
                    case SET::SET_IF: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_IF );
                    case SET::SET_ELIF: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_ELIF );
                    case SET::SET_ELSE: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_ELSE );
                    case SET::SET_WHILE: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_WHILE );
                    case SET::SET_BREAK: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_BREAK );
                    case SET::SET_RETURN: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_RETURN );
                    case SET::SET_ASSIGN: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_ASSIGN );
                    case SET::SET_PLUS_ASSIGN: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_PLUS_ASSIGN );
                    case SET::SET_MINUS_ASSIGN: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_MINUS_ASSIGN );
                    case SET::SET_MUL_ASSIGN: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_MUL_ASSIGN );
                    case SET::SET_DIV_ASSIGN: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_DIV_ASSIGN );
                    case SET::SET_STMTS: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_STMTS );
                    case SET::SET_PARAMDECLS: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_PARAMDECLS );
                    case SET::SET_NODE: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_NODE );
                    case SET::SET_IDENT: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_IDENT );
                    case SET::SET_INVOKE: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_INVOKE );
                    default: return NEBULA_GET_STRING_BY_VALUE( SynthExprType, SET::SET_UNKNOWN );
                };
            }

        protected:
            // debug
            inline nstring depth_prefix( int depth ) const
            {
                return nstring( depth, '\t' );
            }

        public:
            virtual ~SynthExpr();

            virtual SET getExprType() const = 0;
            virtual void addExpr( SynthExprPtr&& param );
            virtual SynthExprList& getExprList();
            virtual void setExpr0( SynthExprPtr&& expr );
            virtual SynthExprPtr getExpr0();
            virtual void setExpr1( SynthExprPtr&& expr );
            virtual SynthExprPtr getExpr1();
            virtual void setExpr2( SynthExprPtr&& expr );
            virtual SynthExprPtr getExpr2();

            inline nstring getExprName() const { return getExprName( getExprType() ); }

            // debug
            virtual nstring to_string( int depth ) const {};
        };

        template<uint32 Type>
        class SynthExprConcrete : public SynthExpr {};

        // synth expression makers
        #define MAKE_SYNTH_EXPR_LITERAL( TYPE, VALUE_TYPE )                                                                 \
        template<>                                                                                                          \
        class SynthExprConcrete<TYPE> : public SynthExpr                                                                    \
        {                                                                                                                   \
            VALUE_TYPE value;                                                                                               \
                                                                                                                            \
        public:                                                                                                             \
            static SynthExprPtr create( VALUE_TYPE&& value )                                                                \
            {                                                                                                               \
                return SynthExprPtr( new SynthExprConcrete( std::move( value ) ) );                                         \
            }                                                                                                               \
                                                                                                                            \
            SynthExprConcrete( VALUE_TYPE&& value )                                                                         \
                : value( std::move( value ) )                                                                               \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual ~SynthExprConcrete()                                                                                    \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual inline SET getExprType() const override { return TYPE; }                                                \
            const VALUE_TYPE& getValue() const { return value; }                                                            \
            VALUE_TYPE getValue() { return value; }                                                                         \
            virtual nstring to_string( int depth ) const override                                                           \
            {                                                                                                               \
                return depth_prefix( depth ) + getExprName() + "(" + std::to_string( value ) + ")\n";                       \
            }                                                                                                               \
        };

        #define MAKE_SYNTH_EXPR_NO_PARAM( TYPE )                                                                            \
        template<>                                                                                                          \
        class SynthExprConcrete<TYPE> : public SynthExpr                                                                    \
        {                                                                                                                   \
        public:                                                                                                             \
            static SynthExprPtr create()                                                                                    \
            {                                                                                                               \
                return SynthExprPtr( new SynthExprConcrete() );                                                             \
            }                                                                                                               \
                                                                                                                            \
            virtual ~SynthExprConcrete()                                                                                    \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual inline SET getExprType() const override { return TYPE; }                                                \
            virtual nstring to_string( int depth ) const override                                                           \
            {                                                                                                               \
                return depth_prefix( depth ) + getExprName() + "\n";                                                        \
            }                                                                                                               \
        };

        #define MAKE_SYNTH_EXPR_UNARY( TYPE )                                                                               \
        template<>                                                                                                          \
        class SynthExprConcrete<TYPE> : public SynthExpr                                                                    \
        {                                                                                                                   \
            SynthExprPtr expr;                                                                                              \
                                                                                                                            \
        public:                                                                                                             \
            static SynthExprPtr create( SynthExprPtr&& expr )                                                               \
            {                                                                                                               \
                return SynthExprPtr( new SynthExprConcrete( std::move( expr ) ) );                                          \
            }                                                                                                               \
                                                                                                                            \
            SynthExprConcrete( SynthExprPtr&& expr )                                                                        \
                : expr( std::move( expr ) )                                                                                 \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual ~SynthExprConcrete()                                                                                    \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual inline SET getExprType() const override { return TYPE; }                                                \
            virtual void setExpr0( SynthExprPtr&& expr ) { expr = std::move( expr ); }                                      \
            virtual inline SynthExprPtr getExpr0() override { return expr; }                                                \
            virtual nstring to_string( int depth ) const override                                                           \
            {                                                                                                               \
                nstring ret = depth_prefix( depth ) + getExprName() + "\n";                                                 \
                if( expr != nullptr ) ret += expr->to_string( depth + 1 );                                                  \
                return ret;                                                                                                 \
            }                                                                                                               \
        };

        #define MAKE_SYNTH_EXPR_BINARY( TYPE )                                                                              \
        template<>                                                                                                          \
        class SynthExprConcrete<TYPE> : public SynthExpr                                                                    \
        {                                                                                                                   \
            SynthExprPtr expr0;                                                                                             \
            SynthExprPtr expr1;                                                                                             \
                                                                                                                            \
        public:                                                                                                             \
            static SynthExprPtr create( SynthExprPtr&& expr0, SynthExprPtr&& expr1 )                                        \
            {                                                                                                               \
                return SynthExprPtr( new SynthExprConcrete( std::move( expr0 ), std::move( expr1 ) ) );                     \
            }                                                                                                               \
                                                                                                                            \
            SynthExprConcrete( SynthExprPtr&& expr0, SynthExprPtr&& expr1 )                                                 \
                : expr0( std::move( expr0 ) )                                                                               \
                , expr1( std::move( expr1 ) )                                                                               \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual ~SynthExprConcrete()                                                                                    \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual inline SET getExprType() const override { return TYPE; }                                                \
            virtual void setExpr0( SynthExprPtr&& expr ) { expr0 = std::move( expr ); }                                     \
            virtual inline SynthExprPtr getExpr0() override { return expr0; }                                               \
            virtual void setExpr1( SynthExprPtr&& expr ) { expr1 = std::move( expr ); }                                     \
            virtual inline SynthExprPtr getExpr1() override { return expr1; }                                               \
            virtual nstring to_string( int depth ) const override                                                           \
            {                                                                                                               \
                nstring ret = depth_prefix( depth ) + getExprName() + "\n";                                                 \
                if( expr0 != nullptr ) ret += expr0->to_string( depth + 1 );                                                \
                if( expr1 != nullptr ) ret += expr1->to_string( depth + 1 );                                                \
                return ret;                                                                                                 \
            }                                                                                                               \
        };

        #define MAKE_SYNTH_EXPR_TERNARY( TYPE )                                                                             \
        template<>                                                                                                          \
        class SynthExprConcrete<TYPE> : public SynthExpr                                                                    \
        {                                                                                                                   \
            SynthExprPtr expr0;                                                                                             \
            SynthExprPtr expr1;                                                                                             \
            SynthExprPtr expr2;                                                                                             \
                                                                                                                            \
        public:                                                                                                             \
            static SynthExprPtr create( SynthExprPtr&& expr0, SynthExprPtr&& expr1, SynthExprPtr&& expr2 )                  \
            {                                                                                                               \
                return SynthExprPtr( new SynthExprConcrete(                                                                 \
                    std::move( expr0 ), std::move( expr1 ), std::move( expr2 ) ) );                                         \
            }                                                                                                               \
                                                                                                                            \
            SynthExprConcrete( SynthExprPtr&& expr0, SynthExprPtr&& expr1, SynthExprPtr&& expr2 )                           \
                : expr0( std::move( expr0 ) )                                                                               \
                , expr1( std::move( expr1 ) )                                                                               \
                , expr2( std::move( expr2 ) )                                                                               \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual ~SynthExprConcrete()                                                                                    \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual inline SET getExprType() const override { return TYPE; }                                                \
            virtual void setExpr0( SynthExprPtr&& expr ) { expr0 = std::move( expr ); }                                     \
            virtual inline SynthExprPtr getExpr0() override { return expr0; }                                               \
            virtual void setExpr1( SynthExprPtr&& expr ) { expr1 = std::move( expr ); }                                     \
            virtual inline SynthExprPtr getExpr1() override { return expr1; }                                               \
            virtual void setExpr2( SynthExprPtr&& expr ) { expr2 = std::move( expr ); }                                     \
            virtual inline SynthExprPtr getExpr2() override { return expr2; }                                               \
            virtual nstring to_string( int depth ) const override                                                           \
            {                                                                                                               \
                nstring ret = depth_prefix( depth ) + getExprName() + "\n";                                                 \
                if( expr0 != nullptr ) ret += expr0->to_string( depth + 1 );                                                \
                if( expr1 != nullptr ) ret += expr1->to_string( depth + 1 );                                                \
                if( expr2 != nullptr ) ret += expr2->to_string( depth + 1 );                                                \
                return ret;                                                                                                 \
            }                                                                                                               \
        };

        #define MAKE_SYNTH_EXPR_LIST( TYPE )                                                                                \
        template<>                                                                                                          \
        class SynthExprConcrete<TYPE> : public SynthExpr                                                                    \
        {                                                                                                                   \
            SynthExprList expr_list;                                                                                        \
                                                                                                                            \
        public:                                                                                                             \
            template<typename... Tn>                                                                                        \
            static SynthExprPtr create( Tn&&... exprs )                                                                     \
            {                                                                                                               \
                return SynthExprPtr( new SynthExprConcrete( std::move( exprs )... ) );                                      \
            }                                                                                                               \
                                                                                                                            \
            template<typename... Tn>                                                                                        \
            SynthExprConcrete( Tn&&... exprs )                                                                              \
                : expr_list{ std::move( exprs )... }                                                                        \
            {                                                                                                               \
            }                                                                                                               \
                                                                                                                            \
            virtual inline SET getExprType() const override { return TYPE; }                                                \
            virtual inline void addExpr( SynthExprPtr&& expr ) override { expr_list.push_back( std::move( expr ) ); }       \
            virtual inline SynthExprList& getExprList() { return expr_list; }                                               \
            virtual nstring to_string( int depth ) const override                                                           \
            {                                                                                                               \
                nstring ret = depth_prefix( depth ) + getExprName() + "\n";                                                 \
                for( SynthExprList::const_iterator it = expr_list.begin(); it != expr_list.end(); ++it )                    \
                {                                                                                                           \
                    if( *it != nullptr )                                                                                    \
                    {                                                                                                       \
                        ret += ( *it )->to_string( depth + 1 );                                                             \
                    }                                                                                                       \
                }                                                                                                           \
                return ret;                                                                                                 \
            }                                                                                                               \
        };

        MAKE_SYNTH_EXPR_NO_PARAM( SET::SET_UNKNOWN );
        MAKE_SYNTH_EXPR_NO_PARAM( SET::SET_NULL );
        MAKE_SYNTH_EXPR_LITERAL( SET::SET_BOOL, nbool );
        MAKE_SYNTH_EXPR_LITERAL( SET::SET_INT, int32 );
        MAKE_SYNTH_EXPR_LITERAL( SET::SET_FLOAT, nfloat );
        MAKE_SYNTH_EXPR_LITERAL( SET::SET_STRING, nstring );
        MAKE_SYNTH_EXPR_LIST( SET::SET_PARENS );
        MAKE_SYNTH_EXPR_LIST( SET::SET_BRACES );
        MAKE_SYNTH_EXPR_LIST( SET::SET_BRACKETS );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_DOT );
        MAKE_SYNTH_EXPR_UNARY( SET::SET_NEGATE );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_PLUS );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_MINUS );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_MUL );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_DIV );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_OR );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_AND );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_LT );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_GT );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_LE );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_GE );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_EQ );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_NE );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_IF );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_ELIF );
        MAKE_SYNTH_EXPR_UNARY( SET::SET_ELSE );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_WHILE );
        MAKE_SYNTH_EXPR_NO_PARAM( SET::SET_BREAK );
        MAKE_SYNTH_EXPR_UNARY( SET::SET_RETURN );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_ASSIGN );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_PLUS_ASSIGN );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_MINUS_ASSIGN );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_MUL_ASSIGN );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_DIV_ASSIGN );
        MAKE_SYNTH_EXPR_LIST( SET::SET_STMTS );
        MAKE_SYNTH_EXPR_LIST( SET::SET_PARAMDECLS );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_NODE );
        MAKE_SYNTH_EXPR_LITERAL( SET::SET_IDENT, nstring );
        MAKE_SYNTH_EXPR_BINARY( SET::SET_INVOKE );
    }
}

#pragma warning( pop )