#pragma once

#include "Memory/MemoryDefine.hpp"

namespace Nebula
{
    namespace Memory
    {
        struct CoalesceBlock
        {
            using Block = CoalesceBlock;

            enum Flag : uint32
            {
                Used = 0
            };

			uint32 flag;
            Block* prev;
            Block* next;

            CoalesceBlock()
                : prev( nullptr )
                , next( nullptr )
                , flag( 0 )
            {
            }

            static inline Block* pointerToBlock( void* p ) { return reinterpret_cast<Block*>( p ) - 1; }
            static inline void* blockToPointer( Block* p ) { return reinterpret_cast<void*>( p + 1 ); }
            static void insertAfter( Block* prev, Block* block );
            static void remove( Block* block );

            inline void setFree() { flag &= ~( 1 << Flag::Used ); }
            inline void setUsed() { flag |= ( 1 << Flag::Used ); }
            inline nbool isFree() const { return ( flag & ( 1 << Flag::Used ) ) == 0; }
            inline nbool isUsed() const { return ( flag & ( 1 << Flag::Used ) ) != 0; }

            inline int8* getBlock() { return reinterpret_cast<int8*>( this ); }
			inline const int8* getBlock() const { return reinterpret_cast<const int8*>( this ); }
			inline int8* getBlockEnd() { return reinterpret_cast<int8*>( next ); }
			inline const int8* getBlockEnd() const { return reinterpret_cast<const int8*>( next ); }
            inline nsize getBlockSize() const { return getBlockEnd() - getBlock(); }

            inline int8* getData() { return getBlock() + sizeof( Block ); }
			inline const int8* getData() const { return getBlock() + sizeof( Block ); }
            inline int8* getDataEnd() { return getBlockEnd(); }
			inline const int8* getDataEnd() const { return getBlockEnd(); }
            inline nsize getDataSize() const { return getDataEnd() - getData(); }

			static nsize getAvailableSize( const Block* block, nsize align );
            static int compare( const Block* left, const Block* right );
        };
    }
}