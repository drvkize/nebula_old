#include "Reflection/NType.hpp"

namespace Nebula
{
    namespace Reflection
    {
        nunordered_map<uint32, type_stub_base*> NType::types;
    }
}