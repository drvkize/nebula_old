#pragma once

#include "IO/Serialization/NTypeDefine.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Serialization
        {
            template<typename T>
            struct ScalarTypeToTag
            {
                using Type = T;
                static constexpr NTypeTag Tag = NTypeTag::NTT_Unknown;
            };

            template<> struct ScalarTypeToTag<void> { static constexpr NTypeTag Tag = NTypeTag::NTT_Null; };
            template<> struct ScalarTypeToTag<nbool> { static constexpr NTypeTag Tag = NTypeTag::NTT_Bool; };
            template<> struct ScalarTypeToTag<int8> { static constexpr NTypeTag Tag = NTypeTag::NTT_Int8; };
            template<> struct ScalarTypeToTag<int16> { static constexpr NTypeTag Tag = NTypeTag::NTT_Int16; };
            template<> struct ScalarTypeToTag<int32> { static constexpr NTypeTag Tag = NTypeTag::NTT_Int32; };
            template<> struct ScalarTypeToTag<int64> { static constexpr NTypeTag Tag = NTypeTag::NTT_Int64; };
            template<> struct ScalarTypeToTag<uint8> { static constexpr NTypeTag Tag = NTypeTag::NTT_UInt8; };
            template<> struct ScalarTypeToTag<uint16> { static constexpr NTypeTag Tag = NTypeTag::NTT_UInt16; };
            template<> struct ScalarTypeToTag<uint32> { static constexpr NTypeTag Tag = NTypeTag::NTT_UInt32; };
            template<> struct ScalarTypeToTag<uint64> { static constexpr NTypeTag Tag = NTypeTag::NTT_UInt64; };
            template<> struct ScalarTypeToTag<nfloat> { static constexpr NTypeTag Tag = NTypeTag::NTT_Float; };
            template<> struct ScalarTypeToTag<ndouble> { static constexpr NTypeTag Tag = NTypeTag::NTT_Double; };
            template<> struct ScalarTypeToTag<int2> { static constexpr NTypeTag Tag = NTypeTag::NTT_Int2; };
            template<> struct ScalarTypeToTag<int3> { static constexpr NTypeTag Tag = NTypeTag::NTT_Int3; };
            template<> struct ScalarTypeToTag<int4> { static constexpr NTypeTag Tag = NTypeTag::NTT_Int4; };
            template<> struct ScalarTypeToTag<float2> { static constexpr NTypeTag Tag = NTypeTag::NTT_Float2; };
            template<> struct ScalarTypeToTag<float3> { static constexpr NTypeTag Tag = NTypeTag::NTT_Float3; };
            template<> struct ScalarTypeToTag<float4> { static constexpr NTypeTag Tag = NTypeTag::NTT_Float4; };
            template<> struct ScalarTypeToTag<float2x2> { static constexpr NTypeTag Tag = NTypeTag::NTT_Float2x2; };
            template<> struct ScalarTypeToTag<float3x3> { static constexpr NTypeTag Tag = NTypeTag::NTT_Float3x3; };
            template<> struct ScalarTypeToTag<float4x4> { static constexpr NTypeTag Tag = NTypeTag::NTT_Float4x4; };
            template<> struct ScalarTypeToTag<float4x3> { static constexpr NTypeTag Tag = NTypeTag::NTT_Float4x3; };
            template<> struct ScalarTypeToTag<double2> { static constexpr NTypeTag Tag = NTypeTag::NTT_Double2; };
            template<> struct ScalarTypeToTag<double3> { static constexpr NTypeTag Tag = NTypeTag::NTT_Double3; };
            template<> struct ScalarTypeToTag<double4> { static constexpr NTypeTag Tag = NTypeTag::NTT_Double4; };
            template<> struct ScalarTypeToTag<double2x2> { static constexpr NTypeTag Tag = NTypeTag::NTT_Double2x2; };
            template<> struct ScalarTypeToTag<double3x3> { static constexpr NTypeTag Tag = NTypeTag::NTT_Double3x3; };
            template<> struct ScalarTypeToTag<double4x4> { static constexpr NTypeTag Tag = NTypeTag::NTT_Double4x4; };
            template<> struct ScalarTypeToTag<double4x3> { static constexpr NTypeTag Tag = NTypeTag::NTT_Double4x3; };
        }
    }
}