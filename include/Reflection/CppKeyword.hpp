#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Utility/ctstring.hpp"
#include "Math/Hash.hpp"
#include "Utility/string_view.hpp"

namespace Nebula
{
    namespace Reflection
    {
        #pragma warning( push )
        #pragma warning( disable: 4307 )
        
        #define ENUM_NAME_HASH( name ) Math::Hash::fnv_ct( STRINGFY( name ) )
        #define KEYWORD_ENTRY( name, value ) ckw_##name = value
        #define MAKE_KEYWORD_ENTRY( name ) KEYWORD_ENTRY( name, ENUM_NAME_HASH( name ) )

        enum ckw : uint32
        {
            ckw_unknown = 0,
            MAKE_KEYWORD_ENTRY( alignas ),
            MAKE_KEYWORD_ENTRY( alignof ),
            MAKE_KEYWORD_ENTRY( and ),
            MAKE_KEYWORD_ENTRY( asm ),
            MAKE_KEYWORD_ENTRY( auto ),
            MAKE_KEYWORD_ENTRY( bitand ),
            MAKE_KEYWORD_ENTRY( bitor ),
            MAKE_KEYWORD_ENTRY( bool ),
            MAKE_KEYWORD_ENTRY( break ),
            MAKE_KEYWORD_ENTRY( case ),
            MAKE_KEYWORD_ENTRY( catch ),
            MAKE_KEYWORD_ENTRY( char ),
            MAKE_KEYWORD_ENTRY( char8_t ),
            MAKE_KEYWORD_ENTRY( char16_t ),
            MAKE_KEYWORD_ENTRY( char32_t ),
            MAKE_KEYWORD_ENTRY( class ),
            MAKE_KEYWORD_ENTRY( compl ),
            MAKE_KEYWORD_ENTRY( const ),
            MAKE_KEYWORD_ENTRY( constexpr ),
            MAKE_KEYWORD_ENTRY( constcast ),
            MAKE_KEYWORD_ENTRY( continue ),
            MAKE_KEYWORD_ENTRY( decltype ),
            MAKE_KEYWORD_ENTRY( default ),
            MAKE_KEYWORD_ENTRY( delete ),
            MAKE_KEYWORD_ENTRY( do ),
            MAKE_KEYWORD_ENTRY( double ),
            MAKE_KEYWORD_ENTRY( dynamic_cast ),
            MAKE_KEYWORD_ENTRY( else ),
            MAKE_KEYWORD_ENTRY( enum ),
            MAKE_KEYWORD_ENTRY( explicit ),
            MAKE_KEYWORD_ENTRY( extern ),
            MAKE_KEYWORD_ENTRY( false ),
            MAKE_KEYWORD_ENTRY( float ),
            MAKE_KEYWORD_ENTRY( for ),
            MAKE_KEYWORD_ENTRY( friend ),
            MAKE_KEYWORD_ENTRY( goto ),
            MAKE_KEYWORD_ENTRY( if ),
            MAKE_KEYWORD_ENTRY( inline ),
            MAKE_KEYWORD_ENTRY( int ),
            MAKE_KEYWORD_ENTRY( long ),
            MAKE_KEYWORD_ENTRY( mutable ),
            MAKE_KEYWORD_ENTRY( namespace ),
            MAKE_KEYWORD_ENTRY( new ),
            MAKE_KEYWORD_ENTRY( noexcept ),
            MAKE_KEYWORD_ENTRY( not ),
            MAKE_KEYWORD_ENTRY( not_eq ),
            MAKE_KEYWORD_ENTRY( nullptr ),
            MAKE_KEYWORD_ENTRY( operator ),
            MAKE_KEYWORD_ENTRY( or ),
            MAKE_KEYWORD_ENTRY( eq ),
            MAKE_KEYWORD_ENTRY( private ),
            MAKE_KEYWORD_ENTRY( protected ),
            MAKE_KEYWORD_ENTRY( public ),
            MAKE_KEYWORD_ENTRY( register ),
            MAKE_KEYWORD_ENTRY( reinterpret_cast ),
            MAKE_KEYWORD_ENTRY( return ),
            MAKE_KEYWORD_ENTRY( short ),
            MAKE_KEYWORD_ENTRY( signed ),
            MAKE_KEYWORD_ENTRY( sizeof ),
            MAKE_KEYWORD_ENTRY( static ),
            MAKE_KEYWORD_ENTRY( static_assert ),
            MAKE_KEYWORD_ENTRY( static_cast ),
            MAKE_KEYWORD_ENTRY( struct ),
            MAKE_KEYWORD_ENTRY( switch ),
            MAKE_KEYWORD_ENTRY( template ),
            MAKE_KEYWORD_ENTRY( this ),
            MAKE_KEYWORD_ENTRY( thread_local ),
            MAKE_KEYWORD_ENTRY( throw ),
            MAKE_KEYWORD_ENTRY( true ),
            MAKE_KEYWORD_ENTRY( try ),
            MAKE_KEYWORD_ENTRY( typedef ),
            MAKE_KEYWORD_ENTRY( typeid ),
            MAKE_KEYWORD_ENTRY( typename ),
            MAKE_KEYWORD_ENTRY( union ),
            MAKE_KEYWORD_ENTRY( unsigned ),
            MAKE_KEYWORD_ENTRY( using ),
            MAKE_KEYWORD_ENTRY( virtual ),
            MAKE_KEYWORD_ENTRY( void ),
            MAKE_KEYWORD_ENTRY( volatile ),
            MAKE_KEYWORD_ENTRY( wchar_t ),
            MAKE_KEYWORD_ENTRY( while ),
            MAKE_KEYWORD_ENTRY( xor ),
            MAKE_KEYWORD_ENTRY( xor_eq )
        };

        #pragma warning( pop )

        ckw getKeyword( const nstring_view name )
        {
            return static_cast<ckw>( Math::Hash::fnv( name.c_str() ) );
        }
    }
}