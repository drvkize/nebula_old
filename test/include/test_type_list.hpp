#pragma once

#include "Nebula.hpp"
#include "Reflection/NType.hpp"
#include "Platform/Language/type_list.hpp"
using Nebula::Reflection::NType;

int test( int argc, char **argv )
{
    using t0 = typename ntraits::make_type_list<int8, int16, int32>::type;
    using t1 = typename ntraits::push_back_n<t0, int64, nstring>::type;
    using t2 = typename ntraits::push_back_n<t0, ntraits::type_list<int64, nstring>>::type;
    using t3 = typename ntraits::push_front_n<t0, int64, nstring>::type;
    using t4 = typename ntraits::push_front_n<t0, ntraits::type_list<int64, nstring>>::type;
    using t5 = typename ntraits::pop_back<t0>::type;
    using t6 = typename ntraits::pop_front<t5>::type;
    using t7 = typename ntraits::pop_back<t6>::type;
    using t8 = typename ntraits::pop_front<t6>::type;

    printf( "%s\n", ntraits::is_type_list_v<t0> ? "true" : "false" );
    printf( "%s\n", ntraits::is_type_list_v<std::tuple<int8, int16, int32>> ? "true" : "false" );
    printf( "%s\n", NType::getSignature<ntraits::type_list_element_t<0, t0>>().c_str() );
    printf( "%s\n", NType::getSignature<ntraits::type_list_element_t<1, t0>>().c_str() );
    printf( "%s\n", NType::getSignature<ntraits::type_list_element_t<2, t0>>().c_str() );

    printf( "%s\n", NType::getSignature<t0>().c_str() );
    printf( "%s\n", NType::getSignature<t1>().c_str() );
    printf( "%s\n", NType::getSignature<t2>().c_str() );
    printf( "%s\n", NType::getSignature<t3>().c_str() );
    printf( "%s\n", NType::getSignature<t4>().c_str() );
    printf( "%s\n", NType::getSignature<t5>().c_str() );
    printf( "%s\n", NType::getSignature<t6>().c_str() );
    printf( "%s\n", NType::getSignature<t7>().c_str() );
    printf( "%s\n", NType::getSignature<t8>().c_str() );
    
    getchar();
    return 0;
}