#pragma once

#include "IO/IODefine.hpp"
#include "IO/HeteroStream.hpp"

namespace Nebula
{
    namespace IO
    {
        class NEBULA_INTERFACE StdFileStream : public HeteroStream
        {
            StdFilePtr file;

        public:
            static StdFileStreamPtr create( StdFilePtr file );
            StdFileStream();
            virtual ~StdFileStream();

            StdFileStream( const StdFileStream& other ) = delete;
            StdFileStream& operator=( const StdFileStream& rhs ) = delete;

            nbool init( StdFilePtr file );
            void release();

            virtual nsize write( const int8* data, nsize data_size ) override;
            virtual nsize read( int8* buffer, nsize buffer_size ) override;
            virtual nbool seek( SeekOption option, int64 offset ) override;
            virtual int64 tell() override;
            virtual nbool isEnd() const override;
        };
    }
}