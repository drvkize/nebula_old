#include "Concurrent/ReadWriteLock.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        ReadWriteLock::ReadWriteLock()
            : count( 0 )
        {
        }

        ReadWriteLock::~ReadWriteLock()
        {
            nassert( count == 0, "[ReadWriteLock::~ReadWriteLock()]: read count is no zero" );
        }

        void ReadWriteLock::lockRead()
        {
            count_lock.acquire();
            {
                ++count;
                if( count > 0 )
                {
                    write_lock.acquire();
                }
            }
            count_lock.release();
        }

        void ReadWriteLock::unlockRead()
        {
            count_lock.acquire();
            {
                --count;
                if( count == 0 )
                {
                    write_lock.release();
                }
            }
            count_lock.release();
        }

        void ReadWriteLock::lockWrite()
        {
            write_lock.acquire();
        }

        void ReadWriteLock::unlockWrite()
        {
            write_lock.release();
        }
    }
}