#pragma once

#include "Log/LogDefine.hpp"

namespace Nebula
{
    namespace Log
    {
        struct NEBULA_INTERFACE LogMessage
        {
            const nstring* logger_name;
            LogLevel level;
            std::tm time;
            nsize thread_id;
            Buffer data;

            LogMessage();
            LogMessage( const nstring* logger_name, LogLevel level, const nstring& message );

            LogMessage( const LogMessage& other ) = delete;
            LogMessage& operator=( const LogMessage& rhs ) = delete;
            LogMessage( const LogMessage&& other ) noexcept = delete;
            LogMessage& operator=( LogMessage&& rhs ) noexcept = delete;
        };
    }
}