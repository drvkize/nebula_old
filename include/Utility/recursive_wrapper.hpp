#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;

namespace Nebula
{
    namespace Platform
    {
        template<typename T>
        class recursive_wrapper
        {
            T* p;

            void assign( T const& rhs )
            {
                this->get() = rhs;
            }

        public:
            using type = T;

            recursive_wrapper()
                : p( new T )
            {
            }
            
            recursive_wrapper( const T& other )
                : p( new T( other ) )
            {
            }

            recursive_wrapper( const recursive_wrapper& other )
                : p( new T( other.get() ) )
            {
            }

            recursive_wrapper( recursive_wrapper&& other ) noexcept
            {
                swap( *this, other );
            }

            ~recursive_wrapper() noexcept
            {
                delete p;
            }

            inline friend void swap( recursive_wrapper& lhs, recursive_wrapper& rhs ) noexcept
            {
                std::swap( lhs.p, rhs.p );
            }

            inline recursive_wrapper& operator=( const recursive_wrapper& rhs )
            {
                assign( rhs.get() );
                return *this;
            }

            inline recursive_wrapper& operator=( const T& rhs )
            {
                assign( rhs );
                return *this;
            }

            inline recursive_wrapper& operator=( T&& rhs ) noexcept
            {
                get() = std::move( rhs );
                return *this;
            }

            inline recursive_wrapper& operator=( recursive_wrapper&& rhs ) noexcept
            {
                swap( *this, rhs );
                return *this;
            }            

            T& get()
            {
                assert( p && "recursive wrapper, pointer is null" );
                return *get_pointer();
            }

            const T& get() const
            {
                assert( p && "recursive wrapper, pointer is null" );
                return *get_pointer();
            }

            T* get_pointer() { return p; }

            const T* get_pointer() const { return p; }

            operator const T&() const { return this->get(); }

            operator T&() { this->get(); }
        };
    }
}