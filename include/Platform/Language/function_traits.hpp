#pragma once

#include "Platform/Language/common.hpp"
#include "Platform/Language/type_list.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
            template<typename T>
            struct function_traits {};

            template<typename R, typename... Tn>
            struct function_traits<R ( Tn... )>
            {
                using return_type = R;
                using parameter_types = ntraits::make_type_list_t<Tn...>;
            };

            template<typename R, typename... Tn>
            struct function_traits<R (*)( Tn... )> : public function_traits<R ( Tn... )> {};
            template<typename R, typename... Tn>
            struct function_traits<R (&)( Tn... )> : public function_traits<R ( Tn... )> {};
            template<typename R, typename C, typename... Tn>
            struct function_traits<R (C::*)( Tn... )> : public function_traits<R ( Tn... )> { using class_type = C; };
            template<typename R, typename C, typename... Tn>
            struct function_traits<R (C::*)( Tn... ) const> : public function_traits<R ( Tn... )> { using class_type = C; };
            template<typename R, typename C, typename... Tn>
            struct function_traits<R (C::*)( Tn... ) volatile> : public function_traits<R ( Tn... )> { using class_type = C; };
            template<typename R, typename C, typename... Tn>
            struct function_traits<R (C::*)( Tn... ) const volatile> : public function_traits<R ( Tn... )> { using class_type = C; };
        }
    }
}