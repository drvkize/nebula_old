#pragma once

#include "Log/LogDefine.hpp"
#include "Log/LoggerSink.hpp"
#include "Concurrent/Lock.hpp"

namespace Nebula
{
    namespace Log
    {
        struct LoggerSinkDummyLock
        {
            inline void acquire() {}
            inline void release() {}
            inline nbool tryAcquire() { return true; }
        };

        template<typename LockType>
        class LoggerSinkBase : public LoggerSink
        {
            LockType lock;

        protected:
            virtual void doLog( const LogMessage& message ) = 0;
            virtual void doFlush() = 0;

        public:
            LoggerSinkBase() = default;

            LoggerSinkBase( const LoggerSinkBase& other ) = delete;
            LoggerSinkBase& operator=( const LoggerSinkBase& rhs ) = delete;

            virtual void log( const LogMessage& msg ) override
            {
                lock.acquire();
                {
                    doLog( msg );
                }
                lock.release();
            }

            virtual void flush() override
            {
                lock.acquire();
                {
                    doFlush();
                }
                lock.release();
            }
        };
    }
}