#pragma once

#include "Nebula.hpp"
#include "Reflection/Reflection.hpp"
#include "Reflection/NParameter.hpp"
using Nebula::Reflection::NParameter;
using Nebula::Reflection::NParameterConcrete;
using Nebula::Reflection::NParameterPtr;
using Nebula::Reflection::NParameterHelper;
#include "Reflection/NProperty.hpp"
using Nebula::Reflection::NProperty;
using Nebula::Reflection::NPropertyConcrete;
using Nebula::Reflection::NPropertyPtr;
#include "Reflection/NConstructor.hpp"
using Nebula::Reflection::NConstructor;
using Nebula::Reflection::NConstructorConcrete;
using Nebula::Reflection::NConstructorPtr;
#include "Reflection/NConstructorBuilder.hpp"
using Nebula::Reflection::NConstructorBuilder;
#include "Reflection/NMethod.hpp"
using Nebula::Reflection::NMethod;
using Nebula::Reflection::NMethodConcrete;
using Nebula::Reflection::NMethodPtr;
#include "Reflection/NMethodBuilder.hpp"
using Nebula::Reflection::NMethodBuilder;

class tstruct
{
    int a;
    int b;

public:
    static tstruct create()
    {
        return tstruct();
    }

    tstruct()
        : a( 0 )
        , b ( -1 )
    {
    }

    tstruct( int a, int b )
        : a( a )
        , b( b )
    {
    }

    void print()
    {
        printf( "a = %d, b = %d\n", a, b );
    }

    int sum()
    {
        return a + b;
    }

    nfloat sum_scale( nfloat c )
    {
        return sum() * c;
    }
};

template<typename T, typename C>
NPropertyPtr createProperty( const T C::* address )
{
    return NPropertyConcrete<T C::*, false, false>::create( address );
}

template<typename T, typename C>
NPropertyPtr createProperty( const T C::* address, const nstring_view name )
{
    return NPropertyConcrete<T C::*, true, false>::create( address, name );
}

tstruct test_create( int, int )
{
    return tstruct();
}

int test( int argc, char** argv )
{
    NConstructorBuilder<tstruct> builder0;
    NConstructorPtr refl_ctr0 = builder0.build();
    NConstructorBuilder<tstruct, int, int> builder1;
    NConstructorPtr refl_ctr1 = builder1.build();
    NMethodBuilder<decltype( &tstruct::print )> builder2( &tstruct::print, "print" );
    NMethodPtr method_print = builder2.build();
    NMethodBuilder<decltype( &tstruct::sum )> builder3( &tstruct::sum, "sum" );
    NMethodPtr method_sum = builder3.build();
    NMethodBuilder<decltype( &tstruct::sum_scale )> builder4( &tstruct::sum_scale, "sum_scale" );
    NMethodPtr method_sum_scale = builder4.build();

    tstruct x0 = any_cast<tstruct>( refl_ctr0->invoke() );
    tstruct x1 = any_cast<tstruct>( refl_ctr1->invoke( 1234, 737 ) );

    method_print->invoke( any_ptr( &x0 ) );
    method_print->invoke( any_ptr( &x1 ) );
    
    printf( "sum = %d\n", any_cast<int>( method_sum->invoke( any_ptr( &x0 ) ) ) );
    printf( "sum_scale = %f\n", any_cast<nfloat>( method_sum_scale->invoke( any_ptr( &x1 ), 2.0f ) ) );
    
    getchar();
    return 0;
}