#pragma once

#include "Math/MathDefine.hpp"
#include "Math/Vector.hpp"

namespace Nebula
{
    namespace Math
    {
        #define NEBULA_MATRIX_LOOP( op ) NEBULA_UNROLLED_LOOP( i, Columns, op )

        template<typename T, int R, int C>
        class Matrix
        {
        public:
            using ScalarType = T;
            static constexpr int Rows = R;
            static constexpr int Columns = C;
        
            Vector<T, R> data[C];

            inline Matrix()
            {
            }

            inline Matrix( const Matrix& m )
            {
                NEBULA_MATRIX_LOOP( data[i] = m.data[i] );
            }

            explicit inline Matrix( const T s )
            {
                NEBULA_MATRIX_LOOP( data[i] = s );
            }

            inline Matrix(
                const T s00, const T s10,
                const T s01, const T s11
            )
            {
                static_assert( Rows == 2 && Columns == 2, "[Matrix::Matrix()]: dimension mismatch" );
                data[0] = Vector<T, Rows>( s00, s10 );
                data[1] = Vector<T, Rows>( s01, s11 );
            }

            inline Matrix(
                const T s00, const T s10, const T s20,
                const T s01, const T s11, const T s21,
                const T s02, const T s12, const T s22
            )
            {
                static_assert( Rows == 3 && Columns == 3, "[Matrix::Matrix()]: dimension mismatch" );
                data[0] = Vector<T, Rows>( s00, s10, s20 );
                data[1] = Vector<T, Rows>( s01, s11, s21 );
                data[2] = Vector<T, Rows>( s02, s12, s22 );
            }

            inline Matrix(
                const T s00, const T s10, const T s20, const T s30,
                const T s01, const T s11, const T s21, const T s31,
                const T s02, const T s12, const T s22, const T s32
            )
            {
                static_assert( Rows == 4 && Columns == 3, "[Matrix::Matrix()]: dimension mismatch" );
                data[0] = Vector<T, Rows>( s00, s10, s20, s30 );
                data[1] = Vector<T, Rows>( s01, s11, s21, s31 );
                data[2] = Vector<T, Rows>( s02, s12, s22, s32 );
            }

            inline Matrix(
                const T s00, const T s10, const T s20, const T s30,
                const T s01, const T s11, const T s21, const T s31,
                const T s02, const T s12, const T s22, const T s32,
                const T s03, const T s13, const T s23, const T s33
            )
            {
                static_assert( Rows == 4 && Columns == 4, "[Matrix::Matrix()]: dimension mismatch" );
                data[0] = Vector<T, Rows>( s00, s10, s20, s30 );
                data[1] = Vector<T, Rows>( s01, s11, s21, s31 );
                data[2] = Vector<T, Rows>( s02, s12, s22, s32 );
                data[3] = Vector<T, Rows>( s03, s13, s23, s33 );
            }

            inline Matrix( const Vector<T, 4>& v0, const Vector<T, 4>& v1, const Vector<T, 4>& v2, const Vector<T, 4>& v3 )
            {
                static_assert( Rows == 4 && Columns == 4, "[Matrix::Matrix()]: dimension mismatch" );
                data[0] = v0;
                data[1] = v1;
                data[2] = v2;
                data[3] = v3;
            }

            explicit inline Matrix( const T* const arr )
            {
                NEBULA_MATRIX_LOOP( ( data[i] = Vector<T, Rows>( &arr[i * Columns] ) ) );
            }

            explicit inline Matrix( const VectorPacked<T, Rows>* const vs )
            {
                NEBULA_MATRIX_LOOP( ( data[i] = Vector<T, Rows>( vs[i] ) ) );
            }

            inline T& operator()( const int r, const int c )
            {
                return data[c][r];
            }

            inline const T& operator()( const int r, const int c ) const
            {
                return data[c][r];
            }

            inline T operator()( const int i )
            {
                return operator[]( i );
            }

            inline const T operator()( const int i ) const
            {
                return operator[]( i );
            }

            inline T operator[]( const int i )
            {
                return reinterpret_cast<T*>( data )[i];
            }

            inline const T operator[]( const int i ) const
            {
                return reinterpret_cast<const T*>( data )[i];
            }

            inline Vector<T, Rows>& GetColumn( const int i )
            {
                return data[i];
            }

            inline const Vector<T, Rows>& GetColumn( const int i ) const
            {
                return data[i];
            }

            inline void pack( VectorPacked<T, Rows>* const vs ) const
            {
                NEBULA_MATRIX_LOOP( GetColumn( i ).pack( &vs[i] ) );
            }
        };
    }

    namespace Platform
    {
        using float2x2 = Math::Matrix<nfloat, 2, 2>;
        using float3x3 = Math::Matrix<nfloat, 3, 3>;
        using float4x4 = Math::Matrix<nfloat, 4, 4>;
        using float4x3 = Math::Matrix<nfloat, 4, 3>;
        using double2x2 = Math::Matrix<ndouble, 2, 2>;
        using double3x3 = Math::Matrix<ndouble, 3, 3>;
        using double4x4 = Math::Matrix<ndouble, 4, 4>;
        using double4x3 = Math::Matrix<ndouble, 4, 3>;
    }
}