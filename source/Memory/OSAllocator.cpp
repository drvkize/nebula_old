#include "Memory/OSAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        OSAllocator::OSAllocator()
        {
        }

        OSAllocator::~OSAllocator()
        {
        }

        void* OSAllocator::allocate( nsize size, nsize align )
        {
            return OS::allocate( size );
        }

        nbool OSAllocator::deallocate( void* p )
        {
            OS::deallocate( p );
            return true;
        }

        NebulaAllocator& getOSAllocator()
        {
            static OSAllocator _stub_os_allocator;
            return _stub_os_allocator;
        }
    }
}