#include "Memory/CoalesceAllocator.hpp"

namespace Nebula
{
	namespace Memory
	{
		int CoalesceAllocator::findChunkByAddress( const void* p )
		{
			int left = 0;
			int right = static_cast<int>( chunk_count ) - 1;

			while( left <= right )
			{
				int middle = ( left + right ) >> 1;

				const void* start = chunks[middle].getFirstBlock();
				const void* end = chunks[middle].getLastBlock();
				if( p < start )
				{
					right = middle - 1;
				}
				else if( p > end )
				{
					left = middle + 1;
				}
				else
				{
					return middle;
				}
			}

			return left - 1;
		}

		CoalesceChunk* CoalesceAllocator::createChunk()
		{
			if( chunk_count == chunk_capacity )
			{
				chunk_capacity = ( chunk_capacity > 0 ) ? chunk_capacity + chunk_capacity / 2 : 2;

				CoalesceChunk* p = static_cast<CoalesceChunk*>( parent_allocator->allocate( chunk_capacity * sizeof( CoalesceChunk ), 16 ) );
				assert( p && "[CoalesceAllocator::createChunk()]: allocate chunk array failed" );
				if( p != nullptr )
				{
					if( chunks != nullptr )
					{
						memcpy( p, chunks, chunk_count * sizeof( CoalesceChunk ) );
						parent_allocator->deallocate( chunks );
					}

					chunks = p;
				}
			}

			CoalesceChunk new_chunk;
			if( new_chunk.init( parent_allocator, chunk_size, 16, block_tree ) )
			{
				int insert_position = findChunkByAddress( new_chunk.getStartAddress() ) + 1;
				for( int i = static_cast<int>( chunk_count ) - 1; i >= insert_position; --i )
				{
					chunks[i] = std::move( chunks[i - 1] );
				}

				// chunks[insert_position] = std::move( *( new( &chunks[insert_position] ) CoalesceChunk() ) );
				chunks[insert_position] = std::move( new_chunk );
				++chunk_count;

				// printChunks();

				return &chunks[insert_position];
			}

			return nullptr;
		}

		void CoalesceAllocator::destroyChunk( int index )
		{
			if( index >= 0 && index < chunk_count )
			{
				chunks[index].release();
				for( nsize i = index + 1; i < chunk_count; ++i )
				{
					chunks[i - 1] = std::move( chunks[i] );
				}

				--chunk_count;
			}
		}

		CoalesceAllocator::CoalesceAllocator()
			: parent_allocator( nullptr )
			, chunk_size( 0 )
			, chunk_capacity( 0 )
			, chunk_count( 0 )
			, chunks( nullptr )
			, block_tree( nullptr )
		{
		}

		CoalesceAllocator::~CoalesceAllocator()
		{
		}

		nbool CoalesceAllocator::init( NebulaAllocator* parent_allocator, nsize chunk_size )
		{
			block_tree = reinterpret_cast<CoalesceBlockTree*>( parent_allocator->allocate( sizeof( CoalesceBlockTree ) ) );
			block_tree = new( block_tree ) CoalesceBlockTree();
			if( !block_tree->init( parent_allocator ) )
			{
				return false;
			}

			// commit
			this->parent_allocator = parent_allocator;
			this->chunk_size = chunk_size;

			return true;
		}

		void CoalesceAllocator::release()
		{
			// clean up
			if( parent_allocator != nullptr && chunk_count > 0 )
			{
				for( nsize i = 0; i < chunk_count; ++i )
				{
					chunks[i].release();
				}

				block_tree->release();
				block_tree->~CoalesceBlockTree();
				parent_allocator->deallocate( block_tree );
				block_tree = nullptr;

				parent_allocator->deallocate( chunks );
				parent_allocator = nullptr;

                chunk_size = 0;
                chunk_capacity = 0;
                chunk_count = 0;
                chunks = nullptr;
			}
		}

		void* CoalesceAllocator::allocate( nsize size, nsize align )
		{
			if( size == 0 )
			{
				return nullptr;
			}

			// try find best block
			Block* block = const_cast<Block*>( block_tree->find( size, align ) );
			if( block != nullptr )
			{
				int chunk_index = findChunkByAddress( block );
				if( chunk_index >= 0 )
				{
					Block* new_block = chunks[chunk_index].split( block, size, align );
					if( new_block != nullptr )
					{
						return new_block->getData();
					}
				}
			}

			// no chunk or 
			// try create new chunk for new block
			CoalesceChunk* chunk = createChunk();
			if( chunk != nullptr )
			{
				Block* new_block = chunk->split( chunk->getFirstBlock(), size, align );
				return new_block != nullptr ? Block::blockToPointer( new_block ) : nullptr;
			}
			else
			{
				assert( 0 && "[CoalesceAllocator::allocate]: create new coalesce chunk failed" );
			}

			return nullptr;
		}

		nbool CoalesceAllocator::deallocate( void* p )
		{
			int chunk_index = findChunkByAddress( p );

			if( chunk_index >= 0 )
			{
				Block* block = Block::pointerToBlock( p );
				CoalesceChunk* chunk = &chunks[chunk_index];
				if( block->isUsed() && chunk->isAllocated( p ) )
				{
					chunk->merge( block );

                    if( chunk->isEmpty() )
					{
						destroyChunk( chunk_index );
					}

					return true;
				}
			}

			return false;
		}

		void CoalesceAllocator::printChunks()
		{
			for( nsize i = 0; i < chunk_count; ++i )
			{
				chunks[i].printBlocks();
			}
		}
	}
}