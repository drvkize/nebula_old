#pragma once

#include "Log/LogDefine.hpp"
#include "Log/LoggerSinkBase.hpp"
#include "IO/StdConsoleSPI.hpp"
#include "Platform/PlatformImpl.hpp"

namespace Nebula
{
    namespace Log
    {
        static constexpr IO::ConsoleColor color_scheme[] =
        {
            IO::ConsoleColor::CC_White,             // Off
            IO::ConsoleColor::CC_White,             // Trace
            IO::ConsoleColor::CC_Cyan,              // Debug
            IO::ConsoleColor::CC_Green,             // Info
            IO::ConsoleColor::CC_Yellow,            // Warning
            IO::ConsoleColor::CC_Magenta,           // Error
            IO::ConsoleColor::CC_Red                // Red
        };

        template<typename LockType>
        class StdConsoleSink : public LoggerSinkBase<LockType>
        {
            nchar last_char;

            void writeContent( const LogMessage& message )
            {
                if( last_char == '\n' || last_char == '\0' )
                {
                    nstring line = nformat( "[{:%H:%M:%S}] ", message.time );
                    IO::StdConsole::write( line );
                }

                IO::StdConsole::write( message.data.getData(), message.data.getSize() );
                last_char = *reinterpret_cast<const nchar*>( message.data.getData() + message.data.getSize() - 1 );
            }

        protected:
            virtual void doLog( const LogMessage& message ) override
            {
                IO::StdConsole::setForegroundColor( color_scheme[message.level] );
                writeContent( message );
                doFlush();
            }

            virtual void doFlush() override
            {
                IO::StdConsole::flush();
            }

        public:
            static LoggerSinkPtr create( LogLevel level = LogLevel::LL_Info )
            {
                StdConsoleSinkPtr<LockType> ret = std::allocate_shared<StdConsoleSink>( Memory::nallocator<StdConsoleSink>() );
                if( ret != nullptr && ret->init( level ) )
                {
                    return ret;
                }

                return nullptr;
            }

            StdConsoleSink()
                : last_char( '\0' )
            {
            }

            ~StdConsoleSink()
            {
            }

            StdConsoleSink( const StdConsoleSink& other ) = delete;
            StdConsoleSink& operator=( const StdConsoleSink& rhs ) = delete;

            nbool init( LogLevel level = LogLevel::LL_Info )
            {
                LoggerSinkBase<LockType>::setLogLevel( level );
                last_char = '\0';
                return true;
            }

            void release()
            {
                IO::StdConsole::flush();
            }
        };

        using ConsoleSinkST = StdConsoleSink<DummyLock>;
        using ConsoleSinkMT = StdConsoleSink<Concurrent::Lock>;
    }
}