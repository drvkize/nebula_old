#pragma once

#include "Graphics/GraphicsDefine.hpp"
#include "System/NebulaDevice.hpp"

namespace Nebula
{
    namespace Graphics
    {
        class RenderDevice : public System::NebulaDevice
        {
        public:
            RenderDevice();
            virtual ~RenderDevice();

            createShader();
            createTexture();
            createBuffer();
        };
    }
}