#include "Platform/Linux/PlatformLinux.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Linux
        {
            std::tm* localtime( const std::time_t* t )
            {
                return ::localtime( t );
            }
        }
    }

    namespace Memory
    {
        namespace Linux
        {
            void* allocate( nsize size )
            {
                return ::malloc( size );
            }

            void deallocate( void* p )
            {
                ::free( p );
            }
        }
    }

    namespace Concurrent
    {
        namespace Linux
        {
            nsize getThreadID()
            {
                return pthread_self();
            }
        }
    }

    namespace System
    {
        namespace Linux
        {
            nbool isValid( OSPluginHandle handle )
            {
                return handle != nullptr;
            }

            OSPluginHandle loadPlugin( const nchar* path )
            {
                return dlopen( path, RTLD_LAZY );
            }

            void unloadPlugin( OSPluginHandle handle )
            {
                dlclose(handle );
            }

            void* getEntryPoint( OSPluginHandle handle, const nchar* name )
            {
                return dlsym( handle, name );
            }
        }
    }
}