#pragma once

#include "Math/Hash.hpp"
#include "Synth/SynthDefine.hpp"
#include "Reflection/NType.hpp"
using Nebula::Reflection::NType;
#include "Math/Hash.hpp"

namespace Nebula
{
    namespace Synth
    {
        struct SynthIdentifier
        {
            nstring name;
            NType type;

            static inline SynthIdentifierPtr create( const nstring_view name )
            {
                return SynthIdentifierPtr( new SynthIdentifier( name, NType::get<void>() ) );
            }

            static inline SynthIdentifierPtr create( const nstring_view name, NType type )
            {
                return SynthIdentifierPtr( new SynthIdentifier( name, type ) );
            }

            SynthIdentifier()
            {
            }

            SynthIdentifier( const nstring_view name, NType type )
                : name( name.c_str() )
                , type( type )
            {
            }
        };

        struct SynthScope
        {
            using ScopeTable = nunordered_map<uint32, SynthScopePtr>;
            using IdentifierTable = nunordered_map<uint32, SynthIdentifierPtr>;

            SynthScope* parent_scope;
            nstring name;
            IdentifierTable identifiers;
            ScopeTable scopes;

            static inline SynthScopePtr create( const nstring_view name )
            {
                return create( nullptr, name );
            }

            static inline SynthScopePtr create( SynthScope* parent, const nstring_view name )
            {
                return SynthScopePtr( new SynthScope( parent, name ) );
            }

            SynthScope()
                : parent_scope( nullptr )
            {
            }

            SynthScope( const nstring_view name )
                : parent_scope( nullptr )
                , name( name.c_str() )
            {
            }

            SynthScope( SynthScope* parent, const nstring_view name )
                : parent_scope( parent )
                , name( name.c_str() )
            {
            }

            SynthScope& push( const nstring_view name )
            {
                std::pair<ScopeTable::iterator, nbool> ret = scopes.insert( ScopeTable::value_type( Math::Hash::fnv( name.c_str() ), SynthScope::create( this, name ) ) );
                return ret.second ? *ret.first->second : *this;
            }
            
            SynthScope& pop()
            {
                return parent_scope != nullptr ? *parent_scope : *this;
            }

            SynthScopePtr getScope( const nstring_view name )
            {
                ScopeTable::iterator it = scopes.find( Math::Hash::fnv( name.c_str() ) );
                return it != scopes.end() ? it->second : nullptr;
            }

            void addIdentifier( const nstring_view name, SynthIdentifierPtr ident )
            {
                identifiers.insert( IdentifierTable::value_type( Math::Hash::fnv( name.c_str() ), SynthIdentifier::create( name ) ) );
            }

            SynthIdentifierPtr getIdentifier( const nstring_view name )
            {
                IdentifierTable::iterator it = identifiers.find( Math::Hash::fnv( name.c_str() ) );
                return it != identifiers.end() ? it->second : nullptr;
            }
        };

        struct SynthContext
        {
            const nchar* cursor;
            const nchar* marker;
            location loc;

            SynthExprPtr root_expr;
            SynthScope root_scope;
            SynthScope& current_scope;

            SynthContext()
                : current_scope( root_scope )
                , cursor( nullptr )
                , marker( nullptr )
            {
            }

            void push( const nstring_view name )
            {
                current_scope = current_scope.push( name );
            }

            void pop()
            {
                current_scope = current_scope.pop();
            }
        };
    }
}