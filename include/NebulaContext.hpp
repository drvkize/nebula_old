#pragma once

#include "Memory/NebulaAllocator.hpp"

namespace Nebula
{
    class NEBULA_INTERFACE NebulaContext
    {
    public:
        NebulaContext();
        ~NebulaContext();

        NebulaContext( const NebulaContext& other ) = delete;
        NebulaContext& operator=( const NebulaContext& rhs ) = delete;
    };
}