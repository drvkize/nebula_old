#pragma once

#include "Memory/MemoryDefine.hpp"
#include "Memory/NebulaAllocator.hpp"

namespace Nebula
{
    namespace Memory
    {
        class FixedSizeChunk
        {
        public:
            FixedSizeChunk( const FixedSizeChunk& other ) = delete;
            FixedSizeChunk& operator=( const FixedSizeChunk& rhs ) = delete;

        private:
            NebulaAllocator* parent_allocator;
            void* address;
            int8* data_address;
            nsize block_size;
            nsize block_count;
            nsize next_free_block;
            nsize free_block_count;

            inline nsize readBlock( const int8* p )
            {
                return *( reinterpret_cast<const nsize*>( p ) );
            }

            inline void writeBlock( int8* p, nsize value )
            {
                *( reinterpret_cast<nsize*>( p ) ) = value;
            }

        public:
            FixedSizeChunk();
            FixedSizeChunk( FixedSizeChunk&& other ) noexcept;
            ~FixedSizeChunk();

			void move( FixedSizeChunk&& rhs ) noexcept;
            friend void swap( FixedSizeChunk& lhs, FixedSizeChunk& rhs ) noexcept;
            FixedSizeChunk& operator=( FixedSizeChunk&& rhs ) noexcept;

            nbool init( NebulaAllocator* parent_allocator, nsize block_size, nsize count, nsize align );
            void release();

            void* allocate();
            void deallocate( void* p );

            inline nbool isFull() const { return free_block_count == 0; }
            inline nbool isEmpty() const { return free_block_count == block_count; }
            inline const int8* getDataAddress() const { return data_address; }
            inline nbool isAllocated( void* p ) const { return p >= data_address && p < data_address + block_size * block_count; }
        };
    }
}