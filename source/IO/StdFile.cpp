#include "IO/StdFile.hpp"

namespace Nebula
{
    namespace IO
    {
        StdFilePtr StdFile::create( const nchar* path, const nchar* mode )
        {
            StdFilePtr ret = std::allocate_shared<StdFile>( Memory::nallocator<StdFile>() );
            if( ret->init( path, mode ) )
            {
                return ret;
            }

            return nullptr;
        }

        StdFile::StdFile()
            : file( nullptr )
        {
        }

        StdFile::~StdFile()
        {
            release();
        }

        nbool StdFile::init( const nchar* path, const nchar* mode )
        {
            file = std::fopen( path, mode );
            return file != nullptr;
        }

        void StdFile::release()
        {
            if( file != nullptr )
            {
                std::fclose( file );
                file = nullptr;
            }
        }

        nsize StdFile::read( int8* buffer, nsize buffer_size )
        {
            if( file != nullptr )
            {
                return std::fread( buffer, 1, buffer_size, file );
            }

            return 0;
        }

        nsize StdFile::write( const int8* data, nsize data_size )
        {
            if( file != nullptr )
            {
                return std::fwrite( data, 1, data_size, file );
            }

            return 0;
        }

        nbool StdFile::seek( SeekOption option, int64 offset )
        {
            return std::fseek( file, (int32)offset, option ) == 0;
        }

        int64 StdFile::tell() const
        {
            return std::ftell( file );
        }

        nbool StdFile::isEnd() const
        {
            return std::feof( file ) == 0;
        }

        nsize StdFile::getFileSize()
        {
            if( file != nullptr )
            {
                int64 offset = tell();
                seek( SeekOption::SO_End, 0 );
                nsize ret = static_cast<nsize>( tell() );
                seek( SeekOption::SO_Set, offset );
                return ret;
            }

            return 0;
        }

        void StdFile::flush()
        {
            fflush( file );
        }
    }
}