#include "Concurrent/Thread.hpp"

namespace Nebula
{
    namespace Concurrent
    {
        void Thread::process( void* args )
        {
            Thread* owner = reinterpret_cast<Thread*>( args );
            if( owner == nullptr )
            {
                return;
            }

            if( owner->isInitialized() )
            {
                owner->suspend();
            }

            owner->func( owner->args );

            owner->setState( ThreadState::TS_Finished );
            owner->signal_stop.setAll();
        }

        void Thread::setState( ThreadState new_state )
        {
            state_lock.acquire();
            state = new_state;
            state_lock.release();
        }

        Thread::Thread()
            : func( nullptr )
            , args( nullptr )
            , state( ThreadState::TS_Unknown )
        {
        }

        Thread::~Thread()
        {
        }

        nbool Thread::init( ThreadFunc func, void* args )
        {
            if( isInitialized() )
            {
                return false;
            }

            if( func != nullptr )
            {
                this->func = func;
                this->args = args;
                setState( ThreadState::TS_Initialized );
                thread = std::thread( &Thread::process, this );

                return true;
            }

            return false;
        }

        void Thread::release()
        {
            while( isRunning() || isSuspended() )
            {
                signal_stop.waitOne();
            }

            if( thread.joinable() )
            {
                thread.join();
            }
        }

        void Thread::resume()
        {
            if( isSuspended() )
            {
                setState( ThreadState::TS_Running );
                signal_run.setAll();
            }
        }

        void Thread::suspend( nsize time )
        {
            if( state == ThreadState::TS_Running )
            {
                setState( ThreadState::TS_Suspended );
                signal_run.waitOne( time );
            }
        }
    }
}