#pragma once

#include "Log/LogDefine.hpp"
#include "Log/LoggerSinkBase.hpp"
#include "IO/StdFile.hpp"

namespace Nebula
{
    namespace Log
    {
        template<typename LockType>
        class StdFileSink : public LoggerSinkBase<LockType>
        {
            IO::StdFilePtr file;
            nchar last_char;

        protected:
            virtual void doLog( const LogMessage& message ) override
            {
                if( last_char == '\n' || last_char == '\0' )
                {
                    nstring line = nformat( "[{:%H:%M:%S}] ", message.time );
                    file->write( reinterpret_cast<const int8*>( line.c_str() ), line.size() );
                }

                file->write( message.data.getData(), message.data.getSize() );
                last_char = *reinterpret_cast<const nchar*>( message.data.getData() + message.data.getSize() - 1 );
            }

            virtual void doFlush() override
            {
                file->flush();
            }
        
        public:
            static LoggerSinkPtr create( const nchar* path, LogLevel level = LogLevel::LL_Info )
            {
                StdFileSinkPtr<LockType> ret = std::allocate_shared<StdFileSink>( Memory::nallocator<StdFileSink>() );
                if( ret != nullptr && ret->init( path, level ) )
                {
                    return ret;
                }

                return nullptr;
            }

            StdFileSink()
                : last_char( '\0' )
            {
            }

            ~StdFileSink()
            {
                release();
            }

            nbool init( const nchar* path, LogLevel level = LogLevel::LL_Info )
            {
                file = IO::StdFile::create( path, "w" );
                if( file != nullptr )
                {
                    LoggerSink::setLogLevel( level );
                    return true;
                }

                return false;
            }

            void release()
            {
                file = nullptr;
            }
        };

        using StdFileSinkST = StdFileSink<LoggerSinkDummyLock>;
        using StdFileSinkMT = StdFileSink<Concurrent::Lock>;
    }
}