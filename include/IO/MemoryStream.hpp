#pragma once

#include "IO/IODefine.hpp"
#include "IO/HeteroStream.hpp"

namespace Nebula
{
    namespace IO
    {
        class NEBULA_INTERFACE MemoryStream : public HeteroStream
        {
            BufferPtr buffer;
            int64 offset;

        public:
            static MemoryStreamPtr create( BufferPtr buffer = nullptr );
            MemoryStream();
            virtual ~MemoryStream();

            MemoryStream( const MemoryStream& other ) = delete;
            MemoryStream& operator=( const MemoryStream& rhs ) = delete;

            nbool init( BufferPtr buffer );
            void release();

            virtual nsize write( const int8* data, nsize data_size ) override;
            virtual nsize read( int8* buffer, nsize buffer_size ) override;
            virtual nbool seek( SeekOption option, int64 offset ) override;
            virtual int64 tell() override;
            virtual nbool isEnd() const override;

            inline BufferPtr getDataBuffer() { return buffer; }
        };
    }
}