#include "IO/Serialization/NTypeWriter.hpp"

namespace Nebula
{
    namespace IO
    {
        namespace Serialization
        {
            // NTypeWriter
            NTypeWriter::NTypeWriter()
            {
            }

            NTypeWriter::~NTypeWriter()
            {
            }

            // NStringWriter
            NStringWriterPtr NStringWriter::create( const nstring& value )
            {
                return NStringWriterPtr( new NStringWriter( value ) );
            }

            NStringWriter::NStringWriter( const nstring& value )
                : tag( NTypeTag::NTT_String )
                , data( value )
            {
            }

            NStringWriter::~NStringWriter()
            {
            }

            nsize NStringWriter::serialize( HeteroStreamPtr stream )
            {
                nsize written = 0;
                written += stream->write( tag );                    
                written += stream->write( static_cast<uint64>( sizeof( NTypeTag ) + sizeof( uint64 ) + data.length() + 1 ) );
                written += stream->write( reinterpret_cast<const int8*>( data.c_str() ), data.length() );
                written += stream->write( '\0' );
                return written;
            }            

            // NObjectWriter
            NObjectWriterPtr NObjectWriter::create()
            {
                return NObjectWriterPtr( new NObjectWriter() );
            }

            NObjectWriter::NObjectWriter()
                : tag( NTypeTag::NTT_Object )
            {
            }

            NObjectWriter::~NObjectWriter()
            {
            }

            nsize NObjectWriter::serialize( HeteroStreamPtr stream )
            {
                nsize written = 0;

                // tag
                written += stream->write( tag );

                // size
                int64 size_offset = stream->tell();
                written += stream->write( 0ull );

                // count
                written += stream->write( static_cast<uint64>( members.size() ) );

                for( nmap<nstring, NTypeWriterPtr>::iterator it = members.begin(); it != members.end(); ++it )
                {
                    const nstring& name = it->first;
                    NTypeWriterPtr writer = it->second;

                    // name
                    written += stream->write( static_cast<uint64>( name.length() + 1 ) );
                    written += stream->write( reinterpret_cast<const int8*>( name.c_str() ), name.length() );
                    written += stream->write( '\0' );

                    // data
                    written += it->second->serialize( stream );
                }

                 // fill size value
                int64 end_offset = stream->tell();
                stream->seek( SeekOption::SO_Set, size_offset );
                stream->write( static_cast<uint64>( written ) );
                stream->seek( SeekOption::SO_Set, end_offset );

                return written;
            }

            void NObjectWriter::add( const nstring& name, nbool value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NBoolWriter::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, int8 value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NInt8Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, int16 value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NInt16Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, int32 value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NInt32Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, int64 value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NInt64Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, uint8 value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NUInt8Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, uint16 value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NUInt16Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, uint32 value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NUInt32Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, uint64 value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NUInt64Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, nfloat value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NFloatWriter::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, ndouble value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NDoubleWriter::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const int2& value )
            {
                 members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NInt2Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const int3& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NInt3Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const int4& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NInt4Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const float2& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NFloat2Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const float3& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NFloat3Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const float4& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NFloat4Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const float2x2& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NFloat2x2Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const float3x3& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NFloat3x3Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const float4x4& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NFloat4x4Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const float4x3& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NFloat4x3Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const double2& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NDouble2Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const double3& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NDouble3Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const double4& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NDouble4Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const double2x2& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NDouble2x2Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const double3x3& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NDouble3x3Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const double4x4& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NDouble4x4Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const double4x3& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NDouble4x3Writer::create( value ) ) );
            }

            void NObjectWriter::add( const nstring& name, const nstring& value )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NStringWriter::create( value ) ) );
            }

            NObjectWriter& NObjectWriter::addObject( const nstring& name )
            {
                NObjectWriterPtr writer;
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, writer ) );
                return *writer;
            }

            void NObjectWriter::addBinary( const nstring& name, const int8* data, nsize size )
            {
                members.insert( nmap<nstring, NTypeWriterPtr>::value_type( name, NBinaryWriter::create( data, size ) ) );
            }

            // NBinaryWriter
            NBinaryWriterPtr NBinaryWriter::create( const int8* value, nsize size )
            {
                return NBinaryWriterPtr( new NBinaryWriter( value, size ) );
            }

            NBinaryWriter::NBinaryWriter( const int8* value, nsize size )
            {
                data.copy( value, size );
            }

            NBinaryWriter::~NBinaryWriter()
            {
            }

            nsize NBinaryWriter::serialize( HeteroStreamPtr stream )
            {
                nsize written = 0;

                // tag
                written += stream->write( tag );

                // size
                written += stream->write( static_cast<uint64>( sizeof( NTypeTag ) + sizeof( uint64 ) + data.getSize() ) );

                // data
                written += stream->write( data.getData(), data.getSize() );

                return written;
            }
        }
    }
}