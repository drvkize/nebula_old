#pragma once

#include "Memory/MemoryDefine.hpp"
#include "Memory/FixedSizeChunk.hpp"

namespace Nebula
{
    namespace Memory
    {
        class NEBULA_INTERFACE FixedSizeChunkAllocator : public NebulaAllocator
        {
            NebulaAllocator* parent_allocator;
            nsize block_size;
            nsize chunk_count;
            nsize chunk_capacity;
            FixedSizeChunk* chunks;
            FixedSizeChunk* last_allocated_chunk;
            FixedSizeChunk* last_deallocated_chunk;

            nsize findFreeChunk();
            int findChunkByAddress( const void* p );
            FixedSizeChunk* createChunk();
            void destroyChunk( nsize index );

        public:
            FixedSizeChunkAllocator();
            virtual ~FixedSizeChunkAllocator();

            nbool init( NebulaAllocator* parent_allocator, nsize block_size );
            virtual void release() override;

            virtual void* allocate( nsize size, nsize align = 1 ) override;
            virtual nbool deallocate( void* p ) override;
        };
    }
}