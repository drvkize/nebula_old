#pragma once

#include "Nebula.hpp"
#include "Reflection/NType.hpp"
using Nebula::Reflection::NType;

RegisterType( int64, int64 );

int test(int argc, nchar **argv)
{
    NType x = NType::get<int64>();
    printf( "%u\n", x.getID() );
    printf( "%u\n", NType::getID<int64>() );
    printf( "%s\n", NType::getSignature<int64>().c_str() );
    printf( "%s\n", NType::getName<int64>() );

    getchar();

    return 0;
}