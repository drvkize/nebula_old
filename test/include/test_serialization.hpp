#pragma once

#include "Nebula.hpp"
#include "Log/Logger.hpp"
#include "IO/ByteSwap.hpp"
#include "IO/StdFile.hpp"
using Nebula::IO::StdFile;
using Nebula::IO::StdFilePtr;
#include "IO/MemoryStream.hpp"
using Nebula::IO::MemoryStream;
using Nebula::IO::MemoryStreamPtr;
using Nebula::IO::HeteroStream;
using Nebula::IO::HeteroStreamPtr;
#include "IO/StdFileStream.hpp"
using Nebula::IO::StdFileStream;
using Nebula::IO::StdFileStreamPtr;
#include "IO/Serialization/NTypeDefine.hpp"
#include "IO/Serialization/NTypeWriter.hpp"
#include "IO/Serialization/NTypeReader.hpp"
using namespace Nebula::IO::Serialization;

int test(int argc, nchar **argv)
{
    StdFileStreamPtr stream = StdFileStream::create( StdFile::create( "test.bin", "wb" ) );
    NObjectWriterPtr ndoc = NObjectWriter::create();
    ndoc->add( "bool", true );
    ndoc->add( "int8", (int8)1 );
    ndoc->add( "int16", (int16)2 );
    ndoc->add( "int32", (int32)3 );
    ndoc->add( "int64", (int64)4 );
    ndoc->add( "uint8", (uint8)5 );
    ndoc->add( "uint16", (uint16)6 );
    ndoc->add( "uint32", (uint32)7 );
    ndoc->add( "uint64", (uint64)8 );
    ndoc->add( "nfloat", (nfloat)9.0f );
    ndoc->add( "ndouble", (ndouble)10.0 );
    ndoc->add( "string", "hello" );
    NArrayWriter<int>& arr_in = ndoc->addArray<int>( "array" );
    arr_in.add( 127 );
    arr_in.add( 194 );
    ndoc->serialize( stream );

    stream = StdFileStream::create( StdFile::create( "test.bin", "rb" ) );

    stream->seek( Nebula::IO::SeekOption::SO_End, 0 );
    nsize size = stream->tell();
    stream->seek( Nebula::IO::SeekOption::SO_Set, 0 );
    MemoryBufferPtr<int8> buffer = MemoryBuffer<int8>::create( size );
    nsize read_size = stream->read( buffer->getData(), size );

    for( nsize i = 0; i < size; ++i )
    {
        printf( "%08x", *reinterpret_cast<const uint8*>( buffer->getData() + i ) );
    }
    printf( "\n" );

    const NTypeReader& reader = NTypeReader::create( buffer->getData() );
    const NObjectReader& nobj = reader.asObject();
    printf( "bool = %s\n", nobj.getMember( "bool" ).asBool() ? "true" : "false" );
    printf( "int8 = %d\n", nobj.getMember( "int8" ).asInt8() );
    printf( "int16 = %d\n", nobj.getMember( "int16" ).asInt16() );
    printf( "int32 = %d\n", nobj.getMember( "int32" ).asInt32() );
    printf( "int64 = %lld\n", nobj.getMember( "int64" ).asInt64() );
    printf( "uint8 = %u\n", nobj.getMember( "uint8" ).asUInt8() );
    printf( "uint16 = %u\n", nobj.getMember( "uint16" ).asUInt16() );
    printf( "uint32 = %u\n", nobj.getMember( "uint32" ).asUInt32() );
    printf( "uint64 = %llu\n", nobj.getMember( "uint64" ).asUInt64() );
    printf( "nfloat = %f\n", nobj.getMember( "nfloat" ).asFloat() );
    printf( "ndouble = %f\n", nobj.getMember( "ndouble" ).asDouble() );
    printf( "string = %s\n", nobj.getMember( "string" ).asString().c_str() );
    const NArrayReader<int>& arr_out = nobj.getMember( "array" ).asArray<int>();
    for( uint64 i = 0; i < arr_out.getCount(); ++i )
    {
        printf( "array[%llu] = %d\n", i, arr_out[i] );
    }

    getchar();

    return 0;
}