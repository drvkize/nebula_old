#pragma once

#include "Platform/Language/common.hpp"
#include "Platform/Language/numeric.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
            template<typename T, T... I>
            struct integer_sequence
            {
                using type = T;

                template<T N>
                using append = integer_sequence<T, I..., N>;
                using next = append<sizeof...( I )>;

                static constexpr nsize count = sizeof...( I );
            };

            template<typename T, T Index, nsize N>
            struct gen_integer_sequence
            {
                using type = typename gen_integer_sequence<T, Index - 1, N - 1>::type::next;
            };

            template<typename T, T Index>
            struct gen_integer_sequence<T, Index, 0ull>
            {
                using type = integer_sequence<T>;
            };

            template<typename T, T From, nsize Count, T... I>
            struct gen_integer_sequence_from_to
            {
                using type = typename std::conditional<Count == 0,
                    type_holder<integer_sequence<T, I...>>,
                    gen_integer_sequence_from_to<T, From + 1, Count - 1, I..., From>
                >::type::type;
            };

            template<nsize... I>
            using index_sequence = integer_sequence<nsize, I...>;

            template<typename T, T N>
            using make_integer_sequence = typename gen_integer_sequence<T, N, N>::type;

			template<nsize From, nsize Count, nsize... I>
			using make_index_sequence_from_to = typename gen_integer_sequence_from_to<nsize, From, Count, I...>::type;

            template<nsize N>
            using make_index_sequence = make_integer_sequence<nsize, N>;

            template<typename... Tn>
            using index_sequence_for = make_index_sequence<sizeof...( Tn )>;

            // rindex_of
            namespace detail
            {
                template<typename T, typename... Tn>
                struct rindex_of_impl {};

                template<typename T, typename T0, typename... Tn>
                struct rindex_of_impl<T, T0, Tn...>
                {
                    static constexpr nsize value = std::is_same<T, T0>::value ? sizeof...( Tn ) : ntraits::detail::rindex_of_impl<T, Tn...>::value;
                };

                template<typename T>
                struct rindex_of_impl<T>
                {
                    static constexpr nsize value = numeric<nsize>::invalid;
                };
            }

            template<typename T, typename... Tn>
            struct rindex_of : ntraits::detail::rindex_of_impl<T, Tn...> {};

            // convertible_rindex_of
            namespace detail
            {
                
                template<typename T, typename... Tn>
                struct convertible_rindex_of_impl {};

                template<typename T, typename T0, typename... Tn>
                struct convertible_rindex_of_impl<T, T0, Tn...>
                {
                    static constexpr nsize value = std::is_convertible<T, T0>::value ?
                    ( ntraits::disjunction<std::is_convertible<T, Tn>...>::value ? numeric<nsize>::invalid : sizeof...( Tn ) )
                    : convertible_rindex_of_impl<T, Tn...>::value;
                };

                template<typename T>
                struct convertible_rindex_of_impl<T>
                {
                    static constexpr nsize value = numeric<nsize>::invalid;
                };
            }

            template<typename T, typename... Tn>
            struct convertible_rindex_of : ntraits::detail::convertible_rindex_of_impl<T, Tn...> {};
        }
    }
}