#pragma once

#include "Reflection/ReflectionDefine.hpp"
#include "Utility/ctstring.hpp"
#include "Utility/string_view.hpp"
#include "Math/Hash.hpp"

namespace Nebula
{
    namespace Reflection
    {
        namespace detail
        {
            template<typename T>
            struct get_type_name_ct
            {
                static constexpr auto getTypeName()
                {
                    constexpr nsize prefix_length = sizeof( "Nebula::Reflection::detail::get_type_name_ct<" ) - 1;
                    constexpr nsize suffix_length = sizeof( ">::getTypeName" ) - 1;
                    constexpr nsize name_length = sizeof( __FUNCTION__ ) - prefix_length - suffix_length - 1;

                    static_assert( name_length < 255, "type name tool long, consider using get_type_name_rt" );

                    return sub_string<prefix_length, name_length>( string_factory( __FUNCTION__ ) );
                }

                static constexpr auto name = getTypeName();
            };

            template<>
            struct get_type_name_ct<void>
            {
                static constexpr auto name = string_factory( "void" );
            };

            template<typename T>
            struct get_type_name_rt
            {
                static nstring_view getTypeName()
                {
                    static constexpr nsize prefix_length = sizeof( "Nebula::Reflection::detail::get_type_name_rt<" ) - 1;
                    static constexpr nsize suffix_length = sizeof( ">::getTypeName" ) - 1;
                    static constexpr nsize name_length = sizeof( __FUNCTION__ ) - prefix_length - suffix_length - 1;

                    static nchar _stub_typename[name_length + 1] = {};
                    if( _stub_typename[0] == '\0' )
                    {
                        memcpy( _stub_typename, __FUNCTION__ + prefix_length, name_length );
                        _stub_typename[name_length] = '\0';
                    }
                    return _stub_typename;
                }
            };

            template<>
            struct get_type_name_rt<void>
            {
                static nstring_view getTypeName() { return "void"; }
            };

            class type_stub_base
            {
                virtual uint32 getID() const = 0;
                virtual const nchar* getName() const = 0;
                virtual const nchar* getSignature() const = 0;
            };

            template<typename T>
            struct type_info_base
            {
                static constexpr nstring_view signature = get_type_name_ct<T>::name.c_str();

                // turn off warning 4307 because MSVC bug: integer multiplication overflow
                #pragma warning( push )
                #pragma warning( disable : 4307 )
                static constexpr const uint32 id = Math::Hash::fnv_ct( signature.c_str() );
                #pragma warning( pop )
            };

            template<typename T>
            struct type_info : public type_info_base<T> { static constexpr const nchar* name = "(unknown)"; };

            template<typename T>
            class type_stub : public type_stub_base
            {
                virtual uint32 getID() const override { return type_info<T>::getID(); }
                virtual const nchar* getName() const override { return type_info<T>::name; }
                virtual const nchar* getSignature() const override { return type_info<T>::getSignature().c_str(); }
            };

            template<uint32 N> struct type_mapper { using type = void; };
        }

        #define RegisterType( T, Name ) \
                    template<> struct ::Nebula::Reflection::detail::type_info<T> : public detail::type_info_base<T> { static constexpr const nchar* name = #Name; }; \
                    template<> struct ::Nebula::Reflection::detail::type_mapper<::Nebula::Reflection::detail::type_info<T>::id> { using type = T; };

        #define RegisterTypeWithSignature( T ) \
                    template<> struct ::Nebula::Reflection::detail::type_info<T> : public detail::type_info_base<T> { static constexpr const nchar* name = NType::getSignature<T>().c_str(); }; \
                    template<> struct ::Nebula::Reflection::detail::type_mapper<::Nebula::Reflection::detail::type_info<T>::id> { using type = T; };
        

        class NType
        {
        public:
            const uint32 id;
            
            template<typename T>
            static constexpr NType get()
            {
                return NType( getID<T>() );
            }

            constexpr NType( uint32 id )
                : id( id )
            {
            }

            constexpr NType()
                : id( 0 )
            {
            }

            constexpr NType( const NType& other )
                : id( other.id )
            {
            }

            constexpr NType( NType&& other ) noexcept
                : id( other.id )
            {
            }

            constexpr explicit operator uint32() const
            {
                return id;
            }

            friend nbool operator==( const NType& lhs, const NType& rhs )
            {
                return lhs.id == rhs.id;
            }

            friend constexpr nbool operator!=( const NType& lhs, const NType& rhs )
            {
                return lhs.id != rhs.id;
            }

            template<typename T>
            constexpr nbool isType() const { return id == getID<T>(); }

            constexpr uint32 getID() const { return id; }

            constexpr nbool isValid() const { return id != 0; }

            template<typename T>
            static constexpr uint32 getID() { return detail::type_info<T>::id; }

            template<typename T>
            static constexpr nstring_view getSignature() { return detail::type_info<T>::signature; }
            
            template<typename T>
            static constexpr nstring_view const getName() { return detail::type_info<T>::name; }
        };
    }
}