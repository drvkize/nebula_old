#include "IO/MemoryStream.hpp"

namespace Nebula
{
    namespace IO
    {
        MemoryStreamPtr MemoryStream::create( BufferPtr buffer )
        {
            MemoryStreamPtr ptr = std::allocate_shared<MemoryStream>( Memory::nallocator<MemoryStream>() );
            if( ptr != nullptr && ptr->init( buffer ) )
            {
                return ptr;
            }

            return nullptr;
        }

        MemoryStream::MemoryStream()
            : offset( 0 )
        {
        }

        MemoryStream::~MemoryStream()
        {
        }

        nbool MemoryStream::init( BufferPtr buffer )
        {
            if( buffer != nullptr )
            {
                this->buffer = buffer;
            }
            else
            {
                this->buffer = Buffer::create();
            }

            return true;
        }

        void MemoryStream::release()
        {
            this->buffer = nullptr;
        }

        nsize MemoryStream::write( const int8* data, nsize data_size )
        {
            buffer->insert( data, offset, data_size );
            offset += data_size;
            return data_size;
        }

        nsize MemoryStream::read( int8* buffer, nsize buffer_size )
        {
            int64 rest_size = this->buffer->getSize() - offset;
            nsize read_size = rest_size < (int64)buffer_size ? rest_size : buffer_size;
            memcpy( buffer, this->buffer->getData(), read_size );
            offset += read_size;
            return read_size;
        }

        nbool MemoryStream::seek( SeekOption option, int64 offset )
        {
            int64 buffer_size = static_cast<int64>( buffer->getSize() );

            switch( option )
            {
                case SeekOption::SO_Current:
                {
                    if( this->offset + offset > buffer_size )
                    {
                        return false;
                    }

                    this->offset += offset;
                }
                break;
                case SeekOption::SO_Set:
                {
                    if( offset > buffer_size )
                    {
                        return false;
                    }

                    this->offset = offset;
                }
                break;
                case SeekOption::SO_End:
                {
                    if( buffer_size - offset < 0 )
                    {
                        return false;
                    }

                    this->offset = buffer_size - offset;
                }
                break;
            }

            // if( this->offset < 0 )
            // {
            //     this->offset = 0;
            // }
            // else if( this->offset > buffer_size )
            // {
            //     this->offset = buffer_size;
            // }

            return true;
        }

        int64 MemoryStream::tell()
        {
            return offset;
        }

        nbool MemoryStream::isEnd() const
        {
            return offset >= static_cast<int64>( buffer->getSize() );
        }
    }
}