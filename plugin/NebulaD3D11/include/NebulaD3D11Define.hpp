#pragma once

#include "Inline/Config.hpp"
#include "Nebula.hpp"
#include <dxgi.h>
#include <d3d11.h>

#ifdef NEBULA_D3D11_BUILD_LIBRARY
#define NEBULA_D3D11_INTERFACE NEBULA_SHARED_LIBRARY_EXPORT
#else
#define NEBULA_D3D11_INTERFACE NEBULA_SHARED_LIBRARY_IMPORT
#endif