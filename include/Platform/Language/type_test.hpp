#pragma once

#include "common.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace Traits
        {
            // is_one_of
            template<typename T, typename... Tn>
            struct is_one_of : ntraits::disjunction<std::is_same<T, Tn>...> {};

            // is_specialization_of
            template<typename T, template<typename...> class Template>
			struct is_specialization_of : std::false_type {};
			template<template<typename...> class Template, typename... Tn>
			struct is_specialization_of<Template<Tn...>, Template> : std::true_type {};

            template<typename T>
            struct is_vector : ntraits::is_specialization_of<T, nvector> {};            

            template<typename T>
            struct is_string : ntraits::is_specialization_of<T, nbasic_string> {};

            template<typename T>
            struct is_unordered_map : ntraits::is_specialization_of<T, nunordered_map> {};

            template<typename T>
            struct is_map : ntraits::is_specialization_of<T, nmap> {};

            namespace detail
            {
                template<typename T>
                struct is_string_literal_impl : std::false_type {};

                template<nsize N>
                struct is_string_literal_impl<const nchar(&)[N]> : std::true_type {};
            }

            template<typename T>
            struct is_string_literal : public detail::is_string_literal_impl<T> {};
        }
    }
}