#pragma once

#include "Platform/Platform.hpp"
using namespace Nebula::Platform;
#include "Platform/Language/index_sequence.hpp"
#include "Utility/recursive_wrapper.hpp"
#include "Utility/string_view.hpp"

namespace Nebula
{
    namespace Platform
    {
        namespace detail
        {
            // variant_helper
            template<typename... Tn>
            struct variant_helper;

            template<typename T, typename... Tn>
            struct variant_helper<T, Tn...>
            {
                static inline void destroy( const nsize index, void* data )
                {
                    if( index == sizeof...( Tn ) )
                    {
                        reinterpret_cast<T*>( data )->~T();
                    }
                    else
                    {
                        variant_helper<Tn...>::destroy( index, data );
                    }
                }

                static inline void move( const nsize old_index, void* old_value, void* new_value )
                {
                    if( old_index == sizeof...( Tn ) )
                    {
                        ::new( new_value )( T )( std::move( *reinterpret_cast<T*>( old_value ) ) );
                    }
                    else
                    {
                        variant_helper<Tn...>::move( old_index, old_value, new_value );
                    }
                }

                static inline void copy( const nsize old_index, const void* old_value, void* new_value )
                {
                    if( old_index == sizeof...( Tn ) )
                    {
                        ::new( new_value )( T )( *reinterpret_cast<const T*>( old_value ) );
                    }
                    else
                    {
                        variant_helper<Tn...>::copy( old_index, old_value, new_value );
                    }
                }
            };

            template<>
            struct variant_helper<>
            {
                static inline void destroy( const nsize, void* ) {}
                static inline void move( const nsize, void*, void* ) {}
                static inline void copy( const nsize, const void*, void* ) {}
            };

            // variant_traits
            template<typename... Tn>
            struct variant_traits
            {
                static constexpr nsize type_size = ntraits::static_max<sizeof( Tn )...>::value;
                static constexpr nsize type_align = ntraits::static_max<std::alignment_of<Tn>::value...>::value;
                using data_t = typename std::aligned_storage<type_size, type_align>::type;
                using first_t = typename std::tuple_element<0, std::tuple<Tn...>>::type;
                using helper_t = variant_helper<Tn...>;
            };

            // traits
            template<typename T, typename... Tn>
            struct variant_element
            {
                using direct_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;
                static constexpr nsize direct_rindex = ntraits::rindex_of<direct_type, Tn...>::value;
                static constexpr nbool is_direct = direct_rindex != ntraits::numeric<nsize>::invalid;
                static constexpr nsize convertible_rindex = is_direct ? direct_rindex : ntraits::convertible_rindex_of<direct_type, Tn...>::value;
                static constexpr nbool is_valid = convertible_rindex != ntraits::numeric<nsize>::invalid;
                static constexpr nsize index = is_valid ? sizeof...( Tn ) - convertible_rindex : 0;
                using type = typename std::tuple_element<index, std::tuple<void, Tn...>>::type;
            };

            template<typename T, typename E = void>
            struct type_enable
            {
                using type = E;
            };

            template<typename F, typename V, typename E = void>
            struct result_of_unary_visit
            {
                using type = typename std::result_of<F( V& )>::type;
            };

            template<typename F, typename V>
            struct result_of_unary_visit<F, V, typename type_enable<typename F::type>::type>
            {
                using type = typename F::type;
            };

            template<typename F, typename V, typename E = void>
            struct result_of_binary_visit
            {
                using type = typename std::result_of<F( V&, V& )>::type;
            };

            template<typename F, typename V>
            struct result_of_binary_visit<F, V, typename type_enable<typename F::type>::type>
            {
                using type = typename F::type;
            };

            // unwrapper
            template<typename T>
            struct unwrapper
            {
                static T const& apply_const( const T& obj ) { return obj; }
                static T& apply( T& obj ) { return obj; }
            };
            
            template<typename T>
            struct unwrapper<recursive_wrapper<T>>
            {
                static auto apply_const( const recursive_wrapper<T>& obj )
                    -> const typename recursive_wrapper<T>::type&
                {
                    return obj.get();
                }

                static auto apply( recursive_wrapper<T>& obj )
                    -> typename recursive_wrapper<T>::type&
                {
                    return obj.get();
                }
            };

            template<typename T>
            struct unwrapper<std::reference_wrapper<T>>
            {
                static auto apply_const( const std::reference_wrapper<T>& obj )
                    -> const typename std::reference_wrapper<T>::type&
                {
                    return obj.get();
                }

                static auto apply( std::reference_wrapper<T>& obj )
                    -> typename std::reference_wrapper<T>::type&
                {
                    return obj.get();
                }
            };
            
            // dispatcher
            template<typename F, typename V, typename R, typename... Tn>
            struct dispatcher;
            template<typename F, typename V, typename R, typename T, typename... Tn>
            struct dispatcher<F, V, R, T, Tn...>
            {
                static R apply_const( const V& v, F&& f )
                {
                    if( v.template is<T>() )
                    {
                        return f( v.template get_unsafe<T>() );
                    }
                    else
                    {
                        return dispatcher<F, V, R, Tn...>::apply_const( v, std::forward<F>( f ) );
                    }
                }

                static R apply( V& v, F&& f )
                {
                    if( v.template is<T>() )
                    {
                        return f( v.template get_unsafe<T>() );
                    }
                    else
                    {
                        return dispatcher<F, V, R, Tn...>::apply( v, std::forward<F>( f ) );
                    }
                }
            };

            template<typename F, typename V, typename R, typename T>
            struct dispatcher<F, V, R, T>
            {
                static R apply_const( const V& v, F&& f )
                {
                    return f( v.template get_unsafe<T>() );
                }
                static R apply( V& v, F&& f )
                {
                    return f( v.template get_unsafe<T>() );
                }
            };
            
            // binary_dispatcher_rhs
            template<typename F, typename V, typename R, typename T, typename... Tn>
            struct binary_dispatcher_rhs;

            template<typename F, typename V, typename R, typename T0, typename T1, typename... Tn>
            struct binary_dispatcher_rhs<F, V, R, T0, T1, Tn...>
            {
                static R apply_const( const V& lhs, const V& rhs, F&& f )
                {
                    if( rhs.template is<T1>() )
                    {
                        return f( unwrapper<T0>::apply_const( lhs.template get_unsafe<T0>() ),
                        unwrapper<T1>::apply_const( rhs.template get_unsafe<T1>() ) );
                    }
                    else
                    {
                        return binary_dispatcher_rhs<F, V, T0, Tn...>::apply_const( lhs, rhs, std::forward<F>( f ) );
                    }
                }

                static R apply( V& lhs, V& rhs, F&& f )
                {
                    if( rhs.template is<T1>() )
                    {
                        return f( unwrapper<T0>::apply( lhs.template get_unsafe<T0>() ),
                        unwrapper<T1>::apply( rhs.template get_unsafe<T1>() ) );
                    }
                    else
                    {
                        return binary_dispatcher_rhs<F, V, R, T0, Tn...>::apply( lhs, rhs, std::forward<F>( f ) );
                    }
                }
            };

            template<typename F, typename V, typename R, typename T0, typename T1>
            struct binary_dispatcher_rhs<F, V, R, T0, T1>
            {
                static R apply_const( const V& lhs, const V& rhs, F&& f )
                {
                    return f( unwrapper<T0>::apply_const( lhs.template get_unsafe<T0>() ),
                    unwrapper<T1>::apply_const( rhs.template get_unsafe<T1>() ) );
                }

                static R apply( V& lhs, V& rhs, F&& f )
                {
                    return f( unwrapper<T0>::apply( lhs.template get_unsafe<T0>() ),
                    unwrapper<T1>::apply( rhs.template get_unsafe<T1>() ) );
                }
            };

            // binary_dispatcher_lhs
            template<typename F, typename V, typename R, typename T, typename... Tn>
            struct binary_dispatcher_lhs;

            template<typename F, typename V, typename R, typename T0, typename T1, typename... Tn>
            struct binary_dispatcher_lhs<F, V, R, T0, T1, Tn...>
            {
                static R apply_const( const V& lhs, const V& rhs, F&& f )
                {
                    if( lhs.template is<T1>() )
                    {
                        return f( unwrapper<T1>::apply_const( lhs.template get_unsafe<T1>() ),
                        unwrapper<T0>::apply_const( rhs.template get_unsafe<T0>() ) );
                    }
                    else
                    {
                        return binary_dispatcher_lhs<F, V, R, T0, Tn...>::apply_const( lhs, rhs, std::forward<F>( f ) );
                    }
                }

                static R apply( V& lhs, V& rhs, F&& f )
                {
                    if( lhs.template is<T1>() )
                    {
                        return f( unwrapper<T1>::apply( lhs.template get_unsafe<T1>() ),
                        unwrapper<T0>::apply( rhs.template get_unsafe<T0>() ) );
                    }
                    else
                    {
                        return binary_dispatcher_lhs<F, V, R, T0, Tn...>::apply( lhs, rhs, std::forward<F>( f ) );
                    }
                }
            };

            template<typename F, typename V, typename R, typename T0, typename T1>
            struct binary_dispatcher_lhs<F, V, R, T0, T1>
            {
                static R apply_const( const V& lhs, const V& rhs, F&& f )
                {
                    return f( unwrapper<T1>::apply_const( lhs.template get_unsafe<T1>() ),
                    unwrapper<T0>::apply_const( rhs.template get_unsafe<T0>() ) );
                }
                
                static R apply( V& lhs, const V& rhs, F&& f )
                {
                    return f( unwrapper<T1>::apply( lhs.template get_unsafe<T1>() ),
                    unwrapper<T0>::apply( rhs.template get_unsafe<T0>() ) );
                }
            };

            // binary_dispatcher
            template<typename F, typename V, typename R, typename... Tn>
            struct binary_dispatcher;

            template<typename F, typename V, typename R, typename T, typename... Tn>
            struct binary_dispatcher<F, V, R, T, Tn...>
            {
                static R apply_const( const V& v0, const V& v1, F&& f )
                {
                    if( v0.template is<T>() )
                    {
                        if( v1.template is<T>() )
                        {
                            return f( unwrapper<T>::apply_const( v0.template get_unsafe<T>() ),
                            unwrapper<T>::apply_const( v1.template get_unsafe<T>() ) );
                        }
                        else
                        {
                            return binary_dispatcher_rhs<F, V, R, T, Tn...>::apply_const( v0, v1, std::forward<F>( f ) );
                        }
                    }
                    else if( v1.template is<T>() )
                    {
                        return binary_dispatcher_lhs<F, V, R, T, Tn...>::apply_const( v0, v1, std::forward<F>( f ) );
                    }

                    return binary_dispatcher<F, V, R, Tn...>::apply_const( v0, v1, std::forward<F>( f ) );
                };

                static R apply( V& v0, V& v1, F&& f )
                {
                    if( v0.template is<T>() )
                    {
                        if( v1.template is<T>() )
                        {
                            return f( unwrapper<T>::apply( v0.template get_unsafe<T>() ),
                            unwrapper<T>::apply( v1.template get_unsafe<T>() ) );
                        }
                        else
                        {
                            return binary_dispatcher_rhs<F, V, R, T, Tn...>::apply( v0, v1, std::forward<F>( f ) );
                        }
                    }
                    else if( v1.template is<T>() )
                    {
                        return binary_dispatcher_lhs<F, V, R, T, Tn...>::apply( v0, v1, std::forward<F>( f ) );
                    }

                    return binary_dispatcher<F, V, R, Tn...>::apply( v0, v1, std::forward<F>( f ) );
                }
            };

            template<typename F, typename V, typename R, typename T>
            struct binary_dispatcher<F, V, R, T>
            {
                static R apply_const( const V& v0, const V& v1, F&& f )
                {
                    return f( unwrapper<T>::apply_const( v0.template get_unsafe<T>() ),
                    unwrapper<T>::apply_const( v1.template get_unsafe<T>() ) );
                }

                static R apply( const V& v0, const V& v1, F&& f )
                {
                    return f( unwrapper<T>::apply( v0.template get_unsafe<T>() ),
                    unwrapper<T>::apply( v1.template get_unsafe<T>() ) );
                }
            };

            struct compare_equal
            {
                template<typename T>
                nbool operator()( const T& lhs, const T& rhs ) const
                {
                    return lhs == rhs;
                }
            };

            struct compare_less
            {
                template<typename T>
                nbool operator()( const T& lhs, const T& rhs ) const
                {
                    return lhs < rhs;
                }
            };
            
            template<typename V, typename Op>
            class comparer
            {
                const V& lhs;

            public:
                explicit comparer( const V& lhs ) noexcept
                    : lhs( lhs )
                {
                }

                comparer& operator=( const comparer& ) = delete;

                template<typename T>
                nbool operator()( const T& rhs_content ) const
                {
                    const T& lhs_content = lhs.template get_unsafe<T>();
                    return Op()( lhs_content, rhs_content );
                }
            };
        }

        template<typename... Tn>
        class variant
        {
            static_assert( sizeof...( Tn ) > 0, "variant, type count can not be 0" );
            static_assert( !ntraits::disjunction<std::is_reference<Tn>...>::value, "variant, variant can not hold reference type" );

            using data_t = typename detail::variant_traits<Tn...>::data_t;
            using first_t = typename detail::variant_traits<Tn...>::first_t;
            using helper_t = typename detail::variant_traits<Tn...>::helper_t;
            
            nsize index;
            data_t data;

        public:
            template<typename T>
            using element_t = detail::variant_element<T, Tn...>;

            variant() noexcept( std::is_nothrow_default_constructible<first_t>::value )
                : index( sizeof...( Tn ) - 1 )
            {
                static_assert( std::is_nothrow_default_constructible<first_t>::value, "variant, first type in variant must be default constructible" );
                new( &data )( first_t )();
            }

            template<typename T, typename E = typename std::enable_if<element_t<T>::is_valid>::type>
            variant( T&& value ) noexcept( std::is_nothrow_default_constructible<typename element_t<T>::type>::value )
                : index( element_t<T>::convertible_rindex )
            {
                new ( &data )( typename element_t<T>::type )( std::forward<T>( value ) );
            }

            variant( const variant<Tn...>& other )
            {
                *this = other;
            }

            variant( variant<Tn...>&& other ) noexcept( std::is_nothrow_default_constructible<std::tuple<Tn...>>::value )
            {
                *this = std::move( other );
            }

            ~variant() noexcept
            {
                helper_t::destroy( index, &data );    
            }

            inline friend void swap( variant& lhs, variant& rhs ) noexcept
            {
                // slow, if only want to move, use move assign operator
                variant temp( lhs );
                lhs = std::move( rhs );
                rhs = std::move( temp );
            }

            inline friend nbool validate( const variant& value )
            {
                return value.index != ntraits::numeric<nsize>::invalid;
            }

            variant& operator=( const variant& rhs )
            {
                if( index == rhs.index )
                {
                    helper_t::copy( rhs.index, &rhs.data, &data );    
                }
                else
                {
                    helper_t::destroy( index, &data );
                    index = ntraits::numeric<nsize>::invalid;
                    helper_t::copy( rhs.index, &rhs.data, &data );
                    index = rhs.index;
                }
                
                return *this;
            }

            variant& operator=( variant&& rhs ) noexcept
            {
                if( index == rhs.index )
                {
                    helper_t::move( rhs.index, &rhs.data, &data );    
                }
                else
                {
                    helper_t::destroy( index, &data );
                    index = ntraits::numeric<nsize>::invalid;
                    helper_t::move( rhs.index, &rhs.data, &data );
                    index = rhs.index;                    
                }
                
                return *this;
            }

            inline friend nbool operator==( const variant& lhs, const variant& rhs )
            {
                if( lhs.index != rhs.index )
                {
                    return false;
                }

                detail::comparer<variant, detail::compare_equal> visitor( lhs );
                return variant::visit( rhs, visitor );
            }

            inline friend nbool operator!=( const variant& lhs, const variant& rhs )
            {
                return !( lhs == rhs );
            }

            inline friend nbool operator<( const variant& lhs, const variant& rhs )
            {
                if( lhs.index != rhs.index )
                {
                    // compare index if type is not the same, notice the index is actually reverse index, so less means lhs.index > rhs.index
                    return lhs.index > rhs.index;
                }

                detail::comparer<variant, detail::compare_less> visitor( lhs );
                return variant::visit( rhs, visitor );
            }

            inline friend nbool operator>( const variant& lhs, const variant& rhs )
            {
                return rhs < lhs;
            }

            inline friend nbool operator<=( const variant& lhs, const variant& rhs )
            {
                return !( lhs > rhs );
            }

            inline friend nbool operator>=( const variant& lhs, const variant& rhs )
            {
                return !( lhs < rhs );
            }

            template<typename T,
                typename std::enable_if<element_t<T>::direct_rindex != ntraits::numeric<nsize>::invalid>::type* = nullptr>
            nbool is() const
            {
                return index == element_t<T>::direct_rindex;
            }

            template<typename T, typename std::enable_if<
                ( element_t<recursive_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            nbool is() const
            {
                return index = element_t<recursive_wrapper<T>>::direct_rindex;
            }

            template<typename T, typename... An>
            void set( An&&... args )
            {
                if( is<T>() )
                {
                    new( &data )( T )( std::forward<An...>( args )... );
                }
                else
                {
                    helper_t::destroy( index, &data );
                    index = ntraits::numeric<nsize>::invalid;
                    new( &data )( T )( std::forward<An...>( args )... );
                    index = element_t<T>::convertible_rindex;
                }
            }

            //get_unsafe
            template<typename T, typename std::enable_if<
                ( element_t<T>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            T& get_unsafe()
            {
                return *reinterpret_cast<T*>( &data );
            }

            template<typename T, typename std::enable_if<
                ( element_t<T>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            const T& get_unsafe() const
            {
                return *reinterpret_cast<const T*>( &data );
            }

            template<typename T, typename std::enable_if<
                ( element_t<recursive_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            T& get_unsafe()
            {
                return ( *reinterpret_cast<recursive_wrapper<T>*>( &data ) ).get();
            }

            template<typename T, typename std::enable_if<
                ( element_t<recursive_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            const T& get_unsafe() const
            {
                return ( *reinterpret_cast<const recursive_wrapper<T>*>( &data ) ).get();
            }

            template<typename T, typename std::enable_if<
                ( element_t<std::reference_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            T& get_unsafe()
            {
                return ( *reinterpret_cast<std::reference_wrapper<T>*>( &data ) ).get();
            }

            template<typename T, typename std::enable_if<
                ( element_t<std::reference_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            const T& get_unsafe() const
            {
                return ( *reinterpret_cast<const std::reference_wrapper<T>*>( &data ) ).get();
            }

            // get
            template<typename T, typename std::enable_if<
                ( element_t<T>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            T& get()
            {
                assert( index == element_t<T>::direct_rindex && "variant, type mismatch" );
                return *reinterpret_cast<T*>( &data );
            }

            template<typename T, typename std::enable_if<
                ( element_t<T>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            const T& get() const
            {
                assert( index == element_t<T>::direct_rindex && "variant, type mismatch" );
                return *reinterpret_cast<const T*>( &data );
            }

            template<typename T, typename std::enable_if<
                ( element_t<recursive_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            T& get()
            {
                assert( index == element_t<recursive_wrapper<T>>::direct_rindex && "variant, type mismatch" );
                return ( *reinterpret_cast<recursive_wrapper<T>*>( &data ) ).get();
            }

            template<typename T, typename std::enable_if<
                ( element_t<recursive_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            const T& get() const
            {
                assert( index == element_t<recursive_wrapper<T>>::direct_rindex && "variant, type mismatch" );
                return ( *reinterpret_cast<const recursive_wrapper<T>*>( &data ) ).get();
            }

            template<typename T, typename std::enable_if<
                ( element_t<std::reference_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            T& get()
            {
                assert( index == element_t<std::reference_wrapper<T>>::direct_rindex && "variant, type mismatch" );
                return ( *reinterpret_cast<std::reference_wrapper<T>*>( &data ) ).get();
            }

            template<typename T, typename std::enable_if<
                ( element_t<std::reference_wrapper<T>>::direct_rindex != ntraits::numeric<nsize>::invalid )>::type* = nullptr>
            const T& get() const
            {
                assert( index == element_t<std::reference_wrapper<T>>::direct_rindex && "variant, type mismatch" );
                return ( *reinterpret_cast<const std::reference_wrapper<T>*>( &data ) ).get();
            }

            // visitor
            template<typename F, typename V, typename R = typename detail::result_of_unary_visit<F, first_t>::type>
            auto static visit( const V& v, F&& f )
                -> decltype( detail::dispatcher<F, V, R, Tn...>::apply_const( v, std::forward<F>( f ) ) )
            {
                return detail::dispatcher<F, V, R, Tn...>::apply_const( v, std::forward<F>( f ) );
            }

            template<typename F, typename V, typename R = typename detail::result_of_unary_visit<F, first_t>::type>
            auto static visit( V& v, F&& f )
                -> decltype( detail::dispatcher<F, V, R, Tn...>::apply( v, std::forward<F>( f ) ) )
            {
                return detail::dispatcher<F, V, R, Tn...>::apply( v, std::forward<F>( f ) );
            }

            template<typename F, typename V, typename R = typename detail::result_of_binary_visit<F, first_t>::type>
            auto static visit( const V& v0, const V& v1, F&& f )
                -> decltype( detail::binary_dispatcher<F, V, R, Tn...>::const_apply( v0, v1, std::forward<F>( f ) ) )
            {
                return detail::dispatcher<F, V, R, Tn...>::apply_const( v0, v1, std::forward<F>( f ) );
            }

            template<typename F, typename V, typename R = typename detail::result_of_binary_visit<F, first_t>::type>
            auto static visit( V& v0, V& v1, F&& f )
                -> decltype( detail::binary_dispatcher<F, V, R, Tn...>::apply( v0, v1, std::forward<F>( f ) ) )
            {
                return detail::dispatcher<F, V, R, Tn...>::apply( v0, v1, std::forward<F>( f ) );
            }

            template<typename T>
            static constexpr nsize index_of()
            {
                return element_t<T>::index;
            }

            nsize get_index() const { return sizeof...( Tn ) - index; }
        };

        using nvar = variant<ntraits::null_type, int8, int16, int32, int64, uint8, uint16, uint32, uint64, nfloat, ndouble, nstring>;
    }
}