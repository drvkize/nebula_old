#include "Memory/SystemAllocator.hpp"
#include "Memory/OSAllocator.hpp"
#include "Memory/FixedSizeChunkAllocator.hpp"
#include "Memory/CoalesceAllocator.hpp"
#include "Math/CalculationCommon.hpp"

namespace Nebula
{
    namespace Memory
    {
        SystemAllocator::SystemAllocator()
            : parent_allocator( nullptr )
            , fixed_allocators( nullptr )
            , fixed_allocator_count( 0 )
            , coalesce_allocators( nullptr )
            , coalesce_scale_step( 0 )
        {
        }

        SystemAllocator::~SystemAllocator()
        {
        }

        nbool SystemAllocator::init( NebulaAllocator* parent_allocator )
        {
            // build fixed allocators
            nsize min_scale = Math::log2Fast( (uint64)FIXED_MIN );
            nsize max_scale = Math::log2Fast( (uint64)FIXED_MAX );
            nsize fixed_allocator_count = max_scale - min_scale + 1;

            fixed_allocators = reinterpret_cast<FixedSizeChunkAllocator*>( parent_allocator->allocate( fixed_allocator_count * sizeof( FixedSizeChunkAllocator ), 4 ) );
            uint32 allocator_index = 0;
            for( nsize i = min_scale; i <= max_scale; ++i )
            {
                nsize block_size = 1llu << i;
                FixedSizeChunkAllocator* allocator = new( &fixed_allocators[allocator_index++] ) FixedSizeChunkAllocator();
                if( !allocator->init( parent_allocator, block_size ) )
                {
                    allocator->release();
                }
            }

            this->fixed_allocator_count = fixed_allocator_count;

            // build coalesce allocators
            min_scale = Math::log2Fast( (uint64)COALESCE_MIN );
            max_scale = Math::log2Fast( (uint64)COALESCE_MAX );
            decltype( min_scale ) scale_step = ( max_scale - min_scale ) / COALESCE_COUNT;

			coalesce_allocators = reinterpret_cast<CoalesceAllocator*>( parent_allocator->allocate( COALESCE_COUNT * sizeof( CoalesceAllocator ), 4 ) );
			allocator_index = 0;
            for( auto i = min_scale; i < max_scale; i += scale_step )
            {
                nsize allocator_size = 1llu << i;
                CoalesceAllocator* allocator = new( &coalesce_allocators[allocator_index++] ) CoalesceAllocator();
                if( !allocator->init( parent_allocator, allocator_size ) )
                {
                    allocator->release();
                }
            }

            coalesce_scale_step = scale_step;
            this->parent_allocator = parent_allocator;

            return true;
        }

        void SystemAllocator::release()
        {
            if( parent_allocator != nullptr )
            {
				if( coalesce_allocators != nullptr )
				{
					for( nsize i = 0; i < COALESCE_COUNT; ++i )
					{
						coalesce_allocators[i].release();
						coalesce_allocators[i].~CoalesceAllocator();
					}
					parent_allocator->deallocate( coalesce_allocators );

					coalesce_allocators = nullptr;
					coalesce_scale_step = 0;
				}

                if( fixed_allocators != nullptr && fixed_allocator_count > 0 )
                {
                    for( nsize i = 0; i < fixed_allocator_count; ++i )
                    {
                        fixed_allocators[i].release();
						fixed_allocators[i].~FixedSizeChunkAllocator();
                    }
                    parent_allocator->deallocate( fixed_allocators );

                    fixed_allocator_count = 0;
                    fixed_allocators = nullptr;
                }

                parent_allocator = nullptr;
            }
        }

        void* SystemAllocator::allocate( nsize size, nsize align )
        {
            assert( fixed_allocators != nullptr || coalesce_allocators != nullptr && "[SystemAllocator::allocate]: system allocator is not initialized" );

            if( align < 2 && size < FIXED_MAX )
            {
                nsize test_size = FIXED_MIN;
                for( int i = 0; i < fixed_allocator_count; ++i )
                {
                    if( test_size >= size )
                    {
                        return fixed_allocators[i].allocate( size, align );
                    }

                    test_size <<= 1;
                }
            }
            
            nsize test_size = COALESCE_MIN;
            for( int i = 0; i < COALESCE_COUNT; ++i )
            {
                if( size < test_size )
                {
                    return coalesce_allocators[i].allocate( size, align );
                }

                test_size <<= coalesce_scale_step;
            }

			assert( 0 );
            return nullptr;
        }

        nbool SystemAllocator::deallocate( void* p )
        {
            assert( fixed_allocators != nullptr || coalesce_allocators != nullptr && "[SystemAllocator::allocate]: system allocator is not initialized" );

            if( p != nullptr )
            {
                for( int i = 0; i < fixed_allocator_count; ++i )
                {
                    if( fixed_allocators[i].deallocate( p ) )
                    {
                        return true;
                    }
                }

                for( int i = 0; i < COALESCE_COUNT; ++i )
                {
                    if( coalesce_allocators[i].deallocate( p ) )
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        NebulaAllocator& getSystemAllocator()
        {
            static SystemAllocator _stub_system_allocator;
            return _stub_system_allocator;
        }
    }
}